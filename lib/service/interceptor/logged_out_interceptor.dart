import 'package:dio/dio.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/service/logout_service.dart';
import 'package:GossipGram/service/navigator_service.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class LoggedOutInterceptor extends Interceptor {
  LogoutService _logoutService;
  NavigatorService _navigatorService;

  LoggedOutInterceptor(this._logoutService, this._navigatorService);

  @override
  Future onError(DioError err) {
    if (_loggedOutError(err)) {
      _logoutService.execute();
      _navigatorService.logoutUser(
          error: 'User logged out - please login again');
      FirebaseAnalytics analytics = GetIt.I.get(instanceName: 'analytics');

      analytics.logEvent(
        name: 'Logged out',
        parameters: {
          "uri_path": err.request.uri.path,
        },
      );
    }

    return Future.value(err);
  }

  bool _loggedOutError(DioError err) {
    return err.type == DioErrorType.RESPONSE &&
            (err.response.statusCode == 403 &&
                err.response.data['logout_reason'] != null &&
                err.response.data['message'] == 'login_required') ||
        (err.response.statusCode == 404 &&
            err.response.data.toString().contains('not-logged-in')) ||
        (err.response.statusCode == 400 &&
            err.response.data['message'] == 'challenge_required' &&
            err.response.data['status'] == 'fail');
  }
}
