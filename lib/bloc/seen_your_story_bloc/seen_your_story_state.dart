import 'package:flutter/material.dart';
import 'package:GossipGram/model/view_data/seen_your_story_user_item.dart';

@immutable
abstract class SeenYourStoryState {}

class SeenYourStoryInitial extends SeenYourStoryState {}

class SeenYourStoryLoadingState extends SeenYourStoryState {}

class SeenYourStoryLoadedState extends SeenYourStoryState {
  final List<SeenYourStoryUserItem> seenYourStory;
  SeenYourStoryLoadedState(this.seenYourStory) : super();
}
