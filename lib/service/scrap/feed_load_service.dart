import 'dart:math';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/cupertino.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/feed_repository.dart';
import 'package:GossipGram/model/objects/feed.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/model/objects/thumbnail.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/comment_load_service.dart';
import 'package:GossipGram/service/scrap/like_load_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'dart:io' show Platform;

@singleton
class FeedLoadService {
  static const String FEED_SINGLE_LOAD_THRESHOLD = "feed_single_load_threshold";
  static const String FEED_MAX_LOAD_HISTORICAL_DATA =
      "feed_max_load_historical_data";
  static const String FEED_PROCESS_BUFFER = "feed_process_buffer";
  static const String SERVICE_NAME = "feed_load_service";

  FeedRepository _feedRepository;
  LikeLoadService _likeLoadService;
  CommentLoadService _commentLoadService;
  InstagramPrivateApi _instagramPrivateApi;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  FeedLoadService(
    this._likeLoadService,
    this._commentLoadService,
    this._feedRepository,
    this._appConfig,
    this._localStorage,
  ) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<void> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    int nowDate = firstLoad ? 0 : DateTime.now().millisecondsSinceEpoch;
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);
    var feedResponse = await _instagramPrivateApi.feed.user(
      _instagramPrivateApi.myUserId,
    );
    DataLoadBloc.printProfiling("API feed media step loaded", now);

    var feed = feedResponse.response.items;
    while (feedResponse.hasNext()) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));
      feedResponse = await feedResponse.next();
      feed.addAll(feedResponse.response.items);
      DataLoadBloc.printProfiling("API feed media step loaded", now);
    }
    DataLoadBloc.printProfiling("API feed media all loaded", now);

    List<String> activeFeedIds = await _feedRepository.getFeedIds();

    for (String id in activeFeedIds) {
      if (!feed.any((feed) => feed.pk.toString() == id)) {
        await _feedRepository.deleteFeedById(
          id: id,
        );
      }
    }

    List<Feed> toUpsertFeeds = [];
    List<Feed> dbFeeds = await _feedRepository.queryFeeds();

    int processedFeedElements = 0;

    for (var feedElement in feed) {
      String urlThumbnail = feedElement.carouselMedia != null
          ? feedElement.carouselMedia.first.imageVersions2.candidates.first.url
          : feedElement.imageVersions.candidates.first.url;

      Feed dbFeed = dbFeeds.firstWhere(
          (element) => element.id == feedElement.pk.toString(),
          orElse: () => null);
      int createdAt = dbFeed != null ? dbFeed.createdAt : nowDate;

      Feed newFeed = Feed(
        id: feedElement.pk.toString(),
        mediaUrl: urlThumbnail,
        mediaType: feedElement.carouselMedia != null
            ? FeedThumbnail.CAROUSEL
            : feedElement.mediaType == 2
                ? Thumbnail.VIDEO
                : Thumbnail.IMAGE,
        likeCount: feedElement.likeCount,
        commentCount: feedElement.commentCount,
        originalCreatedAtDate: feedElement.takenAt,
        createdAt: createdAt,
      );

      if (processedFeedElements <
          _appConfig.getInt(FEED_SINGLE_LOAD_THRESHOLD)) {
        await _feedRepository.upsertFeed(feed: newFeed);

        if (firstLoad)
          await Future.delayed(Duration(
              milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
        else
          await Future.delayed(
              Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));

        await _likeLoadService.execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
          feed: newFeed,
        );

        await _commentLoadService.execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
          feed: newFeed,
        );
      } else {
        toUpsertFeeds.add(newFeed);
      }
      ++processedFeedElements;
    }

    await _feedRepository.upsertFeeds(
      feeds: toUpsertFeeds,
    );

    DataLoadBloc.printProfiling("DB feed loaded", now);

    if (firstLoad) {
      List<int> feedProcessBuffer = feed
          .getRange(
            min(feed.length, _appConfig.getInt(FEED_SINGLE_LOAD_THRESHOLD)),
            min(feed.length, _appConfig.getInt(FEED_MAX_LOAD_HISTORICAL_DATA)),
          )
          .map((e) => e.pk)
          .toList();
      await _localStorage.write(FEED_PROCESS_BUFFER, feedProcessBuffer);
    }

    if (_localStorage.hasData(FEED_PROCESS_BUFFER) &&
        _localStorage.read(FEED_PROCESS_BUFFER).isEmpty) {
      String packageBaseName = 'com.cremapps.gossipgram';
      await BackgroundFetch.stop("$packageBaseName.postqueue");
    }
  }
}
