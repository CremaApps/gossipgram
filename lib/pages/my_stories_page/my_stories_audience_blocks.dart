part of 'my_stories_page.dart';

class _StoriesAudienceBlocks extends StatelessWidget {
  final bool hasStories;
  final SubscriptionService _subscriptionService =
      GetIt.I.get<SubscriptionService>();
  final AppConfig _appConfig = GetIt.instance.get<AppConfig>();

  _StoriesAudienceBlocks(this.hasStories) : super();

  @override
  Widget build(BuildContext context) {
    List<String> analysisBoxes =
        _appConfig.getListString(AppConfig.STORY_BOXES);

    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: analysisBoxes.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (contxt, indx) => infoBox(
        context,
        MyStoryBox.fromConstants(
          context: context,
          boxName: analysisBoxes[indx],
        ),
      ),
    );
  }

  InkWell infoBox(context, MyStoryBox myStoryBox) {
    Color dividerColor = getThemeModeAsInt() == 0
        ? Color(0xFFCCD1D6)
        : (hasStories ? Color(0xFF829AC1) : Color(0x50829AC1));

    return InkWell(
      onTap: hasStories
          ? () {
              if ((myStoryBox.isPaidFunctionality ?? false) &&
                  !_subscriptionService.isPayed) {
                showGeneralDialog(
                  context: context,
                  barrierColor: AppTheme.primaries['900'],
                  // background color
                  barrierDismissible: false,
                  // should dialog be dismissed when tapped outside// label for barrier
                  transitionDuration: Duration(milliseconds: 200),
                  // how long it takes to popup dialog after button click
                  pageBuilder: (_, __, ___) {
                    // your widget implementation
                    return SizedBox.expand(
                      // makes widget fullscreen
                      child: PremiumPopup(
                        callback: () {
                          //Toggle restart render for reloading paid functionalities.
                          GetIt.instance
                              .get<MyStoriesBloc>()
                              .add(MyStoriesStartLoadEvent());
                        },
                      ),
                    );
                  },
                );
              } else {
                if (myStoryBox.value != null) {
                  myStoryBox.redirectFunction();
                }
              }
            }
          : null,
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width / 3.3,
        margin: EdgeInsets.fromLTRB(2, 0, 2, 16),
        child: Container(
          height: 42,
          alignment: Alignment.center,
          padding: const EdgeInsets.all(4),
          decoration: BoxDecoration(
            border: Border.all(width: 1.0, color: dividerColor),
            borderRadius: BorderRadius.all(
                Radius.circular(8.0) //         <--- border radius here
                ),
          ),
          child: AutoSizeText(myStoryBox.title,
              overflow: TextOverflow.clip,
              textAlign: TextAlign.center,
              maxLines: 2,
              style: appTextTheme.subtitle2.copyWith(
                  fontSize: 13,
                  color: hasStories ? appColorScheme.secondary : dividerColor)),
        ),
      ),
    );
  }
}
