import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/comment_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user/user.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@injectable
class SeenYourProfileBloc extends IAnalysisBloc {
  UserRepository _userRepository;
  CommentRepository _commentRepository;
  LikeRepository _likeRepository;
  FollowerUserRepository _followerUserRepository;
  LocalStorage _localStorage;
  AppConfig _appConfig;
  int _profileViewsRelevantDataDate;

  SeenYourProfileBloc(
    this._userRepository,
    this._commentRepository,
    this._likeRepository,
    this._followerUserRepository,
    this._localStorage,
    this._appConfig,
  ) : super() {
    _profileViewsRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig
              .getInt(DashboardBloc.PROFILE_VIEWS_RELEVANT_DATA_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;
  }

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      Set<int> comments =
          Set<int>.from(await _commentRepository.getCommentUserIds(
        createdAtGreaterThan: _profileViewsRelevantDataDate,
        deletedAtGreaterThan: _profileViewsRelevantDataDate,
      ))
              .union(Set<int>.from(await _commentRepository.getCommentUserIds(
        isValid: false,
        createdAtGreaterThan: _profileViewsRelevantDataDate,
        deletedAtGreaterThan: _profileViewsRelevantDataDate,
      )));

      Set<int> likes = Set<int>.from(await _likeRepository.getLikeUserIds(
        stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
      ))
          .union(Set<int>.from(await _likeRepository.getLikeUserIds(
        isValid: false,
        stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
      )));

      Set<int> followers = Set<int>.from(
              await _followerUserRepository.getFollowerUserIds(
        isValid: false,
        stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
      ))
          .union(Set<int>.from(await _followerUserRepository.getFollowerUserIds(
        isValid: false,
        stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
      )));

      Set<int> interactions = comments.union(likes).union(followers);

      Set<int> simulatedProfileViews =
          _localStorage.hasData(DashboardBloc.SIMULATED_PROFILE_VIEWS) &&
                  _localStorage.hasData(DataLoadBloc.FIRST_LOAD_DATA) &&
                  _localStorage.read(DataLoadBloc.FIRST_LOAD_DATA) >
                      DateTime.now()
                          .add(Duration(
                            days: -_appConfig.getInt(
                                DashboardBloc.USERS_RELEVANT_DATA_THRESHOLD),
                          ))
                          .millisecondsSinceEpoch
              ? Set.from(List.from(_localStorage
                  .read<List<dynamic>>(DashboardBloc.SIMULATED_PROFILE_VIEWS)))
              : Set.from(<int>[]);

      Set<int> profileViews = interactions.union(simulatedProfileViews);

      List<User> userWithInteractions = await Future.wait(
        profileViews.toList().map(
              (e) => _userRepository.getUser(id: e),
            ),
      );

      List<AnalysisUserItem> analysisUserItems = [];

      userWithInteractions.forEach((element) async {
        analysisUserItems.add(
          AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            imageUrl: element.profilePicUrl,
          ),
        );
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
