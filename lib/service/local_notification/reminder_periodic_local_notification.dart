part of 'local_notification_register_service.dart';

Future<void> _reminderPeriodicLocalNotification(BuildContext context) async {
  LocalNotificationService _localNotificationService = getIt.get<LocalNotificationService>();
  AppConfig _appConfig = getIt.get<AppConfig>();

  RepeatInterval interval;

  switch (_appConfig.getString(LocalNotificationRegisterService.NOTIFICATION_REMINDER_PERIODICITY)) {
    case "every_minute":
      interval = LocalNotificationService.PERIODIC_NOTIFICATIONS_INTERVAL_EVERY_MINUTE;
      break;
    case "hourly":
      interval = LocalNotificationService.PERIODIC_NOTIFICATIONS_INTERVAL_HOURLY;
      break;
    case "daily":
      interval = LocalNotificationService.PERIODIC_NOTIFICATIONS_INTERVAL_DAILY;
      break;
    case "weekly":
      interval =
          LocalNotificationService.PERIODIC_NOTIFICATIONS_INTERVAL_WEEKLY;
      break;
    default:
      interval = LocalNotificationService.PERIODIC_NOTIFICATIONS_INTERVAL_DAILY;
      break;
  }

  await _localNotificationService.sendPeriodicNotification(
    notificationId: LocalNotificationRegisterService.NOTIFICATION_REMINDER_ID,
    title: S.of(context).notificationDailyReminderTitle,
    body: S.of(context).notificationDailyReminderBody,
    payload: LocalNotificationRegisterService.NOTIFICATION_REMINDER_PAYLOAD,
    interval: interval,
  );
}