part of 'like_sorted_users_load_bloc.dart';

abstract class LikeSortedUsersLoadState extends Equatable {
  const LikeSortedUsersLoadState();

  @override
  List<Object> get props => [];
}

class LikeSortedUsersLoadInitial extends LikeSortedUsersLoadState {}

abstract class LikeSortedUsersLoadData extends LikeSortedUsersLoadState {
  final List<UserLikeCount> usersWhoMostLikedYou;
  final List<UserLikeCount> usersWhoLeastLikedYou;
  const LikeSortedUsersLoadData({
    @required this.usersWhoMostLikedYou,
    @required this.usersWhoLeastLikedYou,
  });

  @override
  List<Object> get props => [
    usersWhoMostLikedYou,
    usersWhoLeastLikedYou,
  ];
}

class LikeSortedUsersLoaded extends LikeSortedUsersLoadData {
  const LikeSortedUsersLoaded({
    @required List<UserLikeCount> usersWhoMostLikedYou,
    @required List<UserLikeCount> usersWhoLeastLikedYou,
  }) : super(
    usersWhoMostLikedYou: usersWhoMostLikedYou,
    usersWhoLeastLikedYou: usersWhoLeastLikedYou,
  );
}
