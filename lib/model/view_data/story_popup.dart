import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'story_popup.freezed.dart';

@freezed
abstract class StoryPopup with _$StoryPopup {
  static const int VIDEO_TYPE = 0;
  static const int IMAGE_TYPE = 1;

  const factory StoryPopup({
    @required String storyAsset,
    @required int storyType,
  }) = _StoryPopup;
}
