import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginPage extends StatelessWidget {
  final AppConfig _appConfig = GetIt.I.get<AppConfig>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            height: 30,
          ),
          Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Image.asset(
                    'assets/images/logo_login.png',
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      S.of(context).homeEntry,
                      textAlign: TextAlign.center,
                      style:
                          appTextTheme.subtitle1.copyWith(color: Colors.black),
                    ),
                  ),
                ],
              )),
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 60, 20, 0),
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/login-form');
                    },
                    child: Text(
                      S.of(context).loginWithInstagram.toLowerCase(),
                      style: appTextTheme.button.copyWith(color: Colors.white),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsetsDirectional.only(
                    top: 30.0,
                    start: 40,
                    end: 40,
                  ),
                  child: RichText(
                    text: TextSpan(
                      style: appTextTheme.caption,
                      children: _signInText(context),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  List<TextSpan> _signInText(BuildContext context) {
    var textSpans = <TextSpan>[];
    var trans = S.of(context);

    var termsOfUse = trans.termsOfUse;
    var privacyPolicy = trans.privacyPolicy;

    var text = S.of(context).signIn("||$privacyPolicy||", "||$termsOfUse||");

    text.split("||").forEach((partialText) {
      TextSpan textSpan;
      if (partialText == termsOfUse) {
        textSpan = TextSpan(
            text: partialText,
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                launch(
                  _appConfig.getString(AppConfig.TERMS_OF_USE_URL),
                  forceWebView: true,
                );
              },
            style: TextStyle(
              color: appColorScheme.secondary,
              decoration: TextDecoration.underline,
            ));
      } else if (partialText == privacyPolicy) {
        textSpan = TextSpan(
            text: partialText,
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                launch(
                  _appConfig.getString(AppConfig.PRIVACY_POLICY_URL),
                  forceWebView: true,
                );
              },
            style: TextStyle(
              color: appColorScheme.secondary,
              decoration: TextDecoration.underline,
            ));
      } else {
        textSpan = TextSpan(
          text: partialText,
        );
      }

      textSpans.add(textSpan);
    });

    return textSpans;
  }
}
