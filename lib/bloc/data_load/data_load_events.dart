import 'package:equatable/equatable.dart';

abstract class DataLoadEvent extends Equatable {
  const DataLoadEvent();

  @override
  List<Object> get props => [];
}

class DataLoadStartEvent extends DataLoadEvent {
  final bool showLoading;

  DataLoadStartEvent(this.showLoading);
}
