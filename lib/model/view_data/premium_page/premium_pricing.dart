import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:purchases_flutter/object_wrappers.dart';

part 'premium_pricing.freezed.dart';

@freezed
abstract class PremiumPricing with _$PremiumPricing {
  static const String SUBSCRIPTION_LIFETIME = 'SUBSCRIPTION_LIFETIME';
  static const String SUBSCRIPTION_ANNUAL = 'SUBSCRIPTION_ANNUAL';
  static const String SUBSCRIPTION_SIX_MONTH = 'SUBSCRIPTION_SIX_MONTH';
  static const String SUBSCRIPTION_THREE_MONTH = 'SUBSCRIPTION_THREE_MONTH';
  static const String SUBSCRIPTION_TWO_MONTH = 'SUBSCRIPTION_TWO_MONTH';
  static const String SUBSCRIPTION_MONTHLY = 'SUBSCRIPTION_MONTHLY';
  static const String SUBSCRIPTION_WEEKLY = 'SUBSCRIPTION_WEEKLY';

  const factory PremiumPricing({
    Package package,
    String title,
    String savePercentage,
    String monthlyPrice,
    String totalPrice,
    String boxLabel,
  }) = _PremiumPricing;

  factory PremiumPricing.fromKey({
    @required String key,
    @required Offering offering,
    @required BuildContext buildContext,
  }) {
    S translation = S.of(buildContext);
    AppConfig appConfig = GetIt.I.get<AppConfig>();

    Map<String, dynamic> labelMap = appConfig.getMap(
      AppConfig.SUBSCRIPTION_ORDER_LABEL,
    );

    const Map<String, String> currencyConverter = {
      'AED': 'د.إ',
      'AFN': '؋',
      'ALL': 'L',
      'AMD': '֏',
      'ANG': 'ƒ',
      'AOA': 'Kz',
      'ARS': '\$',
      'AUD': '\$',
      'AWG': 'ƒ',
      'AZN': '₼',
      'BAM': 'KM',
      'BBD': '\$',
      'BDT': '৳',
      'BGN': 'лв',
      'BHD': '.د.ب',
      'BIF': 'FBu',
      'BMD': '\$',
      'BND': '\$',
      'BOB': '\$b',
      'BRL': 'R\$',
      'BSD': '\$',
      'BTC': '฿',
      'BTN': 'Nu.',
      'BWP': 'P',
      'BYR': 'Br',
      'BYN': 'Br',
      'BZD': 'BZ\$',
      'CAD': '\$',
      'CDF': 'FC',
      'CHF': 'CHF',
      'CLP': '\$',
      'CNY': '¥',
      'COP': '\$',
      'CRC': '₡',
      'CUC': '\$',
      'CUP': '₱',
      'CVE': '\$',
      'CZK': 'Kč',
      'DJF': 'Fdj',
      'DKK': 'kr',
      'DOP': 'RD\$',
      'DZD': 'دج',
      'EEK': 'kr',
      'EGP': '£',
      'ERN': 'Nfk',
      'ETB': 'Br',
      'ETH': 'Ξ',
      'EUR': '€',
      'FJD': '\$',
      'FKP': '£',
      'GBP': '£',
      'GEL': '₾',
      'GGP': '£',
      'GHC': '₵',
      'GHS': 'GH₵',
      'GIP': '£',
      'GMD': 'D',
      'GNF': 'FG',
      'GTQ': 'Q',
      'GYD': '\$',
      'HKD': '\$',
      'HNL': 'L',
      'HRK': 'kn',
      'HTG': 'G',
      'HUF': 'Ft',
      'IDR': 'Rp',
      'ILS': '₪',
      'IMP': '£',
      'INR': '₹',
      'IQD': 'ع.د',
      'IRR': '﷼',
      'ISK': 'kr',
      'JEP': '£',
      'JMD': 'J\$',
      'JOD': 'JD',
      'JPY': '¥',
      'KES': 'KSh',
      'KGS': 'лв',
      'KHR': '៛',
      'KMF': 'CF',
      'KPW': '₩',
      'KRW': '₩',
      'KWD': 'KD',
      'KYD': '\$',
      'KZT': 'лв',
      'LAK': '₭',
      'LBP': '£',
      'LKR': '₨',
      'LRD': '\$',
      'LSL': 'M',
      'LTC': 'Ł',
      'LTL': 'Lt',
      'LVL': 'Ls',
      'LYD': 'LD',
      'MAD': 'MAD',
      'MDL': 'lei',
      'MGA': 'Ar',
      'MKD': 'ден',
      'MMK': 'K',
      'MNT': '₮',
      'MOP': 'MOP\$',
      'MRO': 'UM',
      'MRU': 'UM',
      'MUR': '₨',
      'MVR': 'Rf',
      'MWK': 'MK',
      'MXN': '\$',
      'MYR': 'RM',
      'MZN': 'MT',
      'NAD': '\$',
      'NGN': '₦',
      'NIO': 'C\$',
      'NOK': 'kr',
      'NPR': '₨',
      'NZD': '\$',
      'OMR': '﷼',
      'PAB': 'B/.',
      'PEN': 'S/.',
      'PGK': 'K',
      'PHP': '₱',
      'PKR': '₨',
      'PLN': 'zł',
      'PYG': 'Gs',
      'QAR': '﷼',
      'RMB': '￥',
      'RON': 'lei',
      'RSD': 'Дин.',
      'RUB': '₽',
      'RWF': 'R₣',
      'SAR': '﷼',
      'SBD': '\$',
      'SCR': '₨',
      'SDG': 'ج.س.',
      'SEK': 'kr',
      'SGD': '\$',
      'SHP': '£',
      'SLL': 'Le',
      'SOS': 'S',
      'SRD': '\$',
      'SSP': '£',
      'STD': 'Db',
      'STN': 'Db',
      'SVC': '\$',
      'SYP': '£',
      'SZL': 'E',
      'THB': '฿',
      'TJS': 'SM',
      'TMT': 'T',
      'TND': 'د.ت',
      'TOP': 'T\$',
      'TRL': '₤',
      'TRY': '₺',
      'TTD': 'TT\$',
      'TVD': '\$',
      'TWD': 'NT\$',
      'TZS': 'TSh',
      'UAH': '₴',
      'UGX': 'USh',
      'USD': '\$',
      'UYU': '\$U',
      'UZS': 'лв',
      'VEF': 'Bs',
      'VND': '₫',
      'VUV': 'VT',
      'WST': 'WS\$',
      'XAF': 'FCFA',
      'XBT': 'Ƀ',
      'XCD': '\$',
      'XOF': 'CFA',
      'XPF': '₣',
      'YER': '﷼',
      'ZAR': 'R',
      'ZWD': 'Z\$'
    };

    switch (key) {
      case SUBSCRIPTION_LIFETIME:
        return PremiumPricing(
          package: offering?.lifetime,
          title: translation.buy_text_lifetime,
          savePercentage: '',
          monthlyPrice: '',
          boxLabel: labelMap[SUBSCRIPTION_LIFETIME],
        );
      case SUBSCRIPTION_ANNUAL:
        String percentageValue = "0";
        String monthlyPriceCalculated = "0";

        String totalPriceCalculated = "0.00";

        String currency = currencyConverter["USD"];

        if (offering?.annual != null) {
          percentageValue = (100 -
                  (offering.annual.product.price *
                      100 /
                      (offering.monthly.product.price * 12)))
              .toStringAsFixed(0);

          monthlyPriceCalculated =
              (offering.annual.product.price / 12).toStringAsFixed(2);

          totalPriceCalculated =
              (offering.annual.product.price).toStringAsFixed(2);

          currency = currencyConverter[offering.annual.product.currencyCode];
        }

        return PremiumPricing(
          package: offering?.annual,
          title: translation.buy_text_yearly,
          savePercentage: 'Save $percentageValue%',
          monthlyPrice: '$currency$monthlyPriceCalculated/mo',
          totalPrice: '$currency$totalPriceCalculated',
          boxLabel: labelMap[SUBSCRIPTION_ANNUAL],
        );
      case SUBSCRIPTION_SIX_MONTH:
        String percentageValue = "0";
        String monthlyPriceCalculated = "0";

        String totalPriceCalculated = "0.00";

        String currency = currencyConverter["USD"];

        if (offering?.sixMonth != null) {
          percentageValue = (100 -
                  (offering.sixMonth.product.price *
                      100 /
                      (offering.monthly.product.price * 6)))
              .toStringAsFixed(0);
          monthlyPriceCalculated =
              (offering.sixMonth.product.price / 6).toStringAsFixed(2);

          totalPriceCalculated =
              (offering.sixMonth.product.price).toStringAsFixed(2);

          currency = currencyConverter[offering.annual.product.currencyCode];
        }

        return PremiumPricing(
          package: offering?.sixMonth,
          title: translation.buy_text_six_months,
          savePercentage: 'Save $percentageValue%',
          monthlyPrice: '$currency$monthlyPriceCalculated/mo',
          totalPrice: '$currency$totalPriceCalculated',
          boxLabel: labelMap[SUBSCRIPTION_SIX_MONTH],
        );
      case SUBSCRIPTION_THREE_MONTH:
        return PremiumPricing(
          package: offering?.threeMonth,
          title: translation.buy_text_three_months,
          savePercentage: '',
          monthlyPrice: '',
          totalPrice: '',
          boxLabel: labelMap[SUBSCRIPTION_THREE_MONTH],
        );
      case SUBSCRIPTION_TWO_MONTH:
        return PremiumPricing(
          package: offering?.twoMonth,
          title: translation.buy_text_two_months,
          savePercentage: '',
          monthlyPrice: '',
          totalPrice: '',
          boxLabel: labelMap[SUBSCRIPTION_TWO_MONTH],
        );
      case SUBSCRIPTION_MONTHLY:
        return PremiumPricing(
          package: offering?.monthly,
          title: translation.buy_text_monthly,
          savePercentage: '',
          monthlyPrice: '',
          totalPrice: '',
          boxLabel: labelMap[SUBSCRIPTION_MONTHLY],
        );
      case SUBSCRIPTION_WEEKLY:
        return PremiumPricing(
          package: offering?.weekly,
          title: translation.buy_text_one_week,
          savePercentage: '',
          monthlyPrice: '',
          totalPrice: '',
          boxLabel: labelMap[SUBSCRIPTION_WEEKLY],
        );
      default:
        throw UnimplementedError();
    }
  }
}
