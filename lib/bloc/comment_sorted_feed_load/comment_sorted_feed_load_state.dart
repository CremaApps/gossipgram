part of 'comment_sorted_feed_load_bloc.dart';

abstract class CommentSortedFeedLoadState extends Equatable {
  const CommentSortedFeedLoadState();

  @override
  List<Object> get props => [];
}

class CommentSortedFeedLoadInitial extends CommentSortedFeedLoadState {}

abstract class CommentSortedFeedLoadData extends CommentSortedFeedLoadState {
  final List<FeedThumbnail> mostLikedFeed;
  final List<FeedThumbnail> leastLikedFeed;
  const CommentSortedFeedLoadData({
    @required this.mostLikedFeed,
    @required this.leastLikedFeed,
  });

  @override
  List<Object> get props => [mostLikedFeed];
}

class CommentSortedFeedLoaded extends CommentSortedFeedLoadData {
  const CommentSortedFeedLoaded({
    @required mostLikedFeed,
    @required leastLikedFeed,
  }) : super(
    mostLikedFeed: mostLikedFeed,
    leastLikedFeed: leastLikedFeed,
  );
}
