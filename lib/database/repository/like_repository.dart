import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/like.dart';
import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class LikeRepository {
  final DbProvider _dbProvider;

  LikeRepository(this._dbProvider);

  Future<Like> getLike({
    @required String feedId,
    @required int userId,
  }) async {
    final db = await _dbProvider.database;
    var res = await db.query(
      'Like',
      where: 'feedId = ? AND userId = ?',
      whereArgs: [feedId, userId],
    );
    return res.isNotEmpty ? Like.fromMap(res.first) : null;
  }

  Future<List<Like>> queryLikes({
    String feedId,
    int userId,
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
            'FROM Like '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (feedId != null) query += 'AND feedId = ' + feedId.toString() + ' ';
    if (userId != null) query += 'AND userId = ' + userId.toString() + ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var dbLikes = await db.rawQuery(query);
    List<Like> likes =
        dbLikes.isNotEmpty ? dbLikes.map((a) => Like.fromMap(a)).toList() : [];
    return likes;
  }

  Future<int> queryCountLikes({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM Like '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var dbLikesCount = await db.rawQuery(query);

    return dbLikesCount.first.values.first;
  }

  Future<int> upsertLike({@required Like like}) async {
    final db = await _dbProvider.database;
    var dbLike = await db.query(
      'Like',
      where: 'feedId = ? AND userId = ?',
      whereArgs: [like.feedId, like.userId],
    );
    if (dbLike.isNotEmpty) {
      return db.update(
        'Like',
        like.toMap(),
        where: 'feedId = ? AND userId = ?',
        whereArgs: [like.feedId, like.userId],
      );
    } else {
      return await db.insert('Like', like.toMap());
    }
  }

  Future<void> upsertLikes({@required List<Like> likes}) async {
    final db = await _dbProvider.database;
    Batch batch = db.batch();
    var dbLikes = await db.query(
      'Like',
    );

    Set<Like> allLikes = Set<Like>.from(
        dbLikes.isNotEmpty ? dbLikes.map((a) => Like.fromMap(a)).toList() : []);

    Set<Like> toUpdateLikes = Set<Like>.from(likes)
        .where((element) => allLikes.any((Like e) =>
            e.userId == element.userId && e.feedId == element.feedId))
        .toSet();
    Set<Like> toInsertLikes = Set<Like>.from(likes).difference(toUpdateLikes);

    for (Like like in toUpdateLikes) {
      batch.update(
        'Like',
        like.toMap(),
        where: 'feedId = ? AND userId = ?',
        whereArgs: [like.feedId, like.userId],
      );
    }

    for (Like like in toInsertLikes) {
      batch.insert('Like', like.toMap());
    }
    await batch.commit(noResult: true);
  }

  Future<int> deleteLike({@required Like like}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'Like',
      where: 'feedId = ? AND userId = ?',
      whereArgs: [like.feedId, like.userId],
    );
  }

  Future<List<dynamic>> getLikeIds({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
    String feedId,
    int userId,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT feedId, userId '
            'FROM Like '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (feedId != null) {
      query += 'AND feedId = ' + feedId + ' ';
    }

    if (userId != null) {
      query += 'AND userId = ' + userId.toString() + ' ';
    }

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<dynamic> likeIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            return {
              "feedId": a['feedId'],
              "userId": a['userId'],
            };
          }).toList()
        : [];
    return likeIds;
  }

  Future<List<int>> getLikeUserIds({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
    List<int> excluding,
    String feedId,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT DISTINCT(userId) '
            'FROM Like '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (excluding != null && excluding.isNotEmpty) {
      String elements = excluding.join(', ');
      query += "AND userId NOT IN ($elements) ";
    }

    if (feedId != null) {
      query += 'AND feedId = ' + feedId + ' ';
    }

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> likeIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int userId = a['userId'];
            return userId;
          }).toList()
        : [];
    return likeIds;
  }

  Future<List<int>> getLikeUserIdsWithFeedDate({
    bool isValid = true,
    int feedCreationOriginalDateGreaterThan,
    int feedCreationOriginalDateLesserThan,
    List<int> excluding,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT DISTINCT(l.userId) AS id '
            'FROM Like l, Feed f '
            'WHERE l.isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (excluding != null && excluding.isNotEmpty) {
      String elements = excluding.join(', ');
      query += "AND l.userId NOT IN ($elements) ";
    }

    query += 'AND l.feedId = f.id ';

    if (feedCreationOriginalDateGreaterThan != null) {
      query += 'AND f.originalCreatedAtDate > ' +
          (feedCreationOriginalDateGreaterThan / 1000).toString() +
          ' ';
    }

    if (feedCreationOriginalDateLesserThan != null) {
      query += 'AND f.originalCreatedAtDate < ' +
          (feedCreationOriginalDateLesserThan / 1000).toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> likeIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int userId = a['id'];
            return userId;
          }).toList()
        : [];
    return likeIds;
  }
}
