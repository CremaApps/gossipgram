import 'dart:async';

import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:injectable/injectable.dart';

@injectable
class LikedButDontFollowBloc extends IAnalysisBloc {
  final UserRepository _userRepository;
  LocalStorage _localStorage;

  LikedButDontFollowBloc(
    this._userRepository,
    this._localStorage,
  );

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      List<dynamic> nonFollowingAdmiredUsers =
          _localStorage.hasData(DashboardBloc.NON_FOLLOWING_LIKED_USERS)
              ? _localStorage
                  .read<List<dynamic>>(DashboardBloc.NON_FOLLOWING_LIKED_USERS)
              : [];

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          nonFollowingAdmiredUsers.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            youFollow: false,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
