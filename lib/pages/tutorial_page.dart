import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/service/analytics/appsflyer_service.dart';
import 'package:GossipGram/service/local_notification/local_notification_register_service.dart';
import 'package:GossipGram/service/local_notifications_service.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:introduction_screen/introduction_screen.dart';

class TutorialPage extends StatelessWidget {
  static final String TUTORIAL_SEEN = 'TUTORIAL_SEEN';
  final LocalStorage _localStorage = GetIt.I.get<LocalStorage>();
  final AppsflyerService _appsflyerService = GetIt.I.get<AppsflyerService>();
  final LocalNotificationService _localNotificationService =
      GetIt.I.get<LocalNotificationService>();
  final LocalNotificationRegisterService _localNotificationRegisterService =
      GetIt.I.get<LocalNotificationRegisterService>();

  final TextStyle _bodyTextStyle = TextStyle(
    color: AppTheme.neutrals['600'],
    fontWeight: FontWeight.w300,
    fontFamily: "Poppins",
    fontStyle: FontStyle.normal,
    fontSize: 14.0,
  );

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: IntroductionScreen(
        pages: [
          PageViewModel(
            title: S.of(context).tutorialPageTitleOne,
            body: S.of(context).tutorialPageBodyOne,
            image: imagePage(
                pictureUrl: './assets/images/tutorial/1.png',
                width: MediaQuery.of(context).size.width * 0.75),
            decoration: _pageDecoration(context),
          ),
          PageViewModel(
            title: S.of(context).tutorialPageTitleTwo,
            body: S.of(context).tutorialPageBodyTwo,
            image: imagePage(
                pictureUrl: './assets/images/tutorial/2.png',
                width: MediaQuery.of(context).size.width * 0.75),
            decoration: _pageDecoration(context),
          ),
          PageViewModel(
            title: S.of(context).tutorialPageTitleThree,
            body: S.of(context).tutorialPageBodyThree,
            image: imagePage(
                pictureUrl: './assets/images/tutorial/3.png',
                width: MediaQuery.of(context).size.width * 0.75),
            decoration: _pageDecoration(context),
          ),
          PageViewModel(
            title: S.of(context).tutorialPageTitleFour,
            bodyWidget: Column(
              children: [
                Text(
                  S.of(context).tutorialPageBodyFour,
                  style: _bodyTextStyle,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () async {
                    _localStorage.write(TUTORIAL_SEEN, true,
                        generalStorage: true);
                    await _localNotificationService
                        .requestNotificationPermissions();
                    await _localNotificationService.initialize();
                    await _localNotificationRegisterService
                        .registerNotifications(
                      context: context,
                      notificationIds: [
                        LocalNotificationRegisterService
                            .NOTIFICATION_REMINDER_ID,
                        LocalNotificationRegisterService
                            .NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_ID,
                        LocalNotificationRegisterService
                            .NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_ID,
                        LocalNotificationRegisterService
                            .NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_ID,
                      ],
                    );
                    _appsflyerService.logEvent(
                      eventName: 'af_tutorial_completion',
                      eventValues: {'af_success': true},
                    );
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    S.of(context).tutorialStartButton,
                    style: appTextTheme.button.copyWith(color: Colors.white),
                  ),
                )
              ],
            ),
            image: imagePage(
                pictureUrl: './assets/images/tutorial/4.png',
                width: MediaQuery.of(context).size.width * 0.75),
            decoration: _pageDecoration(context),
          ),
        ],
        showDoneButton: false,
        next: Image.asset(
          './assets/images/tutorial/arrow.png',
          width: 24,
        ),
        dotsDecorator: DotsDecorator(
          size: const Size.square(6),
          activeSize: const Size.square(6.0),
          spacing: const EdgeInsets.symmetric(horizontal: 2.0),
          activeColor: AppTheme.primaries['600'],
        ),
      ),
    );
  }

  PageDecoration _pageDecoration(BuildContext context) {
    return PageDecoration(
      imageFlex: 3,
      bodyFlex: 2,
      titleTextStyle: TextStyle(
        color: AppTheme.primaries['900'],
        fontWeight: FontWeight.w900,
        fontFamily: "Poppins",
        fontStyle: FontStyle.normal,
        fontSize: 16.0,
      ),
      bodyTextStyle: _bodyTextStyle,
      titlePadding: EdgeInsets.fromLTRB(
        MediaQuery.of(context).size.width * 0.10,
        0,
        MediaQuery.of(context).size.width * 0.10,
        15,
      ),
      descriptionPadding: EdgeInsets.symmetric(
        vertical: 0,
        horizontal: MediaQuery.of(context).size.width * 0.15,
      ),
    );
  }

  Align imagePage({String pictureUrl, double width}) {
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.7,
        child: Image.asset(
          pictureUrl,
        ),
      ),
    );
  }
}
