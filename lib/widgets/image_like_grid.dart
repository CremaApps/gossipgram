import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/profile_load/profile_load_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:GossipGram/widgets/feed_picture.dart';
import 'package:GossipGram/widgets/media_popup.dart';

class ImageLikeGrid extends StatefulWidget {
  final ProfileLoadBloc profileLoadBloc;
  final String emptyMessage;

  ImageLikeGrid({
    @required this.profileLoadBloc,
    @required this.emptyMessage,
  });

  @override
  _ImageGridState createState() => _ImageGridState();
}

class _ImageGridState extends State<ImageLikeGrid> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileLoadBloc, ProfileLoadState>(
        cubit: widget.profileLoadBloc,
        builder: (context, state) {
          if (state is ProfileLoadData &&
              (state is ProfileLoadFullyLoaded ||
                  state is ProfileLoadFeedLoading)) {
            if (state.profile.likedFeedThumbnail.isNotEmpty) {
              return Container(
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.profile.likedFeedThumbnail.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                          mainAxisSpacing: 3,
                          crossAxisSpacing: 3,
                    childAspectRatio: 1,
                  ),
                  itemBuilder: (contxt, indx) {
                    return InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return MediaPopup(
                              mediaId: state.profile.likedFeedThumbnail
                                  .elementAt(indx)
                                  .id,
                              mediaType: state.profile.likedFeedThumbnail
                                  .elementAt(indx)
                                  .mediaType,
                            );
                          },
                        );
                      },
                      child: LayoutBuilder(
                        builder: (_, BoxConstraints constraints) {
                          final String imageUrl =
                              state.profile.likedFeedThumbnail
                                  .elementAt(indx)
                                  .image
                                  .getSizedCandidate(
                                    width: constraints.maxWidth,
                                    height: constraints.maxHeight,
                                  )
                                  .url;

                          return Container(
                            child: FeedPicture(profilePicUrl: imageUrl),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(imageUrl),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(6),
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  },
                ),
              );
            } else {
              return Center(
                child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(S.of(context).noPosts,
                            style: appTextTheme.headline6),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(widget.emptyMessage,
                              style: appTextTheme.bodyText1,
                              textAlign: TextAlign.center),
                        ),
                      ],
                    ),
                    height: MediaQuery.of(context).size.height / 3.5,
                    width: MediaQuery.of(context).size.width / 1.5),
              );
            }
          } else {
            return CenteredLoader(size: 'medium');
          }
        });
  }
}
