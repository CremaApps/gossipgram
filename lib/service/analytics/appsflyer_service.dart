import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class AppsflyerService {
  final LocalStorage _localStorage;
  // final AppsflyerSdk _appsflyerSdk =
  //     GetIt.I.get<AppsflyerSdk>(instanceName: 'appsFlyer');

  AppsflyerService(this._localStorage);

  Future<bool> logEvent({
    @required String eventName,
    Map<String, dynamic> eventValues,
  }) async {
    if (eventValues == null) eventValues = {};
    eventValues["af_customer_user_id"] = _localStorage.read("USER_ID");

    return (!kDebugMode)
        ? GetIt.I
            .get<AppsflyerSdk>(instanceName: 'appsFlyer')
            .logEvent(eventName, eventValues)
        : true;
  }
}
