part of 'my_story_bloc.dart';

abstract class MyStoriesState extends Equatable {
  const MyStoriesState();
}

class MyStoriesInitial extends MyStoriesState {
  @override
  List<Object> get props => [];
}

class MyStoriesLoading extends MyStoriesState {
  @override
  List<Object> get props => [];
}

class MyStoriesLoaded extends MyStoriesState {
  final List<Story> stories;
  final int currentIndex;

  MyStoriesLoaded({
    @required this.stories,
    @required this.currentIndex,
  });

  @override
  List<Object> get props => [stories, currentIndex];
}
