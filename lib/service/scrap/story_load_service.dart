import 'dart:io';
import 'package:http/http.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/story_repository.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:GossipGram/model/view_data/story_popup.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/story_view_load_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';

@singleton
class StoryLoadService {
  static const String SERVICE_NAME = "story_load_service";

  StoryRepository _storyRepository;
  StoryViewLoadService _storyViewLoadService;
  InstagramPrivateApi _instagramPrivateApi;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  StoryLoadService(
    this._storyRepository,
    this._storyViewLoadService,
    this._appConfig,
    this._localStorage,
  ) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<void> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    int nowDate = firstLoad ? 0 : DateTime.now().millisecondsSinceEpoch;
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);

    var storyResponse = await _instagramPrivateApi.feed.reelsMedia([
      _instagramPrivateApi.myUserId.toString(),
    ]);
    DataLoadBloc.printProfiling("API stories media step loaded", now);

    var reel = storyResponse.response.reels;

    var stories = reel.isNotEmpty ? reel.first.items : [];

    List<int> activeStoryIds = await _storyRepository.getStoryIds(
      isValid: true,
    );

    for (int id in activeStoryIds) {
      if (!stories.any((story) => story.pk == id)) {
        await _storyRepository.setStoryAsInactive(
          storyId: id,
        );
      }
    }
    DataLoadBloc.printProfiling("Stories media DB updated", now);

    for (var story in stories) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));

      Story newStory = Story(
        id: story.pk,
        thumbnailUrl: null,
        mediaUrl: null,
        mediaType: story.videoVersions != null
            ? StoryPopup.VIDEO_TYPE
            : StoryPopup.IMAGE_TYPE,
        viewCount: 0,
        originalCreatedAtDate: story.takenAt * 1000,
        createdAt: nowDate,
        isValid: true,
      );

      if (!await _storyRepository.existsStory(id: story.pk)) {
        var storyImage = await get(
          story.imageVersions2.candidates.first.url,
        );
        Directory documentDirectory = await getApplicationDocumentsDirectory();
        String firstPath = documentDirectory.path + "/images";
        String filePathAndName =
            documentDirectory.path + '/images/' + story.pk.toString() + '.jpg';
        await Directory(firstPath).create(recursive: true);
        File file = File(filePathAndName);
        file.writeAsBytesSync(storyImage.bodyBytes);

        if (newStory.mediaType == StoryPopup.VIDEO_TYPE) {
          var storyVideo = await get(
            story.videoVersions?.first?.url,
          );
          Directory documentDirectory =
              await getApplicationDocumentsDirectory();
          String firstPath = documentDirectory.path + "/videos";
          String filePathAndName = documentDirectory.path +
              '/videos/' +
              story.pk.toString() +
              '.mp4';
          await Directory(firstPath).create(recursive: true);
          File file = File(filePathAndName);
          file.writeAsBytesSync(storyVideo.bodyBytes);
        }

        await _storyRepository.upsertStory(
          story: newStory,
        );
        DataLoadBloc.printProfiling(
            "Stories media - story downloaded on local", now);
      }

      await _storyViewLoadService.execute(
        story: newStory,
        firstLoad: firstLoad,
        processedUsers: processedUsers,
      );
      DataLoadBloc.printProfiling("Stories media - story loaded on DB", now);
    }
    DataLoadBloc.printProfiling("Stories media DB loaded", now);
  }
}
