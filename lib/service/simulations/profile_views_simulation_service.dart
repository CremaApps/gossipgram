import 'dart:math';

import 'package:flutter/material.dart';
import 'package:GossipGram/database/repository/comment_repository.dart';
import 'package:GossipGram/database/repository/feed_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@singleton
class ProfileViewsSimulationService {
  static const PROFILE_VIEWS_MAX_FEED_ELEMENTS_MIN =
      "profile_views_max_feed_elements_min";
  static const PROFILE_VIEWS_MAX_FEED_ELEMENTS_MAX =
      "profile_views_max_feed_elements_max";
  static const PROFILE_VIEWS_MIN_ELEMENTS_MIN =
      "profile_views_min_elements_min";
  static const PROFILE_VIEWS_MIN_ELEMENTS_MAX =
      "profile_views_min_elements_max";
  static const PROFILE_VIEWS_COEFFICIENT_MIN = "profile_views_coefficient_min";
  static const PROFILE_VIEWS_COEFFICIENT_MAX = "profile_views_coefficient_max";

  FollowerUserRepository _followerUserRepository;
  LikeRepository _likeRepository;
  CommentRepository _commentRepository;
  FeedRepository _feedRepository;
  AppConfig _appConfig;

  int _profileViewsMaxFeedElements;
  int _profileViewsMinElements;
  double _profileViewsCoefficient;

  ProfileViewsSimulationService(
    this._followerUserRepository,
    this._likeRepository,
    this._commentRepository,
    this._feedRepository,
    this._appConfig,
  ) {
    _profileViewsMaxFeedElements = _getRandomIntFromIntervalNumber(
      min: _appConfig.getInt(PROFILE_VIEWS_MAX_FEED_ELEMENTS_MIN),
      max: _appConfig.getInt(PROFILE_VIEWS_MAX_FEED_ELEMENTS_MAX),
    );

    _profileViewsMinElements = _getRandomIntFromIntervalNumber(
      min: _appConfig.getInt(PROFILE_VIEWS_MIN_ELEMENTS_MIN),
      max: _appConfig.getInt(PROFILE_VIEWS_MIN_ELEMENTS_MAX),
    );

    _profileViewsCoefficient = _getRandomDoubleFromIntervalNumber(
      min: _appConfig.getDouble(PROFILE_VIEWS_COEFFICIENT_MIN),
      max: _appConfig.getDouble(PROFILE_VIEWS_COEFFICIENT_MAX),
    );
  }

  Future<List<int>> execute() async {
    List<String> lastFeedIds = await _feedRepository.getFeedIds(
      maxElems: _profileViewsMaxFeedElements,
      newestFirst: true,
    );

    Set<int> interactingUsers = Set.from(<int>[]);
    for (String feedId in lastFeedIds) {
      Set<int> commentUsers =
          Set.from(await _commentRepository.getCommentUserIds(
        feedId: feedId,
      ));
      Set<int> likeUsers = Set.from(await _likeRepository.getLikeUserIds(
        feedId: feedId,
      ));
      interactingUsers = interactingUsers.union(commentUsers).union(likeUsers);
    }

    Set<int> followers =
        Set.from(await _followerUserRepository.getFollowerUserIds(
      isValid: true,
    ));

    List<int> validUsers = List.from(interactingUsers.intersection(followers));

    int numFollowers = followers.length;

    int maxSeenYourProfileUsers = min(
      numFollowers,
      _profileViewsMinElements +
          sqrt(numFollowers *
                  min(
                    _profileViewsCoefficient,
                    (max(0, numFollowers - 2 * _profileViewsMinElements))
                        .toDouble(),
                  ))
              .ceil(),
    );

    int elementsToSimulate = maxSeenYourProfileUsers;

    if (maxSeenYourProfileUsers > (0.8 * validUsers.length).floor()) {
      elementsToSimulate = (validUsers.length *
              (Random.secure().nextInt(301).toDouble() + 500) /
              1000)
          .floor();
    }

    List<int> simulatedUsers = [];

    for (int i = 0; i < elementsToSimulate && validUsers.isNotEmpty; ++i) {
      int i = Random.secure().nextInt(validUsers.length);
      simulatedUsers.add(validUsers[i]);
      validUsers.removeAt(i);
    }

    return simulatedUsers;
  }

  int _getRandomIntFromIntervalNumber({@required int min, @required int max}) {
    return min == max ? min : min + Random.secure().nextInt(max - min);
  }

  double _getRandomDoubleFromIntervalNumber(
      {@required double min, @required double max}) {
    return min + Random.secure().nextDouble() * (max - min);
  }
}
