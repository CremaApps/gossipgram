import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user/user.dart';

import 'package:injectable/injectable.dart';

part 'my_story_popup_event.dart';
part 'my_story_popup_state.dart';

@injectable
class MyStoryPopupBloc extends Bloc<MyStoryPopupEvent, MyStoryPopupState> {
  UserRepository _userRepository;

  MyStoryPopupBloc(
    this._userRepository,
  ) : super(MyStoryPopupInitial());

  @override
  Stream<MyStoryPopupState> mapEventToState(
    MyStoryPopupEvent event,
  ) async* {
    if (event is MyPopupStoryStartLoadEvent) {
      yield MyStoryPopupLoading();

      List<User> users = await _userRepository.getUsersSortedByStoryViewTime(
        storyId: event.storyId,
        maxElems: 3,
        newestFirst: true,
      );

      yield MyStoryPopupLoaded(
        seenBy: users.isNotEmpty
            ? users.map((user) => user.profilePicUrl).toList()
            : [],
      );
    }
  }
}
