import 'package:flutter/foundation.dart';
import 'package:GossipGram/model/objects/thumbnail.dart';

class StoryThumbnail extends Thumbnail {
  int viewCount;

  StoryThumbnail({
    @required id,
    @required image,
    @required mediaType,
    @required this.viewCount,
  }) : super(
          id: id,
          image: image,
          mediaType: mediaType,
        );
}
