import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/database/repository/feed_repository.dart';
import 'package:GossipGram/model/objects/feed.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/model/objects/user_like_count.dart';
import 'package:GossipGram/service/api/get_my_stored_feed_images_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_events.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_state.dart';
import 'package:GossipGram/database/repository/comment_repository.dart';
import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/database/repository/story_repository.dart';
import 'package:GossipGram/database/repository/story_view_repository.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:GossipGram/model/view_data/dashboard.dart';
import 'package:GossipGram/service/api/been_blocked_by_user_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:injectable/injectable.dart';

@singleton
class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  static const String USERS_RELEVANT_DATA_THRESHOLD =
      "users_relevant_data_threshold";
  static const String USER_STORIES_RELEVANT_DATA_THRESHOLD =
      "user_stories_relevant_data_threshold";
  static const String SECRET_ADMIRERS_RELEVANT_DATA_THRESHOLD =
      "secret_admirers_relevant_data_threshold";
  static const String PROFILE_VIEWS_RELEVANT_DATA_DATA_THRESHOLD =
      "profile_views_relevant_data_data_threshold";

  static const String NON_FOLLOWING_LIKED_USERS = 'NON_FOLLOWING_LIKED_USERS';
  static const String SIMULATED_PROFILE_VIEWS = 'SIMULATED_PROFILE_VIEWS';
  static const String USER_ID = 'USER_ID';

  FollowerUserRepository _followerUserRepository;
  FollowedUserRepository _followedUserRepository;
  StoryRepository _storyRepository;
  LikeRepository _likeRepository;
  CommentRepository _commentRepository;
  StoryViewRepository _storyViewRepository;
  UserRepository _userRepository;
  LocalStorage _localStorage;
  List<int> _excludeUserIds;

  InstagramPrivateApi _instagramPrivateApi;
  Dashboard _dashboard = Dashboard();
  AppConfig _appConfig;

  int _usersRelevantDataDate;
  int _userStoriesRelevantDataDate;
  int _secretAdmirersRelevantDataDate;
  int _profileViewsRelevantDataDate;

  DashboardBloc(
    this._followerUserRepository,
    this._followedUserRepository,
    this._storyRepository,
    this._likeRepository,
    this._commentRepository,
    this._storyViewRepository,
    this._appConfig,
    this._localStorage,
    this._userRepository,
  ) : super(DashboardStateLoading(
          dashboard: Dashboard(),
        )) {
    _instagramPrivateApi = InstagramPrivateApi.instance();

    _usersRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig.getInt(USERS_RELEVANT_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;

    _userStoriesRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig.getInt(USER_STORIES_RELEVANT_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;

    _secretAdmirersRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig.getInt(SECRET_ADMIRERS_RELEVANT_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;

    _profileViewsRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig.getInt(PROFILE_VIEWS_RELEVANT_DATA_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;
  }

  @override
  Stream<DashboardState> mapEventToState(DashboardEvent event) async* {
    _excludeUserIds =
        _localStorage.hasData(DashboardBloc.USER_ID, generalStorage: true)
            ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
            : [];
    double now = DateTime.now().millisecondsSinceEpoch / 1000;
    DataLoadBloc.printProfiling(
        "DASHBOARD - Comienza la carga en dashboard", now);

    if (event is DashboardLoadProfileEvent) {
      await loadMyself();
      DataLoadBloc.printProfiling("DASHBOARD - Myself loaded", now);
      yield DashboardStateProfileLoaded(dashboard: _dashboard);
    }
    if (event is DashboardLoadFollowersEvent || event is DashboardLoadFromDb) {
      await loadFollowers();
      DataLoadBloc.printProfiling("DASHBOARD - Followers loaded", now);
    }
    if (event is DashboardLoadFollowedsEvent || event is DashboardLoadFromDb) {
      await loadFolloweds();
      DataLoadBloc.printProfiling("DASHBOARD - Followeds loaded", now);
    }
    if (event is DashboardLoadFollowRelationshipsEvent ||
        event is DashboardLoadFromDb) {
      await loadFollowBackDifferences();
      DataLoadBloc.printProfiling("DASHBOARD - FollowBack loaded", now);
    }
    if (event is DashboardLoadStoriesEvent || event is DashboardLoadFromDb) {
      await loadStories();
      DataLoadBloc.printProfiling("DASHBOARD - Stories loaded", now);
    }
    if (event is DashboardLoadFeedEvent || event is DashboardLoadFromDb) {
      await loadProfileViews();
      DataLoadBloc.printProfiling("DASHBOARD - Profile views loaded", now);
      await getBlockedByUsers();
      DataLoadBloc.printProfiling("DASHBOARD - Blocked by loaded", now);
      await getDeletedLikesAndComments();
      DataLoadBloc.printProfiling(
          "DASHBOARD - Deleted likes and comments loaded", now);
      await getSecretAdmirers();
      DataLoadBloc.printProfiling("DASHBOARD - Secret admirers loaded", now);
    }
    if (event is DashboardLoadDataEvent || event is DashboardLoadFromDb) {
      await getLikedButDontFollow();
      await getlikedYouTheMost();
      await getNumberOfPosts();
      DataLoadBloc.printProfiling(
          "DASHBOARD - Liked But don't follow loaded", now);
    }
    if (event is DashboardReloadPayment) {
      // TODO: REMOVE THIS JUST BUGFIX FOR RELOADING PAYMENT AFTER RESTORING PURCHASES
      yield DashboardStateProfileLoaded(
        dashboard: _dashboard,
      );
    }
    if (!(event is DashboardEvent)) {
      throw UnimplementedError();
    }
    DataLoadBloc.printProfiling("DASHBOARD - Finished event load", now);

    yield DashboardStateFullLoaded(
      dashboard: _dashboard,
    );
  }

  Future<void> loadFollowers() async {
    int followers =
        await _followerUserRepository.queryCountFollowerUsers(isValid: true);
    int lostFollowers = await _followerUserRepository.queryCountFollowerUsers(
      stateChangedAtGreaterThan: _usersRelevantDataDate,
      isValid: false,
    );
    int newFollowers = await _followerUserRepository.queryCountFollowerUsers(
      stateChangedAtGreaterThan: _usersRelevantDataDate,
      isValid: true,
    );

    _dashboard = _dashboard.copyWith(
      followers: followers,
      lostFollowers: lostFollowers,
      newFollowers: newFollowers,
    );
  }

  Future<void> loadMyself() async {
    var myself =
        await _userRepository.getUser(id: _instagramPrivateApi.myUserId);

    if (myself != null) {
      _dashboard = _dashboard.copyWith(
        fullName: myself.fullName,
        username: myself.username,
        profilePicUrl: myself.profilePicUrl,
      );
    } else {
      var accountInfo = await _instagramPrivateApi.user
          .getAccountInfo(_instagramPrivateApi.myUserId);

      _dashboard = _dashboard.copyWith(
        fullName: accountInfo.response.user.fullName,
        username: accountInfo.response.user.username,
        profilePicUrl: accountInfo.response.user.profilePicUrl,
      );
    }
  }

  Future<void> loadFolloweds() async {
    int following =
        await _followedUserRepository.queryCountFollowedUsers(isValid: true);
    _dashboard = _dashboard.copyWith(
      following: following,
    );
  }

  Future<void> loadFollowBackDifferences() async {
    List<int> followedIds = await _followedUserRepository.getFollowedUserIds();
    List<int> followerIds = await _followerUserRepository.getFollowerUserIds();

    int notFollowerBack = 0;
    for (int followerId in followerIds) {
      if (!followedIds.any((element) => (element == followerId)))
        ++notFollowerBack;
    }

    int notFollowingBack = 0;
    for (int followedId in followedIds) {
      if (!followerIds.any((element) => (element == followedId)))
        ++notFollowingBack;
    }
    _dashboard = _dashboard.copyWith(
      notFollowerBack: notFollowerBack,
      notFollowingBack: notFollowingBack,
    );
  }

  Future<void> loadProfileViews() async {
    Set<int> comments =
        Set<int>.from(await _commentRepository.getCommentUserIds(
      createdAtGreaterThan: _profileViewsRelevantDataDate,
      deletedAtGreaterThan: _profileViewsRelevantDataDate,
    ))
            .union(Set<int>.from(await _commentRepository.getCommentUserIds(
      isValid: false,
      createdAtGreaterThan: _profileViewsRelevantDataDate,
      deletedAtGreaterThan: _profileViewsRelevantDataDate,
    )));

    Set<int> likes = Set<int>.from(await _likeRepository.getLikeUserIds(
      stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
    ))
        .union(Set<int>.from(await _likeRepository.getLikeUserIds(
      isValid: false,
      stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
    )));

    Set<int> followers = Set<int>.from(
            await _followerUserRepository.getFollowerUserIds(
      isValid: false,
      stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
    ))
        .union(Set<int>.from(await _followerUserRepository.getFollowerUserIds(
      isValid: false,
      stateChangedAtGreaterThan: _profileViewsRelevantDataDate,
    )));

    Set<int> interactions = comments.union(likes).union(followers);

    Set<int> simulatedProfileViews = _localStorage
                .hasData(SIMULATED_PROFILE_VIEWS) &&
            _localStorage.hasData(DataLoadBloc.FIRST_LOAD_DATA) &&
            _localStorage.read(DataLoadBloc.FIRST_LOAD_DATA) >
                DateTime.now()
                    .add(Duration(
                      days: -_appConfig.getInt(USERS_RELEVANT_DATA_THRESHOLD),
                    ))
                    .millisecondsSinceEpoch
        ? Set.from(List.from(
            _localStorage.read<List<dynamic>>(SIMULATED_PROFILE_VIEWS)))
        : Set.from(<int>[]);

    Set<int> profileViews = interactions.union(simulatedProfileViews);

    _dashboard = _dashboard.copyWith(
      profileViews: profileViews.length,
    );
  }

  Future<void> loadStories() async {
    List<Story> activeStories = (await _storyRepository.queryStories(
      isValid: true,
    ));

    int storyViews = 0;
    for (Story story in activeStories) {
      storyViews += (await _storyViewRepository.queryCountStoryViews(
        storyId: story.id,
        isValid: true,
      ));
    }

    _dashboard = _dashboard.copyWith(
      activeStories: activeStories.length, // getActiveUsers
      storyViews: storyViews, // getActiveUsers
    );
  }

  Future<void> getSecretAdmirers() async {
    List<int> usersLikedYouByDate = (await _likeRepository.getLikeUserIds(
      isValid: true,
      stateChangedAtGreaterThan: _secretAdmirersRelevantDataDate,
      excluding: _excludeUserIds,
    ));
    List<int> usersCommentedYouByDate =
        (await _commentRepository.getCommentUserIds(
      isValid: true,
      createdAtGreaterThan: _secretAdmirersRelevantDataDate,
      excluding: _excludeUserIds,
    ));

    List<int> usersLikedYouByFeedDate =
        (await _likeRepository.getLikeUserIdsWithFeedDate(
      isValid: true,
      feedCreationOriginalDateGreaterThan: _secretAdmirersRelevantDataDate,
      excluding: _excludeUserIds,
    ));
    List<int> usersCommentedYouByFeedDate =
        (await _commentRepository.getCommentUserIdsWithFeedDate(
      isValid: true,
      feedCreationOriginalDateGreaterThan: _secretAdmirersRelevantDataDate,
      excluding: _excludeUserIds,
    ));

    List<int> usersWatchedYourStories =
        (await _storyViewRepository.getStoryViewUserIds(
      createdAtGreaterThan: _secretAdmirersRelevantDataDate,
    ));

    List<int> followers = (await _followerUserRepository.getFollowerUserIds(
      isValid: true,
    ));

    List<int> users = [
      ...{
        ...[
          ...usersLikedYouByDate,
          ...usersCommentedYouByDate,
          ...usersLikedYouByFeedDate,
          ...usersCommentedYouByFeedDate,
          ...usersWatchedYourStories
        ]
      }
    ];

    List<int> secretAdmirers = [];
    for (int user in users) {
      if (!followers.any((element) => (element == user)))
        secretAdmirers.add(user);
    }

    _dashboard = _dashboard.copyWith(
      secretAdmirers: secretAdmirers.length, // getActiveUsers
    );
  }

  Future<void> getDeletedLikesAndComments() async {
    List<int> usersDeletedLikes = await _likeRepository.getLikeUserIds(
      isValid: false,
      stateChangedAtGreaterThan: _usersRelevantDataDate,
      excluding: _excludeUserIds,
    );

    List<int> usersDeletedComments = await _commentRepository.getCommentUserIds(
      isValid: false,
      deletedAtGreaterThan: _usersRelevantDataDate,
      excluding: _excludeUserIds,
    );

    List<int> deletedItems = [
      ...{
        ...[
          ...usersDeletedLikes,
          ...usersDeletedComments,
        ]
      }
    ];

    _dashboard = _dashboard.copyWith(
      deletedLikesOrComments: deletedItems.length, // getActiveUsers
    );
  }

  Future<void> getBlockedByUsers() async {
    List<int> blockedByUsers = _localStorage
        .read<List<dynamic>>(BeenBlockedByUserService.BLOCKED_CACHE)
        .map((e) => (e is int) ? e : int.parse(e))
        .toList();

    _dashboard = _dashboard.copyWith(
      blockedUsers: blockedByUsers.length,
    );
  }

  Future<void> getLikedButDontFollow() async {
    List<int> nonFollowingAdmiredUsers =
        _localStorage.hasData(NON_FOLLOWING_LIKED_USERS)
            ? _localStorage
                .read<List<dynamic>>(NON_FOLLOWING_LIKED_USERS)
                .map((user) {
                int userId = user;
                if (userId != _excludeUserIds.first) return userId;
              }).toList()
            : [];

    _dashboard = _dashboard.copyWith(
      nonFollowingAdmiredUsers: nonFollowingAdmiredUsers.length,
    );
  }

  Future<void> getlikedYouTheMost() async {
    List<UserLikeCount> usersWhoMostLikedYou =
        await _userRepository.getUsersSortedByLikeCount(
      isValid: true,
      maxElems: 20,
      asc: false,
      excluding: _localStorage.hasData(DashboardBloc.USER_ID,
              generalStorage: true)
          ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
          : [],
    );
    _dashboard = _dashboard.copyWith(
      likedYouTheMost: usersWhoMostLikedYou.length,
    );
  }

  Future<void> getNumberOfPosts() async {
    final db = await DbProvider(_localStorage).database;
    var query = 'SELECT COUNT(*) '
        'FROM Feed ';

    var dbFeedsCount = await db.rawQuery(query);

    _dashboard = _dashboard.copyWith(
      totalPosts: dbFeedsCount.first.values.first,
    );
  }
}
