import 'dart:io';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/feed_repository.dart';
import 'package:GossipGram/model/objects/feed.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/comment_load_service.dart';
import 'package:GossipGram/service/scrap/feed_load_service.dart';
import 'package:GossipGram/service/scrap/like_load_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:GossipGram/service/storage/secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import '../../injection.dart';

part 'load_feed_initial_buffer_background_task.dart';

void backgroundTaskHeadlessListener(String taskId) async {
  GetIt getIt = configureDependencies();
  String instagramCookies =
      await getIt.get<SecureStorage>().getInstagramCookie();

  if (instagramCookies != null) {
    List<String> cookiesString = instagramCookies.split(
      SecureStorage.COOKIE_SPLIT,
    );

    List<InstaCookie> instaCookies = [];

    cookiesString.forEach((cookieString) {
      instaCookies.add(
        InstaCookie.fromStringCookie(cookieString),
      );
    });

    InstagramPrivateApi.instance().cookies = instaCookies;
  }
  backgroundTaskListener(taskId);
}

void backgroundTaskListener(String taskId) async {
  String packageBaseName = Platform.isAndroid
      ? 'com.cremapps.gossipgram'
      : 'com.cremapps.gossipgram';
  taskId = taskId.replaceAll("$packageBaseName.", "");
  switch (taskId) {
    case BackgroundTaskRegisterService.LOAD_FEED_INITIAL_BUFFER:
      await _loadFeedInitialBufferBackgroundTask();
      break;
    default:
      break;
  }
  await BackgroundFetch.finish(taskId);
}

@lazySingleton
class BackgroundTaskRegisterService {
  static const String LOAD_FEED_INITIAL_BUFFER = 'postqueue';

  String _packageBaseName = Platform.isAndroid
      ? 'com.cremapps.gossipgram'
      : 'com.cremapps.gossipgram';
  List<String> _allValidTasks = [
    LOAD_FEED_INITIAL_BUFFER,
  ];
  BackgroundTaskRegisterService();

  // For tasks with the app open
  Future<void> setBackgroundListeners() async {
    return BackgroundFetch.configure(
      BackgroundFetchConfig(
          minimumFetchInterval: 15,
          stopOnTerminate: false,
          enableHeadless: false,
          requiresBatteryNotLow: false,
          requiresCharging: false,
          requiresStorageNotLow: false,
          requiresDeviceIdle: false,
          requiredNetworkType: NetworkType.ANY),
      backgroundTaskListener,
    );
  }

  // For tasks with the app closed
  Future<void> setHeadlessListeners() {
    return BackgroundFetch.registerHeadlessTask(backgroundTaskHeadlessListener);
  }

  // For tasks with the app closed
  Future<void> registerTask({
    @required String taskName,
    @required bool periodic,
    @required int delay,
  }) async {
    if (_allValidTasks.contains(taskName)) {
      await BackgroundFetch.scheduleTask(
        TaskConfig(
          taskId: "$_packageBaseName.$taskName",
          delay: delay,
          requiredNetworkType: NetworkType.ANY,
          enableHeadless: true,
          forceAlarmManager: true,
          periodic: periodic,
          requiresBatteryNotLow: true,
        ),
      );
    }
  }
}
