import 'package:equatable/equatable.dart';

abstract class DashboardEvent extends Equatable {

  const DashboardEvent();

  @override
  List<Object> get props => [];

}

class DashboardLoadProfileEvent extends DashboardEvent {}
class DashboardLoadFollowersEvent extends DashboardEvent {}
class DashboardLoadFollowedsEvent extends DashboardEvent {}
class DashboardLoadFollowRelationshipsEvent extends DashboardEvent {}
class DashboardLoadStoriesEvent extends DashboardEvent {}
class DashboardLoadFeedEvent extends DashboardEvent {}
class DashboardLoadDataEvent extends DashboardEvent {}
class DashboardLoadFromDb extends DashboardEvent {}
class DashboardReloadPayment extends DashboardEvent {}

