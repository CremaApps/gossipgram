import 'package:freezed_annotation/freezed_annotation.dart';

part 'seen_your_story_user_item.freezed.dart';

@freezed
abstract class SeenYourStoryUserItem with _$SeenYourStoryUserItem {
  const factory SeenYourStoryUserItem({
    int userId,
    String name,
    String userName,
    String imageUrl,
    int storyId,
  }) = _SeenYourStoryUserItem;
}