import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/my_profile_load/my_profile_load_bloc.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:GossipGram/widgets/media_popup.dart';

class MyImageGrid extends StatefulWidget {
  final MyProfileLoadBloc profileLoadBloc;

  MyImageGrid({@required this.profileLoadBloc});

  @override
  _ImageGridState createState() => _ImageGridState();
}

class _ImageGridState extends State<MyImageGrid> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MyProfileLoadBloc, MyProfileLoadState>(
        cubit: widget.profileLoadBloc,
        builder: (context, state) {
          if (state is MyProfileLoadData &&
              (state is MyProfileLoadFullyLoaded ||
                  state is MyProfileLoadFeedLoading)) {
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.myProfile.feedThumbnail.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 3,
                    crossAxisSpacing: 3,
                    childAspectRatio: 1,
                  ),
                  itemBuilder: (contxt, indx) {
                    return InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return MediaPopup(
                              mediaId: state.myProfile.feedThumbnail
                                  .elementAt(indx)
                                  .id,
                              mediaType: state.myProfile.feedThumbnail
                                  .elementAt(indx)
                                  .mediaType,
                            );
                          },
                        );
                      },
                      child: LayoutBuilder(
                        builder: (_, BoxConstraints constraints) {
                          return Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(state
                                    .myProfile.feedThumbnail
                                    .elementAt(indx)
                                    .image
                                    .getSizedCandidate(
                                        width: constraints.maxWidth,
                                        height: constraints.maxHeight)
                                    .url),
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  },
                ));
          } else {
            return CenteredLoader(
              size: 'medium',
            );
          }
        });
  }
}
