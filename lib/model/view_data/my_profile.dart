import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

part 'my_profile.freezed.dart';

@freezed
abstract class MyProfile with _$MyProfile {
  const factory MyProfile({
    String fullName,
    String profilePicUrl,
    String bio,
    int followers,
    int following,
    double engagementRate,
    double averageLikes,
    double followerRatio,
    List<FeedThumbnail> feedThumbnail,
    InstagramResponse feedResponse,
  }) = _MyProfile;
}
