import 'package:flutter/material.dart';
import 'package:GossipGram/database/repository/feed_repository.dart';
import 'package:GossipGram/model/objects/feed.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/src/Client/Response/CommonClasses.dart';

@singleton
class GetMyStoredFeedImagesService {
  FeedRepository feedRepository;

  GetMyStoredFeedImagesService({
    @required this.feedRepository,
  });

  Future<List<FeedThumbnail>> execute() async {
    List<Feed> feedItems = await feedRepository.queryFeeds();
    List<FeedThumbnail> feedThumbnailData = [];

    for (var feedItem in feedItems) {
      ImageVersions2 image = ImageVersions2(
          candidates: [
        ImageCandidate(url: feedItem.mediaUrl, width: 0, height: 0),
      ].toList());

      // Hemos puesto 0 a la width y a la height porque la imagen se recoge de base de datos
      // Y no tenemos estos valores. Si no ponemos nada (width y height no son required) peta la vista.
      // Está claramente pendiente de refactor

      try {
        feedThumbnailData.add(
          FeedThumbnail(
            id: int.tryParse(feedItem.id),
            image: image,
            mediaType: feedItem.mediaType,
            likeCount: feedItem.likeCount,
            commentCount: feedItem.commentCount,
          ),
        );
      } catch (e) {
        print("The id was not a string, ignore media. We should change this.");
      }
    }

    return feedThumbnailData;
  }
}
