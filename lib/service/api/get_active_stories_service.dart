import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/model/view_data/people_story.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

@lazySingleton
class GetActiveStoriesService {
  final InstagramPrivateApi _instagramPrivateApi =
      InstagramPrivateApi.instance();
  final LocalStorage _localStorage;

  GetActiveStoriesService(
    this._localStorage,
  );

  Future<dynamic> execute({
    List<Tray> storyFeed,
  }) async {
    if (storyFeed == null) {
      ReelsTrayFeedResponse stories =
          (await _instagramPrivateApi.feed.reelsTray()).response;
      stories.tray.removeWhere(
          (story) => story.id == _localStorage.read(DashboardBloc.USER_ID));
      storyFeed = stories.tray;
    }

    var storyContent = [];

    for (int i = 0; storyFeed.isNotEmpty && i < 6; ++i) {
      storyContent.add(storyFeed.removeAt(0).user.pk.toString());
    }

    storyContent = await Future.wait(
      storyContent.map((e) => _instagramPrivateApi.feed.reelsMedia([e])),
    );

    List<PeopleStory> response = storyContent
        .map(
          (e) => PeopleStory(
            userId: e.response.reels.first.user.pk.toString(),
            fullName: e.response.reels.first.user.fullName != ''
                ? e.response.reels.first.user.fullName
                : e.response.reels.first.user.username,
            profilePic: e.response.reels.first.user.profilePicUrl,
            storyPic: e.response.reels.first.items.first.imageVersions2
                .candidates.first.url,
          ),
        )
        .toList();

    return {
      "storyFeed": storyFeed,
      "response": response,
    };
  }
}
