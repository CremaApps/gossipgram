import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:GossipGram/generated/l10n.dart';

part 'premium_subtitle_text.freezed.dart';

@freezed
abstract class PremiumSubtitleText with _$PremiumSubtitleText {
  static const UNCOVER_ANYONE_ACTIVITIES = 'UNCOVER_ANYONE_ACTIVITIES';
  static const WHO_REALLY_LIKES_YOUR_STORY = 'WHO_REALLY_LIKES_YOUR_STORY';
  static const SEE_SECRET_ADMIRERS = 'SEE_SECRET_ADMIRERS';
  static const WHO_ARE_YOUR_BIGGEST_FANS = 'WHO_ARE_YOUR_BIGGEST_FANS';
  static const EXTRA_HIDDEN_INFORMATION = 'EXTRA_HIDDEN_INFORMATION';

  const factory PremiumSubtitleText({
    String title,
    String subtitle,
  }) = _PremiumSubtitleText;

  factory PremiumSubtitleText.byKey({
    BuildContext context,
    String premiumSubtitleKey,
  }) {
    final S trans = S.of(context);

    switch (premiumSubtitleKey) {
      case UNCOVER_ANYONE_ACTIVITIES:
        return PremiumSubtitleText(
          title: trans.uncoverAnyoneActivities,
          subtitle: trans.upgradePrimiumAndFindOutWhatPeopleUpTo,
        );
      case WHO_REALLY_LIKES_YOUR_STORY:
        return PremiumSubtitleText(
          title: trans.whoReallyLikesYourStory,
          subtitle: trans.discoverWhoSpendsTheMostTimeLooking,
        );
      case SEE_SECRET_ADMIRERS:
        return PremiumSubtitleText(
          title: trans.seeSecretAdmirers,
          subtitle: trans.findWhoLooksAtYourStories,
        );
      case WHO_ARE_YOUR_BIGGEST_FANS:
        return PremiumSubtitleText(
          title: trans.whoAreYourBiggestFans,
          subtitle: trans.seeWhoSeenStoriesTheMostAndLeast,
        );
      case EXTRA_HIDDEN_INFORMATION:
        return PremiumSubtitleText(
          title: trans.extraHiddenInformation,
          subtitle: trans.discoverWhoBlockedYouAndMore,
        );
      default:
        throw UnimplementedError();
    }
  }
}
