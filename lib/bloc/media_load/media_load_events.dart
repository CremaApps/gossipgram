import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class MediaLoadEvent extends Equatable {
  const MediaLoadEvent();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class MediaLoadEventStart extends MediaLoadEvent {
  final int mediaId;

  const MediaLoadEventStart({
    @required this.mediaId,
  });
}