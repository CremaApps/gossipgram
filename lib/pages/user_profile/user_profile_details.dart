part of 'user_profile_page.dart';

class _UserProfileDetails extends StatelessWidget {
  final Profile profile;
  final bool printStatisticsBox;

  const _UserProfileDetails(
      {@required this.profile, this.printStatisticsBox = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: _rowPainting(context),
    );
  }

  List<Widget> _rowPainting(BuildContext context) {
    List<Widget> rows = [];

    rows.addAll([
      TopBar(
        loading: false,
        userName: profile.bio,
        profilePicUrl: profile.profilePicUrl,
        fullName: profile.fullName,
        followers: profile.followers,
        following: profile.following,
        clicableProfile: false,
        dataLoadBloc: null,
      ),
      Container(
        margin: EdgeInsetsDirectional.only(top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _followersFollowingRow(
              context,
              text: S.of(context).followers,
              value: IntNumberSymbolConverter(profile.followers),
            ),
            _followersFollowingRow(
              context,
              text: S.of(context).following,
              value: IntNumberSymbolConverter(profile.following),
            ),
          ],
        ),
      ),
    ]);

    if (printStatisticsBox) {
      rows.add(AnalyticsRow(
          profile.averageLikes, profile.engagementRate, profile.followerRatio));
    }

    return rows;
  }

  Widget _profilePicture(BuildContext context, Profile profile) {
    return Container(
      margin: EdgeInsetsDirectional.only(
        start: MediaQuery.of(context).size.width / 25,
        end: MediaQuery.of(context).size.width / 25,
      ),
      height: 70,
      width: 70,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
        image: DecorationImage(
          image: NetworkImage(profile.profilePicUrl),
        ),
        border: Border.all(
          color: Colors.white,
          width: 2.0,
        ),
      ),
    );
  }

  Widget _profileBio(BuildContext context, Profile profile) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.4,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              profile.fullName,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: AppTheme.neutrals['900'],
                fontWeight: FontWeight.w700,
                fontFamily: "Poppins",
                fontStyle: FontStyle.normal,
                fontSize: 18.0,
              ),
            ),
          ),
          Container(
            height: 50,
            child: Text(
              profile.bio,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: AppTheme.neutrals['900'],
                  fontWeight: FontWeight.w400,
                  fontFamily: "Poppins",
                  height: 1.5,
                  fontStyle: FontStyle.normal,
                  fontSize: 12.0),
            ),
          ),
        ],
      ),
    );
  }

  Row _followersFollowingRow(
    BuildContext context, {
    @required String text,
    @required String value,
  }) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsetsDirectional.only(
            start: MediaQuery.of(context).size.width / 25,
          ),
          child: Text(value, style: appTextTheme.subtitle1),
        ),
        Container(
          margin: EdgeInsetsDirectional.only(
            start: MediaQuery.of(context).size.width / 50,
          ),
          child: Text(
            text,
            style: appTextTheme.subtitle2,
          ),
        ),
      ],
    );
  }

  Container _statisticsBox({@required String text, @required String value}) {
    return Container(
      width: 110,
      height: 92,
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            text,
            style: appTextTheme.subtitle2,
          ),
          Text(
            value,
            style: appTextTheme.subtitle1,
          )
        ],
      ),
      margin: const EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(8)),
        boxShadow: [
          const BoxShadow(
            color: Color(0x5a1e2021),
            offset: Offset(0, 2),
            blurRadius: 6,
            spreadRadius: 0,
          )
        ],
        color: AppTheme.neutrals['50'],
      ),
    );
  }
}
