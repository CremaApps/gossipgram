// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'seen_your_story_user_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SeenYourStoryUserItemTearOff {
  const _$SeenYourStoryUserItemTearOff();

// ignore: unused_element
  _SeenYourStoryUserItem call(
      {int userId,
      String name,
      String userName,
      String imageUrl,
      int storyId}) {
    return _SeenYourStoryUserItem(
      userId: userId,
      name: name,
      userName: userName,
      imageUrl: imageUrl,
      storyId: storyId,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SeenYourStoryUserItem = _$SeenYourStoryUserItemTearOff();

/// @nodoc
mixin _$SeenYourStoryUserItem {
  int get userId;
  String get name;
  String get userName;
  String get imageUrl;
  int get storyId;

  $SeenYourStoryUserItemCopyWith<SeenYourStoryUserItem> get copyWith;
}

/// @nodoc
abstract class $SeenYourStoryUserItemCopyWith<$Res> {
  factory $SeenYourStoryUserItemCopyWith(SeenYourStoryUserItem value,
          $Res Function(SeenYourStoryUserItem) then) =
      _$SeenYourStoryUserItemCopyWithImpl<$Res>;
  $Res call(
      {int userId, String name, String userName, String imageUrl, int storyId});
}

/// @nodoc
class _$SeenYourStoryUserItemCopyWithImpl<$Res>
    implements $SeenYourStoryUserItemCopyWith<$Res> {
  _$SeenYourStoryUserItemCopyWithImpl(this._value, this._then);

  final SeenYourStoryUserItem _value;
  // ignore: unused_field
  final $Res Function(SeenYourStoryUserItem) _then;

  @override
  $Res call({
    Object userId = freezed,
    Object name = freezed,
    Object userName = freezed,
    Object imageUrl = freezed,
    Object storyId = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as int,
      name: name == freezed ? _value.name : name as String,
      userName: userName == freezed ? _value.userName : userName as String,
      imageUrl: imageUrl == freezed ? _value.imageUrl : imageUrl as String,
      storyId: storyId == freezed ? _value.storyId : storyId as int,
    ));
  }
}

/// @nodoc
abstract class _$SeenYourStoryUserItemCopyWith<$Res>
    implements $SeenYourStoryUserItemCopyWith<$Res> {
  factory _$SeenYourStoryUserItemCopyWith(_SeenYourStoryUserItem value,
          $Res Function(_SeenYourStoryUserItem) then) =
      __$SeenYourStoryUserItemCopyWithImpl<$Res>;
  @override
  $Res call(
      {int userId, String name, String userName, String imageUrl, int storyId});
}

/// @nodoc
class __$SeenYourStoryUserItemCopyWithImpl<$Res>
    extends _$SeenYourStoryUserItemCopyWithImpl<$Res>
    implements _$SeenYourStoryUserItemCopyWith<$Res> {
  __$SeenYourStoryUserItemCopyWithImpl(_SeenYourStoryUserItem _value,
      $Res Function(_SeenYourStoryUserItem) _then)
      : super(_value, (v) => _then(v as _SeenYourStoryUserItem));

  @override
  _SeenYourStoryUserItem get _value => super._value as _SeenYourStoryUserItem;

  @override
  $Res call({
    Object userId = freezed,
    Object name = freezed,
    Object userName = freezed,
    Object imageUrl = freezed,
    Object storyId = freezed,
  }) {
    return _then(_SeenYourStoryUserItem(
      userId: userId == freezed ? _value.userId : userId as int,
      name: name == freezed ? _value.name : name as String,
      userName: userName == freezed ? _value.userName : userName as String,
      imageUrl: imageUrl == freezed ? _value.imageUrl : imageUrl as String,
      storyId: storyId == freezed ? _value.storyId : storyId as int,
    ));
  }
}

/// @nodoc
class _$_SeenYourStoryUserItem implements _SeenYourStoryUserItem {
  const _$_SeenYourStoryUserItem(
      {this.userId, this.name, this.userName, this.imageUrl, this.storyId});

  @override
  final int userId;
  @override
  final String name;
  @override
  final String userName;
  @override
  final String imageUrl;
  @override
  final int storyId;

  @override
  String toString() {
    return 'SeenYourStoryUserItem(userId: $userId, name: $name, userName: $userName, imageUrl: $imageUrl, storyId: $storyId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SeenYourStoryUserItem &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.userName, userName) ||
                const DeepCollectionEquality()
                    .equals(other.userName, userName)) &&
            (identical(other.imageUrl, imageUrl) ||
                const DeepCollectionEquality()
                    .equals(other.imageUrl, imageUrl)) &&
            (identical(other.storyId, storyId) ||
                const DeepCollectionEquality().equals(other.storyId, storyId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(userName) ^
      const DeepCollectionEquality().hash(imageUrl) ^
      const DeepCollectionEquality().hash(storyId);

  @override
  _$SeenYourStoryUserItemCopyWith<_SeenYourStoryUserItem> get copyWith =>
      __$SeenYourStoryUserItemCopyWithImpl<_SeenYourStoryUserItem>(
          this, _$identity);
}

abstract class _SeenYourStoryUserItem implements SeenYourStoryUserItem {
  const factory _SeenYourStoryUserItem(
      {int userId,
      String name,
      String userName,
      String imageUrl,
      int storyId}) = _$_SeenYourStoryUserItem;

  @override
  int get userId;
  @override
  String get name;
  @override
  String get userName;
  @override
  String get imageUrl;
  @override
  int get storyId;
  @override
  _$SeenYourStoryUserItemCopyWith<_SeenYourStoryUserItem> get copyWith;
}
