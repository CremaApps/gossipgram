import 'package:intl/intl.dart';

class DateUtils {
  static String getFormattedDate(String unfformattedDate) {
    DateTime dateTime = DateTime.parse(unfformattedDate).toLocal();
    DateFormat df = new DateFormat("dd/MM/yyyy").add_Hm();
    return df.format(dateTime);
  }

  static String getFormattedDateDMY(String dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    DateTime dateTime = dateFormat.parse(dateString);

    return "${dateTime.day} - ${dateTime.month} - ${dateTime.year}";
  }

  static String getFormattedDateDMYwithLetters(String dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateTime dateTime = DateTime.parse(dateString).toLocal();

    return DateFormat.yMMMd().format(dateTime);
  }

  static String getFormattedTime(String dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateTime dateTime = DateTime.parse(dateString).toLocal();
    String formattedTime = DateFormat.Hm().format(dateTime);

    return formattedTime;
  }

  static String getDaysBetweenDateAndToday(String dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    DateTime dateTime = dateFormat.parse(dateString);
    DateTime today = DateTime.now();
    int difference = dateTime.difference(today).inDays;

    return difference == 0
        ? "Ahora"
        : "${difference * -1} días"; //multipli *-1 to convert to positive
  }

  static DateTime getFormattedDateTimeDMY(String dateString) {
    if (dateString == null || dateString.isEmpty) {
      return DateTime.now();
    }
    DateFormat dateFormat = DateFormat('dd-MM-yyyy');
    return dateFormat.parse(dateString);
  }
}
