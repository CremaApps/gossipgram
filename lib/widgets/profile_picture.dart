import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ProfilePicture extends StatelessWidget {
  final String profilePicUrl;

  const ProfilePicture({@required this.profilePicUrl});

  @override
  Widget build(BuildContext context) {
    try {
      return CachedNetworkImage(
        imageUrl: profilePicUrl,
        imageBuilder: (context, imageProvider) {
          return CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 25,
              backgroundImage: imageProvider);
        },
        placeholder: (context, url) => Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Container(
            width: MediaQuery.of(context).size.width / 2,
            height: 20.0,
            color: Colors.white,
          ),
        ),
        errorWidget: (context, url, error) =>
            Image.asset('./assets/images/image_profile_placeholder.jpg'),
      );
    } catch (e) {
      return Image.asset('./assets/images/image_profile_placeholder.jpg');
    }
  }
}
