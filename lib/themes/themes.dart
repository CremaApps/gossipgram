import 'dart:convert';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

final TextTheme textThemeLight = TextTheme(
  headline1: GoogleFonts.lato(
      fontSize: 14,
      fontWeight: FontWeight.w300,
      letterSpacing: -1.5,
      color: Colors.black),
  headline2: GoogleFonts.lato(
      fontSize: 63, fontWeight: FontWeight.w300, letterSpacing: -0.5),
  headline3: GoogleFonts.lato(fontSize: 50, fontWeight: FontWeight.w400),
  headline4: GoogleFonts.lato(
      fontSize: 36, fontWeight: FontWeight.w400, letterSpacing: 0.25),
  headline5: GoogleFonts.lato(fontSize: 25, fontWeight: FontWeight.w400),
  headline6: GoogleFonts.lato(
      fontSize: 21, fontWeight: FontWeight.w500, letterSpacing: 0.15),
  subtitle1: GoogleFonts.lato(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.15,
      color: Color(0xFF000000)),
  subtitle2: GoogleFonts.lato(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: Color(0xFF283133),
    letterSpacing: 0.1,
  ),
  bodyText1: GoogleFonts.lato(
      fontSize: 14,
      color: Color(0xFF636D77),
      fontWeight: FontWeight.w400,
      letterSpacing: 0.5),
  bodyText2: GoogleFonts.lato(
      fontSize: 12, color: Color(0xFF636D77), letterSpacing: 0.25),
  button: GoogleFonts.lato(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.5,
      color: appColorScheme.secondary),
  caption: GoogleFonts.lato(
      fontSize: 12,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.4,
      color: Color(0xFF636D77)),
  overline: GoogleFonts.lato(
      fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
);

final TextTheme textThemeDark = TextTheme(
  headline1: GoogleFonts.lato(
      fontSize: 14,
      fontWeight: FontWeight.w600,
      letterSpacing: 0.15,
      color: Colors.white),
  headline2: GoogleFonts.lato(
      fontSize: 63,
      fontWeight: FontWeight.w300,
      letterSpacing: -0.5,
      color: Color(0xFFF2F5FA)),
  headline3: GoogleFonts.lato(
      fontSize: 50, fontWeight: FontWeight.w400, color: Color(0xFFF2F5FA)),
  headline4: GoogleFonts.lato(
      fontSize: 36,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      color: Color(0xFFF2F5FA)),
  headline5: GoogleFonts.lato(
      fontSize: 25, fontWeight: FontWeight.w400, color: Color(0xFFF2F5FA)),
  headline6: GoogleFonts.lato(
      fontSize: 21,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.15,
      color: Color(0xFFF2F5FA)),
  subtitle1: GoogleFonts.lato(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.15,
      color: Color(0xFFF2F5FA)),
  subtitle2: GoogleFonts.lato(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: Color(0xFFF2F5FA),
    letterSpacing: 0.1,
  ),
  bodyText1: GoogleFonts.lato(
      fontSize: 14,
      color: Color(0xFF829AC1),
      fontWeight: FontWeight.w400,
      letterSpacing: 0.3),
  bodyText2: GoogleFonts.lato(
      fontSize: 12, color: Color(0xFF829AC1), letterSpacing: 0.25),
  button: GoogleFonts.lato(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.5,
      color: appColorScheme.secondary),
  caption: GoogleFonts.lato(
      fontSize: 12,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.4,
      color: Color(0xFF636D77)),
  overline: GoogleFonts.lato(
      fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
);

TextTheme appTextTheme =
    getThemeModeAsInt() == 0 ? textThemeLight : textThemeDark;

final ColorScheme colorSchemeLight = ColorScheme(
    primary: Color(0xFFFFFFFF),
    primaryVariant: Color(0xFFFFFFFF),
    secondary: Color(0xFF0080FF),
    secondaryVariant: Color(0xFF0080FF),
    surface: Color(0xFFF5F5F5),
    background: Color(0xFFFFFFFF),
    error: Color(0xFFB00020),
    onPrimary: Color(0xFF636D77),
    onSecondary: Color(0xFFFFFFFF),
    onSurface: Colors.grey[400],
    onBackground: Color(0xFF000000),
    onError: Color(0xFFE30425),
    brightness: Brightness.light);

final ColorScheme colorSchemeDark = ColorScheme(
    primary: Color(0xFF222F62),
    primaryVariant: Color(0xFF222F62),
    secondary: Color(0xFF0080FF),
    secondaryVariant: Color(0xFF0080FF),
    surface: Color(0xFF1B254C),
    background: Color(0xFF1B254C),
    error: Color(0xFFB00020),
    onPrimary: Color(0xFF829AC1),
    onSecondary: Color(0xFFF2F5FA),
    onSurface: Color(0xFF1B254C),
    onBackground: Color(0xFF829AC1),
    onError: Color(0xFFE30425),
    brightness: Brightness.light);

void initializeThemeValuesFirstTime() async {
  if (GetIt.instance
          .get<SharedPreferences>()
          .getString(AdaptiveTheme.prefKey) ==
      null) {
    GetIt.instance
        .get<SharedPreferences>()
        .setString(AdaptiveTheme.prefKey, """{"theme_mode":0}""");
    appTextTheme = textThemeLight;
    appColorScheme = colorSchemeLight;
  }
}

void changeTextTheme() {
  appTextTheme = getThemeModeAsInt() == 1 ? textThemeLight : textThemeDark;
}

void changeColorScheme() {
  appColorScheme =
      getThemeModeAsInt() == 1 ? colorSchemeLight : colorSchemeDark;
}

int getThemeModeAsInt() {
  int themeMode = 0;
  try {
    themeMode = json.decode(GetIt.instance
        .get<SharedPreferences>()
        .getString(AdaptiveTheme.prefKey))["theme_mode"];
  } catch (e) {
    print(e);
  }
  return themeMode;
}

ColorScheme appColorScheme =
    getThemeModeAsInt() == 0 ? colorSchemeLight : colorSchemeDark;
