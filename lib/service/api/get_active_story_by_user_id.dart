import 'package:flutter/foundation.dart';
import 'package:GossipGram/model/view_data/story_popup.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

@lazySingleton
class GetActiveStoryByUserId {
  final InstagramPrivateApi _instagramPrivateApi =
      InstagramPrivateApi.instance();

  Future<List<StoryPopup>> execute({
    @required String userId,
  }) async {
    var userStory = await this._instagramPrivateApi.feed.reelsMedia([userId]);

    return userStory.response.reels.first.items
        .map((e) => StoryPopup(
              storyAsset: e.videoVersions?.first?.url ??
                  e.imageVersions2.candidates.first.url,
              storyType: e.videoVersions != null
                  ? StoryPopup.VIDEO_TYPE
                  : StoryPopup.IMAGE_TYPE,
            ))
        .toList();
  }
}
