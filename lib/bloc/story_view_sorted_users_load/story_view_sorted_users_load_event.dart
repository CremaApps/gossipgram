part of 'story_view_sorted_users_load_bloc.dart';

abstract class StoryViewSortedUsersLoadEvent extends Equatable {
  const StoryViewSortedUsersLoadEvent();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class StoryViewSortedUsersLoadEventStart extends StoryViewSortedUsersLoadEvent {}
