import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/avatar_pic.dart';
import 'package:injectable/injectable.dart';

@singleton
class AvatarPicRepository {
  final DbProvider _dbProvider;

  AvatarPicRepository(this._dbProvider);

  Future<AvatarPic> getAvatarPic({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('AvatarPic', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? AvatarPic.fromMap(res.first) : null;
  }

  Future<int> queryCountAvatarPics({
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
        'FROM AvatarPic ';

    if (createdAtGreaterThan != null) {
      query += 'WHERE createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += createdAtGreaterThan != null ? 'AND ' : 'WHERE ';
      query += 'createdAt < ' + createdAtLesserThan.toString();
    }

    var dbPicCount = await db.rawQuery(query);

    return dbPicCount.first.values.first;
  }

  Future<int> upsertAvatarPic(AvatarPic pic) async {
    final db = await _dbProvider.database;
    var dbPic = await db.query(
      'AvatarPic',
      where: 'id = ?',
      whereArgs: [pic.id],
    );
    if (dbPic.isNotEmpty) {
      return db.update(
        'AvatarPic',
        pic.toMap(),
        where: 'id = ?',
        whereArgs: [pic.id],
      );
    } else {
      return await db.insert('AvatarPic', pic.toMap());
    }
  }

  Future<int> deleteAvatarPic(AvatarPic pic) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'AvatarPic',
      where: 'id = ?',
      whereArgs: [pic.id],
    );
  }
}
