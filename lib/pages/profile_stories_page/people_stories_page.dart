import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import '../../bloc/people_stories/people_stories_bloc.dart';
import '../../generated/l10n.dart';
import '../../model/view_data/people_stories.dart';
import '../../model/view_data/people_story.dart';
import '../../themes/app_theme_default.dart';
import '../../themes/themes.dart';
import '../../widgets/centered_loader.dart';
import '../../widgets/story_popup.dart';

part 'people_stories_list.dart';

class PeopleStoriesPage extends StatefulWidget {
  @override
  _PeopleStoriesPageState createState() => _PeopleStoriesPageState();
}

class _PeopleStoriesPageState extends State<PeopleStoriesPage> {
  PeopleStoriesBloc _peopleStoriesBloc;

  @override
  void initState() {
    super.initState();
    _peopleStoriesBloc = GetIt.instance.get<PeopleStoriesBloc>();
    _peopleStoriesBloc.add(PeopleStoriesStartLoadEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: false,
          elevation: 0.0,
          title:
              Text(S.of(context).peopleStories, style: appTextTheme.subtitle2)),
      body: BlocBuilder(
        cubit: _peopleStoriesBloc,
        builder: (context, state) {
          if (state is PeopleStoriesLoading) {
            return const CenteredLoader();
          } else if (state is PeopleStoriesLoadData) {
            return _StoriesList(
              peopleStories: state.peopleStories,
              finished: state is PeopleStoriesDataFullyLoaded,
            );
          } else {
            return Center(
              child: Text(
                S.of(context).genericError,
                style: appTextTheme.subtitle2,
              ),
            );
          }
        },
      ),
    );
  }
}
