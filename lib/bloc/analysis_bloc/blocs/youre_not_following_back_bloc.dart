import 'dart:async';

import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:injectable/injectable.dart';

@injectable
class YoureNotFollowingBackBloc extends IAnalysisBloc {
  final FollowerUserRepository _followerUserRepository;
  final UserRepository _userRepository;

  YoureNotFollowingBackBloc(
    this._followerUserRepository,
    this._userRepository,
  ) : super();

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      var notFollowedYouBack = await _followerUserRepository.queryFollowerUsers(
          isValid: true, notFollowed: true);

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          notFollowedYouBack.map((e) => _userRepository.getUser(id: e.id)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            youFollow: element.youFollow,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
