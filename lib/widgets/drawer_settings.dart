import 'dart:convert';
import 'dart:io';

import 'package:GossipGram/model/view_data/dashboard.dart';
import 'package:GossipGram/pages/my_profile_page.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:GossipGram/widgets/premium_popup.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:alert/alert.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_events.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/pages/debug_page.dart';
import 'package:GossipGram/pages/support_page.dart';
import 'package:GossipGram/service/logout_service.dart';
import 'package:GossipGram/service/navigator_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:GossipGram/service/subscription/subscription_service.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'loading_overlay.dart';

class DrawerSettings extends StatefulWidget {
  DrawerSettings(this.dashboard, this.callback);
  final Dashboard dashboard;
  final Function callback;

  @override
  _DrawerSettingsState createState() => _DrawerSettingsState();
}

class _DrawerSettingsState extends State<DrawerSettings> {
  final LogoutService _logoutService = GetIt.instance.get<LogoutService>();

  final AppConfig _appConfig = GetIt.I.get<AppConfig>();

  bool restoringPurchases = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: appColorScheme.primary,
      margin:
          EdgeInsets.only(left: MediaQuery.of(context).size.width * 30 / 100),
      alignment: Alignment.centerRight,
      padding: EdgeInsetsDirectional.only(
          top: MediaQuery.of(context).size.height * 8 / 100),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyProfilePage(
                        dashboard: widget.dashboard,
                      ),
                    ),
                  );
                },
                child: _drawerItem(
                  icon: Icon(
                    Icons.person,
                    color: appColorScheme.onBackground,
                  ),
                  label: S.of(context).profile,
                ),
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
              ListTile(
                title: Text(
                  S.of(context).nightMode,
                  style: appTextTheme.subtitle2,
                ),
                leading: Transform.scale(
                  scale: 1.5,
                  child: ImageIcon(
                    AssetImage('assets/images/icon-nightmode.png'),
                    color: appColorScheme.onBackground,
                  ),
                ),
                onTap: () {
                  AdaptiveTheme.of(context).toggleThemeMode();
                  changeTextTheme();
                  changeColorScheme();
                },
                trailing: Switch.adaptive(
                    activeColor: appColorScheme.secondary,
                    value: getThemeModeAsInt() == 1,
                    onChanged: (value) {
                      AdaptiveTheme.of(context).toggleThemeMode();
                      changeTextTheme();
                      changeColorScheme();
                    }),
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
              InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => SupportPage(),
                  ),
                ),
                child: _drawerItem(
                  icon: Icon(
                    Icons.question_answer,
                    color: appColorScheme.onBackground,
                  ),
                  label: S.of(context).support,
                ),
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
              ListTile(
                leading: Container(
                  padding: EdgeInsets.all(2.5),
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                    gradient: LinearGradient(
                      colors: [Color(0xFFFDC52A), Color(0xFFFF9714)],
                    ),
                  ),
                  child: Icon(
                    Icons.star,
                    size: 15,
                    color: Colors.white,
                  ),
                ),
                title: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Gossipgram",
                        style: appTextTheme.bodyText1,
                      ),
                      WidgetSpan(
                        child: SizedBox(
                          width: 4,
                        ),
                      ),
                      TextSpan(
                        text: "Pro",
                        style: appTextTheme.subtitle2.copyWith(fontSize: 16),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  if (Platform.isIOS) {
                    showCupertinoModalBottomSheet(
                      context: context,
                      expand: false,
                      builder: (context) {
                        return PremiumPopup(
                          callback: () {
                            widget.callback();
                          },
                        );
                      },
                    );
                  } else {
                    showMaterialModalBottomSheet(
                      context: context,
                      expand: false,
                      builder: (context) {
                        return PremiumPopup(
                          callback: () {
                            widget.callback();
                          },
                        );
                      },
                    );
                  }
                },
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
              InkWell(
                onTap: () async {
                  try {
                    setState(() {
                      restoringPurchases = true;
                    });
                    bool isPaid = await GetIt.I
                        .get<SubscriptionService>()
                        .restorePurchases();
                    await GetIt.I.get<SubscriptionService>().restorePurchases();
                    setState(() {
                      restoringPurchases = false;
                    });
                    if (isPaid) {
                      Alert(
                        message: S.of(context).purchasesRestoredSuccess,
                      ).show();
                      Navigator.of(context).pop();
                      GetIt.I
                          .get<DashboardBloc>()
                          .add(DashboardReloadPayment());
                    } else {
                      Alert(
                        message: S.of(context).purchasesRestoredFailed,
                      ).show();
                    }
                  } catch (e) {
                    Alert(
                      message: S.of(context).failedPurchaseRestore,
                    ).show();
                    Navigator.of(context).pop();
                  }
                },
                child: restoringPurchases
                    ? ListTile(leading: CircularProgressIndicator())
                    : _drawerItem(
                        icon: Icon(
                          Icons.replay,
                          color: appColorScheme.onBackground,
                        ),
                        label: S.of(context).restorePurchases,
                      ),
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
              InkWell(
                onTap: () {
                  launch(
                    _appConfig.getString(AppConfig.TERMS_OF_USE_URL),
                    forceWebView: true,
                  );
                },
                child: _drawerItem(
                  icon: Icon(
                    Icons.description,
                    color: appColorScheme.onBackground,
                  ),
                  label: S.of(context).termsOfUse,
                ),
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
              InkWell(
                onTap: () {
                  launch(
                    _appConfig.getString(AppConfig.PRIVACY_POLICY_URL),
                    forceWebView: true,
                  );
                },
                child: _drawerItem(
                  icon: Icon(
                    Icons.lock,
                    color: appColorScheme.onBackground,
                  ),
                  label: S.of(context).privacyPolicy,
                ),
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
              Builder(
                builder: (context) {
                  AppConfig appConfig = GetIt.I.get<AppConfig>();

                  List<String> debugUsers =
                      appConfig.getListString(AppConfig.DEBUG_USERS);

                  if (debugUsers.contains(
                    InstagramPrivateApi.instance().myUserId.toString(),
                  )) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) => DebugPage(),
                          ),
                        );
                      },
                      child: _drawerItem(
                        icon: Icon(
                          Icons.bug_report,
                          color: Colors.black,
                        ),
                        label: "Debug Settings",
                      ),
                    );
                  }

                  return Container();
                },
              ),
              ListTile(
                title: Text("Log out", style: appTextTheme.subtitle2),
                leading: Transform.scale(
                  scale: 1.5,
                  child: ImageIcon(
                    AssetImage(
                      'assets/images/icon-logout.png',
                    ),
                    color: appColorScheme.onBackground,
                  ),
                ),
                onTap: () async {
                  await _logoutService.execute();
                  await GetIt.I.get<NavigatorService>().logoutUser();
                },
              ),
              Divider(
                thickness: 0.5,
                height: 0.5,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _drawerItem({
    @required Icon icon,
    @required String label,
  }) {
    return ListTile(
      title: Text(label, style: appTextTheme.subtitle2),
      leading: icon,
    );
  }
}
