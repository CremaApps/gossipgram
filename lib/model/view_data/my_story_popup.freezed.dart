// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'my_story_popup.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$MyStoryPopupTearOff {
  const _$MyStoryPopupTearOff();

// ignore: unused_element
  _MyStoryPopup call(
      {@required String fullName,
      @required String profilePic,
      @required String storyAsset,
      @required String storyType,
      @required List<String> userSeenStories,
      @required int seenBy}) {
    return _MyStoryPopup(
      fullName: fullName,
      profilePic: profilePic,
      storyAsset: storyAsset,
      storyType: storyType,
      userSeenStories: userSeenStories,
      seenBy: seenBy,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $MyStoryPopup = _$MyStoryPopupTearOff();

/// @nodoc
mixin _$MyStoryPopup {
  String get fullName;
  String get profilePic;
  String get storyAsset;
  String get storyType;
  List<String> get userSeenStories;
  int get seenBy;

  $MyStoryPopupCopyWith<MyStoryPopup> get copyWith;
}

/// @nodoc
abstract class $MyStoryPopupCopyWith<$Res> {
  factory $MyStoryPopupCopyWith(
          MyStoryPopup value, $Res Function(MyStoryPopup) then) =
      _$MyStoryPopupCopyWithImpl<$Res>;
  $Res call(
      {String fullName,
      String profilePic,
      String storyAsset,
      String storyType,
      List<String> userSeenStories,
      int seenBy});
}

/// @nodoc
class _$MyStoryPopupCopyWithImpl<$Res> implements $MyStoryPopupCopyWith<$Res> {
  _$MyStoryPopupCopyWithImpl(this._value, this._then);

  final MyStoryPopup _value;
  // ignore: unused_field
  final $Res Function(MyStoryPopup) _then;

  @override
  $Res call({
    Object fullName = freezed,
    Object profilePic = freezed,
    Object storyAsset = freezed,
    Object storyType = freezed,
    Object userSeenStories = freezed,
    Object seenBy = freezed,
  }) {
    return _then(_value.copyWith(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePic:
          profilePic == freezed ? _value.profilePic : profilePic as String,
      storyAsset:
          storyAsset == freezed ? _value.storyAsset : storyAsset as String,
      storyType: storyType == freezed ? _value.storyType : storyType as String,
      userSeenStories: userSeenStories == freezed
          ? _value.userSeenStories
          : userSeenStories as List<String>,
      seenBy: seenBy == freezed ? _value.seenBy : seenBy as int,
    ));
  }
}

/// @nodoc
abstract class _$MyStoryPopupCopyWith<$Res>
    implements $MyStoryPopupCopyWith<$Res> {
  factory _$MyStoryPopupCopyWith(
          _MyStoryPopup value, $Res Function(_MyStoryPopup) then) =
      __$MyStoryPopupCopyWithImpl<$Res>;
  @override
  $Res call(
      {String fullName,
      String profilePic,
      String storyAsset,
      String storyType,
      List<String> userSeenStories,
      int seenBy});
}

/// @nodoc
class __$MyStoryPopupCopyWithImpl<$Res> extends _$MyStoryPopupCopyWithImpl<$Res>
    implements _$MyStoryPopupCopyWith<$Res> {
  __$MyStoryPopupCopyWithImpl(
      _MyStoryPopup _value, $Res Function(_MyStoryPopup) _then)
      : super(_value, (v) => _then(v as _MyStoryPopup));

  @override
  _MyStoryPopup get _value => super._value as _MyStoryPopup;

  @override
  $Res call({
    Object fullName = freezed,
    Object profilePic = freezed,
    Object storyAsset = freezed,
    Object storyType = freezed,
    Object userSeenStories = freezed,
    Object seenBy = freezed,
  }) {
    return _then(_MyStoryPopup(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePic:
          profilePic == freezed ? _value.profilePic : profilePic as String,
      storyAsset:
          storyAsset == freezed ? _value.storyAsset : storyAsset as String,
      storyType: storyType == freezed ? _value.storyType : storyType as String,
      userSeenStories: userSeenStories == freezed
          ? _value.userSeenStories
          : userSeenStories as List<String>,
      seenBy: seenBy == freezed ? _value.seenBy : seenBy as int,
    ));
  }
}

/// @nodoc
class _$_MyStoryPopup with DiagnosticableTreeMixin implements _MyStoryPopup {
  const _$_MyStoryPopup(
      {@required this.fullName,
      @required this.profilePic,
      @required this.storyAsset,
      @required this.storyType,
      @required this.userSeenStories,
      @required this.seenBy})
      : assert(fullName != null),
        assert(profilePic != null),
        assert(storyAsset != null),
        assert(storyType != null),
        assert(userSeenStories != null),
        assert(seenBy != null);

  @override
  final String fullName;
  @override
  final String profilePic;
  @override
  final String storyAsset;
  @override
  final String storyType;
  @override
  final List<String> userSeenStories;
  @override
  final int seenBy;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'MyStoryPopup(fullName: $fullName, profilePic: $profilePic, storyAsset: $storyAsset, storyType: $storyType, userSeenStories: $userSeenStories, seenBy: $seenBy)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'MyStoryPopup'))
      ..add(DiagnosticsProperty('fullName', fullName))
      ..add(DiagnosticsProperty('profilePic', profilePic))
      ..add(DiagnosticsProperty('storyAsset', storyAsset))
      ..add(DiagnosticsProperty('storyType', storyType))
      ..add(DiagnosticsProperty('userSeenStories', userSeenStories))
      ..add(DiagnosticsProperty('seenBy', seenBy));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MyStoryPopup &&
            (identical(other.fullName, fullName) ||
                const DeepCollectionEquality()
                    .equals(other.fullName, fullName)) &&
            (identical(other.profilePic, profilePic) ||
                const DeepCollectionEquality()
                    .equals(other.profilePic, profilePic)) &&
            (identical(other.storyAsset, storyAsset) ||
                const DeepCollectionEquality()
                    .equals(other.storyAsset, storyAsset)) &&
            (identical(other.storyType, storyType) ||
                const DeepCollectionEquality()
                    .equals(other.storyType, storyType)) &&
            (identical(other.userSeenStories, userSeenStories) ||
                const DeepCollectionEquality()
                    .equals(other.userSeenStories, userSeenStories)) &&
            (identical(other.seenBy, seenBy) ||
                const DeepCollectionEquality().equals(other.seenBy, seenBy)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(fullName) ^
      const DeepCollectionEquality().hash(profilePic) ^
      const DeepCollectionEquality().hash(storyAsset) ^
      const DeepCollectionEquality().hash(storyType) ^
      const DeepCollectionEquality().hash(userSeenStories) ^
      const DeepCollectionEquality().hash(seenBy);

  @override
  _$MyStoryPopupCopyWith<_MyStoryPopup> get copyWith =>
      __$MyStoryPopupCopyWithImpl<_MyStoryPopup>(this, _$identity);
}

abstract class _MyStoryPopup implements MyStoryPopup {
  const factory _MyStoryPopup(
      {@required String fullName,
      @required String profilePic,
      @required String storyAsset,
      @required String storyType,
      @required List<String> userSeenStories,
      @required int seenBy}) = _$_MyStoryPopup;

  @override
  String get fullName;
  @override
  String get profilePic;
  @override
  String get storyAsset;
  @override
  String get storyType;
  @override
  List<String> get userSeenStories;
  @override
  int get seenBy;
  @override
  _$MyStoryPopupCopyWith<_MyStoryPopup> get copyWith;
}
