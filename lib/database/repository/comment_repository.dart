import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/comment.dart';
import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class CommentRepository {
  final DbProvider _dbProvider;

  CommentRepository(this._dbProvider);

  Future<Comment> getComment({
    @required int id,
  }) async {
    final db = await _dbProvider.database;
    var res = await db.query(
      'Comment',
      where: 'id = ?',
      whereArgs: [id],
    );
    return res.isNotEmpty ? Comment.fromMap(res.first) : null;
  }

  Future<List<Comment>> queryComments({
    String feedId,
    int id,
    int userId,
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
            'FROM Comment '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (id != null) query += 'AND id = ' + id.toString() + ' ';
    if (feedId != null) query += 'AND feedId = ' + feedId + ' ';
    if (userId != null) query += 'AND userId = ' + userId.toString() + ' ';

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var dbComments = await db.rawQuery(query);
    List<Comment> comments = dbComments.isNotEmpty
        ? dbComments.map((a) => Comment.fromMap(a)).toList()
        : [];
    return comments;
  }

  Future<int> queryCountComments({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM Comment '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var dbCommentsCount = await db.rawQuery(query);

    return dbCommentsCount.first.values.first;
  }

  Future<int> upsertComment({@required Comment comment}) async {
    final db = await _dbProvider.database;
    var dbComment = await db.query(
      'Comment',
      where: 'id = ?',
      whereArgs: [comment.id],
    );
    if (dbComment.isNotEmpty) {
      return db.update(
        'Comment',
        comment.toMap(),
        where: 'id = ?',
        whereArgs: [comment.id],
      );
    } else {
      return await db.insert('Comment', comment.toMap());
    }
  }

  Future<void> upsertComments({@required List<Comment> comments}) async {
    final db = await _dbProvider.database;
    Batch batch = db.batch();
    var dbComments = await db.query(
      'Comment',
    );

    Set<Comment> allComments = Set<Comment>.from(dbComments.isNotEmpty
        ? dbComments.map((a) => Comment.fromMap(a)).toList()
        : []);

    Set<Comment> toUpdateComments = Set<Comment>.from(comments)
        .where((element) => allComments.any((Comment e) => e.id == element.id))
        .toSet();
    Set<Comment> toInsertComments =
        Set<Comment>.from(comments).difference(toUpdateComments);

    for (Comment comment in toUpdateComments) {
      batch.update(
        'Comment',
        comment.toMap(),
        where: 'id = ?',
        whereArgs: [comment.id],
      );
    }

    for (Comment comment in toInsertComments) {
      batch.insert('Comment', comment.toMap());
    }
    await batch.commit(noResult: true);
  }

  Future<int> deleteComment({@required Comment comment}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'Comment',
      where: 'id = ?',
      whereArgs: [comment.id],
    );
  }

  Future<List<int>> getCommentIds({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
    int deletedAtGreaterThan,
    int deletedAtLesserThan,
    String feedId,
    int userId,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT id '
            'FROM Comment '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (feedId != null) {
      query += 'AND feedId = ' + feedId + ' ';
    }

    if (userId != null) {
      query += 'AND userId = ' + userId.toString() + ' ';
    }

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    if (deletedAtGreaterThan != null) {
      query += 'AND deletedAt > ' + deletedAtGreaterThan.toString() + ' ';
    }

    if (deletedAtLesserThan != null) {
      query += 'AND deletedAt < ' + deletedAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> commentIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int userId = a["id"];
            return userId;
          }).toList()
        : [];
    return commentIds;
  }

  Future<List<int>> getCommentUserIds({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
    int deletedAtGreaterThan,
    int deletedAtLesserThan,
    List<int> excluding,
    String feedId,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT DISTINCT(userId) '
            'FROM Comment '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (excluding != null && excluding.isNotEmpty) {
      String elements = excluding.join(', ');
      query += "AND userId NOT IN ($elements) ";
    }

    if (feedId != null) {
      query += 'AND feedId = ' + feedId + ' ';
    }

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    if (deletedAtGreaterThan != null) {
      query += 'AND deletedAt > ' + deletedAtGreaterThan.toString() + ' ';
    }

    if (deletedAtLesserThan != null) {
      query += 'AND deletedAt < ' + deletedAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> commentIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int userId = a["userId"];
            return userId;
          }).toList()
        : [];
    return commentIds;
  }

  Future<List<int>> getCommentUserIdsWithFeedDate({
    bool isValid = true,
    int feedCreationOriginalDateGreaterThan,
    int feedCreationOriginalDateLesserThan,
    List<int> excluding,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT DISTINCT(c.userId) AS id '
            'FROM Comment c, Feed f '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (excluding != null && excluding.isNotEmpty) {
      String elements = excluding.join(', ');
      query += "AND userId NOT IN ($elements) ";
    }

    query += 'AND c.feedId = f.id ';

    if (feedCreationOriginalDateGreaterThan != null) {
      query += 'AND f.originalCreatedAtDate > ' +
          (feedCreationOriginalDateGreaterThan / 1000).toString() +
          ' ';
    }

    if (feedCreationOriginalDateLesserThan != null) {
      query += 'AND f.originalCreatedAtDate < ' +
          (feedCreationOriginalDateLesserThan / 1000).toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> commentIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int userId = a["id"];
            return userId;
          }).toList()
        : [];
    return commentIds;
  }
}
