import 'package:flutter/material.dart';
import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user/followed_user.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@singleton
class FollowedUserLoadService extends UserLoadService {
  static const String SERVICE_NAME = "followed_user_load_service";

  FollowedUserRepository _followedUserRepository;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  FollowedUserLoadService(
    this._followedUserRepository,
    this._appConfig,
    this._localStorage,
    UserRepository userRepository,
  ) : super(userRepository);

  Future<void> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);
    int nowDate = firstLoad ? 0 : DateTime.now().millisecondsSinceEpoch;

    var followedsResponse = await instagramPrivateApi.friendship.following(
      instagramPrivateApi.myUserId,
    );
    DataLoadBloc.printProfiling("API followeds media step loaded", now);

    var followeds = followedsResponse.response.users;
    while (followedsResponse.hasNext()) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));
      followedsResponse = await followedsResponse.next();
      followeds.addAll(followedsResponse.response.users);
      DataLoadBloc.printProfiling("API followeds media step loaded", now);
    }
    DataLoadBloc.printProfiling("API followeds all loaded", now);

    List<int> activeFollowedUserIds =
        await _followedUserRepository.getFollowedUserIds(
      isValid: true,
    );

    for (int id in activeFollowedUserIds) {
      if (!followeds.any((user) => user.pk == id)) {
        await _followedUserRepository.upsertFollowedUser(
          fUser: FollowedUser(
            id: id,
            isValid: false,
            youFollow: true,
          ),
        );
      }
    }

    for (var followed in followeds) {
      Account.User user = Account.User(
        id: followed.pk,
        username: followed.username,
        fullName: followed.fullName,
        isPrivate: followed.isPrivate,
        isVerified: followed.isVerified,
        youFollow: true,
        profilePicUrl: followed.profilePicUrl,
        anonymousProfilePicture: followed.hasAnonymousProfilePicture,
      );

      await processUserIfNotAlreadyProcessed(user, processedUsers);

      FollowedUser dbUser =
          await _followedUserRepository.getFollowedUser(id: user.id);
      int onStateChange = dbUser != null && dbUser.isValid == true
          ? dbUser.onStateChange
          : nowDate;

      await _followedUserRepository.upsertFollowedUser(
          fUser: FollowedUser(
        id: user.id,
        isValid: true,
        onStateChange: onStateChange,
        youFollow: true,
      ));
    }
    DataLoadBloc.printProfiling("API followeds DB loaded", now);
  }
}
