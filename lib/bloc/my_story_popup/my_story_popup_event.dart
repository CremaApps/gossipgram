part of 'my_story_popup_bloc.dart';

abstract class MyStoryPopupEvent extends Equatable {
  const MyStoryPopupEvent();

  @override
  List<Object> get props => throw UnimplementedError();
}

class MyPopupStoryStartLoadEvent extends MyStoryPopupEvent {
  final int storyId;

  const MyPopupStoryStartLoadEvent({
    @required this.storyId,
  });
}