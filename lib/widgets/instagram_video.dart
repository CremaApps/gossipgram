import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:video_player/video_player.dart';

class InstagramVideo extends StatefulWidget {
  final String videoUrl;

  InstagramVideo(this.videoUrl);

  @override
  _InstagramVideoState createState() => _InstagramVideoState();
}

class _InstagramVideoState extends State<InstagramVideo> {
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    print(widget.videoUrl);
    _controller = VideoPlayerController.network(widget.videoUrl)
      ..initialize().then((_) {
        // reload with video
        setState(() {});
        _controller.play();
        _controller.setLooping(true);
      });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () => _controller.value.isPlaying
            ? _controller.pause()
            : _controller.play(),
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height:
              MediaQuery.of(context).size.width / _controller.value.aspectRatio,
          child: _controller.value.initialized
              ? VideoPlayer(_controller)
              : Lottie.asset(
                  'assets/animations/loader.json',
                  height: 50,
                ),
        ));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
