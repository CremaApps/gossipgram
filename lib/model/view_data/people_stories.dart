import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:GossipGram/model/view_data/people_story.dart';

part 'people_stories.freezed.dart';

@freezed
abstract class PeopleStories with _$PeopleStories {
  const factory PeopleStories({
    List<PeopleStory> stories,
  }) = _PeopleStories;
}
