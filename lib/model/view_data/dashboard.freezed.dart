// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'dashboard.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$DashboardTearOff {
  const _$DashboardTearOff();

// ignore: unused_element
  _Dashboard call(
      {String fullName,
      String username,
      String profilePicUrl,
      int followers,
      int following,
      int activeStories,
      int notFollowingBack,
      int notFollowerBack,
      int blockedUsers,
      int lostFollowers,
      int profileViews,
      int storyViews,
      int newFollowers,
      int nonFollowingAdmiredUsers,
      int secretAdmirers,
      int deletedLikesOrComments,
      int likedYouTheMost,
      int totalPosts}) {
    return _Dashboard(
      fullName: fullName,
      username: username,
      profilePicUrl: profilePicUrl,
      followers: followers,
      following: following,
      activeStories: activeStories,
      notFollowingBack: notFollowingBack,
      notFollowerBack: notFollowerBack,
      blockedUsers: blockedUsers,
      lostFollowers: lostFollowers,
      profileViews: profileViews,
      storyViews: storyViews,
      newFollowers: newFollowers,
      nonFollowingAdmiredUsers: nonFollowingAdmiredUsers,
      secretAdmirers: secretAdmirers,
      deletedLikesOrComments: deletedLikesOrComments,
      likedYouTheMost: likedYouTheMost,
      totalPosts: totalPosts,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Dashboard = _$DashboardTearOff();

/// @nodoc
mixin _$Dashboard {
  String get fullName;
  String get username;
  String get profilePicUrl;
  int get followers;
  int get following;
  int get activeStories;
  int get notFollowingBack;
  int get notFollowerBack;
  int get blockedUsers;
  int get lostFollowers;
  int get profileViews;
  int get storyViews;
  int get newFollowers;
  int get nonFollowingAdmiredUsers;
  int get secretAdmirers;
  int get deletedLikesOrComments;
  int get likedYouTheMost;
  int get totalPosts;

  $DashboardCopyWith<Dashboard> get copyWith;
}

/// @nodoc
abstract class $DashboardCopyWith<$Res> {
  factory $DashboardCopyWith(Dashboard value, $Res Function(Dashboard) then) =
      _$DashboardCopyWithImpl<$Res>;
  $Res call(
      {String fullName,
      String username,
      String profilePicUrl,
      int followers,
      int following,
      int activeStories,
      int notFollowingBack,
      int notFollowerBack,
      int blockedUsers,
      int lostFollowers,
      int profileViews,
      int storyViews,
      int newFollowers,
      int nonFollowingAdmiredUsers,
      int secretAdmirers,
      int deletedLikesOrComments,
      int likedYouTheMost,
      int totalPosts});
}

/// @nodoc
class _$DashboardCopyWithImpl<$Res> implements $DashboardCopyWith<$Res> {
  _$DashboardCopyWithImpl(this._value, this._then);

  final Dashboard _value;
  // ignore: unused_field
  final $Res Function(Dashboard) _then;

  @override
  $Res call({
    Object fullName = freezed,
    Object username = freezed,
    Object profilePicUrl = freezed,
    Object followers = freezed,
    Object following = freezed,
    Object activeStories = freezed,
    Object notFollowingBack = freezed,
    Object notFollowerBack = freezed,
    Object blockedUsers = freezed,
    Object lostFollowers = freezed,
    Object profileViews = freezed,
    Object storyViews = freezed,
    Object newFollowers = freezed,
    Object nonFollowingAdmiredUsers = freezed,
    Object secretAdmirers = freezed,
    Object deletedLikesOrComments = freezed,
    Object likedYouTheMost = freezed,
    Object totalPosts = freezed,
  }) {
    return _then(_value.copyWith(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      username: username == freezed ? _value.username : username as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl as String,
      followers: followers == freezed ? _value.followers : followers as int,
      following: following == freezed ? _value.following : following as int,
      activeStories: activeStories == freezed
          ? _value.activeStories
          : activeStories as int,
      notFollowingBack: notFollowingBack == freezed
          ? _value.notFollowingBack
          : notFollowingBack as int,
      notFollowerBack: notFollowerBack == freezed
          ? _value.notFollowerBack
          : notFollowerBack as int,
      blockedUsers:
          blockedUsers == freezed ? _value.blockedUsers : blockedUsers as int,
      lostFollowers: lostFollowers == freezed
          ? _value.lostFollowers
          : lostFollowers as int,
      profileViews:
          profileViews == freezed ? _value.profileViews : profileViews as int,
      storyViews: storyViews == freezed ? _value.storyViews : storyViews as int,
      newFollowers:
          newFollowers == freezed ? _value.newFollowers : newFollowers as int,
      nonFollowingAdmiredUsers: nonFollowingAdmiredUsers == freezed
          ? _value.nonFollowingAdmiredUsers
          : nonFollowingAdmiredUsers as int,
      secretAdmirers: secretAdmirers == freezed
          ? _value.secretAdmirers
          : secretAdmirers as int,
      deletedLikesOrComments: deletedLikesOrComments == freezed
          ? _value.deletedLikesOrComments
          : deletedLikesOrComments as int,
      likedYouTheMost: likedYouTheMost == freezed
          ? _value.likedYouTheMost
          : likedYouTheMost as int,
      totalPosts: totalPosts == freezed ? _value.totalPosts : totalPosts as int,
    ));
  }
}

/// @nodoc
abstract class _$DashboardCopyWith<$Res> implements $DashboardCopyWith<$Res> {
  factory _$DashboardCopyWith(
          _Dashboard value, $Res Function(_Dashboard) then) =
      __$DashboardCopyWithImpl<$Res>;
  @override
  $Res call(
      {String fullName,
      String username,
      String profilePicUrl,
      int followers,
      int following,
      int activeStories,
      int notFollowingBack,
      int notFollowerBack,
      int blockedUsers,
      int lostFollowers,
      int profileViews,
      int storyViews,
      int newFollowers,
      int nonFollowingAdmiredUsers,
      int secretAdmirers,
      int deletedLikesOrComments,
      int likedYouTheMost,
      int totalPosts});
}

/// @nodoc
class __$DashboardCopyWithImpl<$Res> extends _$DashboardCopyWithImpl<$Res>
    implements _$DashboardCopyWith<$Res> {
  __$DashboardCopyWithImpl(_Dashboard _value, $Res Function(_Dashboard) _then)
      : super(_value, (v) => _then(v as _Dashboard));

  @override
  _Dashboard get _value => super._value as _Dashboard;

  @override
  $Res call({
    Object fullName = freezed,
    Object username = freezed,
    Object profilePicUrl = freezed,
    Object followers = freezed,
    Object following = freezed,
    Object activeStories = freezed,
    Object notFollowingBack = freezed,
    Object notFollowerBack = freezed,
    Object blockedUsers = freezed,
    Object lostFollowers = freezed,
    Object profileViews = freezed,
    Object storyViews = freezed,
    Object newFollowers = freezed,
    Object nonFollowingAdmiredUsers = freezed,
    Object secretAdmirers = freezed,
    Object deletedLikesOrComments = freezed,
    Object likedYouTheMost = freezed,
    Object totalPosts = freezed,
  }) {
    return _then(_Dashboard(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      username: username == freezed ? _value.username : username as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl as String,
      followers: followers == freezed ? _value.followers : followers as int,
      following: following == freezed ? _value.following : following as int,
      activeStories: activeStories == freezed
          ? _value.activeStories
          : activeStories as int,
      notFollowingBack: notFollowingBack == freezed
          ? _value.notFollowingBack
          : notFollowingBack as int,
      notFollowerBack: notFollowerBack == freezed
          ? _value.notFollowerBack
          : notFollowerBack as int,
      blockedUsers:
          blockedUsers == freezed ? _value.blockedUsers : blockedUsers as int,
      lostFollowers: lostFollowers == freezed
          ? _value.lostFollowers
          : lostFollowers as int,
      profileViews:
          profileViews == freezed ? _value.profileViews : profileViews as int,
      storyViews: storyViews == freezed ? _value.storyViews : storyViews as int,
      newFollowers:
          newFollowers == freezed ? _value.newFollowers : newFollowers as int,
      nonFollowingAdmiredUsers: nonFollowingAdmiredUsers == freezed
          ? _value.nonFollowingAdmiredUsers
          : nonFollowingAdmiredUsers as int,
      secretAdmirers: secretAdmirers == freezed
          ? _value.secretAdmirers
          : secretAdmirers as int,
      deletedLikesOrComments: deletedLikesOrComments == freezed
          ? _value.deletedLikesOrComments
          : deletedLikesOrComments as int,
      likedYouTheMost: likedYouTheMost == freezed
          ? _value.likedYouTheMost
          : likedYouTheMost as int,
      totalPosts: totalPosts == freezed ? _value.totalPosts : totalPosts as int,
    ));
  }
}

/// @nodoc
class _$_Dashboard implements _Dashboard {
  const _$_Dashboard(
      {this.fullName,
      this.username,
      this.profilePicUrl,
      this.followers,
      this.following,
      this.activeStories,
      this.notFollowingBack,
      this.notFollowerBack,
      this.blockedUsers,
      this.lostFollowers,
      this.profileViews,
      this.storyViews,
      this.newFollowers,
      this.nonFollowingAdmiredUsers,
      this.secretAdmirers,
      this.deletedLikesOrComments,
      this.likedYouTheMost,
      this.totalPosts});

  @override
  final String fullName;
  @override
  final String username;
  @override
  final String profilePicUrl;
  @override
  final int followers;
  @override
  final int following;
  @override
  final int activeStories;
  @override
  final int notFollowingBack;
  @override
  final int notFollowerBack;
  @override
  final int blockedUsers;
  @override
  final int lostFollowers;
  @override
  final int profileViews;
  @override
  final int storyViews;
  @override
  final int newFollowers;
  @override
  final int nonFollowingAdmiredUsers;
  @override
  final int secretAdmirers;
  @override
  final int deletedLikesOrComments;
  @override
  final int likedYouTheMost;
  @override
  final int totalPosts;

  @override
  String toString() {
    return 'Dashboard(fullName: $fullName, username: $username, profilePicUrl: $profilePicUrl, followers: $followers, following: $following, activeStories: $activeStories, notFollowingBack: $notFollowingBack, notFollowerBack: $notFollowerBack, blockedUsers: $blockedUsers, lostFollowers: $lostFollowers, profileViews: $profileViews, storyViews: $storyViews, newFollowers: $newFollowers, nonFollowingAdmiredUsers: $nonFollowingAdmiredUsers, secretAdmirers: $secretAdmirers, deletedLikesOrComments: $deletedLikesOrComments, likedYouTheMost: $likedYouTheMost, totalPosts: $totalPosts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Dashboard &&
            (identical(other.fullName, fullName) ||
                const DeepCollectionEquality()
                    .equals(other.fullName, fullName)) &&
            (identical(other.username, username) ||
                const DeepCollectionEquality()
                    .equals(other.username, username)) &&
            (identical(other.profilePicUrl, profilePicUrl) ||
                const DeepCollectionEquality()
                    .equals(other.profilePicUrl, profilePicUrl)) &&
            (identical(other.followers, followers) ||
                const DeepCollectionEquality()
                    .equals(other.followers, followers)) &&
            (identical(other.following, following) ||
                const DeepCollectionEquality()
                    .equals(other.following, following)) &&
            (identical(other.activeStories, activeStories) ||
                const DeepCollectionEquality()
                    .equals(other.activeStories, activeStories)) &&
            (identical(other.notFollowingBack, notFollowingBack) ||
                const DeepCollectionEquality()
                    .equals(other.notFollowingBack, notFollowingBack)) &&
            (identical(other.notFollowerBack, notFollowerBack) ||
                const DeepCollectionEquality()
                    .equals(other.notFollowerBack, notFollowerBack)) &&
            (identical(other.blockedUsers, blockedUsers) ||
                const DeepCollectionEquality()
                    .equals(other.blockedUsers, blockedUsers)) &&
            (identical(other.lostFollowers, lostFollowers) ||
                const DeepCollectionEquality()
                    .equals(other.lostFollowers, lostFollowers)) &&
            (identical(other.profileViews, profileViews) ||
                const DeepCollectionEquality()
                    .equals(other.profileViews, profileViews)) &&
            (identical(other.storyViews, storyViews) ||
                const DeepCollectionEquality()
                    .equals(other.storyViews, storyViews)) &&
            (identical(other.newFollowers, newFollowers) ||
                const DeepCollectionEquality()
                    .equals(other.newFollowers, newFollowers)) &&
            (identical(
                    other.nonFollowingAdmiredUsers, nonFollowingAdmiredUsers) ||
                const DeepCollectionEquality().equals(
                    other.nonFollowingAdmiredUsers,
                    nonFollowingAdmiredUsers)) &&
            (identical(other.secretAdmirers, secretAdmirers) ||
                const DeepCollectionEquality()
                    .equals(other.secretAdmirers, secretAdmirers)) &&
            (identical(other.deletedLikesOrComments, deletedLikesOrComments) ||
                const DeepCollectionEquality().equals(
                    other.deletedLikesOrComments, deletedLikesOrComments)) &&
            (identical(other.likedYouTheMost, likedYouTheMost) ||
                const DeepCollectionEquality()
                    .equals(other.likedYouTheMost, likedYouTheMost)) &&
            (identical(other.totalPosts, totalPosts) ||
                const DeepCollectionEquality()
                    .equals(other.totalPosts, totalPosts)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(fullName) ^
      const DeepCollectionEquality().hash(username) ^
      const DeepCollectionEquality().hash(profilePicUrl) ^
      const DeepCollectionEquality().hash(followers) ^
      const DeepCollectionEquality().hash(following) ^
      const DeepCollectionEquality().hash(activeStories) ^
      const DeepCollectionEquality().hash(notFollowingBack) ^
      const DeepCollectionEquality().hash(notFollowerBack) ^
      const DeepCollectionEquality().hash(blockedUsers) ^
      const DeepCollectionEquality().hash(lostFollowers) ^
      const DeepCollectionEquality().hash(profileViews) ^
      const DeepCollectionEquality().hash(storyViews) ^
      const DeepCollectionEquality().hash(newFollowers) ^
      const DeepCollectionEquality().hash(nonFollowingAdmiredUsers) ^
      const DeepCollectionEquality().hash(secretAdmirers) ^
      const DeepCollectionEquality().hash(deletedLikesOrComments) ^
      const DeepCollectionEquality().hash(likedYouTheMost) ^
      const DeepCollectionEquality().hash(totalPosts);

  @override
  _$DashboardCopyWith<_Dashboard> get copyWith =>
      __$DashboardCopyWithImpl<_Dashboard>(this, _$identity);
}

abstract class _Dashboard implements Dashboard {
  const factory _Dashboard(
      {String fullName,
      String username,
      String profilePicUrl,
      int followers,
      int following,
      int activeStories,
      int notFollowingBack,
      int notFollowerBack,
      int blockedUsers,
      int lostFollowers,
      int profileViews,
      int storyViews,
      int newFollowers,
      int nonFollowingAdmiredUsers,
      int secretAdmirers,
      int deletedLikesOrComments,
      int likedYouTheMost,
      int totalPosts}) = _$_Dashboard;

  @override
  String get fullName;
  @override
  String get username;
  @override
  String get profilePicUrl;
  @override
  int get followers;
  @override
  int get following;
  @override
  int get activeStories;
  @override
  int get notFollowingBack;
  @override
  int get notFollowerBack;
  @override
  int get blockedUsers;
  @override
  int get lostFollowers;
  @override
  int get profileViews;
  @override
  int get storyViews;
  @override
  int get newFollowers;
  @override
  int get nonFollowingAdmiredUsers;
  @override
  int get secretAdmirers;
  @override
  int get deletedLikesOrComments;
  @override
  int get likedYouTheMost;
  @override
  int get totalPosts;
  @override
  _$DashboardCopyWith<_Dashboard> get copyWith;
}
