import 'dart:async';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';
import 'package:purchases_flutter/object_wrappers.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

import '../generated/l10n.dart';
import '../model/view_data/premium_page/premium_pricing.dart';
import '../service/analytics/appsflyer_service.dart';
import '../service/storage/app_config.dart';
import '../service/subscription/subscription_service.dart';
import '../themes/themes.dart';

class PremiumPopup extends StatefulWidget {
  final Function callback;

  PremiumPopup({@required this.callback});

  @override
  _PremiumPopupState createState() => _PremiumPopupState();
}

class _PremiumPopupState extends State<PremiumPopup> {
  final AppConfig _appConfig = GetIt.instance.get<AppConfig>();
  final SubscriptionService _subscriptionService =
      GetIt.I.get<SubscriptionService>();
  final AppsflyerService _appsflyerService = GetIt.I.get<AppsflyerService>();
  List<String> _subtitleKeys;
  List<PremiumPricing> _premiumPricing = [];
  int _current = 0;
  int _currentPrice;
  bool _purchasing = false;
  int _price_mode;
  String _price_color_popup;
  int initialDragTimeStamp;
  int currentDragTimeStamp;
  int timeDelta;
  double initialPositionY;
  double currentPositionY;
  double positionYDelta;

  void _startVerticalDrag(details) {
    // Timestamp of when drag begins
    initialDragTimeStamp = details.sourceTimeStamp.inMilliseconds;

    // Screen position of pointer when drag begins
    initialPositionY = details.globalPosition.dy;
  }

  void _whileVerticalDrag(details) {
    // Gets current timestamp and position of the drag event
    currentDragTimeStamp = details.sourceTimeStamp.inMilliseconds;
    currentPositionY = details.globalPosition.dy;

    // How much time has passed since drag began
    timeDelta = currentDragTimeStamp - initialDragTimeStamp;

    // Distance pointer has travelled since drag began
    positionYDelta = currentPositionY - initialPositionY;

    // If pointer has moved more than 50pt in less than 50ms...
    if (timeDelta < 50 && positionYDelta > 50) {
      // close modal
    }
  }

  _PremiumPopupState() {
    _subtitleKeys = _appConfig.getListString(AppConfig.PREMIUM_SUBTITLE);
    _currentPrice = _appConfig.getInt(AppConfig.SUBSCRIPTION_PRESELECTED);
    _price_mode = _appConfig.getInt(AppConfig.PRICE_MODE);
    _price_color_popup = _appConfig.getString(AppConfig.PRICE_COLOR_POPUP);
  }

  @override
  void initState() {
    _appsflyerService.logEvent(
      eventName: 'af_list_view',
      eventValues: {'af_content_type': 'Premium Page'},
    );
    super.initState();
  }

  // @override
  // Widget build(BuildContext context) {
  //   return SizedBox(
  //     width: 100,
  //     height: 100,
  //     child: TextButton(
  //         onPressed: () async {
  //           getInfo();
  //         },
  //         child: Text("HI")),
  //   );
  // }

  // void getInfo() async {
  //   var products =
  //       await Purchases.getProducts(["gg_2699_1y", "gg_2149_6m", "gg_749_1m"]);

  //   print(products.first.title);
  // }

  @override
  Widget build(BuildContext context) {
    List<String> subscriptionOrder =
        _appConfig.getListString(AppConfig.SUBSCRIPTION_ORDER);

    _premiumPricing = [];
    subscriptionOrder.forEach((key) {
      _premiumPricing.add(
        PremiumPricing.fromKey(
          key: key,
          offering: _subscriptionService.offerings,
          buildContext: context,
        ),
      );
    });

    return Material(
      // color: Platform.isIOS ? Colors.transparent : appColorScheme.background,
      child: GestureDetector(
        onVerticalDragEnd: (details) {
          double velocity = details.primaryVelocity;
          if (velocity > 0) {
            Navigator.pop(context);
          }
        },
        child: Container(
          color: appColorScheme.background,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(bottom: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fitWidth,
                          image: AssetImage(getThemeModeAsInt() == 0
                              ? "assets/images/blurred-background.jpg"
                              : "assets/images/blurred-background-nightmode.jpg"))),
                ),
              ),
              Container(
                color: appColorScheme.background,
                height: 20,
              ),
              Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  gradient: LinearGradient(
                    colors: [Color(0xFFFDC52A), Color(0xFFFF9714)],
                  ),
                ),
                child: Icon(
                  Icons.star,
                  size: 25,
                  color: Colors.white,
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.all(16),
                child: Text(
                  S.of(context).proModeTitle,
                  textAlign: TextAlign.center,
                  style: appTextTheme.subtitle1
                      .copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              Column(
                children: _premiumSubtitles(context),
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 10, end: 10, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: _premiumPricing
                      .asMap()
                      .entries
                      .map(
                        (MapEntry<int, PremiumPricing> map) =>
                            _buildRowPrices(map.key, map.value),
                      )
                      .toList(),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 0),
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () => _payPackage(context),
                  child: Builder(
                    builder: (_) {
                      if (_purchasing) {
                        return SizedBox(
                          height: 24,
                          width: 24,
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Text(
                        S.of(context).getYourPlan,
                      );
                    },
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 0),
                width: double.infinity,
                child: OutlinedButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(
                    S.of(context).returnToLimitedFunctionalities,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _payPackage(BuildContext context) async {
    if (!_purchasing) {
      setState(() {
        _purchasing = true;
      });

      _appsflyerService.logEvent(
        eventName: 'purchase_started',
        eventValues: {
          'af_content': _premiumPricing
              .elementAt(_currentPrice)
              .package
              .product
              .identifier,
          'af_price':
              _premiumPricing.elementAt(_currentPrice).package.product.price,
          'af_currency': _premiumPricing
              .elementAt(_currentPrice)
              .package
              .product
              .currencyCode,
        },
      );

      Package packageToPurchase =
          _premiumPricing.elementAt(_currentPrice).package;
      var purchased =
          await _subscriptionService.purchasePackage(packageToPurchase);

      if (purchased) {
        Navigator.of(context).pop();
        widget.callback();
      } else {
        setState(() {
          _purchasing = false;
        });
        Fluttertoast.showToast(msg: S.of(context).somethingWentWrong);
      }
    }
  }

  List<Padding> _premiumSubtitles(BuildContext context) {
    List<Padding> widgets = [];

    List<String> proSubtitles = [
      S.of(context).proModeSubtitle1,
      S.of(context).proModeSubtitle2,
      S.of(context).proModeSubtitle3,
      S.of(context).proModeSubtitle4,
      S.of(context).proModeSubtitle5,
    ];

    proSubtitles.forEach((proSubtitle) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 4, 4, 4),
          child: Row(
            children: [
              Icon(
                Icons.check_circle_outline,
                color: appColorScheme.onPrimary,
                size: 19,
              ),
              SizedBox(
                width: 5,
              ),
              AutoSizeText(proSubtitle,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: appTextTheme.bodyText1.copyWith(fontSize: 14)),
            ],
          ),
        ),
      );
    });

    return widgets;
  }

  Widget _buildRowPrices(int index, PremiumPricing premiumPricing) {
    return InkWell(
      onTap: () async {
        _appsflyerService.logEvent(
          eventName: 'af_content_view',
          eventValues: {
            'af_content': premiumPricing.package.product.identifier,
            'af_price': premiumPricing.package.product.price,
            'af_currency': premiumPricing.package.product.currencyCode,
          },
        );
        setState(() {
          _currentPrice = index;
        });
      },
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 6,
            width: MediaQuery.of(context).size.width / 3.5,
            margin: EdgeInsets.only(top: 8),
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(12),
              ),
              border: Border.all(
                color: _currentPrice == index
                    ? appColorScheme.secondary
                    : appColorScheme.onPrimary,
                width: 1,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Text(
                          premiumPricing.title,
                          style: appTextTheme.subtitle1.copyWith(
                              fontWeight: FontWeight.bold,
                              color: _currentPrice == index
                                  ? appColorScheme.secondary
                                  : appTextTheme.subtitle1.color),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        child: Text(
                          premiumPricing.savePercentage,
                          style: appTextTheme.bodyText1,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        child: Text(
                          premiumPricing.title == "1 Month"
                              ? "${premiumPricing.package.product.priceString} /mo"
                              : premiumPricing.monthlyPrice,
                          style: appTextTheme.subtitle1,
                          // .copyWith(color: appColorScheme.onSecondary),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 1,
                  decoration: BoxDecoration(
                    color: _currentPrice == index
                        ? appColorScheme.secondary
                        : appColorScheme.onPrimary,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    color: _currentPrice == index
                        ? appColorScheme.secondary
                        : Colors.transparent,
                  ),
                  child: Text(
                    "${premiumPricing.package.product.priceString}",
                    style: appTextTheme.subtitle1.copyWith(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: _currentPrice == index
                            ? Colors.white
                            : appTextTheme.subtitle1.color),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          premiumPricing.title == "1 Year"
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                  child: Text(
                    "Popular",
                    style: appTextTheme.subtitle1
                        .copyWith(fontSize: 12, color: Colors.white),
                  ),
                  decoration: BoxDecoration(
                    color: appColorScheme.secondary,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(12),
                    ),
                    border: Border.all(
                      color: appColorScheme.secondary,
                      width: 1,
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
