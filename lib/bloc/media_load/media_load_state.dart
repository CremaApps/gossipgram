import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:GossipGram/model/objects/media_data.dart';

abstract class MediaLoadState extends Equatable {
  const MediaLoadState();

  @override
  List<Object> get props => [];
}

class MediaLoadInitial extends MediaLoadState {}

abstract class MediaLoadData extends MediaLoadState {
  final MediaData mediaData;

  const MediaLoadData({@required this.mediaData});

  @override
  List<Object> get props => [mediaData];
}

class MediaLoaded extends MediaLoadData {
  const MediaLoaded({
    @required mediaData,
  }) : super(mediaData: mediaData);
}
