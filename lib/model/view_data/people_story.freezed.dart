// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'people_story.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PeopleStoryTearOff {
  const _$PeopleStoryTearOff();

// ignore: unused_element
  _PeopleStory call(
      {@required String userId,
      @required String fullName,
      @required String profilePic,
      @required String storyPic}) {
    return _PeopleStory(
      userId: userId,
      fullName: fullName,
      profilePic: profilePic,
      storyPic: storyPic,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PeopleStory = _$PeopleStoryTearOff();

/// @nodoc
mixin _$PeopleStory {
  String get userId;
  String get fullName;
  String get profilePic;
  String get storyPic;

  $PeopleStoryCopyWith<PeopleStory> get copyWith;
}

/// @nodoc
abstract class $PeopleStoryCopyWith<$Res> {
  factory $PeopleStoryCopyWith(
          PeopleStory value, $Res Function(PeopleStory) then) =
      _$PeopleStoryCopyWithImpl<$Res>;
  $Res call(
      {String userId, String fullName, String profilePic, String storyPic});
}

/// @nodoc
class _$PeopleStoryCopyWithImpl<$Res> implements $PeopleStoryCopyWith<$Res> {
  _$PeopleStoryCopyWithImpl(this._value, this._then);

  final PeopleStory _value;
  // ignore: unused_field
  final $Res Function(PeopleStory) _then;

  @override
  $Res call({
    Object userId = freezed,
    Object fullName = freezed,
    Object profilePic = freezed,
    Object storyPic = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as String,
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePic:
          profilePic == freezed ? _value.profilePic : profilePic as String,
      storyPic: storyPic == freezed ? _value.storyPic : storyPic as String,
    ));
  }
}

/// @nodoc
abstract class _$PeopleStoryCopyWith<$Res>
    implements $PeopleStoryCopyWith<$Res> {
  factory _$PeopleStoryCopyWith(
          _PeopleStory value, $Res Function(_PeopleStory) then) =
      __$PeopleStoryCopyWithImpl<$Res>;
  @override
  $Res call(
      {String userId, String fullName, String profilePic, String storyPic});
}

/// @nodoc
class __$PeopleStoryCopyWithImpl<$Res> extends _$PeopleStoryCopyWithImpl<$Res>
    implements _$PeopleStoryCopyWith<$Res> {
  __$PeopleStoryCopyWithImpl(
      _PeopleStory _value, $Res Function(_PeopleStory) _then)
      : super(_value, (v) => _then(v as _PeopleStory));

  @override
  _PeopleStory get _value => super._value as _PeopleStory;

  @override
  $Res call({
    Object userId = freezed,
    Object fullName = freezed,
    Object profilePic = freezed,
    Object storyPic = freezed,
  }) {
    return _then(_PeopleStory(
      userId: userId == freezed ? _value.userId : userId as String,
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePic:
          profilePic == freezed ? _value.profilePic : profilePic as String,
      storyPic: storyPic == freezed ? _value.storyPic : storyPic as String,
    ));
  }
}

/// @nodoc
class _$_PeopleStory with DiagnosticableTreeMixin implements _PeopleStory {
  const _$_PeopleStory(
      {@required this.userId,
      @required this.fullName,
      @required this.profilePic,
      @required this.storyPic})
      : assert(userId != null),
        assert(fullName != null),
        assert(profilePic != null),
        assert(storyPic != null);

  @override
  final String userId;
  @override
  final String fullName;
  @override
  final String profilePic;
  @override
  final String storyPic;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'PeopleStory(userId: $userId, fullName: $fullName, profilePic: $profilePic, storyPic: $storyPic)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'PeopleStory'))
      ..add(DiagnosticsProperty('userId', userId))
      ..add(DiagnosticsProperty('fullName', fullName))
      ..add(DiagnosticsProperty('profilePic', profilePic))
      ..add(DiagnosticsProperty('storyPic', storyPic));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PeopleStory &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.fullName, fullName) ||
                const DeepCollectionEquality()
                    .equals(other.fullName, fullName)) &&
            (identical(other.profilePic, profilePic) ||
                const DeepCollectionEquality()
                    .equals(other.profilePic, profilePic)) &&
            (identical(other.storyPic, storyPic) ||
                const DeepCollectionEquality()
                    .equals(other.storyPic, storyPic)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(fullName) ^
      const DeepCollectionEquality().hash(profilePic) ^
      const DeepCollectionEquality().hash(storyPic);

  @override
  _$PeopleStoryCopyWith<_PeopleStory> get copyWith =>
      __$PeopleStoryCopyWithImpl<_PeopleStory>(this, _$identity);
}

abstract class _PeopleStory implements PeopleStory {
  const factory _PeopleStory(
      {@required String userId,
      @required String fullName,
      @required String profilePic,
      @required String storyPic}) = _$_PeopleStory;

  @override
  String get userId;
  @override
  String get fullName;
  @override
  String get profilePic;
  @override
  String get storyPic;
  @override
  _$PeopleStoryCopyWith<_PeopleStory> get copyWith;
}
