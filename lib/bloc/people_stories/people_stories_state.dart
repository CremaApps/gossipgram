part of 'people_stories_bloc.dart';

abstract class PeopleStoriesState extends Equatable {
  const PeopleStoriesState();

  @override
  List<Object> get props => [];
}

class PeopleStoriesInitial extends PeopleStoriesState {}

class PeopleStoriesLoading extends PeopleStoriesState {}

abstract class PeopleStoriesLoadData extends PeopleStoriesState {
  final PeopleStories peopleStories;

  const PeopleStoriesLoadData({@required this.peopleStories});

  @override
  List<Object> get props => [peopleStories];
}

class PeopleStoriesDataLoading extends PeopleStoriesLoadData {
  const PeopleStoriesDataLoading({
    @required peopleStories,
  }) : super(peopleStories: peopleStories);
}

class PeopleStoriesDataPageLoading extends PeopleStoriesLoadData {
  const PeopleStoriesDataPageLoading({
    @required peopleStories,
  }) : super(peopleStories: peopleStories);
}

class PeopleStoriesDataFullyLoaded extends PeopleStoriesLoadData {
  const PeopleStoriesDataFullyLoaded({
    @required peopleStories,
  }) : super(peopleStories: peopleStories);
}
