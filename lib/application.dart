import 'dart:io' show Platform;

import 'package:GossipGram/pages/splash_page.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart' show kReleaseMode;
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:get_it/get_it.dart';
import 'package:get_storage/get_storage.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:localizely_sdk/localizely_sdk.dart';

import 'generated/l10n.dart';
import 'pages/support_page.dart';
import 'routes.dart';
import 'service/analytics/appsflyer_service.dart';
import 'service/background_task/background_task_register_service.dart';
import 'service/interceptor/logged_out_interceptor.dart';
import 'service/interceptor/service_denial_interceptor.dart';
import 'service/navigator_service.dart';
import 'service/setup_user_service.dart';
import 'service/storage/app_config.dart';
import 'service/storage/secure_storage.dart';
import 'service/subscription/subscription_service.dart';
import 'themes/themes.dart';

class IGViews extends StatefulWidget {
  final InstagramPrivateApi _instagramPrivateApi =
      InstagramPrivateApi.instance();
  final AppConfig _appConfig = GetIt.I.get<AppConfig>();
  final SubscriptionService _subscriptionService =
      GetIt.I.get<SubscriptionService>();
  final SecureStorage _secureStorage = GetIt.I.get<SecureStorage>();
  final SetUpUserService _setUpUserService = GetIt.I.get<SetUpUserService>();
  final Map<dynamic, String> initialPages = {
    "Login": "/login",
    "Home": "/home",
  };
  final AppsflyerService _appsflyerService = GetIt.I.get<AppsflyerService>();

  Future<String> _startUpApp() async {
    String screen = "Login";
    initializeThemeValuesFirstTime();

    await _startUpConfig();

    String instagramCookies = await _secureStorage.getInstagramCookie();

    if (instagramCookies != null) {
      try {
        screen = "Home";
        await _setUpUserWithCookies(instagramCookies);
      } on Error catch (error, trace) {
        FirebaseCrashlytics.instance.recordError(error, trace);
      }
    }

    _appsflyerService.logEvent(eventName: 'session_started');

    return screen;
  }

  Future _startUpConfig() async {
    FlutterStatusbarManager.setStyle(StatusBarStyle.DARK_CONTENT);

    await Future.wait([
      GetStorage.init(),
      Firebase.initializeApp(),
    ]);

    _setInterceptorsInstagramApi();
    _extractFirebase();

    await _appConfig.init();

    _configureLocalizely();
    AppsflyerSdk appsflyerSdk = _configureAppsFlyer();

    await Future.wait([
      _subscriptionService.startUp(),
      Localizely.updateTranslations(),
      appsflyerSdk.initSdk(
        registerConversionDataCallback: false,
        registerOnAppOpenAttributionCallback: false,
      ),
    ]);
  }

  void _configureLocalizely() {
    var configLocalizely = _appConfig.getMap(AppConfig.LOCALIZELY_CONFIG);

    Localizely.init(
      configLocalizely[AppConfig.LOCALIZELY_SDK],
      configLocalizely[AppConfig.LOCALIZELY_DISTRIBUTION],
    );
  }

  Future<void> _setUpUserWithCookies(String instagramCookies) async {
    List<String> cookiesString = instagramCookies.split(
      SecureStorage.COOKIE_SPLIT,
    );

    List<InstaCookie> instaCookies = [];

    cookiesString.forEach((cookieString) {
      instaCookies.add(
        InstaCookie.fromStringCookie(cookieString),
      );
    });

    _instagramPrivateApi.cookies = instaCookies;

    await _setUpUserService.execute();
  }

  void _extractFirebase() {
    FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(kReleaseMode);
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  }

  void _setInterceptorsInstagramApi() {
    _instagramPrivateApi.addInterceptor(
      GetIt.I.get<LoggedOutInterceptor>(),
    );

    _instagramPrivateApi.addInterceptor(
      GetIt.I.get<ServiceDenialInterceptor>(),
    );
  }

  AppsflyerSdk _configureAppsFlyer() {
    var configAppsFlyer = _appConfig.getMap(AppConfig.APPSFLYER_CONFIG);

    AppsflyerSdk appsflyerSdk = AppsflyerSdk(AppsFlyerOptions(
        afDevKey: configAppsFlyer[AppConfig.APPSFLYER_AFDEVKEY],
        appId: Platform.isIOS
            ? configAppsFlyer[AppConfig.APPSFLYER_AFAPPID]
            : null,
        showDebug: !kReleaseMode));

    GetIt.I.registerSingleton(
      appsflyerSdk,
      instanceName: 'appsFlyer',
    );

    return appsflyerSdk;
  }

  static changeLocale(BuildContext context, Locale newLocale) {
    Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
    context.findAncestorStateOfType<_IGViewsState>().restartApp(newLocale);
  }

  @override
  _IGViewsState createState() => _IGViewsState();
}

class _IGViewsState extends State<IGViews> {
  Locale locale = Locale.fromSubtags(languageCode: 'en', countryCode: 'us');

  @override
  void initState() {
    super.initState();
    BackgroundTaskRegisterService backgroundTaskRegisterService =
        GetIt.I.get<BackgroundTaskRegisterService>();
    backgroundTaskRegisterService.setBackgroundListeners();
  }

  void restartApp(Locale newLocale) {
    setState(() {
      locale = newLocale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: ThemeData(
        colorScheme: colorSchemeLight,
        primaryColor: colorSchemeLight.primary,
        accentColor: colorSchemeLight.secondary,
        backgroundColor: colorSchemeLight.background,
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        textTheme: textThemeLight,
        primaryTextTheme: textThemeLight,
        accentTextTheme: textThemeLight,
        cursorColor: colorSchemeLight.secondary,
        iconTheme: IconThemeData(color: Colors.black),
        scaffoldBackgroundColor: colorSchemeLight.background,
        cardTheme: CardTheme(
          shape: RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(
              primary: colorSchemeLight.secondary,
              backgroundColor: colorSchemeLight.primary,
              textStyle: textThemeLight.button),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              primary: colorSchemeLight.secondary,
              onPrimary: colorSchemeLight.primary,
              textStyle: textThemeLight.button),
        ),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
              padding: EdgeInsets.all(16),
              primary: colorSchemeLight.secondary,
              textStyle: textThemeLight.button),
        ),
      ),
      dark: ThemeData(
        appBarTheme:
            AppBarTheme.of(context).copyWith(brightness: Brightness.dark),
        colorScheme: colorSchemeDark,
        primaryColor: colorSchemeDark.primary,
        accentColor: colorSchemeDark.secondary,
        backgroundColor: colorSchemeDark.background,
        textTheme: textThemeDark,
        primaryTextTheme: textThemeDark,
        accentTextTheme: textThemeDark,
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        cursorColor: colorSchemeDark.secondary,
        iconTheme: IconThemeData(color: Colors.white),
        scaffoldBackgroundColor: colorSchemeDark.primary,
        cardTheme: CardTheme(
          shape: RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(
              primary: colorSchemeLight.secondary,
              backgroundColor: colorSchemeDark.primary,
              textStyle: textThemeLight.button),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              primary: colorSchemeLight.secondary,
              onPrimary: colorSchemeLight.primary,
              textStyle: textThemeLight.button),
        ),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
              padding: EdgeInsets.all(16),
              primary: colorSchemeDark.secondary,
              textStyle: textThemeDark.button),
        ),
      ),
      initial: AdaptiveThemeMode.light,
      builder: (theme, darkTheme) => MaterialApp(
        title: 'Gossipgram',
        theme: theme,
        darkTheme: darkTheme,
        localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: S.delegate.supportedLocales,
        locale: locale,
        home: SplashPage(
          duration: 2000,
          imagePath: 'assets/images/logo_login.png',
          futureCustomFunction: widget._startUpApp,
          home: "Login",
          type: SplashType.BackgroundProcess,
          outputAndHome: widget.initialPages,
        ),
        routes: routes,
        navigatorKey: GetIt.instance.get<NavigatorService>().navigatorKey,
        navigatorObservers: [
          GetIt.I
              .get<FirebaseAnalyticsObserver>(instanceName: 'analyticsObserver')
        ],
      ),
    );
  }
}
