import 'dart:io';

import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/seen_your_story_bloc/seen_your_story_bloc.dart';
import 'package:GossipGram/bloc/seen_your_story_bloc/seen_your_story_event.dart';
import 'package:GossipGram/bloc/seen_your_story_bloc/seen_your_story_state.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/seen_your_story_user_item.dart';
import 'package:GossipGram/pages/user_profile/user_profile_page.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/profile_picture.dart';
import 'package:lottie/lottie.dart';
import 'package:path_provider/path_provider.dart';

class SeenYourStoryPage extends StatefulWidget {
  final String title;
  final String keyName;

  const SeenYourStoryPage({this.title, this.keyName});

  @override
  _SeenYourStoryPageState createState() => _SeenYourStoryPageState();
}

class _SeenYourStoryPageState extends State<SeenYourStoryPage> {
  SeenYourStoryBloc _seenYourStoryBloc;

  @override
  void initState() {
    super.initState();
    _seenYourStoryBloc = GetIt.instance.get<SeenYourStoryBloc>();
    _seenYourStoryBloc.add(SeenYourStoryLoadEvent());
  }

  @override
  void dispose() {
    super.dispose();
    _seenYourStoryBloc?.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        title: _title(),
      ),
      body: Container(
        margin: EdgeInsetsDirectional.only(top: 10),
        child: BlocBuilder(
          cubit: _seenYourStoryBloc,
          builder: (context, state) {
            if (state is SeenYourStoryLoadingState) {
              return _loading();
            } else if (state is SeenYourStoryLoadedState) {
              if (state.seenYourStory.isNotEmpty) {
                return _showListView(state, context);
              } else {
                return _noRecords(context);
              }
            } else {
              return _error();
            }
          },
        ),
      ),
    );
  }

  Widget _title() {
    return Text(S.of(context).seenYourStory, style: appTextTheme.subtitle2);
  }

  Widget _loading() {
    return Center(
      child: Lottie.asset(
        'assets/animations/loader.json',
        width: 100,
        height: 100,
      ),
    );
  }

  Widget _error() {
    return Center(
      child: Text(S.of(context).somethingWentWrong),
    );
  }

  Widget _noRecords(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsetsDirectional.only(
            top: MediaQuery.of(context).size.height / 3.2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(S.of(context).nobodyFound, style: appTextTheme.headline6),
            Container(
              margin: EdgeInsetsDirectional.only(top: 10),
              width: MediaQuery.of(context).size.width / 2,
              child: Text(
                S.of(context).noOneYet,
                style: appTextTheme.bodyText1,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _showListView(SeenYourStoryLoadedState state, BuildContext context) {
    return FutureBuilder(
      future: getApplicationDocumentsDirectory(),
      builder: (BuildContext context, AsyncSnapshot<Directory> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          var firstPath = snapshot.data.path + "/images/";
          return ListView(
            physics: ClampingScrollPhysics(),
            children: state.seenYourStory
                .map((e) => _buildRow(context, e, firstPath))
                .toList(),
          );
        }
      },
    );
  }

  Widget _buildRow(
    BuildContext context,
    SeenYourStoryUserItem seenYourStoryUserItem,
    String path,
  ) {
    return Container(
      padding:
          EdgeInsetsDirectional.only(top: 10, bottom: 10, start: 20, end: 20),
      child: InkWell(
        onTap: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => UserProfile(
              userId: seenYourStoryUserItem.userId,
              username: seenYourStoryUserItem.userName,
            ),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width / 8,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: ProfilePicture(
                    profilePicUrl: seenYourStoryUserItem.imageUrl),
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Container(
                margin: EdgeInsetsDirectional.only(top: 2, start: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsetsDirectional.only(bottom: 8),
                      child: Text(
                          seenYourStoryUserItem.name != null
                              ? seenYourStoryUserItem.name
                              : seenYourStoryUserItem.userName,
                          overflow: TextOverflow.ellipsis,
                          style: appTextTheme.subtitle1),
                    ),
                    Container(
                      child: Text(seenYourStoryUserItem.userName,
                          style: appTextTheme.bodyText1),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 11,
              height: 46.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(4),
                ),
                image: DecorationImage(
                  image: FileImage(
                    File("$path${seenYourStoryUserItem.storyId}.jpg"),
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
