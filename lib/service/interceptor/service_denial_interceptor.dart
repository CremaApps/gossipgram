import 'package:dio/dio.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class ServiceDenialInterceptor extends Interceptor {
  ServiceDenialInterceptor();

  @override
  Future onError(DioError err) {
    if (_serviceDenialError(err)) {
      FirebaseAnalytics analytics = GetIt.I.get(instanceName: 'analytics');

      analytics.logEvent(
        name: 'Soft Ban',
        parameters: {
          "uri_path": err.request.uri.path,
        },
      );
    }

    return Future.value(err);
  }

  bool _serviceDenialError(DioError err) {
    return err.type == DioErrorType.RESPONSE && err.response.statusCode == 429;
  }
}
