// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'my_story_box.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$MyStoryBoxTearOff {
  const _$MyStoryBoxTearOff();

// ignore: unused_element
  _MyStoryBox call(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated}) {
    return _MyStoryBox(
      title: title,
      value: value,
      redirectFunction: redirectFunction,
      isPaidFunctionality: isPaidFunctionality,
      updated: updated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $MyStoryBox = _$MyStoryBoxTearOff();

/// @nodoc
mixin _$MyStoryBox {
  String get title;
  int get value;
  Function get redirectFunction;
  bool get isPaidFunctionality;
  bool get updated;

  $MyStoryBoxCopyWith<MyStoryBox> get copyWith;
}

/// @nodoc
abstract class $MyStoryBoxCopyWith<$Res> {
  factory $MyStoryBoxCopyWith(
          MyStoryBox value, $Res Function(MyStoryBox) then) =
      _$MyStoryBoxCopyWithImpl<$Res>;
  $Res call(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated});
}

/// @nodoc
class _$MyStoryBoxCopyWithImpl<$Res> implements $MyStoryBoxCopyWith<$Res> {
  _$MyStoryBoxCopyWithImpl(this._value, this._then);

  final MyStoryBox _value;
  // ignore: unused_field
  final $Res Function(MyStoryBox) _then;

  @override
  $Res call({
    Object title = freezed,
    Object value = freezed,
    Object redirectFunction = freezed,
    Object isPaidFunctionality = freezed,
    Object updated = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed ? _value.title : title as String,
      value: value == freezed ? _value.value : value as int,
      redirectFunction: redirectFunction == freezed
          ? _value.redirectFunction
          : redirectFunction as Function,
      isPaidFunctionality: isPaidFunctionality == freezed
          ? _value.isPaidFunctionality
          : isPaidFunctionality as bool,
      updated: updated == freezed ? _value.updated : updated as bool,
    ));
  }
}

/// @nodoc
abstract class _$MyStoryBoxCopyWith<$Res> implements $MyStoryBoxCopyWith<$Res> {
  factory _$MyStoryBoxCopyWith(
          _MyStoryBox value, $Res Function(_MyStoryBox) then) =
      __$MyStoryBoxCopyWithImpl<$Res>;
  @override
  $Res call(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated});
}

/// @nodoc
class __$MyStoryBoxCopyWithImpl<$Res> extends _$MyStoryBoxCopyWithImpl<$Res>
    implements _$MyStoryBoxCopyWith<$Res> {
  __$MyStoryBoxCopyWithImpl(
      _MyStoryBox _value, $Res Function(_MyStoryBox) _then)
      : super(_value, (v) => _then(v as _MyStoryBox));

  @override
  _MyStoryBox get _value => super._value as _MyStoryBox;

  @override
  $Res call({
    Object title = freezed,
    Object value = freezed,
    Object redirectFunction = freezed,
    Object isPaidFunctionality = freezed,
    Object updated = freezed,
  }) {
    return _then(_MyStoryBox(
      title: title == freezed ? _value.title : title as String,
      value: value == freezed ? _value.value : value as int,
      redirectFunction: redirectFunction == freezed
          ? _value.redirectFunction
          : redirectFunction as Function,
      isPaidFunctionality: isPaidFunctionality == freezed
          ? _value.isPaidFunctionality
          : isPaidFunctionality as bool,
      updated: updated == freezed ? _value.updated : updated as bool,
    ));
  }
}

/// @nodoc
class _$_MyStoryBox implements _MyStoryBox {
  const _$_MyStoryBox(
      {this.title,
      this.value,
      this.redirectFunction,
      this.isPaidFunctionality,
      this.updated});

  @override
  final String title;
  @override
  final int value;
  @override
  final Function redirectFunction;
  @override
  final bool isPaidFunctionality;
  @override
  final bool updated;

  @override
  String toString() {
    return 'MyStoryBox(title: $title, value: $value, redirectFunction: $redirectFunction, isPaidFunctionality: $isPaidFunctionality, updated: $updated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MyStoryBox &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)) &&
            (identical(other.redirectFunction, redirectFunction) ||
                const DeepCollectionEquality()
                    .equals(other.redirectFunction, redirectFunction)) &&
            (identical(other.isPaidFunctionality, isPaidFunctionality) ||
                const DeepCollectionEquality()
                    .equals(other.isPaidFunctionality, isPaidFunctionality)) &&
            (identical(other.updated, updated) ||
                const DeepCollectionEquality().equals(other.updated, updated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(value) ^
      const DeepCollectionEquality().hash(redirectFunction) ^
      const DeepCollectionEquality().hash(isPaidFunctionality) ^
      const DeepCollectionEquality().hash(updated);

  @override
  _$MyStoryBoxCopyWith<_MyStoryBox> get copyWith =>
      __$MyStoryBoxCopyWithImpl<_MyStoryBox>(this, _$identity);
}

abstract class _MyStoryBox implements MyStoryBox {
  const factory _MyStoryBox(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated}) = _$_MyStoryBox;

  @override
  String get title;
  @override
  int get value;
  @override
  Function get redirectFunction;
  @override
  bool get isPaidFunctionality;
  @override
  bool get updated;
  @override
  _$MyStoryBoxCopyWith<_MyStoryBox> get copyWith;
}
