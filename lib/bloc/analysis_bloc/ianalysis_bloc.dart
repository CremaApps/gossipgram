import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/analysis_bloc/blocs/blocked_you_bloc.dart';
import 'package:GossipGram/bloc/analysis_bloc/blocs/stories_not_following_you_back_bloc.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:meta/meta.dart';

import 'blocs/deleted_you_likes_comments_bloc.dart';
import 'blocs/followed_bloc.dart';
import 'blocs/followers_bloc.dart';
import 'blocs/liked_but_dont_follow_bloc.dart';
import 'blocs/new_followers_bloc.dart';
import 'blocs/not_following_you_back_bloc.dart';
import 'blocs/secret_admirers_bloc.dart';
import 'blocs/seen_your_profile_bloc.dart';
import 'blocs/stopped_following_you_bloc.dart';
import 'blocs/stories_you_dont_follow_back_bloc.dart';
import 'blocs/youre_not_following_back_bloc.dart';

part 'analysis_event.dart';
part 'analysis_state.dart';

abstract class IAnalysisBloc extends Bloc<AnalysisEvent, AnalysisState> {
  IAnalysisBloc() : super(AnalysisInitial());
  static const String NOT_FOLLOWING_BACK = 'NOT_FOLLOWING_BACK';
  static const String YOURE_NOT_FOLLOWING = 'YOURE_NOT_FOLLOWING';
  static const String BLOCKED_YOU = 'BLOCKED_YOU';
  static const String STOPPED_FOLLOWING_YOU = 'STOPPED_FOLLOWING_YOU';
  static const String SEEN_YOUR_PROFILE = 'SEEN_YOUR_PROFILE';
  static const String NEW_FOLLOWERS = 'NEW_FOLLOWERS';
  static const String SECRET_ADMIRERS = 'SECRET_ADMIRERS';
  static const String DELETED_YOU_LIKES_COMMENTS = 'DELETED_YOU_LIKES_COMMENTS';
  static const String LIKED_BUT_DONT_FOLLOW = 'LIKED_BUT_DONT_FOLLOW';
  static const String STORIES_DONT_FOLLOW_YOU = 'STORIES_DONT_FOLLOW_YOU';
  static const String STORIES_YOU_DONT_FOLLOW = 'STORIES_YOU_DONT_FOLLOW';
  static const String FOLLOWERS = 'FOLLOWERS';
  static const String FOLLOWED = 'FOLLOWED';

  factory IAnalysisBloc.getByString(String key) {
    switch (key) {
      case NOT_FOLLOWING_BACK:
        return GetIt.instance.get<NotFollowingYouBackBloc>();
      case YOURE_NOT_FOLLOWING:
        return GetIt.instance.get<YoureNotFollowingBackBloc>();
      case BLOCKED_YOU:
        return GetIt.instance.get<BlockedYouBloc>();
      case STOPPED_FOLLOWING_YOU:
        return GetIt.instance.get<StoppedFollowingYouBloc>();
      case SEEN_YOUR_PROFILE:
        return GetIt.instance.get<SeenYourProfileBloc>();
      case NEW_FOLLOWERS:
        return GetIt.instance.get<NewFollowersBloc>();
      case SECRET_ADMIRERS:
        return GetIt.instance.get<SecretAdmirersBloc>();
      case DELETED_YOU_LIKES_COMMENTS:
        return GetIt.instance.get<DeletedYouLikesCommentsBloc>();
      case LIKED_BUT_DONT_FOLLOW:
        return GetIt.instance.get<LikedButDontFollowBloc>();
      case STORIES_DONT_FOLLOW_YOU:
        return GetIt.instance.get<StoriesNotFollowingYouBackBloc>();
      case STORIES_YOU_DONT_FOLLOW:
        return GetIt.instance.get<StoriesYouDontFollowBackBloc>();
      case FOLLOWERS:
        return GetIt.instance.get<FollowersBloc>();
      case FOLLOWED:
        return GetIt.instance.get<FollowedBloc>();
      default:
        throw UnimplementedError();
    }
  }
}
