import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/user/user.dart';
import 'package:GossipGram/model/objects/user_like_count.dart';
import 'package:GossipGram/model/objects/user_story_view_count.dart';
import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class UserRepository {
  final DbProvider _dbProvider;

  UserRepository(this._dbProvider);

  Future<bool> existsUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('User', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty;
  }

  Future<User> getUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('User', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? User.fromMap(res.first) : null;
  }

  Future<int> insertUser({@required User user}) async {
    final db = await _dbProvider.database;
    return await db.insert('User', user.toMap());
  }

  Future<int> deleteUser({@required User user}) async {
    final db = await _dbProvider.database;
    return await db.delete('User');
  }

  Future<int> upsertUser({@required User user}) async {
    final db = await _dbProvider.database;
    if (await this.existsUser(id: user.id)) {
      return db.update(
        'User',
        user.toMap(),
        where: 'id = ?',
        whereArgs: [user.id],
      );
    } else {
      return await db.insert('User', user.toMap());
    }
  }

  Future<void> upsertUsers({@required List<User> users}) async {
    final db = await _dbProvider.database;
    Batch batch = db.batch();
    var dbUsers = await db.query(
      'User',
    );

    Set<User> allUsers = Set<User>.from(
        dbUsers.isNotEmpty ? dbUsers.map((a) => User.fromMap(a)).toList() : []);

    Set<User> toUpdateUsers = Set<User>.from(users)
        .where((element) => allUsers.any((User e) => e.id == element.id))
        .toSet();
    Set<User> toInsertUsers = Set<User>.from(users).difference(toUpdateUsers);

    for (User user in toUpdateUsers) {
      batch.update(
        'User',
        user.toMap(),
        where: 'id = ?',
        whereArgs: [user.id],
      );
    }

    for (User user in toInsertUsers) {
      batch.insert('User', user.toMap());
    }
    await batch.commit(noResult: true);
  }

  Future<int> updateUser({@required User user}) async {
    final db = await _dbProvider.database;
    return await db
        .update('User', user.toMap(), where: 'id = ?', whereArgs: [user.id]);
  }

  Future<List<User>> getAllUsers() async {
    final db = await _dbProvider.database;
    var dbUsers = await db.query(
      'User',
    );
    List<User> users =
        dbUsers.isNotEmpty ? dbUsers.map((a) => User.fromMap(a)).toList() : [];
    return users;
  }

  Future<List<int>> getAllUserIds() async {
    final db = await _dbProvider.database;
    var dbUsers = await db.rawQuery(
      'SELECT id '
      'FROM User',
    );
    List<int> users = dbUsers.isNotEmpty
        ? dbUsers.map((a) {
            int id = a["id"];
            return id;
          }).toList()
        : [];
    return users;
  }

  Future<int> updateUserYouFollowStatus(
      {@required int userId, @required bool youFollow}) async {
    final db = await _dbProvider.database;
    Map<String, dynamic> row = {
      "youFollow": youFollow,
    };

    return await db.update("User", row, where: 'id = ?', whereArgs: [userId]);
  }

  Future<List<int>> getUsersWithInteractions({
    @required int fromDate,
  }) async {
    final db = await _dbProvider.database;
    var dbUsers = await db.rawQuery("SELECT * "
        "FROM User u "
        "WHERE (SELECT COUNT(*) FROM Comment c WHERE u.id = c.userId "
        "AND (c.createdAt >= $fromDate OR c.deletedAt > $fromDate) ) "
        "OR (SELECT COUNT(*) FROM Like l WHERE u.id = l.userId AND l.onStateChange >= $fromDate ) "
        "OR (SELECT COUNT(*) FROM FollowerUser f WHERE u.id = f.id AND f.onStateChange >= $fromDate ) ");
    List<int> users = dbUsers.isNotEmpty
        ? dbUsers.map((a) {
            int id = a["id"];
            return id;
          }).toList()
        : [];
    return users;
  }

  Future<List<UserLikeCount>> getUsersSortedByLikeCount({
    @required int maxElems,
    @required bool asc,
    List<int> excluding,
    bool isValid = true,
  }) async {
    final db = await _dbProvider.database;
    var query = "SELECT *, COUNT(*) AS likeCount "
            "FROM User u, Like l "
            "WHERE l.isValid = " +
        (isValid ? '1' : '0') +
        " ";

    if (excluding != null && excluding.isNotEmpty) {
      String elements = excluding.join(', ');
      query += "AND u.id NOT IN ($elements) ";
    }

    query += "AND u.id = l.userId "
            "GROUP BY u.id "
            "ORDER BY COUNT(*) " +
        (asc ? "ASC" : "DESC") +
        " "
            "LIMIT $maxElems ";

    var dbUsers = await db.rawQuery(query);

    List<UserLikeCount> users = dbUsers.isNotEmpty
        ? dbUsers
            .map((a) => UserLikeCount(
                  user: User.fromMap(a),
                  likeCount: a['likeCount'],
                ))
            .toList()
        : [];
    return users;
  }

  Future<List<UserStoryViewCount>> getUsersSortedByStoryViewCount({
    @required int maxElems,
    @required bool asc,
    bool isValid = true,
  }) async {
    final db = await _dbProvider.database;
    var dbUsers = await db.rawQuery("SELECT *, COUNT(*) AS viewCount "
            "FROM User u, StoryView s "
            "WHERE s.isValid = " +
        (isValid ? '1' : '0') +
        " "
            "AND u.id = s.seenByUserId "
            "GROUP BY u.id "
            "ORDER BY COUNT(*) " +
        (asc ? "ASC" : "DESC") +
        " "
            "LIMIT $maxElems ");

    List<UserStoryViewCount> users = dbUsers.isNotEmpty
        ? dbUsers
            .map((a) => UserStoryViewCount(
                  user: User.fromMap(a),
                  viewCount: a['viewCount'],
                ))
            .toList()
        : [];
    return users;
  }

  Future<List<User>> getUsersSortedByStoryViewTime({
    @required int storyId,
    @required int maxElems,
    @required bool newestFirst,
    bool isValid = true,
  }) async {
    final db = await _dbProvider.database;
    var dbUsers =
        await db.rawQuery("SELECT u.id, u.username, u.fullName, u.isPrivate, "
                "u.isVerified, u.anonymousProfilePicture, u.profilePicUrl, u.createdAt "
                "FROM User u, StoryView s "
                "WHERE s.storyId = $storyId "
                "AND s.isValid = " +
            (isValid ? '1' : '0') +
            " "
                "AND u.id = s.seenByUserId "
                "ORDER BY s.createdAt " +
            (newestFirst ? "DESC" : "ASC") +
            " "
                "LIMIT $maxElems ");

    List<User> users =
        dbUsers.isNotEmpty ? dbUsers.map((a) => User.fromMap(a)).toList() : [];
    return users;
  }
}
