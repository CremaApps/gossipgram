part of 'ianalysis_bloc.dart';

@immutable
abstract class AnalysisEvent {}

class StartAnalysisEvent extends AnalysisEvent {}
