// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'dashboard_box.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$DashboardBoxTearOff {
  const _$DashboardBoxTearOff();

// ignore: unused_element
  _DashboardBox call(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated,
      Image icon}) {
    return _DashboardBox(
      title: title,
      value: value,
      redirectFunction: redirectFunction,
      isPaidFunctionality: isPaidFunctionality,
      updated: updated,
      icon: icon,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $DashboardBox = _$DashboardBoxTearOff();

/// @nodoc
mixin _$DashboardBox {
  String get title;
  int get value;
  Function get redirectFunction;
  bool get isPaidFunctionality;
  bool get updated;
  Image get icon;

  $DashboardBoxCopyWith<DashboardBox> get copyWith;
}

/// @nodoc
abstract class $DashboardBoxCopyWith<$Res> {
  factory $DashboardBoxCopyWith(
          DashboardBox value, $Res Function(DashboardBox) then) =
      _$DashboardBoxCopyWithImpl<$Res>;
  $Res call(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated,
      Image icon});
}

/// @nodoc
class _$DashboardBoxCopyWithImpl<$Res> implements $DashboardBoxCopyWith<$Res> {
  _$DashboardBoxCopyWithImpl(this._value, this._then);

  final DashboardBox _value;
  // ignore: unused_field
  final $Res Function(DashboardBox) _then;

  @override
  $Res call({
    Object title = freezed,
    Object value = freezed,
    Object redirectFunction = freezed,
    Object isPaidFunctionality = freezed,
    Object updated = freezed,
    Object icon = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed ? _value.title : title as String,
      value: value == freezed ? _value.value : value as int,
      redirectFunction: redirectFunction == freezed
          ? _value.redirectFunction
          : redirectFunction as Function,
      isPaidFunctionality: isPaidFunctionality == freezed
          ? _value.isPaidFunctionality
          : isPaidFunctionality as bool,
      updated: updated == freezed ? _value.updated : updated as bool,
      icon: icon == freezed ? _value.icon : icon as Image,
    ));
  }
}

/// @nodoc
abstract class _$DashboardBoxCopyWith<$Res>
    implements $DashboardBoxCopyWith<$Res> {
  factory _$DashboardBoxCopyWith(
          _DashboardBox value, $Res Function(_DashboardBox) then) =
      __$DashboardBoxCopyWithImpl<$Res>;
  @override
  $Res call(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated,
      Image icon});
}

/// @nodoc
class __$DashboardBoxCopyWithImpl<$Res> extends _$DashboardBoxCopyWithImpl<$Res>
    implements _$DashboardBoxCopyWith<$Res> {
  __$DashboardBoxCopyWithImpl(
      _DashboardBox _value, $Res Function(_DashboardBox) _then)
      : super(_value, (v) => _then(v as _DashboardBox));

  @override
  _DashboardBox get _value => super._value as _DashboardBox;

  @override
  $Res call({
    Object title = freezed,
    Object value = freezed,
    Object redirectFunction = freezed,
    Object isPaidFunctionality = freezed,
    Object updated = freezed,
    Object icon = freezed,
  }) {
    return _then(_DashboardBox(
      title: title == freezed ? _value.title : title as String,
      value: value == freezed ? _value.value : value as int,
      redirectFunction: redirectFunction == freezed
          ? _value.redirectFunction
          : redirectFunction as Function,
      isPaidFunctionality: isPaidFunctionality == freezed
          ? _value.isPaidFunctionality
          : isPaidFunctionality as bool,
      updated: updated == freezed ? _value.updated : updated as bool,
      icon: icon == freezed ? _value.icon : icon as Image,
    ));
  }
}

/// @nodoc
class _$_DashboardBox implements _DashboardBox {
  const _$_DashboardBox(
      {this.title,
      this.value,
      this.redirectFunction,
      this.isPaidFunctionality,
      this.updated,
      this.icon});

  @override
  final String title;
  @override
  final int value;
  @override
  final Function redirectFunction;
  @override
  final bool isPaidFunctionality;
  @override
  final bool updated;
  @override
  final Image icon;

  @override
  String toString() {
    return 'DashboardBox(title: $title, value: $value, redirectFunction: $redirectFunction, isPaidFunctionality: $isPaidFunctionality, updated: $updated, icon: $icon)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DashboardBox &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)) &&
            (identical(other.redirectFunction, redirectFunction) ||
                const DeepCollectionEquality()
                    .equals(other.redirectFunction, redirectFunction)) &&
            (identical(other.isPaidFunctionality, isPaidFunctionality) ||
                const DeepCollectionEquality()
                    .equals(other.isPaidFunctionality, isPaidFunctionality)) &&
            (identical(other.updated, updated) ||
                const DeepCollectionEquality()
                    .equals(other.updated, updated)) &&
            (identical(other.icon, icon) ||
                const DeepCollectionEquality().equals(other.icon, icon)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(value) ^
      const DeepCollectionEquality().hash(redirectFunction) ^
      const DeepCollectionEquality().hash(isPaidFunctionality) ^
      const DeepCollectionEquality().hash(updated) ^
      const DeepCollectionEquality().hash(icon);

  @override
  _$DashboardBoxCopyWith<_DashboardBox> get copyWith =>
      __$DashboardBoxCopyWithImpl<_DashboardBox>(this, _$identity);
}

abstract class _DashboardBox implements DashboardBox {
  const factory _DashboardBox(
      {String title,
      int value,
      Function redirectFunction,
      bool isPaidFunctionality,
      bool updated,
      Image icon}) = _$_DashboardBox;

  @override
  String get title;
  @override
  int get value;
  @override
  Function get redirectFunction;
  @override
  bool get isPaidFunctionality;
  @override
  bool get updated;
  @override
  Image get icon;
  @override
  _$DashboardBoxCopyWith<_DashboardBox> get copyWith;
}
