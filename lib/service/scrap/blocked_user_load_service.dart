import 'package:flutter/material.dart';
import 'package:GossipGram/database/repository/blocked_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user/blocked_user.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@singleton
class BlockedUserLoadService extends UserLoadService {
  static const String SERVICE_NAME = "blocked_user_load_service";

  BlockedUserRepository _blockedUserRepository;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  BlockedUserLoadService(
    this._blockedUserRepository,
    this._appConfig,
    this._localStorage,
    UserRepository userRepository,
  ) : super(userRepository);

  Future<void> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    int nowDate = DateTime.now().millisecondsSinceEpoch;
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);

    var blockedsResponse = await instagramPrivateApi.user.getBlockedUsers();
    DataLoadBloc.printProfiling("API blocked users media step loaded", now);

    var blockeds = blockedsResponse.response.blockedList;
    while (blockedsResponse.hasNext()) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));
      blockedsResponse = await blockedsResponse.next();
      blockeds.addAll(blockedsResponse.response.blockedList);
      DataLoadBloc.printProfiling("API blocked users media step loaded", now);
    }
    DataLoadBloc.printProfiling("API blocked users media all loaded", now);

    List<int> activeFollowerUserIds =
        await _blockedUserRepository.getBlockedUserIds(
      isValid: true,
    );

    for (int id in activeFollowerUserIds) {
      if (!blockeds.any((user) => user.userId == id)) {
        await _blockedUserRepository.upsertBlockedUser(
          user: BlockedUser(id: id, isValid: false, youFollow: false),
        );
      }
    }

    for (var blocked in blockeds) {
      Account.User user = Account.User(
        id: blocked.userId,
        username: blocked.username,
        fullName: blocked.fullName,
        isPrivate: false,
        isVerified: false,
        youFollow: false,
        profilePicUrl: blocked.profilePicUrl,
        anonymousProfilePicture: false,
      );

      await processUserIfNotAlreadyProcessed(user, processedUsers);

      BlockedUser dbUser =
          await _blockedUserRepository.getBlockedUser(id: user.id);
      int onStateChange = dbUser != null && dbUser.isValid == true
          ? dbUser.onStateChange
          : nowDate;

      //  List<int> activeFollowerUserIdsThatUDOntFollow =
      //     await _blockedUserRepository.getBlockedUserIds(
      //         isValid: true, notFollowed: true);

      // bool youFollowUser = false;
      // for (int id in activeFollowerUserIdsThatUDOntFollow) {
      //   if (dbUser.id == id) {
      //     youFollowUser == false;
      //   }
      // }

      await _blockedUserRepository.upsertBlockedUser(
        user: BlockedUser(
          id: user.id,
          isValid: true,
          youFollow: false,
          onStateChange: onStateChange,
        ),
      );
    }
    DataLoadBloc.printProfiling("Blocked users DB loaded", now);
  }
}
