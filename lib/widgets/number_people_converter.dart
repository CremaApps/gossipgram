import 'dart:math';
import "package:intl/intl.dart";

String IntNumberSymbolConverter(int number) {
  if (number >= 1000) {
    return NumberFormat.compactCurrency(
      decimalDigits: 2,
      symbol: '',
    ).format(number);
  }
  return number.toString();
}

String DoubleNumberSymbolConverter(double number, {String symbol = ''}) {
   return number == null ? '-' :
      number >= 1000 ? IntNumberSymbolConverter(number.floor()) + symbol:
      _RoundDouble(number, 2) + symbol;
}

String _RoundDouble(double value, int places){
  double mod = pow(10.0, places);
  double res = ((value * mod).round().toDouble() / mod);
  return res != res.toInt() ? res.toString() : res.toInt().toString();
}
