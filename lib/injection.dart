import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'injection.config.dart';
import 'mock/analytics_mock.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
)
GetIt configureDependencies() {
  GetIt instance = $initGetIt(getIt);

  if (kReleaseMode) {
    instance.registerSingleton(FirebaseAnalytics(), instanceName: 'analytics');
  } else {
    instance.registerSingleton(AnalyticsMock(), instanceName: 'analytics');
  }

  instance.registerSingleton(
    FirebaseAnalyticsObserver(
      analytics: getIt.get<FirebaseAnalytics>(instanceName: 'analytics'),
    ),
    instanceName: 'analyticsObserver',
  );

  return instance;
}