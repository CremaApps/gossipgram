import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user/followed_user.dart';
import 'package:GossipGram/model/objects/user/follower_user.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

part 'friendship_state.dart';
part 'friendship_cubit.freezed.dart';

class FriendshipCubit extends Cubit<FriendshipState> {
  final FollowedUserRepository followedUserRepository;
  final FollowerUserRepository followerUserRepository;
  final UserRepository userRepository;
  final InstagramPrivateApi _instagramPrivateApi =
      InstagramPrivateApi.instance();
  FriendshipCubit(this.followedUserRepository, this.followerUserRepository,
      this.userRepository)
      : super(FriendshipState.initial());

  void followUser(AnalysisUserItem analysisUserItemasync) async {
    emit(_FriendshipLoading());
    try {
      bool response = await _instagramPrivateApi.friendship
          .createFriendship("${analysisUserItemasync.userId}");
      if (response) {
        await followedUserRepository.upsertFollowedUser(
            fUser: FollowedUser(
                id: analysisUserItemasync.userId,
                isValid: true,
                youFollow: true));
        await followerUserRepository.updateFollowerUser(
            fUser: FollowerUser(
                id: analysisUserItemasync.userId,
                isValid: true,
                youFollow: true));
        await userRepository.updateUserYouFollowStatus(
            userId: analysisUserItemasync.userId, youFollow: true);

        emit(_FriendshipLoaded(true));
      } else {
        emit(_FriendshipError("error"));
      }
    } catch (e) {
      print(e);
      emit(_FriendshipError("error"));
    }
  }

  void unfollowUser(AnalysisUserItem analysisUserItem) async {
    emit(_FriendshipLoading());
    try {
      bool response = await _instagramPrivateApi.friendship
          .destroyFriendship("${analysisUserItem.userId}");
      if (response) {
        await followedUserRepository.upsertFollowedUser(
            fUser: FollowedUser(
                id: analysisUserItem.userId, isValid: true, youFollow: false));
        await followerUserRepository.updateFollowerUser(
            fUser: FollowerUser(
                id: analysisUserItem.userId, isValid: true, youFollow: false));
        await userRepository.updateUserYouFollowStatus(
            userId: analysisUserItem.userId, youFollow: false);

        emit(_FriendshipLoaded(false));
      } else {
        emit(_FriendshipError("error"));
      }
    } catch (e) {
      print(e);
      emit(_FriendshipError("error"));
    }
  }
}
