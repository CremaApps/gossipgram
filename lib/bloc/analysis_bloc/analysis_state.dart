part of 'ianalysis_bloc.dart';

@immutable
abstract class AnalysisState {}

class AnalysisInitial extends AnalysisState {}
class AnalysisLoadingState extends AnalysisState {}
class AnalysisLoadedState extends AnalysisState {
  final List<AnalysisUserItem> analysisResult;

  AnalysisLoadedState(this.analysisResult): super();
}
