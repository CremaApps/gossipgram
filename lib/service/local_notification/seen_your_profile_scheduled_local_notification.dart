part of 'local_notification_register_service.dart';

/* TODO No se si mantener las 3 schedules como funciones separadas o juntarlo todo en una genérica
     Porque ahora hacen lo mismo pero quizá queremos personalizarlas más adelante */

Future<void> _seenYourProfileScheduledLocalNotificationGeneric({
  @required BuildContext context,
  @required int notificationId,
  @required String payload,
  @required int seconds,
}) async {
  LocalNotificationService _localNotificationService = getIt.get<LocalNotificationService>();
  SubscriptionService _subscriptionService = getIt.get<SubscriptionService>();
  LocalStorage _localStorage = getIt.get<LocalStorage>();

  if(!_localStorage.hasData(
      LocalNotificationRegisterService.FIRST_NOTIFICATION_STARTUP_TIME,
      generalStorage: true
    )) {
    await _localStorage.write(
      LocalNotificationRegisterService.FIRST_NOTIFICATION_STARTUP_TIME,
      DateTime.now().millisecondsSinceEpoch,
      generalStorage: true,
    );
  }

  if(!_subscriptionService.isPayed) {
    int now = DateTime
        .now()
        .millisecondsSinceEpoch;
    int firstStartupTime = _localStorage.read(
      LocalNotificationRegisterService.FIRST_NOTIFICATION_STARTUP_TIME,
      generalStorage: true,
    );
    if (firstStartupTime + seconds * 1000 > now) {
      await _localNotificationService.sendScheduledNotification(
        notificationId: notificationId,
        title: S.of(context).notificationSeenYourProfileScheduledTitle,
        body: S.of(context).notificationSeenYourProfileScheduledBody,
        payload: payload,
        seconds: (seconds - (now - firstStartupTime) / 1000).ceil(),
      );
    }
  }
}

Future<void> _seenYourProfileScheduledLocalNotificationFirst(BuildContext context) async {
  AppConfig _appConfig = getIt.get<AppConfig>();
  await _seenYourProfileScheduledLocalNotificationGeneric(
    context: context,
    notificationId: LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_ID,
    payload: LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_PAYLOAD,
    seconds: 60*_appConfig.getInt(LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_MINUTES),
  );
}

Future<void> _seenYourProfileScheduledLocalNotificationSecond(BuildContext context) async {
  AppConfig _appConfig = getIt.get<AppConfig>();
  await _seenYourProfileScheduledLocalNotificationGeneric(
    context: context,
    notificationId: LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_ID,
    payload: LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_PAYLOAD,
    seconds: 60*_appConfig.getInt(LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_MINUTES),
  );
}

Future<void> _seenYourProfileScheduledLocalNotificationThird(BuildContext context) async {
  AppConfig _appConfig = getIt.get<AppConfig>();
  await _seenYourProfileScheduledLocalNotificationGeneric(
    context: context,
    notificationId: LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_ID,
    payload: LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_PAYLOAD,
    seconds: 60*_appConfig.getInt(LocalNotificationRegisterService.NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_MINUTES),
  );
}
