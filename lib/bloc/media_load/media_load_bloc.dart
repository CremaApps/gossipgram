import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/media_load/media_load_events.dart';
import 'package:GossipGram/bloc/media_load/media_load_state.dart';
import 'package:GossipGram/service/api/get_media_info_service.dart';
import 'package:injectable/injectable.dart';

@injectable
class MediaLoadBloc extends Bloc<MediaLoadEvent, MediaLoadState> {
  GetMediaInfoService _getMediaInfoService;

  MediaLoadBloc(
    this._getMediaInfoService,
  ) : super(MediaLoadInitial());

  @override
  Stream<MediaLoadState> mapEventToState(MediaLoadEvent event) async* {
    if (event is MediaLoadEventStart) {
      var mediaData = await _getMediaInfoService.execute(
        mediaId: event.mediaId,
      );

      yield MediaLoaded(mediaData: mediaData);
    } else {
      throw UnimplementedError();
    }
  }
}
