import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@injectable
class StoppedFollowingYouBloc extends IAnalysisBloc {
  final FollowerUserRepository _followerUserRepository;
  final UserRepository _userRepository;
  final AppConfig _appConfig;
  int _usersRelevantDataDate;

  StoppedFollowingYouBloc(
    this._followerUserRepository,
    this._userRepository,
    this._appConfig,
  ) {
    _usersRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig.getInt(DashboardBloc.USERS_RELEVANT_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;
  }

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      List<int> _userIds = await _followerUserRepository.getFollowerUserIds(
        stateChangedAtGreaterThan: _usersRelevantDataDate,
        isValid: false,
      );

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          _userIds.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
