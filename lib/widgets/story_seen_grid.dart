import 'dart:io';

import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/profile_load/profile_load_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:path_provider/path_provider.dart';

import 'my_stories_carousel.dart';
import 'my_story_popup.dart';

class StorySeenGrid extends StatefulWidget {
  final ProfileLoadBloc profileLoadBloc;
  final String emptyMessage;

  StorySeenGrid({
    @required this.profileLoadBloc,
    @required this.emptyMessage,
  });

  @override
  _ImageGridState createState() => _ImageGridState();
}

class _ImageGridState extends State<StorySeenGrid> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileLoadBloc, ProfileLoadState>(
        cubit: widget.profileLoadBloc,
        builder: (context, state) {
          if (state is ProfileLoadData &&
              (state is ProfileLoadFullyLoaded ||
                  state is ProfileLoadFeedLoading)) {
            if (state.profile.storyThumbnail.isNotEmpty) {
              return FutureBuilder(
                future: getApplicationDocumentsDirectory(),
                builder:
                    (BuildContext context, AsyncSnapshot<Directory> snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return Container(
                      child: GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: state.profile.storyThumbnail.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          mainAxisSpacing: 3,
                          crossAxisSpacing: 3,
                          childAspectRatio: 0.7,
                        ),
                        itemBuilder: (contxt, indx) {
                          return InkWell(
                            onTap: () {
                              showDialog(
                                context: context,
                                child: MyStoryPopupComponent(
                                  storyId: state.profile.storyThumbnail
                                      .elementAt(indx)
                                      .id,
                                  storyMediaType: state.profile.storyThumbnail
                                      .elementAt(indx)
                                      .mediaType,
                                  storyViewCount: state.profile.storyThumbnail
                                      .elementAt(indx)
                                      .viewCount,
                                  path: snapshot.data.path,
                                ),
                              );
                            },
                            child: _buildRow(
                              state.profile.storyThumbnail.elementAt(indx).id,
                              snapshot.data.path + "/images/",
                            ),
                          );
                        },
                      ),
                    );
                  }
                },
              );
            } else {
              return Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 50,
                    ),
                    Text(S.of(context).noStories,
                        style: appTextTheme.headline6),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(widget.emptyMessage,
                          style: appTextTheme.bodyText1,
                          textAlign: TextAlign.center),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Image.asset(
                      'assets/images/empty-stories.png',
                      fit: BoxFit.cover,
                      color: getThemeModeAsInt() == 0
                          ? null
                          : appColorScheme.onBackground,
                      height: 150,
                      width: 150,
                    ),
                  ],
                ),
              );
            }
          } else {
            return CenteredLoader(size: 'medium');
          }
        });
  }

  Widget _buildRow(int storyId, String path) {
    DecorationImage image;
    var filePathAndName = "$path$storyId.jpg";
    image = DecorationImage(
      image: FileImage(
        File(filePathAndName),
      ),
      fit: BoxFit.cover,
    );

    return Container(
      width: 110,
      height: 20,
      decoration: BoxDecoration(
        image: image,
      ),
    );
  }
}
