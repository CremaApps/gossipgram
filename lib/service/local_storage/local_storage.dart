import 'package:get_storage/get_storage.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:injectable/injectable.dart';

@singleton
class LocalStorage {
  GetStorage _storage;

  LocalStorage() {
    _storage = GetStorage();
  }

  T read<T>(String key, {bool generalStorage = false}) {
    return _storage.read(generalStorage ? key : _getUserKey(key));
  }

  bool hasData(String key, {bool generalStorage = false}) {
    return _storage.hasData(generalStorage ? key : _getUserKey(key));
  }

  Future<void> write(String key, dynamic value,
      {bool generalStorage = false}) async {
    return _storage.write(
      generalStorage ? key : _getUserKey(key),
      value,
    );
  }

  void writeInMemory(String key, dynamic value, {bool generalStorage = false}) {
    _storage.writeInMemory(generalStorage ? key : _getUserKey(key), value);
  }

  String _getUserKey(String key) {
    return key != DashboardBloc.USER_ID &&
            _storage.hasData(DashboardBloc.USER_ID)
        ? "${_storage.read(DashboardBloc.USER_ID)}_$key"
        : key;
  }
}
