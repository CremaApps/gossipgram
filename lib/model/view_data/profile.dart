import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/model/objects/story_thumbnail.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

part 'profile.freezed.dart';

@freezed
abstract class Profile with _$Profile {
  const factory Profile({
    String fullName,
    String profilePicUrl,
    String bio,
    int followers,
    int following,
    int relation,
    double engagementRate,
    double averageLikes,
    double followerRatio,
    List<FeedThumbnail> feedThumbnail,
    List<FeedThumbnail> likedFeedThumbnail,
    List<StoryThumbnail> storyThumbnail,
    InstagramResponse feedResponse,
  }) = _Profile;
}
