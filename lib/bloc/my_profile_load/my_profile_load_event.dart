part of 'my_profile_load_bloc.dart';

abstract class MyProfileLoadEvent extends Equatable {
  const MyProfileLoadEvent();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class MyProfileLoadEventStart extends MyProfileLoadEvent {}

class MyProfileLoadEventGetFeedPage extends MyProfileLoadEvent {}
