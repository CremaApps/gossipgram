import 'package:GossipGram/bloc/analysis_bloc/blocs/friendship_cubit.dart';
import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class FriendshipButton extends StatefulWidget {
  final AnalysisUserItem analysisUserItem;

  FriendshipButton({this.analysisUserItem});
  @override
  _FriendshipButtonState createState() => _FriendshipButtonState();
}

class _FriendshipButtonState extends State<FriendshipButton> {
  FriendshipCubit _friendshipCubit;
  bool youFollow;
  Color dividerColor;

  @override
  void initState() {
    super.initState();
    dividerColor =
        getThemeModeAsInt() == 0 ? Color(0xFFCCD1D6) : Color(0xFF829AC1);
    youFollow = widget.analysisUserItem.youFollow;

    _friendshipCubit = FriendshipCubit(
        GetIt.instance.get<FollowedUserRepository>(),
        GetIt.instance.get<FollowerUserRepository>(),
        GetIt.instance.get<UserRepository>());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FriendshipCubit, FriendshipState>(
      cubit: _friendshipCubit,
      listener: (context, state) {},
      builder: (context, state) {
        return state.when(
          initial: () {
            return youFollow ? unfollowButton() : followButton();
          },
          loading: () {
            return FractionallySizedBox(
              widthFactor: 0.26,
              child: Center(child: CircularProgressIndicator()),
            );
          },
          loaded: (succes) {
            if (!succes) {
              return followButton();
            } else {
              return unfollowButton();
            }
          },
          error: (errorMessage) {
            return youFollow ? unfollowButton() : followButton();
          },
        );
      },
    );
  }

  Widget followButton() {
    return FractionallySizedBox(
      widthFactor: 0.23,
      child: InkWell(
        onTap: () {
          _friendshipCubit.followUser(widget.analysisUserItem);
        },
        child: Container(
            alignment: Alignment.center,
            child: Container(
                height: 36,
                alignment: Alignment.center,
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: dividerColor),
                  borderRadius: BorderRadius.all(
                      Radius.circular(8.0) //         <--- border radius here
                      ),
                ),
                child: Text(
                  "Follow",
                  style: textThemeLight.button.copyWith(fontSize: 12.5),
                ))),
      ),
    );
  }

  Widget unfollowButton() {
    return FractionallySizedBox(
      widthFactor: 0.23,
      child: InkWell(
        onTap: () {
          _friendshipCubit.unfollowUser(widget.analysisUserItem);
        },
        child: Container(
            alignment: Alignment.center,
            child: Container(
                height: 36,
                alignment: Alignment.center,
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: dividerColor),
                  borderRadius: BorderRadius.all(
                      Radius.circular(8.0) //         <--- border radius here
                      ),
                ),
                child: Text(
                  "Unfollow",
                  style: textThemeLight.button.copyWith(fontSize: 12.5),
                ))),
      ),
    );
  }
}
