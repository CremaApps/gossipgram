// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'analysis_user_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AnalysisUserItemTearOff {
  const _$AnalysisUserItemTearOff();

// ignore: unused_element
  _AnalysisUserItem call(
      {int userId,
      String name,
      String userName,
      String imageUrl,
      bool youFollow}) {
    return _AnalysisUserItem(
      userId: userId,
      name: name,
      userName: userName,
      imageUrl: imageUrl,
      youFollow: youFollow,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AnalysisUserItem = _$AnalysisUserItemTearOff();

/// @nodoc
mixin _$AnalysisUserItem {
  int get userId;
  String get name;
  String get userName;
  String get imageUrl;
  bool get youFollow;

  $AnalysisUserItemCopyWith<AnalysisUserItem> get copyWith;
}

/// @nodoc
abstract class $AnalysisUserItemCopyWith<$Res> {
  factory $AnalysisUserItemCopyWith(
          AnalysisUserItem value, $Res Function(AnalysisUserItem) then) =
      _$AnalysisUserItemCopyWithImpl<$Res>;
  $Res call(
      {int userId,
      String name,
      String userName,
      String imageUrl,
      bool youFollow});
}

/// @nodoc
class _$AnalysisUserItemCopyWithImpl<$Res>
    implements $AnalysisUserItemCopyWith<$Res> {
  _$AnalysisUserItemCopyWithImpl(this._value, this._then);

  final AnalysisUserItem _value;
  // ignore: unused_field
  final $Res Function(AnalysisUserItem) _then;

  @override
  $Res call({
    Object userId = freezed,
    Object name = freezed,
    Object userName = freezed,
    Object imageUrl = freezed,
    Object youFollow = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as int,
      name: name == freezed ? _value.name : name as String,
      userName: userName == freezed ? _value.userName : userName as String,
      imageUrl: imageUrl == freezed ? _value.imageUrl : imageUrl as String,
      youFollow: youFollow == freezed ? _value.youFollow : youFollow as bool,
    ));
  }
}

/// @nodoc
abstract class _$AnalysisUserItemCopyWith<$Res>
    implements $AnalysisUserItemCopyWith<$Res> {
  factory _$AnalysisUserItemCopyWith(
          _AnalysisUserItem value, $Res Function(_AnalysisUserItem) then) =
      __$AnalysisUserItemCopyWithImpl<$Res>;
  @override
  $Res call(
      {int userId,
      String name,
      String userName,
      String imageUrl,
      bool youFollow});
}

/// @nodoc
class __$AnalysisUserItemCopyWithImpl<$Res>
    extends _$AnalysisUserItemCopyWithImpl<$Res>
    implements _$AnalysisUserItemCopyWith<$Res> {
  __$AnalysisUserItemCopyWithImpl(
      _AnalysisUserItem _value, $Res Function(_AnalysisUserItem) _then)
      : super(_value, (v) => _then(v as _AnalysisUserItem));

  @override
  _AnalysisUserItem get _value => super._value as _AnalysisUserItem;

  @override
  $Res call({
    Object userId = freezed,
    Object name = freezed,
    Object userName = freezed,
    Object imageUrl = freezed,
    Object youFollow = freezed,
  }) {
    return _then(_AnalysisUserItem(
      userId: userId == freezed ? _value.userId : userId as int,
      name: name == freezed ? _value.name : name as String,
      userName: userName == freezed ? _value.userName : userName as String,
      imageUrl: imageUrl == freezed ? _value.imageUrl : imageUrl as String,
      youFollow: youFollow == freezed ? _value.youFollow : youFollow as bool,
    ));
  }
}

/// @nodoc
class _$_AnalysisUserItem implements _AnalysisUserItem {
  const _$_AnalysisUserItem(
      {this.userId, this.name, this.userName, this.imageUrl, this.youFollow});

  @override
  final int userId;
  @override
  final String name;
  @override
  final String userName;
  @override
  final String imageUrl;
  @override
  final bool youFollow;

  @override
  String toString() {
    return 'AnalysisUserItem(userId: $userId, name: $name, userName: $userName, imageUrl: $imageUrl, youFollow: $youFollow)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AnalysisUserItem &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.userName, userName) ||
                const DeepCollectionEquality()
                    .equals(other.userName, userName)) &&
            (identical(other.imageUrl, imageUrl) ||
                const DeepCollectionEquality()
                    .equals(other.imageUrl, imageUrl)) &&
            (identical(other.youFollow, youFollow) ||
                const DeepCollectionEquality()
                    .equals(other.youFollow, youFollow)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(userName) ^
      const DeepCollectionEquality().hash(imageUrl) ^
      const DeepCollectionEquality().hash(youFollow);

  @override
  _$AnalysisUserItemCopyWith<_AnalysisUserItem> get copyWith =>
      __$AnalysisUserItemCopyWithImpl<_AnalysisUserItem>(this, _$identity);
}

abstract class _AnalysisUserItem implements AnalysisUserItem {
  const factory _AnalysisUserItem(
      {int userId,
      String name,
      String userName,
      String imageUrl,
      bool youFollow}) = _$_AnalysisUserItem;

  @override
  int get userId;
  @override
  String get name;
  @override
  String get userName;
  @override
  String get imageUrl;
  @override
  bool get youFollow;
  @override
  _$AnalysisUserItemCopyWith<_AnalysisUserItem> get copyWith;
}
