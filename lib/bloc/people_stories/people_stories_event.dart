part of 'people_stories_bloc.dart';

abstract class PeopleStoriesEvent extends Equatable {
  const PeopleStoriesEvent();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class PeopleStoriesStartLoadEvent extends PeopleStoriesEvent {}

class PeopleStoriesLoadEventGetStoryPage extends PeopleStoriesEvent {}
