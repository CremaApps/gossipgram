import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/story_view_sorted_users_load/story_view_sorted_users_load_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/objects/user_story_view_count.dart';
import 'package:GossipGram/pages/user_profile/user_profile_page.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/number_people_converter.dart';
import 'package:GossipGram/widgets/profile_picture.dart';
import 'package:lottie/lottie.dart';

class StoriesMostLeastSeenByUserPage extends StatefulWidget {
  final int selectedTab;
  const StoriesMostLeastSeenByUserPage({this.selectedTab});

  @override
  _StoriesMostLeastSeenByUserPageState createState() =>
      _StoriesMostLeastSeenByUserPageState();
}

class _StoriesMostLeastSeenByUserPageState
    extends State<StoriesMostLeastSeenByUserPage>
    with TickerProviderStateMixin {
  StoryViewSortedUsersLoadBloc _storyViewSortedUsersLoadBloc;

  @override
  void initState() {
    super.initState();

    _storyViewSortedUsersLoadBloc =
        GetIt.instance.get<StoryViewSortedUsersLoadBloc>();
    _storyViewSortedUsersLoadBloc.add(StoryViewSortedUsersLoadEventStart());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: widget.selectedTab != null ? widget.selectedTab : 0,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          elevation: 0.0,
          title: Text(
            S.of(context).seenYourStoryTheMostAndTheLeast,
            overflow: TextOverflow.ellipsis,
            style: appTextTheme.subtitle2,
          ),
          backgroundColor: appColorScheme.primary,
          bottom: TabBar(
            labelStyle: appTextTheme.subtitle2.copyWith(fontSize: 15),
            unselectedLabelStyle: appTextTheme.subtitle1.copyWith(fontSize: 15),
            labelColor: appColorScheme.secondary,
            unselectedLabelColor: appColorScheme.onPrimary,
            tabs: [
              Tab(text: S.of(context).Most),
              Tab(text: S.of(context).Least),
            ],
          ),
        ),
        body: BlocBuilder(
            cubit: _storyViewSortedUsersLoadBloc,
            builder: (context, state) {
              if (state is StoryViewSortedUsersLoadInitial) {
                return Center(
                  child: Container(
                    child: Lottie.asset(
                      'assets/animations/loader.json',
                      width: 100,
                      height: 100,
                    ),
                  ),
                );
              } else if (state is StoryViewSortedUsersLoadData) {
                return Column(
                  children: <Widget>[
                    Expanded(
                      child: TabBarView(
                        children: <Widget>[
                          Container(
                            margin:
                                EdgeInsetsDirectional.only(start: 15, end: 15),
                            child: Builder(
                              builder: (context) {
                                if (state
                                    .usersWhoMostStoryViewedYou.isNotEmpty) {
                                  return ListView(
                                    physics: ClampingScrollPhysics(),
                                    children: state.usersWhoMostStoryViewedYou
                                        .asMap()
                                        .entries
                                        .map(
                                          (MapEntry map) => _buildRow(
                                              map.key,
                                              state.usersWhoMostStoryViewedYou,
                                              true),
                                        )
                                        .toList(),
                                  );
                                } else {
                                  return Center(
                                    child: Container(
                                      margin: EdgeInsetsDirectional.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              3.2),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            S.of(context).nobodyFound,
                                            style: appTextTheme.headline6,
                                          ),
                                          Container(
                                            margin: EdgeInsetsDirectional.only(
                                                top: 10),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                            child: Text(
                                              S.of(context).noOneYet,
                                              style: appTextTheme.bodyText1,
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                          Container(
                            child: Builder(
                              builder: (context) {
                                if (state
                                    .usersWhoLeastStoryViewedYou.isNotEmpty) {
                                  return ListView(
                                    physics: ClampingScrollPhysics(),
                                    children: state.usersWhoLeastStoryViewedYou
                                        .asMap()
                                        .entries
                                        .map(
                                          (MapEntry map) => _buildRow(
                                              map.key,
                                              state.usersWhoLeastStoryViewedYou,
                                              false),
                                        )
                                        .toList(),
                                  );
                                } else {
                                  return Center(
                                    child: Container(
                                      margin: EdgeInsetsDirectional.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              3.2),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(S.of(context).nobodyFound,
                                              style: appTextTheme.headline6),
                                          Container(
                                            margin: EdgeInsetsDirectional.only(
                                                top: 10),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                            child: Text(
                                              S.of(context).noOneYet,
                                              style: appTextTheme.bodyText1,
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              } else {
                return Text(S.of(context).somethingWentWrong);
              }
            }),
      ),
    );
  }

  Widget _buildRow(
      int index, List<UserStoryViewCount> userList, bool mostLeast) {
    int viewsNumber = userList[index].viewCount;
    return ListTile(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => UserProfile(
            userId: userList[index].user.id,
            username: userList[index].user.username,
          ),
        ),
      ),
      leading: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "${index + 1}",
            style: appTextTheme.subtitle2,
          ),
          SizedBox(
            width: 8,
          ),
          Container(
            width: 48,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: ProfilePicture(
                profilePicUrl: userList[index].user.profilePicUrl,
              ),
            ),
          ),
        ],
      ),
      trailing: Container(
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
        decoration: BoxDecoration(
            border: Border.all(
                width: 0.5,
                color: getThemeModeAsInt() == 0
                    ? appColorScheme.onSurface
                    : appColorScheme.onPrimary),
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(right: 4),
              child: Icon(
                Icons.remove_red_eye,
                color: getThemeModeAsInt() == 0 ? Colors.black : Colors.white,
                size: 16,
              ),
            ),
            Text(IntNumberSymbolConverter(viewsNumber),
                style: appTextTheme.subtitle1),
          ],
        ),
      ),
      title: Text(
        (userList[index].user.fullName != null &&
                userList[index].user.fullName != "")
            ? userList[index].user.fullName
            : userList[index].user.username,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Text(
        userList[index].user.username,
      ),
    );
  }
}
