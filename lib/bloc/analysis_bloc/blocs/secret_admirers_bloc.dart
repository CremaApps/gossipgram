import 'dart:async';

import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/database/repository/comment_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/database/repository/story_view_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@injectable
class SecretAdmirersBloc extends IAnalysisBloc {
  final FollowerUserRepository _followerUserRepository;
  final CommentRepository _commentRepository;
  final StoryViewRepository _storyViewRepository;
  final LikeRepository _likeRepository;
  final UserRepository _userRepository;
  LocalStorage _localStorage;
  AppConfig _appConfig;

  SecretAdmirersBloc(
    this._followerUserRepository,
    this._commentRepository,
    this._storyViewRepository,
    this._likeRepository,
    this._userRepository,
    this._localStorage,
    this._appConfig,
  ) : super();

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();
      int _secretAdmirersRelevantDataDate = DateTime.now()
          .add(Duration(
            days: -_appConfig
                .getInt(DashboardBloc.SECRET_ADMIRERS_RELEVANT_DATA_THRESHOLD),
          ))
          .millisecondsSinceEpoch;

      List<int> _excludeUserIds = _localStorage.hasData(DashboardBloc.USER_ID,
              generalStorage: true)
          ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
          : [];

      List<int> usersLikedYouByDate = (await _likeRepository.getLikeUserIds(
        isValid: true,
        stateChangedAtGreaterThan: _secretAdmirersRelevantDataDate,
        excluding: _excludeUserIds,
      ));

      List<int> usersCommentedYouByDate =
          (await _commentRepository.getCommentUserIds(
        isValid: true,
        createdAtGreaterThan: _secretAdmirersRelevantDataDate,
        excluding: _excludeUserIds,
      ));

      List<int> usersLikedYouByFeedDate =
          (await _likeRepository.getLikeUserIdsWithFeedDate(
        isValid: true,
        feedCreationOriginalDateGreaterThan: _secretAdmirersRelevantDataDate,
        excluding: _excludeUserIds,
      ));

      List<int> usersCommentedYouByFeedDate =
          (await _commentRepository.getCommentUserIdsWithFeedDate(
        isValid: true,
        feedCreationOriginalDateGreaterThan: _secretAdmirersRelevantDataDate,
        excluding: _excludeUserIds,
      ));

      List<int> usersWatchedYourStories =
          (await _storyViewRepository.getStoryViewUserIds(
        createdAtGreaterThan: _secretAdmirersRelevantDataDate,
      ));

      List<int> followers = (await _followerUserRepository.getFollowerUserIds(
        isValid: true,
      ));

      List<int> users = [
        ...{
          ...[
            ...usersLikedYouByDate,
            ...usersCommentedYouByDate,
            ...usersLikedYouByFeedDate,
            ...usersCommentedYouByFeedDate,
            ...usersWatchedYourStories
          ]
        }
      ];

      List<int> secretAdmirers = [];
      for (int user in users) {
        if (!followers.any((element) => (element == user)))
          secretAdmirers.add(user);
      }

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          secretAdmirers.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
