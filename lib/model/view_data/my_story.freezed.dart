// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'my_story.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$MyStoryTearOff {
  const _$MyStoryTearOff();

// ignore: unused_element
  _MyStory call(
      {@required String userId,
      @required String storyPic,
      @required int likesNumber}) {
    return _MyStory(
      userId: userId,
      storyPic: storyPic,
      likesNumber: likesNumber,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $MyStory = _$MyStoryTearOff();

/// @nodoc
mixin _$MyStory {
  String get userId;
  String get storyPic;
  int get likesNumber;

  $MyStoryCopyWith<MyStory> get copyWith;
}

/// @nodoc
abstract class $MyStoryCopyWith<$Res> {
  factory $MyStoryCopyWith(MyStory value, $Res Function(MyStory) then) =
      _$MyStoryCopyWithImpl<$Res>;
  $Res call({String userId, String storyPic, int likesNumber});
}

/// @nodoc
class _$MyStoryCopyWithImpl<$Res> implements $MyStoryCopyWith<$Res> {
  _$MyStoryCopyWithImpl(this._value, this._then);

  final MyStory _value;
  // ignore: unused_field
  final $Res Function(MyStory) _then;

  @override
  $Res call({
    Object userId = freezed,
    Object storyPic = freezed,
    Object likesNumber = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as String,
      storyPic: storyPic == freezed ? _value.storyPic : storyPic as String,
      likesNumber:
          likesNumber == freezed ? _value.likesNumber : likesNumber as int,
    ));
  }
}

/// @nodoc
abstract class _$MyStoryCopyWith<$Res> implements $MyStoryCopyWith<$Res> {
  factory _$MyStoryCopyWith(_MyStory value, $Res Function(_MyStory) then) =
      __$MyStoryCopyWithImpl<$Res>;
  @override
  $Res call({String userId, String storyPic, int likesNumber});
}

/// @nodoc
class __$MyStoryCopyWithImpl<$Res> extends _$MyStoryCopyWithImpl<$Res>
    implements _$MyStoryCopyWith<$Res> {
  __$MyStoryCopyWithImpl(_MyStory _value, $Res Function(_MyStory) _then)
      : super(_value, (v) => _then(v as _MyStory));

  @override
  _MyStory get _value => super._value as _MyStory;

  @override
  $Res call({
    Object userId = freezed,
    Object storyPic = freezed,
    Object likesNumber = freezed,
  }) {
    return _then(_MyStory(
      userId: userId == freezed ? _value.userId : userId as String,
      storyPic: storyPic == freezed ? _value.storyPic : storyPic as String,
      likesNumber:
          likesNumber == freezed ? _value.likesNumber : likesNumber as int,
    ));
  }
}

/// @nodoc
class _$_MyStory with DiagnosticableTreeMixin implements _MyStory {
  const _$_MyStory(
      {@required this.userId,
      @required this.storyPic,
      @required this.likesNumber})
      : assert(userId != null),
        assert(storyPic != null),
        assert(likesNumber != null);

  @override
  final String userId;
  @override
  final String storyPic;
  @override
  final int likesNumber;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'MyStory(userId: $userId, storyPic: $storyPic, likesNumber: $likesNumber)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'MyStory'))
      ..add(DiagnosticsProperty('userId', userId))
      ..add(DiagnosticsProperty('storyPic', storyPic))
      ..add(DiagnosticsProperty('likesNumber', likesNumber));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MyStory &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.storyPic, storyPic) ||
                const DeepCollectionEquality()
                    .equals(other.storyPic, storyPic)) &&
            (identical(other.likesNumber, likesNumber) ||
                const DeepCollectionEquality()
                    .equals(other.likesNumber, likesNumber)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(storyPic) ^
      const DeepCollectionEquality().hash(likesNumber);

  @override
  _$MyStoryCopyWith<_MyStory> get copyWith =>
      __$MyStoryCopyWithImpl<_MyStory>(this, _$identity);
}

abstract class _MyStory implements MyStory {
  const factory _MyStory(
      {@required String userId,
      @required String storyPic,
      @required int likesNumber}) = _$_MyStory;

  @override
  String get userId;
  @override
  String get storyPic;
  @override
  int get likesNumber;
  @override
  _$MyStoryCopyWith<_MyStory> get copyWith;
}
