import 'package:firebase_analytics/firebase_analytics.dart';

class AnalyticsMock implements FirebaseAnalytics {
  @override
  FirebaseAnalyticsAndroid get android => null;

  @override
  Future<void> logAddPaymentInfo() async {
    print("Analytics Mock: logAddPaymentInfo()");
  }

  @override
  Future<void> logAddToCart(
      {String itemId,
      String itemName,
      String itemCategory,
      int quantity,
      double price,
      double value,
      String currency,
      String origin,
      String itemLocationId,
      String destination,
      String startDate,
      String endDate}) async {
    print("Analytics Mock: logAddToCart($itemId, $itemName, $itemCategory, $quantity, $price, $value, $currency, $origin, $itemLocationId, $destination, $startDate, $endDate)");
  }

  @override
  Future<void> logAddToWishlist(
      {String itemId,
      String itemName,
      String itemCategory,
      int quantity,
      double price,
      double value,
      String currency,
      String itemLocationId}) async {
    print("Analytics Mock: logAddToWishlist($itemId, $itemName, $itemCategory, $quantity, $price, $value, $currency, $itemLocationId)");
  }

  @override
  Future<void> logAppOpen() async {
    print("Analytics Mock: logAppOpen()");
  }

  @override
  Future<void> logBeginCheckout(
      {double value,
      String currency,
      String transactionId,
      int numberOfNights,
      int numberOfRooms,
      int numberOfPassengers,
      String origin,
      String destination,
      String startDate,
      String endDate,
      String travelClass}) async {
    print("Analytics Mock: logBeginCheckout($value, $currency, $transactionId, $numberOfNights, $numberOfRooms, $numberOfPassengers, $origin, $destination, $startDate, $endDate, $travelClass)");
  }

  @override
  Future<void> logCampaignDetails(
      {String source,
      String medium,
      String campaign,
      String term,
      String content,
      String aclid,
      String cp1}) async {
    print("Analytics Mock: logCampaignDetails($source, $medium, $campaign, $term, $content, $aclid, $cp1)");
  }

  @override
  Future<void> logEarnVirtualCurrency(
      {String virtualCurrencyName, num value}) async {
    print("Analytics Mock: logEarnVirtualCurrency($virtualCurrencyName, $value)");
  }

  @override
  Future<void> logEcommercePurchase(
      {String currency,
      double value,
      String transactionId,
      double tax,
      double shipping,
      String coupon,
      String location,
      int numberOfNights,
      int numberOfRooms,
      int numberOfPassengers,
      String origin,
      String destination,
      String startDate,
      String endDate,
      String travelClass}) async {
    print("Analytics Mock: logEcommercePurchase");
  }

  @override
  Future<void> logEvent({String name, Map<String, dynamic> parameters}) async {
    print("Analytics Mock: logEvent($name)");
  }

  @override
  Future<void> logGenerateLead({String currency, double value}) async {
    print("Analytics Mock: logGenerateLead($currency, $value)");
  }

  @override
  Future<void> logJoinGroup({String groupId}) async {
    print("Analytics Mock: logJoinGroup($groupId)");
  }

  @override
  Future<void> logLevelEnd({String levelName, int success}) async {
    print("Analytics Mock: logLevelEnd($levelName, $success)");
  }

  @override
  Future<void> logLevelStart({String levelName}) async {
    print("Analytics Mock: logLevelStart($levelName)");
  }

  @override
  Future<void> logLevelUp({int level, String character}) async {
    print("Analytics Mock: logLevelUp($level, $character)");
  }

  @override
  Future<void> logLogin({String loginMethod}) async {
    print("Analytics Mock: logLogin($loginMethod)");
  }

  @override
  Future<void> logPostScore({int score, int level, String character}) async {
    print("Analytics Mock: logPostScore($score, $level, $character)");
  }

  @override
  Future<void> logPresentOffer(
      {String itemId,
      String itemName,
      String itemCategory,
      int quantity,
      double price,
      double value,
      String currency,
      String itemLocationId}) async {
    print("Analytics Mock: logPresentOffer($itemId, $itemName, $itemCategory, $quantity, $price, $value, $currency, $itemLocationId)");
  }

  @override
  Future<void> logPurchaseRefund(
      {String currency, double value, String transactionId}) async {
    print("Analytics Mock: logPurchaseRefund($currency, $value, $transactionId)");
  }

  @override
  Future<void> logRemoveFromCart(
      {String itemId,
      String itemName,
      String itemCategory,
      int quantity,
      double price,
      double value,
      String currency,
      String origin,
      String itemLocationId,
      String destination,
      String startDate,
      String endDate}) async {
    print("Analytics Mock: logRemoveFromCart($itemId, $itemName, $itemCategory, $quantity, $price, $value, $currency, $origin, $itemLocationId, $destination, $startDate, $endDate)");
  }

  @override
  Future<void> logSearch(
      {String searchTerm,
      int numberOfNights,
      int numberOfRooms,
      int numberOfPassengers,
      String origin,
      String destination,
      String startDate,
      String endDate,
      String travelClass}) async {
    print("Analytics Mock: logSearch($searchTerm, $numberOfNights, $numberOfPassengers, $origin, $destination, $startDate, $endDate, $travelClass)");
  }

  @override
  Future<void> logSelectContent({String contentType, String itemId}) async {
    print("Analytics Mock: logSelectContent($contentType, $itemId)");
  }

  @override
  Future<void> logSetCheckoutOption(
      {int checkoutStep, String checkoutOption}) async {
    print("Analytics Mock: logSetCheckoutOption($checkoutStep, $checkoutOption)");
  }

  @override
  Future<void> logShare(
      {String contentType, String itemId, String method}) async {
    print("Analytics Mock: logShare($contentType, $itemId, $method)");
  }

  @override
  Future<void> logSignUp({String signUpMethod}) async {
    print("Analytics Mock: logSignUp($signUpMethod)");
  }

  @override
  Future<void> logSpendVirtualCurrency(
      {String itemName, String virtualCurrencyName, num value}) async {
    print("Analytics Mock: logSpendVirtualCurrency($itemName, $virtualCurrencyName, $value)");
  }

  @override
  Future<void> logTutorialBegin() async {
    print("Analytics Mock: logTutorialBegin()");
  }

  @override
  Future<void> logTutorialComplete() async {
    print("Analytics Mock: logTutorialComplete()");
  }

  @override
  Future<void> logUnlockAchievement({String id}) async {
    print("Analytics Mock: logUnlockAchievement($id)");
  }

  @override
  Future<void> logViewItem(
      {String itemId,
      String itemName,
      String itemCategory,
      String itemLocationId,
      double price,
      int quantity,
      String currency,
      double value,
      String flightNumber,
      int numberOfPassengers,
      int numberOfNights,
      int numberOfRooms,
      String origin,
      String destination,
      String startDate,
      String endDate,
      String searchTerm,
      String travelClass}) async {
    print(
        "logViewItem($itemId, $itemName, $itemCategory, $itemLocationId, $price, $quantity, $currency, $value, $flightNumber, $numberOfPassengers, $numberOfNights, $numberOfRooms, $origin, $destination, $startDate, $endDate, $searchTerm, $travelClass)");
  }

  @override
  Future<void> logViewItemList({String itemCategory}) async {
    print("Analytics Mock: logViewItemList($itemCategory)");
  }

  @override
  Future<void> logViewSearchResults({String searchTerm}) async {
    print("Analytics Mock: logViewSearchResults($searchTerm)");
  }

  @override
  Future<void> resetAnalyticsData() async {
    print("Analytics Mock: resetAnalyticsData()");
  }

  @override
  Future<void> setAnalyticsCollectionEnabled(bool enabled) async {
    print("Analytics Mock: setAnalyticsCollectionEnabled($enabled)");
  }

  @override
  Future<void> setCurrentScreen(
      {String screenName, String screenClassOverride = 'Flutter'}) async {
    print("Analytics Mock: setCurrentScreen($screenName, $screenClassOverride)");
  }

  @override
  Future<void> setUserId(String id) async {
    print("Analytics Mock: setUserId($id)");
  }

  @override
  Future<void> setUserProperty({String name, String value}) async {
    print("Analytics Mock: setUserProperty($name, $value)");
  }
}
