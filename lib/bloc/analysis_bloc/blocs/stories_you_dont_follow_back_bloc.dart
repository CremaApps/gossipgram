import 'dart:async';

import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/database/repository/story_view_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:injectable/injectable.dart';

@injectable
class StoriesYouDontFollowBackBloc extends IAnalysisBloc {
  final StoryViewRepository _storyViewRepository;
  final UserRepository _userRepository;

  StoriesYouDontFollowBackBloc(
    this._storyViewRepository,
    this._userRepository,
  ) : super();

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      List<int> notFollowedYouBack =
          await _storyViewRepository.getStoryViewUserIds(
        isValid: true,
        notFollowed: true,
      );

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          notFollowedYouBack.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
