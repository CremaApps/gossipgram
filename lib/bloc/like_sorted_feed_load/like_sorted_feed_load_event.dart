part of 'like_sorted_feed_load_bloc.dart';

abstract class LikeSortedFeedLoadEvent extends Equatable {
  const LikeSortedFeedLoadEvent();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class LikeSortedFeedLoadEventStart extends LikeSortedFeedLoadEvent {
  final int maxItems;

  LikeSortedFeedLoadEventStart({ @required this.maxItems });
}
