import 'package:freezed_annotation/freezed_annotation.dart';

part 'dashboard.freezed.dart';

@freezed
abstract class Dashboard with _$Dashboard {
  const factory Dashboard({
    String fullName,
    String username,
    String profilePicUrl,
    int followers,
    int following,
    int activeStories,
    int notFollowingBack,
    int notFollowerBack,
    int blockedUsers,
    int lostFollowers,
    int profileViews,
    int storyViews,
    int newFollowers,
    int nonFollowingAdmiredUsers,
    int secretAdmirers,
    int deletedLikesOrComments,
    int likedYouTheMost,
    int totalPosts,
  }) = _Dashboard;
}
