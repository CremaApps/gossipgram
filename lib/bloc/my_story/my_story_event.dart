part of 'my_story_bloc.dart';

abstract class MyStoriesEvent extends Equatable {
  const MyStoriesEvent();
}

class MyStoriesStartLoadEvent extends MyStoriesEvent {
  final int tabIndex;

  MyStoriesStartLoadEvent({
    this.tabIndex
  });

  @override
  // TODO: implement props
  List<Object> get props => [];
}
