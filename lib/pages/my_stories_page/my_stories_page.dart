import 'package:GossipGram/themes/themes.dart';
import 'package:GossipGram/widgets/my_stories_ordering_dialog.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/my_story/my_story_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/my_story_box.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:GossipGram/service/subscription/subscription_service.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:GossipGram/widgets/my_stories_carousel.dart';
import 'package:GossipGram/widgets/premium_popup.dart';
part 'my_stories_audience_title.dart';
part 'my_stories_audience_blocks.dart';
part 'my_stories_no_stories.dart';

class MyStoriesPage extends StatefulWidget {
  @override
  _MyStoriesPageState createState() => _MyStoriesPageState();
}

class _MyStoriesPageState extends State<MyStoriesPage> {
  MyStoriesBloc _myStoriesBloc;
  MyStoriesOrderingOptions selectedOrdering;

  @override
  void initState() {
    super.initState();
    _myStoriesBloc = GetIt.instance.get<MyStoriesBloc>();
    _myStoriesBloc.add(MyStoriesStartLoadEvent());
    selectedOrdering = MyStoriesOrderingOptions.newest;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MyStoriesBloc, MyStoriesState>(
        cubit: _myStoriesBloc,
        builder: (context, state) {
          return Scaffold(
              backgroundColor: appColorScheme.surface,
              appBar: AppBar(
                elevation: 0,
                centerTitle: false,
                title: Text(
                  S.of(context).yourStories,
                  style: appTextTheme.subtitle2,
                ),
                actions: [
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      S.of(context).sortBy,
                      style: appTextTheme.bodyText1,
                    ),
                  ),
                  actionText(selectedOrdering, state),
                ],
              ),
              body: RefreshIndicator(
                  onRefresh: () async {
                    _myStoriesBloc.add(MyStoriesStartLoadEvent());
                  },
                  child: buildBody(state)),
              bottomNavigationBar: buildBottomBar(state));
        });
  }

  Widget buildBody(MyStoriesState state) {
    if (state is MyStoriesLoading || state is MyStoriesInitial) {
      return CenteredLoader();
    } else if (state is MyStoriesLoaded) {
      if (state.stories.isNotEmpty) {
        return Container(
          padding: EdgeInsetsDirectional.only(top: 15, start: 15, end: 15),
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: [
              MyStoryCarousel(
                myStories: state.stories,
              ),
            ],
          ),
        );
      } else {
        return _StoriesAudienceNoStories();
      }
    }
    return Center(
      child: Text(S.of(context).wopsy),
    );
  }

  Widget buildBottomBar(MyStoriesState state) {
    if (state is MyStoriesLoading || state is MyStoriesInitial) {
      return SizedBox();
    } else if (state is MyStoriesLoaded) {
      return Container(
        height: MediaQuery.of(context).size.height / 6.5,
        child: Column(
          children: [
            _StoriesAudienceTitle(),
            Flexible(child: _StoriesAudienceBlocks(state.stories.isNotEmpty)),
          ],
        ),
      );
    }
    return SizedBox();
  }

  Widget actionText(
      MyStoriesOrderingOptions myStoriesOrderingOption, MyStoriesState state) {
    if (state is MyStoriesLoaded) {
      String title;
      switch (myStoriesOrderingOption) {
        case MyStoriesOrderingOptions.newest:
          title = S.of(context).mostRecent;
          break;
        case MyStoriesOrderingOptions.oldest:
          title = S.of(context).oldest;
          break;
        case MyStoriesOrderingOptions.mostSeen:
          title = S.of(context).mostPopular;
          break;
        case MyStoriesOrderingOptions.leastSeen:
          title = S.of(context).leastSeen;
          break;
        default:
          title = S.of(context).mostRecent;
          break;
      }
      Color dividerColor = getThemeModeAsInt() == 0
          ? Color(0xFFCCD1D6)
          : (state.stories.isNotEmpty ? Color(0xFF829AC1) : Color(0x50829AC1));
      return InkWell(
        onTap: state.stories.isNotEmpty
            ? () async {
                MyStoriesOrderingOptions ordering = await showDialog(
                  context: context,
                  builder: (_) => MyStoriesOrderingDialog(
                    currentOrdering: selectedOrdering,
                  ),
                );
                if (ordering != selectedOrdering) {
                  setState(() {
                    selectedOrdering = ordering;
                  });

                  _myStoriesBloc.add(MyStoriesStartLoadEvent(
                      tabIndex: selectedOrdering.index));
                }
              }
            : null,
        child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(horizontal: 12),
            child: Container(
                height: 42,
                alignment: Alignment.center,
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: dividerColor),
                  borderRadius: BorderRadius.all(
                      Radius.circular(8.0) //         <--- border radius here
                      ),
                ),
                child: Row(
                  children: [
                    Text(
                      title,
                      style: appTextTheme.button.copyWith(
                          color: state.stories.isNotEmpty
                              ? appColorScheme.secondary
                              : dividerColor),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down,
                      color: dividerColor,
                    )
                  ],
                ))),
      );
    } else {
      return SizedBox();
    }
  }
}
