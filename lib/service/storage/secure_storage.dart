import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class SecureStorage {
  static const String _cookieKey = 'INSTAGRAM_KEY';
  static const String COOKIE_SPLIT = ';';
  final FlutterSecureStorage _secureStorage = FlutterSecureStorage();

  Future<String> getInstagramCookie() {
    return _secureStorage.read(key: _cookieKey);
  }

  Future<void> setInstagramCookie(String cookie) {
    return _secureStorage.write(key: _cookieKey, value: cookie);
  }

  Future<void> clearSecureStorage() {
    return _secureStorage.deleteAll();
  }
}