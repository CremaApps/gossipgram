import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'my_story.freezed.dart';

@freezed
abstract class MyStory with _$MyStory {
  const factory MyStory({
    @required String userId,
    @required String storyPic,
    @required int likesNumber,
  }) = _MyStory;
}
