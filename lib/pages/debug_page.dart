import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/application.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/service/subscription/subscription_service.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/loading_overlay.dart';
import 'package:GossipGram/widgets/premium_popup.dart';
import 'package:localizely_sdk/localizely_sdk.dart';

class DebugPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Debug Page"),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsetsDirectional.only(bottom: 15, top: 15),
                child: Text(
                  "Translations",
                  style: TextStyle(
                    color: AppTheme.primaries['900'],
                    fontWeight: FontWeight.w400,
                    fontFamily: "Poppins",
                    fontStyle: FontStyle.normal,
                    fontSize: 25.0,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                child: Row(
                  children: [
                    Expanded(flex: 1, child: Text("Language:")),
                    Expanded(
                      flex: 3,
                      child: DropdownButton(
                        hint: Text('Change Language'),
                        isExpanded: true,
                        iconSize: 24,
                        elevation: 16,
                        items: S.delegate.supportedLocales.map((e) {
                          return DropdownMenuItem(
                            child: Text(e.toLanguageTag()),
                            value: e,
                          );
                        }).toList(),
                        onChanged: (Locale newLocale) {
                          IGViews.changeLocale(context, newLocale);
                          Navigator.of(context).pop();
                          Navigator.of(context).pushNamed('/home');
                        },
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: RaisedButton(
                        onPressed: () {
                          var overlay = LoadingOverlay.of(context);
                          overlay.during(
                              Localizely.updateTranslations().then((value) {
                            Fluttertoast.showToast(msg: 'Localizely updated');
                          }).catchError((error) {
                            Fluttertoast.showToast(msg: 'Error updating');
                          }));
                        },
                        child: Text(
                          "Refresh languages from Localizely",
                          style: TextStyle(
                            color: AppTheme.primaries['900'],
                            fontWeight: FontWeight.w900,
                            fontFamily: "Poppins",
                            fontStyle: FontStyle.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: RaisedButton(
                        onPressed: () {
                          Localizely.setPreRelease(true);
                          Fluttertoast.showToast(
                              msg: 'Localizely in pre-release mode');
                        },
                        child: Text(
                          "Set pre-release localizely",
                          style: TextStyle(
                            color: AppTheme.primaries['900'],
                            fontWeight: FontWeight.w900,
                            fontFamily: "Poppins",
                            fontStyle: FontStyle.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: RaisedButton(
                        onPressed: () {
                          Localizely.setPreRelease(false);
                          Fluttertoast.showToast(
                              msg: 'Localizely in release mode');
                        },
                        child: Text(
                          "Set release localizely",
                          style: TextStyle(
                            color: AppTheme.primaries['900'],
                            fontWeight: FontWeight.w900,
                            fontFamily: "Poppins",
                            fontStyle: FontStyle.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Divider(
                color: Colors.black,
              ),
              Container(
                padding: EdgeInsetsDirectional.only(bottom: 15, top: 15),
                child: Text(
                  "Subscriptions",
                  style: TextStyle(
                    color: AppTheme.primaries['900'],
                    fontWeight: FontWeight.w400,
                    fontFamily: "Poppins",
                    fontStyle: FontStyle.normal,
                    fontSize: 25.0,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: RaisedButton(
                        onPressed: () {
                          var overlay = LoadingOverlay.of(context);
                          overlay.during(GetIt.I
                              .get<SubscriptionService>()
                              .resetSubscription()
                              .then((value) {
                            Fluttertoast.showToast(msg: 'Subscription reset');
                          }).catchError((error) {
                            Fluttertoast.showToast(
                                msg: 'Error reset subscription');
                          }));
                        },
                        child: Text(
                          "Reset subscription",
                          style: TextStyle(
                            color: AppTheme.primaries['900'],
                            fontWeight: FontWeight.w900,
                            fontFamily: "Poppins",
                            fontStyle: FontStyle.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Divider(
                color: Colors.black,
              ),
              Container(
                padding: EdgeInsetsDirectional.only(bottom: 15, top: 15),
                child: Text(
                  "Other pages",
                  style: TextStyle(
                    color: AppTheme.primaries['900'],
                    fontWeight: FontWeight.w400,
                    fontFamily: "Poppins",
                    fontStyle: FontStyle.normal,
                    fontSize: 25.0,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/tutorial');
                        },
                        child: Text(
                          "Check tutorial",
                          style: TextStyle(
                            color: AppTheme.primaries['900'],
                            fontWeight: FontWeight.w900,
                            fontFamily: "Poppins",
                            fontStyle: FontStyle.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: RaisedButton(
                        onPressed: () {
                          showGeneralDialog(
                            context: context,
                            barrierColor:
                                appColorScheme.background, // background color
                            barrierDismissible:
                                false, // should dialog be dismissed when tapped outside// label for barrier
                            transitionDuration: Duration(
                                milliseconds:
                                    200), // how long it takes to popup dialog after button click
                            pageBuilder: (_, __, ___) {
                              // your widget implementation
                              return SizedBox.expand(
                                // makes widget fullscreen
                                child: PremiumPopup(
                                  callback: () {},
                                ),
                              );
                            },
                          );
                        },
                        child: Text(
                          "Paid page",
                          style: TextStyle(
                            color: AppTheme.primaries['900'],
                            fontWeight: FontWeight.w900,
                            fontFamily: "Poppins",
                            fontStyle: FontStyle.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
