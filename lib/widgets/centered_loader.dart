import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class CenteredLoader extends StatelessWidget {
  final String size;
  const CenteredLoader({this.size});

  @override
  Widget build(BuildContext context) {
    // double loaderSize;
    // switch (size) {
    //   case 'big':
    //     loaderSize = MediaQuery.of(context).size.height / 3;
    //     break;
    //   case 'medium':
    //     loaderSize = MediaQuery.of(context).size.height / 5;
    //     break;
    //   case 'small':
    //     loaderSize = MediaQuery.of(context).size.height / 7;
    //     break;
    //   default:
    //     loaderSize = MediaQuery.of(context).size.height / 3;
    //     break;
    // }
    return Center(
      child: Lottie.asset('assets/animations/loader.json',
          height: 100, width: 100),
    );
  }
}
