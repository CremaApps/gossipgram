import 'package:flutter/material.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;
import 'package:instagram_private_api/instagram_private_api.dart';

abstract class UserLoadService {
  @protected
  InstagramPrivateApi instagramPrivateApi;
  UserRepository _userRepository;

  UserLoadService(this._userRepository) {
    instagramPrivateApi = InstagramPrivateApi.instance();
  }

  void execute({
    @required ProcessedUsersWrapper processedUsers,
  });

  @protected
  Future<void> processUserIfNotAlreadyProcessed(
      Account.User user, ProcessedUsersWrapper processedUsers) async {
    if (!processedUsers.value.contains(user.id)) {
      processedUsers.value.add(user.id);
      await _userRepository.upsertUser(user: user);
    }
  }

  @protected
  Future<void> processUsersInBatchIfNotAlreadyProcessed(
      List<Account.User> user, ProcessedUsersWrapper processedUsers) async {
    List<Account.User> toProcessUsers =
        user.where((user) => !processedUsers.value.contains(user.id)).toList();
    await _userRepository.upsertUsers(users: toProcessUsers);
    for (Account.User u in toProcessUsers) {
      processedUsers.value.add(u.id);
    }
  }

  Future<dynamic> getFollowing() async {
    return await instagramPrivateApi.friendship
        .getFollowing(instagramPrivateApi.myUserId);
  }
}
