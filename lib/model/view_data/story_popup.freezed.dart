// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'story_popup.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$StoryPopupTearOff {
  const _$StoryPopupTearOff();

// ignore: unused_element
  _StoryPopup call({@required String storyAsset, @required int storyType}) {
    return _StoryPopup(
      storyAsset: storyAsset,
      storyType: storyType,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $StoryPopup = _$StoryPopupTearOff();

/// @nodoc
mixin _$StoryPopup {
  String get storyAsset;
  int get storyType;

  $StoryPopupCopyWith<StoryPopup> get copyWith;
}

/// @nodoc
abstract class $StoryPopupCopyWith<$Res> {
  factory $StoryPopupCopyWith(
          StoryPopup value, $Res Function(StoryPopup) then) =
      _$StoryPopupCopyWithImpl<$Res>;
  $Res call({String storyAsset, int storyType});
}

/// @nodoc
class _$StoryPopupCopyWithImpl<$Res> implements $StoryPopupCopyWith<$Res> {
  _$StoryPopupCopyWithImpl(this._value, this._then);

  final StoryPopup _value;
  // ignore: unused_field
  final $Res Function(StoryPopup) _then;

  @override
  $Res call({
    Object storyAsset = freezed,
    Object storyType = freezed,
  }) {
    return _then(_value.copyWith(
      storyAsset:
          storyAsset == freezed ? _value.storyAsset : storyAsset as String,
      storyType: storyType == freezed ? _value.storyType : storyType as int,
    ));
  }
}

/// @nodoc
abstract class _$StoryPopupCopyWith<$Res> implements $StoryPopupCopyWith<$Res> {
  factory _$StoryPopupCopyWith(
          _StoryPopup value, $Res Function(_StoryPopup) then) =
      __$StoryPopupCopyWithImpl<$Res>;
  @override
  $Res call({String storyAsset, int storyType});
}

/// @nodoc
class __$StoryPopupCopyWithImpl<$Res> extends _$StoryPopupCopyWithImpl<$Res>
    implements _$StoryPopupCopyWith<$Res> {
  __$StoryPopupCopyWithImpl(
      _StoryPopup _value, $Res Function(_StoryPopup) _then)
      : super(_value, (v) => _then(v as _StoryPopup));

  @override
  _StoryPopup get _value => super._value as _StoryPopup;

  @override
  $Res call({
    Object storyAsset = freezed,
    Object storyType = freezed,
  }) {
    return _then(_StoryPopup(
      storyAsset:
          storyAsset == freezed ? _value.storyAsset : storyAsset as String,
      storyType: storyType == freezed ? _value.storyType : storyType as int,
    ));
  }
}

/// @nodoc
class _$_StoryPopup with DiagnosticableTreeMixin implements _StoryPopup {
  const _$_StoryPopup({@required this.storyAsset, @required this.storyType})
      : assert(storyAsset != null),
        assert(storyType != null);

  @override
  final String storyAsset;
  @override
  final int storyType;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'StoryPopup(storyAsset: $storyAsset, storyType: $storyType)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'StoryPopup'))
      ..add(DiagnosticsProperty('storyAsset', storyAsset))
      ..add(DiagnosticsProperty('storyType', storyType));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _StoryPopup &&
            (identical(other.storyAsset, storyAsset) ||
                const DeepCollectionEquality()
                    .equals(other.storyAsset, storyAsset)) &&
            (identical(other.storyType, storyType) ||
                const DeepCollectionEquality()
                    .equals(other.storyType, storyType)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(storyAsset) ^
      const DeepCollectionEquality().hash(storyType);

  @override
  _$StoryPopupCopyWith<_StoryPopup> get copyWith =>
      __$StoryPopupCopyWithImpl<_StoryPopup>(this, _$identity);
}

abstract class _StoryPopup implements StoryPopup {
  const factory _StoryPopup(
      {@required String storyAsset, @required int storyType}) = _$_StoryPopup;

  @override
  String get storyAsset;
  @override
  int get storyType;
  @override
  _$StoryPopupCopyWith<_StoryPopup> get copyWith;
}
