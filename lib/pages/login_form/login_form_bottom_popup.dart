part of 'login_form.dart';

class _LoginBottomPopup extends StatelessWidget {
  const _LoginBottomPopup();
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 400,
        padding: EdgeInsetsDirectional.only(
          top: 10,
        ),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24.0),
            topRight: Radius.circular(24.0),
          ),
        ),
        child: Column(
          children: [
            Container(
              margin: EdgeInsetsDirectional.only(
                bottom: 20,
              ),
              width: MediaQuery.of(context).size.width / 5,
              height: 4,
              decoration: BoxDecoration(
                color: AppTheme.neutrals['400'],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.5,
              child: Text(S.of(context).ourCommitmentToPrivacy,
                  style: TextStyle(
                      color: AppTheme.primaries['900'],
                      fontWeight: FontWeight.w800,
                      fontFamily: "Poppins",
                      fontStyle: FontStyle.normal,
                      fontSize: 20.0),
                  textAlign: TextAlign.center),
            ),
            Container(
              margin: EdgeInsetsDirectional.only(top: 20),
              width: MediaQuery.of(context).size.width / 1.2,
              height: 1,
              decoration: BoxDecoration(
                color: AppTheme.primaries['50'],
              ),
            ),
            Container(
              margin: EdgeInsetsDirectional.only(
                top: MediaQuery.of(context).size.height / 100,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _listElement(Icons.security, S.of(context).private,
                      S.of(context).weDontShareYourData, context),
                  _listElement(Icons.lock, S.of(context).secure,
                      S.of(context).onlyYouKnowYourPassword, context),
                  _listElement(Icons.check_circle, S.of(context).trusted,
                      S.of(context).weDontTrackOrSend, context),
                ],
              ),
            )
          ],
        ));
  }
}

Widget _listElement(
    IconData icon, String title, String text, BuildContext context) {
  return Container(
    margin: EdgeInsetsDirectional.only(
      top: 15,
      bottom: 15,
    ),
    width: MediaQuery.of(context).size.width / 1.5,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 32,
          height: 32,
          child: Icon(
            icon,
            color: AppTheme.primaries['400'],
            size: 24,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: AppTheme.primaries['50'],
          ),
        ),
        Container(
          margin: EdgeInsetsDirectional.only(start: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text(
                  title,
                  style: TextStyle(
                    color: AppTheme.primaries['900'],
                    fontWeight: FontWeight.w800,
                    fontFamily: "Poppins",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsetsDirectional.only(
                    top: MediaQuery.of(context).size.height / 300),
                width: MediaQuery.of(context).size.width / 2,
                child: Text(
                  text,
                  style: TextStyle(
                    color: AppTheme.primaries['900'],
                    fontWeight: FontWeight.w400,
                    fontFamily: "Arial",
                    fontStyle: FontStyle.normal,
                    fontSize: 12.0,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    ),
  );
}
