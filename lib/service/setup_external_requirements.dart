import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

@lazySingleton
class SetupExternalRequirements {
  final InstagramPrivateApi _instagramPrivateApi =
      InstagramPrivateApi.instance();
  FirebaseAnalytics _firebaseAnalytics;
  AppsflyerSdk _appsflyerSdk;

  SetupExternalRequirements() {
    _firebaseAnalytics = GetIt.I.get(instanceName: 'analytics');
    _appsflyerSdk = GetIt.I.get(instanceName: 'appsFlyer');
  }

  Future<void> execute() async {
    var userRaw = (await _instagramPrivateApi.user
            .getAccountInfo(_instagramPrivateApi.myUserId))
        .response
        .user;

    _firebaseAnalytics.setUserProperty(
        name: 'username', value: userRaw.username);
    _firebaseAnalytics.setUserProperty(
        name: 'ig_id', value: userRaw.pk.toString());

    Purchases.setDisplayName(userRaw.username);
    Purchases.setAppsflyerID(await _appsflyerSdk.getAppsFlyerUID());
  }
}
