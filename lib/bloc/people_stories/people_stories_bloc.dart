import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:GossipGram/model/view_data/people_stories.dart';
import 'package:GossipGram/service/api/get_active_stories_service.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

part 'people_stories_event.dart';
part 'people_stories_state.dart';

@lazySingleton
class PeopleStoriesBloc extends Bloc<PeopleStoriesEvent, PeopleStoriesState> {
  List<Tray> _peopleStories;
  PeopleStories _shownStories;
  GetActiveStoriesService _getActiveStoriesService;
  DateTime _lastLoad;

  PeopleStoriesBloc(this._getActiveStoriesService)
      : super(PeopleStoriesInitial());

  @override
  Stream<PeopleStoriesState> mapEventToState(
    PeopleStoriesEvent event,
  ) async* {
    if (event is PeopleStoriesStartLoadEvent) {
      yield PeopleStoriesLoading();

      if (_lastLoad == null ||
          _lastLoad.isBefore(DateTime.now().subtract(Duration(hours: 1)))) {
        var peopleStoriesData = await _getActiveStoriesService.execute();
        _shownStories = PeopleStories(stories: peopleStoriesData["response"]);
        _peopleStories = peopleStoriesData["storyFeed"];
        _lastLoad = DateTime.now();
      }

      yield _peopleStories.isEmpty
          ? PeopleStoriesDataFullyLoaded(peopleStories: _shownStories)
          : PeopleStoriesDataLoading(peopleStories: _shownStories);
    } else if (event is PeopleStoriesLoadEventGetStoryPage) {
      yield PeopleStoriesDataPageLoading(peopleStories: _shownStories);

      var peopleStoriesData = await _getActiveStoriesService.execute(
        storyFeed: _peopleStories,
      );

      _shownStories = _shownStories.copyWith(
        stories: _shownStories.stories..addAll(peopleStoriesData["response"]),
      );
      _peopleStories = peopleStoriesData["storyFeed"];

      yield _peopleStories.isEmpty
          ? PeopleStoriesDataFullyLoaded(peopleStories: _shownStories)
          : PeopleStoriesDataLoading(peopleStories: _shownStories);
    }
  }
}
