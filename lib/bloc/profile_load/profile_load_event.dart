part of 'profile_load_bloc.dart';

abstract class ProfileLoadEvent extends Equatable {
  const ProfileLoadEvent();

  @override
  List<Object> get props => throw UnimplementedError();
}

class ProfileLoadEventStart extends ProfileLoadEvent {
  final int userId;

  const ProfileLoadEventStart({
    @required this.userId,
  });
}

class ProfileLoadEventGetFeedPage extends ProfileLoadEvent {}