import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/feed.dart';
import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class FeedRepository {
  final DbProvider _dbProvider;

  FeedRepository(this._dbProvider);

  Future<bool> existsFeed({@required String id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('Feed', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty;
  }

  Future<Feed> getFeed({@required String id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('Feed', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? Feed.fromMap(res.first) : null;
  }

  Future<List<Feed>> queryFeeds({
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
        'FROM Feed ';

    if (createdAtGreaterThan != null) {
      query += 'WHERE createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += createdAtGreaterThan == null ? 'WHERE ' : 'AND ';
      query += 'createdAt < ' + createdAtLesserThan.toString();
    }

    var dbFeeds = await db.rawQuery(query);
    List<Feed> feeds =
        dbFeeds.isNotEmpty ? dbFeeds.map((a) => Feed.fromMap(a)).toList() : [];
    return feeds;
  }

  Future<int> queryCountFeeds({
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
        'FROM Feed ';

    if (createdAtGreaterThan != null) {
      query += 'WHERE createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += createdAtGreaterThan == null ? 'WHERE ' : 'AND ';
      query += 'createdAt < ' + createdAtLesserThan.toString();
    }

    var dbFeedsCount = await db.rawQuery(query);

    return dbFeedsCount.first.values.first;
  }

  Future<int> upsertFeed({@required Feed feed}) async {
    final db = await _dbProvider.database;
    var dbFeed = await db.query(
      'Feed',
      where: 'id = ?',
      whereArgs: [feed.id],
    );
    if (dbFeed.isNotEmpty) {
      return db.update(
        'Feed',
        feed.toMap(),
        where: 'id = ?',
        whereArgs: [feed.id],
      );
    } else {
      return await db.insert('Feed', feed.toMap());
    }
  }

  Future<void> upsertFeeds({@required List<Feed> feeds}) async {
    final db = await _dbProvider.database;
    Batch batch = db.batch();
    var dbFeeds = await db.query(
      'Feed',
    );

    Set<Feed> allFeeds = Set<Feed>.from(
        dbFeeds.isNotEmpty ? dbFeeds.map((a) => Feed.fromMap(a)).toList() : []);

    Set<Feed> toUpdateFeeds = Set<Feed>.from(feeds)
        .where((element) => allFeeds.any((Feed e) => e.id == element.id))
        .toSet();
    Set<Feed> toInsertFeeds = Set<Feed>.from(feeds).difference(toUpdateFeeds);

    for (Feed feed in toUpdateFeeds) {
      batch.update(
        'Feed',
        feed.toMap(),
        where: 'id = ?',
        whereArgs: [feed.id],
      );
    }

    for (Feed feed in toInsertFeeds) {
      batch.insert('Feed', feed.toMap());
    }
    await batch.commit(noResult: true);
  }

  Future<int> deleteFeed({@required Feed feed}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'Feed',
      where: 'id = ?',
      whereArgs: [feed.id],
    );
  }

  Future<int> deleteFeedById({@required String id}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'Feed',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<List<String>> getFeedIds({
    int createdAtGreaterThan,
    int createdAtLesserThan,
    int originalDateGreaterThan,
    int originalDateLesserThan,
    int maxElems,
    bool newestFirst = false,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT id '
        'FROM Feed ';

    if (createdAtGreaterThan != null) {
      query += 'WHERE createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += createdAtGreaterThan == null ? 'WHERE ' : 'AND ';
      query += 'createdAt < ' + createdAtLesserThan.toString();
    }

    if (newestFirst) {
      query += "ORDER BY originalCreatedAtDate " +
          (newestFirst ? "DESC" : "ASC") +
          " ";
    }

    if (maxElems != null) query += "LIMIT $maxElems ";

    var queryResponse = await db.rawQuery(query);
    List<String> feedIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            String id = a['id'];
            return id;
          }).toList()
        : [];
    return feedIds;
  }
}
