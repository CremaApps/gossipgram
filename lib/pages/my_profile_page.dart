import 'package:GossipGram/bloc/dashboard/dashboard_state.dart';
import 'package:GossipGram/model/view_data/dashboard.dart';
import 'package:GossipGram/widgets/analytics_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:shimmer/shimmer.dart';

import '../bloc/my_profile_load/my_profile_load_bloc.dart';
import '../generated/l10n.dart';
import '../themes/themes.dart';
import '../widgets/my_image_grid.dart';
import '../widgets/profile_menu/top_bar.dart';

class MyProfilePage extends StatefulWidget {
  final Dashboard dashboard;

  const MyProfilePage({
    @required this.dashboard,
  }) : super();
  @override
  _MyProfilePageState createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage>
    with TickerProviderStateMixin {
  MyProfileLoadBloc _myProfileLoadBloc;
  ScrollController _controller;
  String _username = '';
  String bio;
  bool _lazyLoadWorking = false;

  String fullName;
  int followers;
  int following;
  String profilePictureUrl;

  double averageLikes;
  double followerRatio;
  double engagementRate;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _myProfileLoadBloc = GetIt.instance.get<MyProfileLoadBloc>();
    _myProfileLoadBloc.add(MyProfileLoadEventStart());
    _username = widget.dashboard.username;
    fullName = widget.dashboard.fullName;
    profilePictureUrl = widget.dashboard.profilePicUrl;
    followers = widget.dashboard.followers;
    following = widget.dashboard.following;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appColorScheme.background,
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          fullName,
          style: appTextTheme.subtitle2,
        ),
        elevation: 0,
      ),

      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            color: appColorScheme.background,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocBuilder<MyProfileLoadBloc, MyProfileLoadState>(
                  cubit: _myProfileLoadBloc,
                  builder: (context, state) {
                    if (state is MyProfileLoadProfileInfoLoaded) {
                      print("UserName ${state.myProfile.fullName}");
                      print("profilePicUrl ${state.myProfile.profilePicUrl}");
                      profilePictureUrl = state.myProfile.profilePicUrl;
                      fullName = state.myProfile.fullName;
                      followers = state.myProfile.followers;
                      following = state.myProfile.following;
                      bio = state.myProfile.bio;
                    }
                    if (state is MyProfileLoadFullyLoaded) {
                      print("UserName ${state.myProfile.fullName}");
                      print("profilePicUrl ${state.myProfile.profilePicUrl}");
                      profilePictureUrl = state.myProfile.profilePicUrl;
                      fullName = state.myProfile.fullName;
                      followers = state.myProfile.followers;
                      following = state.myProfile.following;
                      bio = state.myProfile.bio;
                    }

                    return TopBar(
                      loading: state is MyProfileLoadInitial ? true : false,
                      userName: _username,
                      profilePicUrl: profilePictureUrl,
                      fullName: fullName,
                      bio: bio ?? "",
                      followers: followers,
                      following: following,
                      clicableProfile: false,
                      dataLoadBloc: null,
                    );
                  },
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 8),
                  color: appColorScheme.primary,
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                  child: BlocBuilder<MyProfileLoadBloc, MyProfileLoadState>(
                    cubit: _myProfileLoadBloc,
                    builder: (context, state) {
                      if (state is MyProfileLoadInfoAnalyticsLoaded) {
                        averageLikes = state.myProfile.averageLikes;
                        followerRatio = state.myProfile.followerRatio;
                        engagementRate = state.myProfile.engagementRate;
                      }

                      return AnalyticsRow(
                          averageLikes, engagementRate, followerRatio);
                    },
                  ),
                ),
                ListView(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true, // <- added
                  primary: false, // <- added
                  children: <Widget>[
                    MyImageGrid(
                      profileLoadBloc: _myProfileLoadBloc,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),

      // BlocBuilder(
      //     cubit: _myProfileLoadBloc,
      //     builder: (context, state) {
      //       if (state is MyProfileLoadFeedLoading) {
      //         _controller.addListener(_scrollListener);
      //         _lazyLoadWorking = false;
      //       } else {
      //         _controller.removeListener(_scrollListener);
      //       }

      //       if (state is MyProfileLoadInitial) {
      //         return Center(child: CircularProgressIndicator());
      //       } else if (state is MyProfileLoadData) {
      //         return SafeArea(
      //           child: SingleChildScrollView(
      //             physics: ClampingScrollPhysics(),
      //             controller: _controller,
      //             child: Column(
      //               children: <Widget>[
      //                 Stack(
      //                   children: <Widget>[
      //                     Padding(
      //                       padding: const EdgeInsets.only(bottom: 20.0),
      //                       child: ClipPath(
      //                         child: Container(
      //                           height: 250,
      //                           decoration: BoxDecoration(
      //                             borderRadius: BorderRadius.only(
      //                               bottomLeft: Radius.circular(25),
      //                               bottomRight: Radius.circular(25),
      //                             ),
      //                           ),
      //                         ),
      //                       ),
      //                     ),
      //                     Column(
      //                       children: <Widget>[
      //                         Row(
      //                           mainAxisAlignment: MainAxisAlignment.start,
      //                           crossAxisAlignment: CrossAxisAlignment.start,
      //                           children: <Widget>[
      //                             Builder(
      //                               builder: (context) {
      //                                 if (widget.profilePictureUrl != '') {
      //                                   return Container(
      //                                       margin: EdgeInsetsDirectional.only(
      //                                         start: MediaQuery.of(context)
      //                                                 .size
      //                                                 .width /
      //                                             25,
      //                                         end: MediaQuery.of(context)
      //                                                 .size
      //                                                 .width /
      //                                             25,
      //                                       ),
      //                                       width: 70,
      //                                       height: 70,
      //                                       decoration: BoxDecoration(
      //                                         borderRadius: BorderRadius.all(
      //                                           Radius.circular(8),
      //                                         ),
      //                                         image: DecorationImage(
      //                                           image: NetworkImage(
      //                                               widget.profilePictureUrl),
      //                                         ),
      //                                         border: Border.all(
      //                                           color: Colors.white,
      //                                           width: 2.0,
      //                                         ),
      //                                       ));
      //                                 } else {
      //                                   return Shimmer.fromColors(
      //                                     baseColor: Colors.grey[300],
      //                                     highlightColor: Colors.grey[100],
      //                                     child: Container(
      //                                       margin: EdgeInsets.only(
      //                                         left: MediaQuery.of(context)
      //                                                 .size
      //                                                 .width /
      //                                             25,
      //                                         right: MediaQuery.of(context)
      //                                                 .size
      //                                                 .width /
      //                                             25,
      //                                       ),
      //                                       width: 70,
      //                                       height: 70,
      //                                       color: Colors.white,
      //                                     ),
      //                                   );
      //                                 }
      //                               },
      //                             ),
      //                             Container(
      //                               width:
      //                                   MediaQuery.of(context).size.width / 1.4,
      //                               child: Column(
      //                                 crossAxisAlignment:
      //                                     CrossAxisAlignment.start,
      //                                 children: <Widget>[
      //                                   Container(
      //                                     margin: EdgeInsets.only(bottom: 5),
      //                                     child: Text(
      //                                       state.myProfile.fullName,
      //                                       style: TextStyle(
      //                                         color: AppTheme.neutrals['900'],
      //                                         fontWeight: FontWeight.w700,
      //                                         fontFamily: "Poppins",
      //                                         fontStyle: FontStyle.normal,
      //                                         fontSize: 18.0,
      //                                       ),
      //                                     ),
      //                                   ),
      //                                   Container(
      //                                     height: 50,
      //                                     child: Text(
      //                                       state.myProfile.bio,
      //                                       overflow: TextOverflow.ellipsis,
      //                                       maxLines: 2,
      //                                       style: TextStyle(
      //                                         color:
      //                                             AppTheme.neutrals['main-1'],
      //                                         fontWeight: FontWeight.w400,
      //                                         fontFamily: "Poppins",
      //                                         height: 1.5,
      //                                         fontStyle: FontStyle.normal,
      //                                         fontSize: 12.0,
      //                                       ),
      //                                     ),
      //                                   ),
      //                                 ],
      //                               ),
      //                             )
      //                           ],
      //                         ),
      //                         Container(
      //                           margin: EdgeInsetsDirectional.only(top: 25),
      //                           child: Row(
      //                             mainAxisAlignment: MainAxisAlignment.start,
      //                             crossAxisAlignment: CrossAxisAlignment.center,
      //                             children: <Widget>[
      //                               Row(
      //                                 children: <Widget>[
      //                                   Container(
      //                                     margin: EdgeInsetsDirectional.only(
      //                                       start: MediaQuery.of(context)
      //                                               .size
      //                                               .width /
      //                                           25,
      //                                     ),
      //                                     child: Text(
      //                                       IntNumberSymbolConverter(
      //                                           state.myProfile.followers),
      //                                       style: TextStyle(
      //                                           color: AppTheme.neutrals['900'],
      //                                           fontWeight: FontWeight.w400,
      //                                           fontFamily: "Poppins",
      //                                           fontStyle: FontStyle.normal,
      //                                           fontSize: 18.0),
      //                                     ),
      //                                   ),
      //                                   Container(
      //                                     margin: EdgeInsets.only(
      //                                       left: MediaQuery.of(context)
      //                                               .size
      //                                               .width /
      //                                           50,
      //                                     ),
      //                                     child: Text(
      //                                       S.of(context).followers,
      //                                       style: TextStyle(
      //                                           color: AppTheme.neutrals['900'],
      //                                           fontWeight: FontWeight.w400,
      //                                           fontFamily: "Poppins",
      //                                           fontStyle: FontStyle.normal,
      //                                           fontSize: 12.0),
      //                                     ),
      //                                   ),
      //                                   Container(
      //                                     margin: EdgeInsetsDirectional.only(
      //                                       start: MediaQuery.of(context)
      //                                               .size
      //                                               .width /
      //                                           25,
      //                                     ),
      //                                     child: Text(
      //                                       IntNumberSymbolConverter(
      //                                           state.myProfile.following),
      //                                       style: TextStyle(
      //                                           color: AppTheme.neutrals['900'],
      //                                           fontWeight: FontWeight.w400,
      //                                           fontFamily: "Poppins",
      //                                           fontStyle: FontStyle.normal,
      //                                           fontSize: 18.0),
      //                                     ),
      //                                   ),
      //                                   Container(
      //                                     margin: EdgeInsets.only(
      //                                       left: MediaQuery.of(context)
      //                                               .size
      //                                               .width /
      //                                           50,
      //                                     ),
      //                                     child: Text(
      //                                       S.of(context).following,
      //                                       style: TextStyle(
      //                                           color: AppTheme.neutrals['900'],
      //                                           fontWeight: FontWeight.w400,
      //                                           fontFamily: "Poppins",
      //                                           fontStyle: FontStyle.normal,
      //                                           fontSize: 12.0),
      //                                     ),
      //                                   ),
      //                                 ],
      //                               ),
      //                             ],
      //                           ),
      //                         ),
      //                         Row(
      //                           mainAxisAlignment:
      //                               MainAxisAlignment.spaceEvenly,
      //                           crossAxisAlignment: CrossAxisAlignment.center,
      //                           children: <Widget>[
      //                             Container(
      //                                 child: Column(
      //                                   mainAxisAlignment:
      //                                       MainAxisAlignment.spaceAround,
      //                                   crossAxisAlignment:
      //                                       CrossAxisAlignment.start,
      //                                   children: <Widget>[
      //                                     Text(
      //                                       S.of(context).engagementRate,
      //                                       style: TextStyle(
      //                                         color: AppTheme.neutrals['900'],
      //                                         fontWeight: FontWeight.w400,
      //                                         fontFamily: "Poppins",
      //                                         fontStyle: FontStyle.normal,
      //                                         fontSize: 12.0,
      //                                       ),
      //                                     ),
      //                                     Text(
      //                                       DoubleNumberSymbolConverter(
      //                                           state.myProfile.engagementRate,
      //                                           symbol: '%'),
      //                                       style: TextStyle(
      //                                           color: AppTheme.neutrals['900'],
      //                                           fontWeight: FontWeight.w700,
      //                                           fontFamily: "Poppins",
      //                                           fontStyle: FontStyle.normal,
      //                                           fontSize: 16.0),
      //                                     )
      //                                   ],
      //                                 ),
      //                                 padding: EdgeInsets.all(10),
      //                                 width: 110,
      //                                 height: 92,
      //                                 margin: EdgeInsets.only(top: 20),
      //                                 decoration: BoxDecoration(
      //                                     borderRadius: BorderRadius.all(
      //                                         Radius.circular(8)),
      //                                     boxShadow: [
      //                                       BoxShadow(
      //                                           color: const Color(0x5a1e2021),
      //                                           offset: Offset(0, 2),
      //                                           blurRadius: 6,
      //                                           spreadRadius: 0)
      //                                     ],
      //                                     color: AppTheme.neutrals['50'])),
      //                             Container(
      //                                 width: 110,
      //                                 height: 92,
      //                                 padding: EdgeInsets.all(10),
      //                                 child: Column(
      //                                   mainAxisAlignment:
      //                                       MainAxisAlignment.spaceAround,
      //                                   crossAxisAlignment:
      //                                       CrossAxisAlignment.start,
      //                                   children: <Widget>[
      //                                     Text(
      //                                       S.of(context).averageLikes,
      //                                       style: TextStyle(
      //                                         color:
      //                                             AppTheme.neutrals['main-1'],
      //                                         fontWeight: FontWeight.w400,
      //                                         fontFamily: "Poppins",
      //                                         fontStyle: FontStyle.normal,
      //                                         fontSize: 12.0,
      //                                       ),
      //                                     ),
      //                                     Text(
      //                                       DoubleNumberSymbolConverter(
      //                                           state.myProfile.averageLikes),
      //                                       style: TextStyle(
      //                                           color:
      //                                               AppTheme.neutrals['main-1'],
      //                                           fontWeight: FontWeight.w700,
      //                                           fontFamily: "Poppins",
      //                                           fontStyle: FontStyle.normal,
      //                                           fontSize: 16.0),
      //                                     )
      //                                   ],
      //                                 ),
      //                                 margin: EdgeInsets.only(top: 20),
      //                                 decoration: BoxDecoration(
      //                                     borderRadius: BorderRadius.all(
      //                                         Radius.circular(8)),
      //                                     boxShadow: [
      //                                       BoxShadow(
      //                                           color: const Color(0x5a1e2021),
      //                                           offset: Offset(0, 2),
      //                                           blurRadius: 6,
      //                                           spreadRadius: 0)
      //                                     ],
      //                                     color: AppTheme.neutrals['50'])),
      //                             Container(
      //                                 child: Column(
      //                                   mainAxisAlignment:
      //                                       MainAxisAlignment.spaceAround,
      //                                   crossAxisAlignment:
      //                                       CrossAxisAlignment.start,
      //                                   children: <Widget>[
      //                                     Text(
      //                                       S.of(context).followerRatio,
      //                                       style: TextStyle(
      //                                         color:
      //                                             AppTheme.neutrals['main-1'],
      //                                         fontWeight: FontWeight.w400,
      //                                         fontFamily: "Poppins",
      //                                         fontStyle: FontStyle.normal,
      //                                         fontSize: 12.0,
      //                                       ),
      //                                     ),
      //                                     Text(
      //                                       DoubleNumberSymbolConverter(
      //                                           state.myProfile.followerRatio),
      //                                       style: TextStyle(
      //                                           color:
      //                                               AppTheme.neutrals['main-1'],
      //                                           fontWeight: FontWeight.w700,
      //                                           fontFamily: "Poppins",
      //                                           fontStyle: FontStyle.normal,
      //                                           fontSize: 16.0),
      //                                     )
      //                                   ],
      //                                 ),
      //                                 width: 110,
      //                                 height: 92,
      //                                 padding: EdgeInsets.all(10),
      //                                 margin: EdgeInsets.only(top: 20),
      //                                 decoration: BoxDecoration(
      //                                     borderRadius: BorderRadius.all(
      //                                         Radius.circular(8)),
      //                                     boxShadow: [
      //                                       BoxShadow(
      //                                           color: const Color(0x5a1e2021),
      //                                           offset: Offset(0, 2),
      //                                           blurRadius: 6,
      //                                           spreadRadius: 0)
      //                                     ],
      //                                     color: AppTheme.neutrals['50']))
      //                           ],
      //                         ),
      //                       ],
      //                     )
      //                   ],
      //                 ),
      //                 ListView(
      //                   physics: ClampingScrollPhysics(),
      //                   shrinkWrap: true, // <- added
      //                   primary: false, // <- added
      //                   children: <Widget>[
      //                     MyImageGrid(
      //                       profileLoadBloc: _myProfileLoadBloc,
      //                     )
      //                   ],
      //                 ),
      //               ],
      //             ),
      //           ),
      //         );
      //       } else {
      //         return Text(S.of(context).somethingWentWrong);
      //       }
      //     }),
    );
  }

  void _scrollListener() {
    if (!_lazyLoadWorking &&
        _controller.offset >= _controller.position.maxScrollExtent / 2 &&
        !_controller.position.outOfRange) {
      _lazyLoadWorking = true;
      _myProfileLoadBloc.add(
        MyProfileLoadEventGetFeedPage(),
      );
    }
  }
}
