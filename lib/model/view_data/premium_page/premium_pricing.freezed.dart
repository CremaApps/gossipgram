// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'premium_pricing.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PremiumPricingTearOff {
  const _$PremiumPricingTearOff();

// ignore: unused_element
  _PremiumPricing call(
      {Package package,
      String title,
      String savePercentage,
      String monthlyPrice,
      String totalPrice,
      String boxLabel}) {
    return _PremiumPricing(
      package: package,
      title: title,
      savePercentage: savePercentage,
      monthlyPrice: monthlyPrice,
      totalPrice: totalPrice,
      boxLabel: boxLabel,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PremiumPricing = _$PremiumPricingTearOff();

/// @nodoc
mixin _$PremiumPricing {
  Package get package;
  String get title;
  String get savePercentage;
  String get monthlyPrice;
  String get totalPrice;
  String get boxLabel;

  $PremiumPricingCopyWith<PremiumPricing> get copyWith;
}

/// @nodoc
abstract class $PremiumPricingCopyWith<$Res> {
  factory $PremiumPricingCopyWith(
          PremiumPricing value, $Res Function(PremiumPricing) then) =
      _$PremiumPricingCopyWithImpl<$Res>;
  $Res call(
      {Package package,
      String title,
      String savePercentage,
      String monthlyPrice,
      String totalPrice,
      String boxLabel});
}

/// @nodoc
class _$PremiumPricingCopyWithImpl<$Res>
    implements $PremiumPricingCopyWith<$Res> {
  _$PremiumPricingCopyWithImpl(this._value, this._then);

  final PremiumPricing _value;
  // ignore: unused_field
  final $Res Function(PremiumPricing) _then;

  @override
  $Res call({
    Object package = freezed,
    Object title = freezed,
    Object savePercentage = freezed,
    Object monthlyPrice = freezed,
    Object totalPrice = freezed,
    Object boxLabel = freezed,
  }) {
    return _then(_value.copyWith(
      package: package == freezed ? _value.package : package as Package,
      title: title == freezed ? _value.title : title as String,
      savePercentage: savePercentage == freezed
          ? _value.savePercentage
          : savePercentage as String,
      monthlyPrice: monthlyPrice == freezed
          ? _value.monthlyPrice
          : monthlyPrice as String,
      totalPrice:
          totalPrice == freezed ? _value.totalPrice : totalPrice as String,
      boxLabel: boxLabel == freezed ? _value.boxLabel : boxLabel as String,
    ));
  }
}

/// @nodoc
abstract class _$PremiumPricingCopyWith<$Res>
    implements $PremiumPricingCopyWith<$Res> {
  factory _$PremiumPricingCopyWith(
          _PremiumPricing value, $Res Function(_PremiumPricing) then) =
      __$PremiumPricingCopyWithImpl<$Res>;
  @override
  $Res call(
      {Package package,
      String title,
      String savePercentage,
      String monthlyPrice,
      String totalPrice,
      String boxLabel});
}

/// @nodoc
class __$PremiumPricingCopyWithImpl<$Res>
    extends _$PremiumPricingCopyWithImpl<$Res>
    implements _$PremiumPricingCopyWith<$Res> {
  __$PremiumPricingCopyWithImpl(
      _PremiumPricing _value, $Res Function(_PremiumPricing) _then)
      : super(_value, (v) => _then(v as _PremiumPricing));

  @override
  _PremiumPricing get _value => super._value as _PremiumPricing;

  @override
  $Res call({
    Object package = freezed,
    Object title = freezed,
    Object savePercentage = freezed,
    Object monthlyPrice = freezed,
    Object totalPrice = freezed,
    Object boxLabel = freezed,
  }) {
    return _then(_PremiumPricing(
      package: package == freezed ? _value.package : package as Package,
      title: title == freezed ? _value.title : title as String,
      savePercentage: savePercentage == freezed
          ? _value.savePercentage
          : savePercentage as String,
      monthlyPrice: monthlyPrice == freezed
          ? _value.monthlyPrice
          : monthlyPrice as String,
      totalPrice:
          totalPrice == freezed ? _value.totalPrice : totalPrice as String,
      boxLabel: boxLabel == freezed ? _value.boxLabel : boxLabel as String,
    ));
  }
}

/// @nodoc
class _$_PremiumPricing implements _PremiumPricing {
  const _$_PremiumPricing(
      {this.package,
      this.title,
      this.savePercentage,
      this.monthlyPrice,
      this.totalPrice,
      this.boxLabel});

  @override
  final Package package;
  @override
  final String title;
  @override
  final String savePercentage;
  @override
  final String monthlyPrice;
  @override
  final String totalPrice;
  @override
  final String boxLabel;

  @override
  String toString() {
    return 'PremiumPricing(package: $package, title: $title, savePercentage: $savePercentage, monthlyPrice: $monthlyPrice, totalPrice: $totalPrice, boxLabel: $boxLabel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PremiumPricing &&
            (identical(other.package, package) ||
                const DeepCollectionEquality()
                    .equals(other.package, package)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.savePercentage, savePercentage) ||
                const DeepCollectionEquality()
                    .equals(other.savePercentage, savePercentage)) &&
            (identical(other.monthlyPrice, monthlyPrice) ||
                const DeepCollectionEquality()
                    .equals(other.monthlyPrice, monthlyPrice)) &&
            (identical(other.totalPrice, totalPrice) ||
                const DeepCollectionEquality()
                    .equals(other.totalPrice, totalPrice)) &&
            (identical(other.boxLabel, boxLabel) ||
                const DeepCollectionEquality()
                    .equals(other.boxLabel, boxLabel)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(package) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(savePercentage) ^
      const DeepCollectionEquality().hash(monthlyPrice) ^
      const DeepCollectionEquality().hash(totalPrice) ^
      const DeepCollectionEquality().hash(boxLabel);

  @override
  _$PremiumPricingCopyWith<_PremiumPricing> get copyWith =>
      __$PremiumPricingCopyWithImpl<_PremiumPricing>(this, _$identity);
}

abstract class _PremiumPricing implements PremiumPricing {
  const factory _PremiumPricing(
      {Package package,
      String title,
      String savePercentage,
      String monthlyPrice,
      String totalPrice,
      String boxLabel}) = _$_PremiumPricing;

  @override
  Package get package;
  @override
  String get title;
  @override
  String get savePercentage;
  @override
  String get monthlyPrice;
  @override
  String get totalPrice;
  @override
  String get boxLabel;
  @override
  _$PremiumPricingCopyWith<_PremiumPricing> get copyWith;
}
