part of 'background_task_register_service.dart';

Future<void> _loadFeedInitialBufferBackgroundTask() async {
  int now = DateTime.now().millisecondsSinceEpoch;

  int processedFeedElements = 0;
  LocalStorage localStorage = getIt.get<LocalStorage>();
  FeedRepository feedRepository = getIt.get<FeedRepository>();
  LikeLoadService likeLoadService = getIt.get<LikeLoadService>();
  CommentLoadService commentLoadService = getIt.get<CommentLoadService>();
  AppConfig appConfig = getIt.get<AppConfig>();
  List<dynamic> feedProcessBuffer =
      localStorage.read<List<dynamic>>(FeedLoadService.FEED_PROCESS_BUFFER);
  ProcessedUsersWrapper processedUsers = ProcessedUsersWrapper([]);

  while (feedProcessBuffer != null &&
      feedProcessBuffer.isNotEmpty &&
      processedFeedElements <
          appConfig.getInt(AppConfig.BACKGROUND_MAX_FEED_PROCESSING) &&
      ((DateTime.now().millisecondsSinceEpoch - now) / 1000).ceil() <
          (30 - appConfig.getInt(AppConfig.BACKGROUND_SAFE_SECONDS))) {
    Feed feed =
        await feedRepository.getFeed(id: feedProcessBuffer.first.toString());

    await likeLoadService.execute(
      processedUsers: processedUsers,
      firstLoad: true,
      feed: feed,
    );

    await commentLoadService.execute(
      processedUsers: processedUsers,
      firstLoad: true,
      feed: feed,
    );

    ++processedFeedElements;
    feedProcessBuffer.removeAt(0);
  }

  await localStorage.write(
      FeedLoadService.FEED_PROCESS_BUFFER, feedProcessBuffer);
}
