import 'package:flutter/foundation.dart';

class MediaData {
  List<String> urlThumbnail;
  String contents;
  int likes;
  int comments;
  String date;
  String username;

  MediaData({
    @required this.urlThumbnail,
    @required this.contents,
    @required this.likes,
    @required this.comments,
    @required this.date,
    @required this.username,
  });
}
