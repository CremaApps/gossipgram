import 'package:flutter/material.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/model/objects/thumbnail.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:instagram_private_api/src/Client/Response/CommonClasses.dart';

@singleton
class GetFeedNextPageService {
  Future<List<FeedThumbnail>> execute({
    @required InstagramResponse feedItemsResponse,
  }) async {
    List<dynamic> feedImages = [];

    if (feedItemsResponse.hasNext()) {
      feedItemsResponse = await feedItemsResponse.next();
      feedImages.addAll(feedItemsResponse.response.items);
    }

    List<FeedThumbnail> feedThumbnailData = [];

    for (var image in feedImages) {
      ImageVersions2 imageVersion = image.carouselMedia != null
          ? image.carouselMedia.first.imageVersions2
          : image.imageVersions;

      feedThumbnailData.add(
        FeedThumbnail(
          id: image.pk,
          image: imageVersion,
          mediaType: image.carouselMedia != null
              ? FeedThumbnail.CAROUSEL
              : image.mediaType == 2
                  ? Thumbnail.VIDEO
                  : Thumbnail.IMAGE,
          likeCount: image.likeCount,
          commentCount: image.commentCount,
        ),
      );
    }

    return feedThumbnailData;
  }
}
