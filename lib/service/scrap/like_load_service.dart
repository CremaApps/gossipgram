import 'package:flutter/material.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/feed.dart';
import 'package:GossipGram/model/objects/like.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:injectable/injectable.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;

@singleton
class LikeLoadService extends UserLoadService {
  LikeRepository _likeRepository;
  InstagramPrivateApi _instagramPrivateApi;
  LocalStorage _localStorage;

  LikeLoadService(
    this._likeRepository,
    this._localStorage,
    UserRepository userRepository,
  ) : super(userRepository) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<List<Like>> execute({
    @required Feed feed,
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    int nowDate = firstLoad ? 0 : DateTime.now().millisecondsSinceEpoch;
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);

    var likeResponse =
        await _instagramPrivateApi.media.likes(feed.id.toString());
    var likes = likeResponse.response.users;
    DataLoadBloc.printProfiling("API likes media all loaded", now);

    List<dynamic> activeLikeIds =
        await _likeRepository.getLikeIds(isValid: true, feedId: feed.id);

    for (dynamic id in activeLikeIds) {
      if (!likes.any(
          (like) => (feed.id == id["feedId"] && like.pk == id["userId"]))) {
        await _likeRepository.upsertLike(
          like: Like(
            feedId: id["feedId"],
            userId: id["userId"],
            isValid: false,
          ),
        );
      }
    }

    List<Like> toUpsertLikes = [];
    List<Account.User> toProcessUsers = [];
    List<Like> dbLikes = await _likeRepository.queryLikes(
      isValid: true,
    );

    for (var like in likes) {
      Like dbLike = dbLikes.firstWhere(
          (element) => element.feedId == feed.id && element.userId == like.pk,
          orElse: () => null);

      int onStateChange = dbLike != null ? dbLike.onStateChange : nowDate;

      toProcessUsers.add(
        Account.User(
          id: like.pk,
          username: like.username,
          fullName: like.fullName,
          isPrivate: like.isPrivate,
          isVerified: like.isVerified,
          profilePicUrl: like.profilePicUrl,
          anonymousProfilePicture: false,
        ),
      );

      toUpsertLikes.add(
        Like(
          feedId: feed.id,
          userId: like.pk,
          isValid: true,
          onStateChange: onStateChange,
        ),
      );
    }

    await processUsersInBatchIfNotAlreadyProcessed(
        toProcessUsers, processedUsers);

    await _likeRepository.upsertLikes(
      likes: toUpsertLikes,
    );

    DataLoadBloc.printProfiling("Likes media DB loaded", now);

    return <Like>[];
  }
}
