// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'friendship_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$FriendshipStateTearOff {
  const _$FriendshipStateTearOff();

// ignore: unused_element
  _FriendshipInitial initial() {
    return const _FriendshipInitial();
  }

// ignore: unused_element
  _FriendshipLoading loading() {
    return const _FriendshipLoading();
  }

// ignore: unused_element
  _FriendshipLoaded loaded(bool succes) {
    return _FriendshipLoaded(
      succes,
    );
  }

// ignore: unused_element
  _FriendshipError error(String errorMessage) {
    return _FriendshipError(
      errorMessage,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $FriendshipState = _$FriendshipStateTearOff();

/// @nodoc
mixin _$FriendshipState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result loaded(bool succes),
    @required Result error(String errorMessage),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result loaded(bool succes),
    Result error(String errorMessage),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_FriendshipInitial value),
    @required Result loading(_FriendshipLoading value),
    @required Result loaded(_FriendshipLoaded value),
    @required Result error(_FriendshipError value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_FriendshipInitial value),
    Result loading(_FriendshipLoading value),
    Result loaded(_FriendshipLoaded value),
    Result error(_FriendshipError value),
    @required Result orElse(),
  });
}

/// @nodoc
abstract class $FriendshipStateCopyWith<$Res> {
  factory $FriendshipStateCopyWith(
          FriendshipState value, $Res Function(FriendshipState) then) =
      _$FriendshipStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FriendshipStateCopyWithImpl<$Res>
    implements $FriendshipStateCopyWith<$Res> {
  _$FriendshipStateCopyWithImpl(this._value, this._then);

  final FriendshipState _value;
  // ignore: unused_field
  final $Res Function(FriendshipState) _then;
}

/// @nodoc
abstract class _$FriendshipInitialCopyWith<$Res> {
  factory _$FriendshipInitialCopyWith(
          _FriendshipInitial value, $Res Function(_FriendshipInitial) then) =
      __$FriendshipInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$FriendshipInitialCopyWithImpl<$Res>
    extends _$FriendshipStateCopyWithImpl<$Res>
    implements _$FriendshipInitialCopyWith<$Res> {
  __$FriendshipInitialCopyWithImpl(
      _FriendshipInitial _value, $Res Function(_FriendshipInitial) _then)
      : super(_value, (v) => _then(v as _FriendshipInitial));

  @override
  _FriendshipInitial get _value => super._value as _FriendshipInitial;
}

/// @nodoc
class _$_FriendshipInitial implements _FriendshipInitial {
  const _$_FriendshipInitial();

  @override
  String toString() {
    return 'FriendshipState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _FriendshipInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result loaded(bool succes),
    @required Result error(String errorMessage),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result loaded(bool succes),
    Result error(String errorMessage),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_FriendshipInitial value),
    @required Result loading(_FriendshipLoading value),
    @required Result loaded(_FriendshipLoaded value),
    @required Result error(_FriendshipError value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_FriendshipInitial value),
    Result loading(_FriendshipLoading value),
    Result loaded(_FriendshipLoaded value),
    Result error(_FriendshipError value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _FriendshipInitial implements FriendshipState {
  const factory _FriendshipInitial() = _$_FriendshipInitial;
}

/// @nodoc
abstract class _$FriendshipLoadingCopyWith<$Res> {
  factory _$FriendshipLoadingCopyWith(
          _FriendshipLoading value, $Res Function(_FriendshipLoading) then) =
      __$FriendshipLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$FriendshipLoadingCopyWithImpl<$Res>
    extends _$FriendshipStateCopyWithImpl<$Res>
    implements _$FriendshipLoadingCopyWith<$Res> {
  __$FriendshipLoadingCopyWithImpl(
      _FriendshipLoading _value, $Res Function(_FriendshipLoading) _then)
      : super(_value, (v) => _then(v as _FriendshipLoading));

  @override
  _FriendshipLoading get _value => super._value as _FriendshipLoading;
}

/// @nodoc
class _$_FriendshipLoading implements _FriendshipLoading {
  const _$_FriendshipLoading();

  @override
  String toString() {
    return 'FriendshipState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _FriendshipLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result loaded(bool succes),
    @required Result error(String errorMessage),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result loaded(bool succes),
    Result error(String errorMessage),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_FriendshipInitial value),
    @required Result loading(_FriendshipLoading value),
    @required Result loaded(_FriendshipLoaded value),
    @required Result error(_FriendshipError value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_FriendshipInitial value),
    Result loading(_FriendshipLoading value),
    Result loaded(_FriendshipLoaded value),
    Result error(_FriendshipError value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _FriendshipLoading implements FriendshipState {
  const factory _FriendshipLoading() = _$_FriendshipLoading;
}

/// @nodoc
abstract class _$FriendshipLoadedCopyWith<$Res> {
  factory _$FriendshipLoadedCopyWith(
          _FriendshipLoaded value, $Res Function(_FriendshipLoaded) then) =
      __$FriendshipLoadedCopyWithImpl<$Res>;
  $Res call({bool succes});
}

/// @nodoc
class __$FriendshipLoadedCopyWithImpl<$Res>
    extends _$FriendshipStateCopyWithImpl<$Res>
    implements _$FriendshipLoadedCopyWith<$Res> {
  __$FriendshipLoadedCopyWithImpl(
      _FriendshipLoaded _value, $Res Function(_FriendshipLoaded) _then)
      : super(_value, (v) => _then(v as _FriendshipLoaded));

  @override
  _FriendshipLoaded get _value => super._value as _FriendshipLoaded;

  @override
  $Res call({
    Object succes = freezed,
  }) {
    return _then(_FriendshipLoaded(
      succes == freezed ? _value.succes : succes as bool,
    ));
  }
}

/// @nodoc
class _$_FriendshipLoaded implements _FriendshipLoaded {
  const _$_FriendshipLoaded(this.succes) : assert(succes != null);

  @override
  final bool succes;

  @override
  String toString() {
    return 'FriendshipState.loaded(succes: $succes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FriendshipLoaded &&
            (identical(other.succes, succes) ||
                const DeepCollectionEquality().equals(other.succes, succes)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(succes);

  @override
  _$FriendshipLoadedCopyWith<_FriendshipLoaded> get copyWith =>
      __$FriendshipLoadedCopyWithImpl<_FriendshipLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result loaded(bool succes),
    @required Result error(String errorMessage),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return loaded(succes);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result loaded(bool succes),
    Result error(String errorMessage),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(succes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_FriendshipInitial value),
    @required Result loading(_FriendshipLoading value),
    @required Result loaded(_FriendshipLoaded value),
    @required Result error(_FriendshipError value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_FriendshipInitial value),
    Result loading(_FriendshipLoading value),
    Result loaded(_FriendshipLoaded value),
    Result error(_FriendshipError value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _FriendshipLoaded implements FriendshipState {
  const factory _FriendshipLoaded(bool succes) = _$_FriendshipLoaded;

  bool get succes;
  _$FriendshipLoadedCopyWith<_FriendshipLoaded> get copyWith;
}

/// @nodoc
abstract class _$FriendshipErrorCopyWith<$Res> {
  factory _$FriendshipErrorCopyWith(
          _FriendshipError value, $Res Function(_FriendshipError) then) =
      __$FriendshipErrorCopyWithImpl<$Res>;
  $Res call({String errorMessage});
}

/// @nodoc
class __$FriendshipErrorCopyWithImpl<$Res>
    extends _$FriendshipStateCopyWithImpl<$Res>
    implements _$FriendshipErrorCopyWith<$Res> {
  __$FriendshipErrorCopyWithImpl(
      _FriendshipError _value, $Res Function(_FriendshipError) _then)
      : super(_value, (v) => _then(v as _FriendshipError));

  @override
  _FriendshipError get _value => super._value as _FriendshipError;

  @override
  $Res call({
    Object errorMessage = freezed,
  }) {
    return _then(_FriendshipError(
      errorMessage == freezed ? _value.errorMessage : errorMessage as String,
    ));
  }
}

/// @nodoc
class _$_FriendshipError implements _FriendshipError {
  const _$_FriendshipError(this.errorMessage) : assert(errorMessage != null);

  @override
  final String errorMessage;

  @override
  String toString() {
    return 'FriendshipState.error(errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FriendshipError &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(errorMessage);

  @override
  _$FriendshipErrorCopyWith<_FriendshipError> get copyWith =>
      __$FriendshipErrorCopyWithImpl<_FriendshipError>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result loaded(bool succes),
    @required Result error(String errorMessage),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return error(errorMessage);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result loaded(bool succes),
    Result error(String errorMessage),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(errorMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_FriendshipInitial value),
    @required Result loading(_FriendshipLoading value),
    @required Result loaded(_FriendshipLoaded value),
    @required Result error(_FriendshipError value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(loaded != null);
    assert(error != null);
    return error(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_FriendshipInitial value),
    Result loading(_FriendshipLoading value),
    Result loaded(_FriendshipLoaded value),
    Result error(_FriendshipError value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _FriendshipError implements FriendshipState {
  const factory _FriendshipError(String errorMessage) = _$_FriendshipError;

  String get errorMessage;
  _$FriendshipErrorCopyWith<_FriendshipError> get copyWith;
}
