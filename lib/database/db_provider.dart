import 'dart:io';
import 'dart:async';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:path/path.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class DbProvider {
  Database _database;
  LocalStorage _localStorage;
  int _userId;

  DbProvider(
    this._localStorage,
  );

  Future<Database> get database async {
    if (_userId !=
        _localStorage.read(DashboardBloc.USER_ID, generalStorage: true))
      _database = null;
    if (_database == null) {
      _database = await _initialize();
    }
    return _database;
  }

  void dispose() {
    _database?.close();
    _database = null;
  }

  Future<Database> _initialize() async {
    _userId = _localStorage.read(DashboardBloc.USER_ID, generalStorage: true);
    Directory docDir = await getApplicationDocumentsDirectory();
    String path = join(docDir.path, '${_userId}gossipgram.db');
    Database db = await openDatabase(
      path,
      version: 1,
      onOpen: (db) {
        print('Database Open');
      },
      onCreate: _onCreate,
    );
    return db;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE User("
        "id INT PRIMARY KEY, "
        "username TEXT, "
        "fullName TEXT, "
        "isPrivate BIT, "
        "isVerified BIT, "
        "youFollow BIT, "
        "anonymousProfilePicture BIT, "
        "profilePicUrl TEXT, "
        "createdAt INTEGER);");

    await db.execute("CREATE TABLE FollowedUser("
        "id INT PRIMARY KEY, "
        "onStateChange INTEGER, "
        "isValid BIT, "
        "youFollow BIT, "
        "FOREIGN KEY (id) REFERENCES User(id));");

    await db.execute("CREATE TABLE FollowerUser("
        "id INT PRIMARY KEY, "
        "onStateChange INTEGER, "
        "isValid BIT, "
        "youFollow BIT, "
        "FOREIGN KEY (id) REFERENCES User(id));");

    await db.execute("CREATE TABLE BlockedUser("
        "id INT PRIMARY KEY, "
        "onStateChange INTEGER, "
        "isValid BIT, "
        "youFollow BIT, "
        "FOREIGN KEY (id) REFERENCES User(id));");

    await db.execute("CREATE TABLE BestFriendUser("
        "id INT PRIMARY KEY, "
        "onStateChange INTEGER, "
        "isValid BIT, "
        "youFollow BIT, "
        "FOREIGN KEY (id) REFERENCES User(id));");

    await db.execute("CREATE TABLE Like("
        "feedId TEXT NOT NULL, "
        "userId INT NOT NULL, "
        "onStateChange INTEGER, "
        "isValid BIT, "
        "PRIMARY KEY (feedId, userId), "
        "FOREIGN KEY (feedId) REFERENCES Feed(id) ON DELETE CASCADE);");

    await db.execute("CREATE TABLE Comment("
        "id INT NOT NULL, "
        "feedId TEXT NOT NULL, "
        "userId INT NOT NULL, "
        "createdAt INTEGER, "
        "deletedAt INTEGER, "
        "isValid BIT, "
        "PRIMARY KEY (id), "
        "FOREIGN KEY (feedId) REFERENCES Feed(id) ON DELETE CASCADE);");

    await db.execute("CREATE TABLE Feed("
        "id TEXT PRIMARY KEY, "
        "mediaUrl TEXT, "
        "mediaType INTEGER, "
        "likeCount INTEGER, "
        "commentCount INTEGER, "
        "originalCreatedAtDate INTEGER, "
        "createdAt INTEGER);");

    await db.execute("CREATE TABLE StoryView("
        "storyId INT NOT NULL, "
        "seenByUserId INT NOT NULL, "
        "createdAt INTEGER, "
        "isValid BIT, "
        "PRIMARY KEY (storyId, seenByUserId), "
        "FOREIGN KEY (storyId) REFERENCES Story(id) ON DELETE CASCADE);");

    await db.execute("CREATE TABLE Story("
        "id INT NOT NULL, "
        "thumbnailUrl TEXT, "
        "mediaUrl TEXT, "
        "mediaType INT, "
        "viewCount INTEGER, "
        "originalCreatedAtDate INTEGER, "
        "createdAt INTEGER, "
        "isValid BIT);");
  }
}
