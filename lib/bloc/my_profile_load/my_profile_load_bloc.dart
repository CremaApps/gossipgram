import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:GossipGram/database/repository/comment_repository.dart';
import 'package:GossipGram/database/repository/feed_repository.dart';
import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/model/view_data/my_profile.dart';
import 'package:GossipGram/service/api/get_feed_images_service.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

part 'my_profile_load_event.dart';
part 'my_profile_load_state.dart';

@singleton
class MyProfileLoadBloc extends Bloc<MyProfileLoadEvent, MyProfileLoadState> {
  InstagramPrivateApi _instagramPrivateApi;
  GetFeedImagesService _getFeedImagesService;
  FollowedUserRepository _followedUserRepository;
  FollowerUserRepository _followerUserRepository;
  LikeRepository _likeRepository;
  CommentRepository _commentRepository;
  FeedRepository _feedRepository;
  MyProfile _myProfile = MyProfile();

  MyProfileLoadBloc(
    this._getFeedImagesService,
    this._followedUserRepository,
    this._followerUserRepository,
    this._likeRepository,
    this._commentRepository,
    this._feedRepository,
  ) : super(MyProfileLoadInitial()) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  @override
  Stream<MyProfileLoadState> mapEventToState(
    MyProfileLoadEvent event,
  ) async* {
    if (event is MyProfileLoadEventStart) {
      yield MyProfileLoadInitial();

      _myProfile = MyProfile();

      var myUser = await _instagramPrivateApi.user
          .getAccountInfo(_instagramPrivateApi.myUserId);

      var followerCount = await _followerUserRepository.queryCountFollowerUsers(
        isValid: true,
      );

      var followedCount = await _followedUserRepository.queryCountFollowedUsers(
        isValid: true,
      );

      _myProfile = _myProfile.copyWith(
        fullName: myUser.response.user.fullName,
        profilePicUrl: myUser.response.user.profilePicUrl,
        bio: myUser.response.user.biography,
        followers: followerCount,
        following: followedCount,
      );

      yield MyProfileLoadProfileInfoLoaded(myProfile: _myProfile);

      var totalLikes = await _likeRepository.queryCountLikes(
        isValid: true,
      );

      var totalComments = await _commentRepository.queryCountComments(
        isValid: true,
      );

      var totalFeedElements = await _feedRepository.queryCountFeeds();

      double averageLikes =
          totalFeedElements > 0 ? totalLikes / totalFeedElements : 0;
      double averageComments =
          totalFeedElements > 0 ? totalComments / totalFeedElements : 0;
      double followerRatio =
          followedCount > 0 ? followerCount / followedCount : 0;
      double engagementRate = followerCount > 0
          ? (averageLikes + averageComments) * 100 / followerCount
          : 0;

      _myProfile = _myProfile.copyWith(
        averageLikes: averageLikes,
        followerRatio: followerRatio,
        engagementRate: engagementRate,
      );

      yield MyProfileLoadInfoAnalyticsLoaded(myProfile: _myProfile);

      var feedImageData = await _getFeedImagesService.execute(
        userId: _instagramPrivateApi.myUserId,
        singlePage: false,
      );

      _myProfile = _myProfile.copyWith(
        feedThumbnail: feedImageData["feedThumbnailData"],
        feedResponse: feedImageData["response"],
      );

      yield feedImageData["response"].hasNext()
          ? MyProfileLoadFeedLoading(myProfile: _myProfile)
          : MyProfileLoadFullyLoaded(myProfile: _myProfile);
    } else if (event is MyProfileLoadEventGetFeedPage) {
      var feedImageData = await _getFeedImagesService.execute(
        userId: _instagramPrivateApi.myUserId,
        singlePage: true,
        feedItemsResponse: _myProfile.feedResponse,
      );

      _myProfile = _myProfile.copyWith(
        feedThumbnail: _myProfile.feedThumbnail
          ..addAll(feedImageData["feedThumbnailData"]),
        feedResponse: feedImageData["response"],
      );

      yield feedImageData["response"].hasNext()
          ? MyProfileLoadFeedLoading(myProfile: _myProfile)
          : MyProfileLoadFullyLoaded(myProfile: _myProfile);
    }
  }
}
