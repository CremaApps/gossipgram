import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:GossipGram/database/repository/story_repository.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:injectable/injectable.dart';
part 'my_story_event.dart';
part 'my_story_state.dart';

@lazySingleton
class MyStoriesBloc extends Bloc<MyStoriesEvent, MyStoriesState> {
  StoryRepository _storyRepository;
  int currentTab;

  MyStoriesBloc(
    this._storyRepository,
  ) : super(MyStoriesInitial());

  @override
  Stream<MyStoriesState> mapEventToState(MyStoriesEvent event) async* {
    if (event is MyStoriesStartLoadEvent) {
      if (currentTab == null) yield MyStoriesLoading();

      List<Story> stories = await _loadStories();

      _updateIndex(nextIndex: event.tabIndex);

      List<Story> currentStories;

      switch (currentTab) {
        case 0:
          currentStories = stories
            ..sort((a, b) =>
                b.originalCreatedAtDate.compareTo(a.originalCreatedAtDate));
          break;
        case 1:
          currentStories = stories
            ..sort((a, b) => b.viewCount.compareTo(a.viewCount));
          break;
        case 2:
          currentStories = stories
            ..sort((a, b) =>
                a.originalCreatedAtDate.compareTo(b.originalCreatedAtDate));
          break;
        case 3:
          currentStories = stories
            ..sort((a, b) => a.viewCount.compareTo(b.viewCount));
          break;
        default:
          throw UnimplementedError();
      }

      yield MyStoriesLoaded(
          stories: currentStories, currentIndex: event.tabIndex);
    }
  }

  Future<List<Story>> _loadStories() async {
    Set<Story> activeStories = Set.from(await _storyRepository.queryStories(
      isValid: true,
    ));

    Set<Story> historicalStories = Set.from(await _storyRepository.queryStories(
      isValid: false,
    ));

    List<Story> stories =
        List<Story>.from(activeStories.union(historicalStories));
    return stories;
  }

  void _updateIndex({int nextIndex}) {
    if (currentTab == null && nextIndex == null) {
      currentTab = 0;
    } else if (nextIndex != null) {
      currentTab = nextIndex;
    }
  }
}
