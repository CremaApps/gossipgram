import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_state.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/bloc/data_load/data_load_state.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/dashboard.dart';
import 'package:GossipGram/pages/my_profile_page.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:GossipGram/widgets/number_people_converter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';

class TopBar extends StatelessWidget {
  final bool loading;
  final String userName;
  final String profilePicUrl;
  final String fullName;
  final String bio;
  final int followers;
  final int following;
  final bool clicableProfile;
  final DataLoadBloc dataLoadBloc;

  TopBar(
      {@required this.loading,
      this.userName,
      this.profilePicUrl,
      this.fullName,
      this.bio,
      this.followers,
      this.following,
      this.clicableProfile,
      @required this.dataLoadBloc});

  @override
  Widget build(BuildContext context) {
    // bool loading = dashboardState is DashboardStateLoading ? true : false;

    return Container(
      width: double.infinity,
      color: appColorScheme.primary,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top: 3),
                child: (loading && profilePicUrl == null)
                    ? Container(
                        child: CircleAvatar(
                          radius: 34.0,
                          backgroundImage: AssetImage(
                              './assets/images/image_profile_placeholder.jpg'),
                          backgroundColor: Colors.transparent,
                        ),
                      )
                    : InkWell(
                        onTap: () => clicableProfile
                            ? _goUserProfile(context, loading)
                            : null,
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 16),
                          child: CircleAvatar(
                            radius: 34.0,
                            backgroundImage: NetworkImage(profilePicUrl ?? ""),
                            backgroundColor: Colors.transparent,
                          ),
                        ),
                      ),
              ),
              dataLoadBloc == null
                  ? SizedBox()
                  : Positioned(
                      child: BlocBuilder<DataLoadBloc, DataLoadState>(
                        cubit: dataLoadBloc,
                        builder: (context, state) {
                          if (state is DataLoadStateLoading) {
                            if (state.showLoading) {
                              return Container(
                                  child: CircularProgressIndicator());
                            } else {
                              return SizedBox();
                            }
                          } else {
                            return SizedBox();
                          }
                        },
                      ),
                    ),
            ],
          ),
          GestureDetector(
            onTap: () =>
                clicableProfile ? _goUserProfile(context, loading) : null,
            child: Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  loading
                      ? Shimmer.fromColors(
                          baseColor: AppTheme.complementaries['50'],
                          highlightColor: Colors.white,
                          child: Container(
                            width: MediaQuery.of(context).size.width / 10,
                            height: 20.0,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                            ),
                          ),
                        )
                      : Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: Text(
                            clicableProfile ? fullName ?? "" : userName,
                            style: appTextTheme.subtitle1,
                          ),
                        ),
                  loading
                      ? Shimmer.fromColors(
                          baseColor: AppTheme.complementaries['50'],
                          highlightColor: Colors.white,
                          child: Container(
                            width: MediaQuery.of(context).size.width / 10,
                            height: 20.0,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                            ),
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.only(bottom: 10),
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: Text(
                            clicableProfile
                                ? userName ?? ""
                                : (bio != null && bio.isNotEmpty
                                    ? bio
                                    : fullName),
                            style: appTextTheme.bodyText1,
                          ),
                        ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        (loading && followers == null)
                            ? Shimmer.fromColors(
                                baseColor: AppTheme.complementaries['50'],
                                highlightColor: Colors.white,
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 10,
                                  height: 20.0,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                  ),
                                ),
                              )
                            : InkWell(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/followers');
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: followers == null
                                              ? ""
                                              : IntNumberSymbolConverter(
                                                  followers),
                                          style: appTextTheme.subtitle2,
                                        ),
                                        WidgetSpan(
                                          child: SizedBox(
                                            width: 4,
                                          ),
                                        ),
                                        TextSpan(
                                          text: "${S.of(context).followers}" ??
                                              "",
                                          style: appTextTheme.bodyText1,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                        SizedBox(
                          width: 12,
                        ),
                        (loading && following == null)
                            ? Shimmer.fromColors(
                                baseColor: AppTheme.complementaries['50'],
                                highlightColor: Colors.white,
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 10,
                                  height: 20.0,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                  ),
                                ),
                              )
                            : Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context)
                                        .pushNamed('/followed');
                                  },
                                  child: RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: following == null
                                              ? ""
                                              : IntNumberSymbolConverter(
                                                  following),
                                          style: appTextTheme.subtitle2,
                                        ),
                                        WidgetSpan(
                                          child: SizedBox(
                                            width: 4,
                                          ),
                                        ),
                                        TextSpan(
                                          text: "${S.of(context).following}" ??
                                              "",
                                          style: appTextTheme.bodyText1,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _goUserProfile(BuildContext context, bool loading) {
    if (!loading) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MyProfilePage(
            dashboard: Dashboard(
                fullName: fullName,
                profilePicUrl: profilePicUrl,
                username: userName,
                followers: followers,
                following: following),
          ),
        ),
      );
    }
  }
}
