import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user_like_count.dart';
import 'package:injectable/injectable.dart';

part 'like_sorted_users_load_event.dart';
part 'like_sorted_users_load_state.dart';

@injectable
class LikeSortedUsersLoadBloc
    extends Bloc<LikeSortedUsersLoadEvent, LikeSortedUsersLoadState> {
  UserRepository _userRepository;
  LocalStorage _localStorage;

  LikeSortedUsersLoadBloc(
    this._userRepository,
    this._localStorage,
  ) : super(LikeSortedUsersLoadInitial());

  @override
  Stream<LikeSortedUsersLoadState> mapEventToState(
    LikeSortedUsersLoadEvent event,
  ) async* {
    if (event is LikeSortedUsersLoadEventStart) {
      yield LikeSortedUsersLoadInitial();

      List<UserLikeCount> usersWhoMostLikedYou =
          await _userRepository.getUsersSortedByLikeCount(
        isValid: true,
        maxElems: 20,
        asc: false,
        excluding: _localStorage.hasData(DashboardBloc.USER_ID,
                generalStorage: true)
            ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
            : [],
      );

      List<UserLikeCount> usersWhoLeastLikedYou =
          await _userRepository.getUsersSortedByLikeCount(
        isValid: true,
        maxElems: 20,
        asc: true,
        excluding: _localStorage.hasData(DashboardBloc.USER_ID,
                generalStorage: true)
            ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
            : [],
      );

      yield LikeSortedUsersLoaded(
        usersWhoMostLikedYou: usersWhoMostLikedYou,
        usersWhoLeastLikedYou: usersWhoLeastLikedYou,
      );
    }
  }
}
