import 'package:flutter/material.dart';

class Comment {

  final int id;
  final String feedId;
  final int userId;
  int createdAt;
  int deletedAt;
  final bool isValid;

  Comment({
    @required this.id,
    @required this.feedId,
    @required this.userId,
    this.createdAt,
    this.deletedAt,
    this.isValid = true,
  }) {
    if(this.createdAt == null) {
      this.createdAt = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Map<String, dynamic> toMap() => {
    'id': id,
    'feedId': feedId,
    'userId': userId,
    'createdAt': createdAt,
    'deletedAt': deletedAt,
    'isValid': isValid ? 1 : 0,
  };

  factory Comment.fromMap(Map<String, dynamic> map) => Comment(
    id: map['id'],
    feedId: map['feedId'],
    userId: map['userId'],
    createdAt: map['createdAt'],
    deletedAt: map['deletedAt'],
    isValid: map['isValid'] == 1,
  );

}