import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/story_view.dart';
import 'package:injectable/injectable.dart';

@singleton
class StoryViewRepository {
  final DbProvider _dbProvider;

  StoryViewRepository(this._dbProvider);

  Future<StoryView> getStoryView({
    @required int storyId,
    @required int seenByUserId,
  }) async {
    final db = await _dbProvider.database;
    var res = await db.query(
      'StoryView',
      where: 'storyId = ? AND seenByUserId = ?',
      whereArgs: [storyId, seenByUserId],
    );
    return res.isNotEmpty ? StoryView.fromMap(res.first) : null;
  }

  Future<List<StoryView>> queryStoryViews({
    int storyId,
    int seenByUserId,
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
            'FROM StoryView '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (storyId != null) query += 'AND storyId = ' + storyId.toString() + ' ';
    if (seenByUserId != null)
      query += 'AND seenByUserId = ' + seenByUserId.toString() + ' ';

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var dbComments = await db.rawQuery(query);
    List<StoryView> comments = dbComments.isNotEmpty
        ? dbComments.map((a) => StoryView.fromMap(a)).toList()
        : [];
    return comments;
  }

  Future<int> queryCountStoryViews({
    int storyId,
    int seenByUserId,
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM StoryView '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (storyId != null) query += 'AND storyId = ' + storyId.toString() + ' ';
    if (seenByUserId != null)
      query += 'AND seenByUserId = ' + seenByUserId.toString() + ' ';

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var dbCommentsCount = await db.rawQuery(query);

    return dbCommentsCount.first.values.first;
  }

  Future<int> upsertStoryView({@required StoryView storyView}) async {
    final db = await _dbProvider.database;
    var dbComment = await db.query(
      'StoryView',
      where: 'storyId = ? AND seenByUserId = ?',
      whereArgs: [storyView.storyId, storyView.seenByUserId],
    );
    if (dbComment.isNotEmpty) {
      return db.update(
        'StoryView',
        storyView.toMap(),
        where: 'storyId = ? AND seenByUserId = ?',
        whereArgs: [storyView.storyId, storyView.seenByUserId],
      );
    } else {
      return await db.insert('StoryView', storyView.toMap());
    }
  }

  Future<int> deleteStoryView({@required StoryView storyView}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'StoryView',
      where: 'storyId = ? AND seenByUserId = ?',
      whereArgs: [storyView.storyId, storyView.seenByUserId],
    );
  }

  Future<List<dynamic>> getStoryViewIds({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
    int storyId,
    int seenByUserId,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT storyId, seenByUserId '
            'FROM StoryView '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (storyId != null) {
      query += 'AND storyId = ' + storyId.toString() + ' ';
    }

    if (seenByUserId != null) {
      query += 'AND seenByUserId = ' + seenByUserId.toString() + ' ';
    }

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<dynamic> storyViewIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            return {
              "storyId": a['storyId'],
              "seenByUserId": a['seenByUserId'],
            };
          }).toList()
        : [];
    return storyViewIds;
  }

  Future<List<int>> getStoryViewUserIds({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
    int storyId,
    bool notFollower = false,
    bool notFollowed = false,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT DISTINCT(seenByUserId) '
            'FROM StoryView '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (storyId != null) {
      query += 'AND storyId = ' + storyId.toString() + ' ';
    }

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    if (notFollower) {
      query +=
          'AND seenByUserId NOT IN (SELECT id FROM FollowerUser WHERE isValid = 1)';
    }

    if (notFollowed) {
      query +=
          'AND seenByUserId NOT IN (SELECT id FROM FollowedUser WHERE isValid = 1)';
    }

    var queryResponse = await db.rawQuery(query);
    List<int> storyViewIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int userId = a['seenByUserId'];
            return userId;
          }).toList()
        : [];
    return storyViewIds;
  }
}
