import 'package:flutter/material.dart';

const Map themeVarsLight = {
  'primary': {
    'main-1': Color(0xff060F4B),
    'main-2': Color(0xff253077),
    'main-3': Color(0xff676FA2),
    'main-4': Color(0xffBCBFD7),
    'main-5': Color(0xffE4E6EF),
  },
  'secondary': {
    'main-1': Color(0xff971D50),
    'main-2': Color(0xffE82965),
    'main-3': Color(0xffDE668C),
    'main-4': Color(0xffEFA4BC),
    'main-5': Color(0xF6D5DF),
  },
  'complementary': {
    'main-1': Color(0xff009d8f),
    'main-2': Color(0xff00BCB2),
    'main-3': Color(0xffAAE2DE),
    'main-4': Color(0xffddf3f3),
  },
  'neutrals': {
    'main-1': Color(0xff1E2021),
    'main-2': Color(0xff3D4042),
    'main-3': Color(0xff6D7275),
    'main-4': Color(0xffB5BABD),
    'main-5': Color(0xffE7EBEE),
    'main-6': Color(0xffF5F8FA),
  },
  'errors': {
    'main-1': Color(0xffD63737),
    'main-2': Color(0xffF09D9E),
    'main-3': Color(0xffFFECEF)
  }
};

final themeVarsDark = {
  'primary': {
    'main-1': Colors.black,
  },
};
