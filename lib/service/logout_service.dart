import 'package:background_fetch/background_fetch.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/service/local_notifications_service.dart';
import 'package:GossipGram/service/storage/secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'dart:io' show Platform;

@singleton
class LogoutService {
  SecureStorage _secureStorage;
  LocalNotificationService _localNotificationService;
  DbProvider _dbProvider;

  LogoutService(
    this._secureStorage,
    this._localNotificationService,
    this._dbProvider,
  );

  Future<void> execute() async {
    _dbProvider?.dispose();
    _localNotificationService.cancelAllNotifications();
    String packageBaseName = 'com.cremapps.gossipgram';
    await BackgroundFetch.stop("$packageBaseName.postqueue");

    await Future.wait([
      InstagramPrivateApi.instance().clearCookies(),
      _secureStorage.clearSecureStorage(),
    ]);
  }
}
