import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:lottie/lottie.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../bloc/dashboard/dashboard_bloc.dart';
import '../../bloc/dashboard/dashboard_events.dart';
import '../../bloc/dashboard/dashboard_state.dart';
import '../../bloc/data_load/data_load_bloc.dart';
import '../../bloc/data_load/data_load_events.dart';
import '../../bloc/data_load/data_load_state.dart';
import '../../generated/l10n.dart';
import '../../service/local_notification/local_notification_register_service.dart';
import '../../service/local_notifications_service.dart';
import '../../service/local_storage/local_storage.dart';
import '../../service/navigator_service.dart';
import '../../service/storage/app_config.dart';
import '../../themes/themes.dart';
import '../../widgets/dashboard_tabs.dart';
import '../../widgets/drawer_settings.dart';
import '../../widgets/profile_menu/top_bar.dart';
import '../tutorial_page.dart';

part 'dashboard_page_loader.dart';

class DashboardPage extends StatefulWidget {
  DashboardPage();

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with SingleTickerProviderStateMixin {
  //Drawer
  static const Duration toggleDuration = Duration(milliseconds: 250);
  double maxSlide;
  double minDragStartEdge;
  double maxDragStartEdge;
  AnimationController _animationController;
  bool _canBeDragged = true;

  //Logic
  DashboardBloc _dashboardBloc;
  final DataLoadBloc _dataLoadBloc = GetIt.I.get<DataLoadBloc>();
  final AppConfig _appConfig = GetIt.I.get<AppConfig>();
  final LocalNotificationService _localNotificationService =
      GetIt.I.get<LocalNotificationService>();
  final LocalNotificationRegisterService _localNotificationRegisterService =
      GetIt.I.get<LocalNotificationRegisterService>();
  final LocalStorage _localStorage = GetIt.I.get<LocalStorage>();

  //tabs
  int selectedIndex = 0;

  //pull to refresh

  bool _isLoading = false;
  StreamSubscription<DataLoadState> _dataLoadStateSubscription;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    _dashboardBloc = GetIt.instance.get<DashboardBloc>();
    _dataLoadStateSubscription = _dataLoadBloc.listen(_updateLoadStatus);
    _dashboardBloc.add(DashboardLoadProfileEvent());

    DateTime lastLoad = DateTime.tryParse(
        _localStorage.read<String>(DataLoadBloc.LAST_LOAD_KEY) ?? '');
    DateTime threshold = DateTime.now().subtract(Duration(
        minutes: _appConfig.getInt(AppConfig.FULLY_LOAD_ON_DASHBOARD)));

    bool isNotFirstLoad = lastLoad != null;
    if (isNotFirstLoad) {
      _dashboardBloc.add(DashboardLoadFromDb());
      SchedulerBinding.instance.addPostFrameCallback((_) async {
        await Future.delayed(Duration(milliseconds: 900));
        await _localNotificationService.requestNotificationPermissions();
        await _localNotificationService.initialize();
        await _localNotificationRegisterService.registerNotifications(
          context: context,
          notificationIds: [
            LocalNotificationRegisterService.NOTIFICATION_REMINDER_ID,
            LocalNotificationRegisterService
                .NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_ID,
            LocalNotificationRegisterService
                .NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_ID,
            LocalNotificationRegisterService
                .NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_ID,
          ],
        );
      });
    } else {
      SchedulerBinding.instance.addPostFrameCallback((_) async {
        await Future.delayed(Duration(milliseconds: 900));
        await _localNotificationService.requestNotificationPermissions();
        await _localNotificationService.initialize();
        await _localNotificationRegisterService.registerNotifications(
          context: context,
          notificationIds: [
            LocalNotificationRegisterService.NOTIFICATION_REMINDER_ID,
            LocalNotificationRegisterService
                .NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_ID,
            LocalNotificationRegisterService
                .NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_ID,
            LocalNotificationRegisterService
                .NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_ID,
          ],
        );
      });
    }

    if (lastLoad == null || lastLoad.isBefore(threshold)) {
      _dataLoadBloc.add(DataLoadStartEvent(true));
    }

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    maxSlide = -MediaQuery.of(context).size.width * 70 / 100;
    minDragStartEdge = 120;
    maxDragStartEdge = maxSlide + 16;

    //drawer
    _animationController = AnimationController(
      vsync: this,
      duration: _DashboardPageState.toggleDuration,
    );
  }

  void _updateLoadStatus(DataLoadState dataLoadState) {
    if (dataLoadState is DataLoadStateLoading) {
      _isLoading = true;
      _refreshController.isLoading;
    } else if (dataLoadState is DataLoadStateFinished) {
      _isLoading = false;
      _refreshController.refreshCompleted();
    }
  }

  void _onRefresh() {
    if (!_isLoading)
      _dataLoadBloc.add(DataLoadStartEvent(false));
    else
      _refreshController.refreshCompleted();
  }

  //drawer methods
  void close() => _animationController.reverse();

  void open() => _animationController.forward();

  void toggleDrawer() => _animationController.isCompleted ? close() : open();

  void _onDragUpdate(DragUpdateDetails details) {
    double delta = details.primaryDelta / maxSlide;
    _animationController.value += delta;
  }

  void _onDragEnd(DragEndDetails details) {
    if (_animationController.isDismissed || _animationController.isCompleted) {
      return;
    }

    if (_animationController.value < 0.5) {
      close();
    } else {
      open();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
    _dataLoadStateSubscription?.cancel();
    _refreshController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_animationController.isCompleted) {
          close();
          return false;
        }
        return true;
      },
      child: GestureDetector(
        onHorizontalDragUpdate: _onDragUpdate,
        onHorizontalDragEnd: _onDragEnd,
        child: AnimatedBuilder(
          animation: _animationController,
          child: DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: SafeArea(
              child: Scaffold(
                resizeToAvoidBottomPadding: false,
                appBar: AppBar(
                  elevation: 0,
                  actions: [
                    AnimatedBuilder(
                      animation: _animationController,
                      builder: (context, child) {
                        return Padding(
                          padding: EdgeInsets.only(bottom: 36),
                          child: IconButton(
                            color: appColorScheme.onBackground,
                            icon: Icon(_animationController.isCompleted
                                ? Icons.close
                                : Icons.menu),
                            onPressed: () => toggleDrawer(),
                          ),
                        );
                      },
                    ),
                  ],
                  toolbarHeight: 130,
                  flexibleSpace: Container(
                    margin: EdgeInsets.only(top: 16),
                    height: MediaQuery.of(context).size.height / 5.5,
                    child: BlocBuilder<DashboardBloc, DashboardState>(
                      cubit: _dashboardBloc,
                      builder: (context, state) {
                        if (state is DashboardStateProfileLoaded ||
                            state is DashboardStateFullLoaded) {
                          return TopBar(
                            loading: false,
                            userName: state.dashboard.username,
                            profilePicUrl: state.dashboard.profilePicUrl,
                            fullName: state.dashboard.fullName,
                            bio: "",
                            followers: state.dashboard.followers,
                            following: state.dashboard.following,
                            clicableProfile: true,
                            dataLoadBloc: _dataLoadBloc,
                          );
                        } else {
                          return TopBar(
                            loading: true,
                            dataLoadBloc: _dataLoadBloc,
                          );
                        }
                      },
                    ),
                  ),
                  bottom: TabBar(
                    //isScrollable: !_animationController.isCompleted,
                    labelStyle: appTextTheme.subtitle2.copyWith(fontSize: 15),
                    unselectedLabelStyle:
                        appTextTheme.subtitle1.copyWith(fontSize: 15),
                    labelColor: appColorScheme.secondary,
                    unselectedLabelColor: appColorScheme.onPrimary,

                    tabs: [
                      Tab(
                        text: S.of(context).analysis,
                      ),
                      Tab(
                        text: S.of(context).insight,
                      ),
                    ],
                  ),
                ),
                body: SmartRefresher(
                  enablePullDown: true,
                  header: CustomHeader(
                    height: 150,
                    refreshStyle: RefreshStyle.Behind,
                    builder: (context, mode) {
                      Widget body;
                      if (mode == RefreshStatus.idle) {
                        body = Text(
                          "Pull down refresh",
                          style: appTextTheme.subtitle2,
                        );
                      } else if (mode == RefreshStatus.refreshing) {
                        body = Container(
                          padding: EdgeInsetsDirectional.only(top: 40),
                          child: Lottie.asset(
                            'assets/animations/loader.json',
                            width: 100,
                            height: 200,
                          ),
                        );
                      } else if (mode == RefreshStatus.canRefresh) {
                        body = Text("Release to refresh",
                            style: appTextTheme.subtitle2);
                      } else if (mode == RefreshStatus.completed) {
                        body = Text("Refresh completed!",
                            style: appTextTheme.subtitle2);
                      }
                      return Container(
                        height: 100.0,
                        child: Center(
                          child: body,
                        ),
                      );
                    },
                  ),
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  child: Container(
                    color: appColorScheme.surface,
                    child: DashboardTabs(
                      dashboardBloc: _dashboardBloc,
                      dataLoadBloc: _dataLoadBloc,
                    ),
                  ),
                ),
              ),
            ),
          ),
          builder: (context, child) {
            double animValue = _animationController.value;
            final slideAmount = maxSlide * animValue;

            return Stack(
              children: <Widget>[
                BlocBuilder<DashboardBloc, DashboardState>(
                  cubit: _dashboardBloc,
                  builder: (context, state) {
                    if (state is DashboardStateProfileLoaded ||
                        state is DashboardStateFullLoaded) {
                      return DrawerSettings(state.dashboard, () {
                        setState(() {});
                      });
                    } else {
                      return SizedBox();
                    }
                  },
                ),
                Transform(
                  transform: Matrix4.identity()..translate(slideAmount),
                  alignment: Alignment.centerRight,
                  child: child,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
