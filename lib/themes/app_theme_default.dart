import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  AppTheme._();
  static final ThemeData lightTheme = ThemeData.light().copyWith(
      primaryColor: AppTheme.primaries['900'],
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      appBarTheme: AppBarTheme(
        brightness: Brightness.light,
        color: AppTheme.primaries['main-1'],
      ),
      textTheme: GoogleFonts.poppinsTextTheme());

  static final ThemeData darkTheme = ThemeData.dark().copyWith(
    appBarTheme: AppBarTheme(
      color: AppTheme.primaries['main-1'],
    ),
  );

  static const Map<String, Color> primaries = {
    '900': Color(0xff060F4B),
    '600': Color(0xff253077),
    '400': Color(0xff676FA2),
    '200': Color(0xffBCBFD7),
    '50': Color(0xffE4E6EF),
  };

  static const Map<String, Color> secondaries = {
    '900': Color(0xff971D50),
    '600': Color(0xffE82965),
    '400': Color(0xffDE668C),
    '200': Color(0xffEFA4BC),
    '50': Color(0xfff6d5df),
  };

  static const Map<String, Color> complementaries = {
    '900': Color(0xff009d8f),
    '600': Color(0xff00BCB2),
    '200': Color(0xffAAE2DE),
    '50': Color(0xffddf3f3),
  };

  static const Map<String, Color> neutrals = {
    '900': Color(0xff1E2021),
    '800': Color(0xff3D4042),
    '600': Color(0xff6D7275),
    '400': Color(0xffB5BABD),
    '200': Color(0xffE7EBEE),
    '50': Color(0xffF5F8FA),
  };

  static const Map<String, Color> errors = {
    '600': Color(0xffD63737),
    '400': Color(0xffF09D9E),
    '50': Color(0xffFFECEF)
  };

  static const Map<String, String> borders = {
    'regular': '8',
  };
}
