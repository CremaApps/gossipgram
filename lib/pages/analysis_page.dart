import 'package:GossipGram/bloc/analysis_bloc/blocs/friendship_cubit.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:GossipGram/widgets/friendship_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/pages/user_profile/user_profile_page.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/profile_picture.dart';
import 'package:get_it/get_it.dart';
import 'package:lottie/lottie.dart';

class AnalysisPage extends StatefulWidget {
  final String title;
  final String keyName;

  const AnalysisPage({this.title, this.keyName});

  @override
  _AnalysisPageState createState() => _AnalysisPageState();
}

class _AnalysisPageState extends State<AnalysisPage> {
  IAnalysisBloc analysisBloc;

  @override
  void initState() {
    super.initState();
    analysisBloc = IAnalysisBloc.getByString(widget.keyName);

    analysisBloc.add(StartAnalysisEvent());
  }

  @override
  void dispose() {
    super.dispose();
    analysisBloc?.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<IAnalysisBloc, AnalysisState>(
      cubit: analysisBloc,
      builder: (context, state) {
        return Scaffold(
            appBar:
                AppBar(elevation: 0, centerTitle: false, title: title(state)),
            body: Container(
              margin: EdgeInsetsDirectional.only(top: 10),
              child: body(state),
            ));
      },
    );
  }

  Widget body(AnalysisState state) {
    if (state is AnalysisLoadingState) {
      return _loading();
    } else if (state is AnalysisLoadedState) {
      if (state.analysisResult.isNotEmpty) {
        return _showListView(state, context);
      } else {
        return _noRecords(context);
      }
    } else {
      return _error();
    }
  }

  Widget title(AnalysisState state) {
    if (state is AnalysisLoadedState) {
      return Text("${this.widget.title} (${state.analysisResult.length})",
          style: appTextTheme.subtitle2);
    } else {
      return Text(this.widget.title, style: appTextTheme.subtitle2);
    }
  }

  Widget _loading() {
    return Center(
      child: Lottie.asset(
        'assets/animations/loader.json',
        width: 100,
        height: 100,
      ),
    );
  }

  Widget _error() {
    return Center(
      child: Text(S.of(context).somethingWentWrong),
    );
  }

  Widget _noRecords(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsetsDirectional.only(
            top: MediaQuery.of(context).size.height / 3.2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(S.of(context).nobodyFound, style: appTextTheme.headline6),
            Container(
              margin: EdgeInsetsDirectional.only(top: 10),
              width: MediaQuery.of(context).size.width / 2,
              child: Text(
                S.of(context).noOneYet,
                style: appTextTheme.bodyText1,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _showListView(AnalysisLoadedState state, BuildContext context) {
    return ListView(
      physics: ClampingScrollPhysics(),
      children: state.analysisResult.map((e) => _buildRow(context, e)).toList(),
    );
  }

  Widget _buildRow(BuildContext context, AnalysisUserItem analysisUserItem) {
    if (analysisUserItem.userId == 45397590352) {
      print(analysisUserItem.youFollow ? "following" : "notFollowing");
    }
    return ListTile(
        leading: Container(
          width: MediaQuery.of(context).size.width / 8,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(25),
            child: ProfilePicture(profilePicUrl: analysisUserItem.imageUrl),
          ),
        ),
        // trailing: analysisUserItem.youFollow != null
        //     ? BlocProvider<FriendshipCubit>(
        //         create: (context) {
        //           return GetIt.instance.get<FriendshipCubit>();
        //         },
        //         child: FriendshipButton(
        //           analysisUserItem: analysisUserItem,
        //         ),
        //       )
        //     : null,
        title: Text(
            analysisUserItem.name != null
                ? analysisUserItem.name
                : analysisUserItem.userName,
            overflow: TextOverflow.ellipsis,
            style: appTextTheme.subtitle2.copyWith(fontSize: 16)),
        subtitle:
            Text(analysisUserItem.userName, style: appTextTheme.bodyText1),
        onTap: () {
          //setState(() {});
          Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => UserProfile(
              userId: analysisUserItem.userId,
              username: analysisUserItem.userName,
            ),
          ));
        });
  }
}
