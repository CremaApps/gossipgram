import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:injectable/injectable.dart';

@singleton
class StoryRepository {
  final DbProvider _dbProvider;

  StoryRepository(this._dbProvider);

  Future<bool> existsStory({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('Story', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty;
  }

  Future<Story> getStory({
    @required int id,
  }) async {
    final db = await _dbProvider.database;
    var res = await db.query(
      'Story',
      where: 'id = ?',
      whereArgs: [id],
    );
    return res.isNotEmpty ? Story.fromMap(res.first) : null;
  }

  Future<List<Story>> queryStories({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
            'FROM Story '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var dbStories = await db.rawQuery(query);
    List<Story> stories = dbStories.isNotEmpty
        ? dbStories.map((a) => Story.fromMap(a)).toList()
        : [];
    return stories;
  }

  Future<int> queryCountStories({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM Story '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var dbStoriesCount = await db.rawQuery(query);

    return dbStoriesCount.first.values.first;
  }

  Future<int> upsertStory({@required Story story}) async {
    final db = await _dbProvider.database;
    var dbStory = await db.query(
      'Story',
      where: 'id = ?',
      whereArgs: [story.id],
    );
    if (dbStory.isNotEmpty) {
      return db.update(
        'Story',
        story.toMap(),
        where: 'id = ?',
        whereArgs: [story.id],
      );
    } else {
      return await db.insert('Story', story.toMap());
    }
  }

  Future<int> deleteStory({@required Story story}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'Story',
      where: 'id = ?',
      whereArgs: [story.id],
    );
  }

  Future<List<int>> getStoryIds({
    bool isValid = true,
    int createdAtGreaterThan,
    int createdAtLesserThan,
    int storyId,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT id '
            'FROM Story '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (createdAtGreaterThan != null) {
      query += 'AND createdAt > ' + createdAtGreaterThan.toString() + ' ';
    }

    if (createdAtLesserThan != null) {
      query += 'AND createdAt < ' + createdAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> storyIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int id = a['id'];
            return id;
          }).toList()
        : [];
    return storyIds;
  }

  Future<void> setStoryAsInactive({@required int storyId}) async {
    final db = await _dbProvider.database;
    Story dbStory = await this.getStory(id: storyId);
    if (dbStory != null) {
      Story updatedStory = Story(
        id: dbStory.id,
        createdAt: dbStory.createdAt,
        originalCreatedAtDate: dbStory.originalCreatedAtDate,
        mediaUrl: dbStory.mediaUrl,
        mediaType: dbStory.mediaType,
        viewCount: dbStory.viewCount,
        thumbnailUrl: dbStory.thumbnailUrl,
        isValid: false,
      );
      return db.update(
        'Story',
        updatedStory.toMap(),
        where: 'id = ?',
        whereArgs: [storyId],
      );
    }
  }
}
