import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:GossipGram/bloc/profile_load/profile_load_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/image_like_grid.dart';
import 'package:GossipGram/widgets/story_seen_grid.dart';

import 'image_grid.dart';

typedef UpdateIndex = Function(int index);

class UserProfileTabs extends StatefulWidget {
  final ProfileLoadBloc profileLoadBloc;
  final UpdateIndex updateSelectedIndex;

  UserProfileTabs({
    @required this.profileLoadBloc,
    @required this.updateSelectedIndex,
  });

  @override
  _UserProfileTabsState createState() => _UserProfileTabsState();
}

class _UserProfileTabsState extends State<UserProfileTabs>
    with TickerProviderStateMixin {
  TabController _nestedTabController;
  int selectedIndex = 0;

  @override
  void initState() {
    super.initState();

    _nestedTabController = TabController(
      initialIndex: selectedIndex,
      length: 3,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _nestedTabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: TabBar(
            labelStyle: appTextTheme.subtitle2.copyWith(fontSize: 15),
            unselectedLabelStyle: appTextTheme.subtitle1.copyWith(fontSize: 15),
            labelColor: appColorScheme.secondary,
            unselectedLabelColor: appColorScheme.onPrimary,
            controller: _nestedTabController,
            onTap: (int index) {
              setState(() {
                selectedIndex = index;
                widget.updateSelectedIndex(index);
                _nestedTabController.animateTo(
                  selectedIndex,
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.ease,
                );
              });
            },
            tabs: <Widget>[
              Tab(
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width / 4.7,
                  child: Text(
                    S.of(context).posts,
                  ),
                ),
              ),
              Tab(
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width / 4.7,
                  child: Text(
                    S.of(context).likes,
                  ),
                ),
              ),
              Tab(
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width / 4.7,
                  child: Text(
                    S.of(context).stories,
                  ),
                ),
              ),
            ],
          ),
        ),
        IndexedStack(
          children: <Widget>[
            Visibility(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsetsDirectional.only(start: 20.0, top: 16),
                      child: Text(
                        S.of(context).userPosts,
                        style: appTextTheme.caption.copyWith(
                            color: getThemeModeAsInt() == 0
                                ? appColorScheme.onPrimary
                                : appColorScheme.onBackground),
                      ),
                    ),
                    Container(
                      margin: EdgeInsetsDirectional.only(
                          top: 5, start: 16.0, end: 16.0),
                      child: ImageGrid(
                        profileLoadBloc: widget.profileLoadBloc,
                        emptyMessage: S.of(context).noPostsYetUser,
                      ),
                    ),
                  ],
                ),
              ),
              maintainState: true,
              visible: selectedIndex == 0,
            ),
            Visibility(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsetsDirectional.only(start: 20.0, top: 16),
                      child: Text(
                        S.of(context).postsLikedByThisUser,
                        style: appTextTheme.caption.copyWith(
                            color: getThemeModeAsInt() == 0
                                ? appColorScheme.onPrimary
                                : appColorScheme.onBackground),
                      ),
                    ),
                    Container(
                      margin: EdgeInsetsDirectional.only(
                          top: 5, start: 16.0, end: 16.0),
                      child: ImageLikeGrid(
                        profileLoadBloc: widget.profileLoadBloc,
                        emptyMessage: S.of(context).userHasNotSeenYourPostsYet,
                      ),
                    ),
                  ],
                ),
              ),
              maintainState: true,
              visible: selectedIndex == 1,
            ),
            Visibility(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsetsDirectional.only(start: 20.0, top: 16),
                      child: Text(
                        S.of(context).storiesSeenByThisUser,
                        style: appTextTheme.caption.copyWith(
                            color: getThemeModeAsInt() == 0
                                ? appColorScheme.onPrimary
                                : appColorScheme.onBackground),
                      ),
                    ),
                    Builder(
                      builder: (context) {
                        return Container(
                          margin: EdgeInsetsDirectional.only(
                              top: 5, start: 16.0, end: 16.0),
                          child: StorySeenGrid(
                            profileLoadBloc: widget.profileLoadBloc,
                            emptyMessage:
                                S.of(context).userHasNotSeenYourStoriesYet,
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              maintainState: true,
              visible: selectedIndex == 2,
            ),
          ],
          index: selectedIndex,
        ),
      ],
    );
  }
}
