import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/dashboard.dart';
import 'package:GossipGram/service/navigator_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';

part 'dashboard_box.freezed.dart';

@freezed
abstract class DashboardBox with _$DashboardBox {
  // ANALYSIS
  static const String NOT_FOLLOWING_YOU_BACK_BOX = 'NOT_FOLLOWING_YOU_BACK_BOX';
  static const String BLOCKED_YOU_BOX = 'BLOCKED_YOU_BOX';
  static const String STOPPED_FOLLOWING_YOU_BOX = 'STOPPED_FOLLOWING_YOU_BOX';
  static const String SEEN_YOUR_PROFILE_BOX = 'SEEN_YOUR_PROFILE_BOX';
  static const String SEEN_YOUR_STORY_BOX = 'SEEN_YOUR_STORY_BOX';
  static const String YOURE_NOT_FOLLOWING_BACK_BOX =
      'YOURE_NOT_FOLLOWING_BACK_BOX';

  // INSIGHT
  static const String NEW_FOLLOWERS_BOX = 'NEW_FOLLOWERS_BOX';
  static const String LIKED_YOU_THE_MOST_AND_LEAST_BOX =
      'LIKED_YOU_THE_MOST_AND_LEAST_BOX';
  static const String MOST_AND_LEAST_LIKED_POSTS_BOX =
      'MOST_AND_LEAST_LIKED_POSTS_BOX';
  static const String SECRET_ADMIRERS_BOX = 'SECRET_ADMIRERS_BOX';
  static const String DELETED_YOU_LIKES_OR_COMMENTS_BOX =
      'DELETED_YOU_LIKES_OR_COMMENTS_BOX';
  static const String YOU_LIKED_BUT_DONT_FOLLOW_BOX =
      'YOU_LIKED_BUT_DONT_FOLLOW_BOX';

  const factory DashboardBox({
    String title,
    int value,
    Function redirectFunction,
    bool isPaidFunctionality,
    bool updated,
    Image icon,
  }) = _DashboardBox;

  factory DashboardBox.fromConstants({
    @required BuildContext context,
    @required String boxName,
    Dashboard dashboard,
  }) {
    final S trans = S.of(context);
    final NavigatorService navigatorService =
        GetIt.instance.get<NavigatorService>();
    final Map<String, dynamic> paidFunctions =
        GetIt.instance.get<AppConfig>().getMap(AppConfig.PAID_FEATURES);

    switch (boxName) {
      case NOT_FOLLOWING_YOU_BACK_BOX:
        return DashboardBox(
          title: trans.notFollowingYouBack,
          value: dashboard?.notFollowingBack,
          redirectFunction: () => navigatorService.navigateTo(
            '/not-following-you-back',
          ),
          isPaidFunctionality: paidFunctions[
              PaidFeaturesKeys.NOT_FOLLOWING_YOU_BACK_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-dontfollow-you.png',
            width: 21,
            height: 21,
          ),
        );
      case BLOCKED_YOU_BOX:
        return DashboardBox(
          title: trans.blockedYou,
          value: dashboard?.blockedUsers,
          redirectFunction: () => navigatorService.navigateTo(
            '/blocked-you',
          ),
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.BLOCKED_YOU_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-block.png',
            width: 21,
            height: 21,
          ),
        );
      case STOPPED_FOLLOWING_YOU_BOX:
        return DashboardBox(
          title: trans.stoppedFollowingYou,
          value: dashboard?.lostFollowers,
          redirectFunction: () => navigatorService.navigateTo(
            '/stopped-following-you',
          ),
          isPaidFunctionality: paidFunctions[
              PaidFeaturesKeys.STOPPED_FOLLOWING_YOU_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-stop-follow.png',
            width: 21,
            height: 21,
          ),
        );
      case SEEN_YOUR_PROFILE_BOX:
        return DashboardBox(
          title: trans.seenYourProfile,
          value: dashboard?.profileViews,
          redirectFunction: () => navigatorService.navigateTo(
            '/seen-your-profile',
          ),
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.SEEN_YOUR_PROFILE_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-views.png',
            width: 21,
            height: 21,
          ),
        );
      case SEEN_YOUR_STORY_BOX:
        return DashboardBox(
          title: trans.seenYourStory,
          value: dashboard?.storyViews,
          redirectFunction: () => navigatorService.navigateTo(
            '/seen-your-story',
          ),
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.SEEN_YOUR_STORY_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-stories-seen.png',
            width: 21,
            height: 21,
          ),
        );
      case YOURE_NOT_FOLLOWING_BACK_BOX:
        return DashboardBox(
          title: trans.notFollowingBack,
          value: dashboard?.notFollowerBack,
          redirectFunction: () => navigatorService.navigateTo(
            '/youre-not-following-back',
          ),
          isPaidFunctionality: paidFunctions[
              PaidFeaturesKeys.YOURE_NOT_FOLLOWING_BACK_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-you-dontfollow.png',
            width: 21,
            height: 21,
          ),
        );
      // INSIGHT
      case NEW_FOLLOWERS_BOX:
        return DashboardBox(
          title: trans.newfollowers,
          value: dashboard?.newFollowers,
          redirectFunction: () => navigatorService.navigateTo(
            '/new-followers',
          ),
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.NEW_FOLLOWERS_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-new-followers.png',
            width: 21,
            height: 21,
          ),
        );
      case LIKED_YOU_THE_MOST_AND_LEAST_BOX:
        return DashboardBox(
          title: trans.likedYouTheMostAndLeast,
          value: dashboard?.likedYouTheMost,
          redirectFunction: () => navigatorService.navigateTo(
            '/most-least-liked-by-user',
          ),
          isPaidFunctionality: paidFunctions[
              PaidFeaturesKeys.LIKED_YOU_THE_MOST_AND_LEAST_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-like.png',
            width: 21,
            height: 21,
          ),
        );
      case MOST_AND_LEAST_LIKED_POSTS_BOX:
        return DashboardBox(
          title: trans.mostAndLeastLikedPosts,
          value: dashboard?.totalPosts,
          redirectFunction: () => navigatorService.navigateTo(
            '/most-least-liked-feed',
          ),
          isPaidFunctionality: paidFunctions[
              PaidFeaturesKeys.LIKED_YOU_THE_MOST_AND_LEAST_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-liked-posts.png',
            width: 21,
            height: 21,
          ),
        );
      case SECRET_ADMIRERS_BOX:
        return DashboardBox(
          title: trans.secretAdmirers,
          value: dashboard?.secretAdmirers,
          redirectFunction: () => navigatorService.navigateTo(
            '/secret-admirers',
          ),
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.SECRET_ADMIRERS_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-secret-admirers.png',
            width: 21,
            height: 21,
          ),
        );
      case DELETED_YOU_LIKES_OR_COMMENTS_BOX:
        return DashboardBox(
          title: trans.deletedYouLikesOrComments,
          value: dashboard?.deletedLikesOrComments,
          redirectFunction: () => navigatorService.navigateTo(
            '/deleted-you-likes-comments',
          ),
          isPaidFunctionality: paidFunctions[
              PaidFeaturesKeys.DELETED_YOU_LIKES_OR_COMMENTS_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-deleted-likes.png',
            width: 21,
            height: 21,
          ),
        );
      case YOU_LIKED_BUT_DONT_FOLLOW_BOX:
        return DashboardBox(
          title: trans.youLikedButDontFollow,
          value: dashboard?.nonFollowingAdmiredUsers,
          redirectFunction: () => navigatorService.navigateTo(
            '/liked-but-dont-follow',
          ),
          isPaidFunctionality: paidFunctions[
              PaidFeaturesKeys.YOU_LIKED_BUT_DONT_FOLLOW_PAID_FEATURE],
          updated: false,
          icon: Image.asset(
            'assets/images/card-icon-like-dontfollow.png',
            width: 21,
            height: 21,
          ),
        );
      default:
        throw UnimplementedError();
    }
  }
}
