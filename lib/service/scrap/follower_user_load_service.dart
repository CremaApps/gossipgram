import 'package:flutter/material.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user/follower_user.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@singleton
class FollowerUserLoadService extends UserLoadService {
  static const String SERVICE_NAME = "follower_user_load_service";

  FollowerUserRepository _followerUserRepository;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  FollowerUserLoadService(
    this._followerUserRepository,
    this._appConfig,
    this._localStorage,
    UserRepository userRepository,
  ) : super(userRepository);

  Future<void> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    int nowDate = firstLoad ? 0 : DateTime.now().millisecondsSinceEpoch;
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);

    var followersResponse = await instagramPrivateApi.friendship.followers(
      instagramPrivateApi.myUserId,
    );
    DataLoadBloc.printProfiling("API followers media step loaded", now);

    var followers = followersResponse.response.users;
    while (followersResponse.hasNext()) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));

      followersResponse = await followersResponse.next();
      followers.addAll(followersResponse.response.users);
      DataLoadBloc.printProfiling("API followers media step loaded", now);
    }
    DataLoadBloc.printProfiling("API followers media all loaded", now);

    List<int> activeFollowerUserIds =
        await _followerUserRepository.getFollowerUserIds(
      isValid: true,
    );

    for (int id in activeFollowerUserIds) {
      if (!followers.any((user) => user.pk == id)) {
        await _followerUserRepository.upsertFollowerUser(
          fUser: FollowerUser(id: id, isValid: false, youFollow: false),
        );
      }
    }

    List<FollowerUser> dbFollowerUsers =
        await _followerUserRepository.queryFollowerUsers(
      isValid: true,
    );
    List<FollowerUser> dbFollowerUsersThatYouDontFollow =
        await _followerUserRepository.queryFollowerUsers(
            isValid: true, notFollowed: true);
    List<Account.User> toProcessUsers = [];
    List<FollowerUser> toUpsertFollowerUsers = [];
    for (var follower in followers) {
      bool youFollowUser = false;
      for (var user in dbFollowerUsersThatYouDontFollow) {
        if (follower.pk == user.id) {
          youFollowUser == false;
        }
      }
      toProcessUsers.add(
        Account.User(
          id: follower.pk,
          username: follower.username,
          fullName: follower.fullName,
          isPrivate: follower.isPrivate,
          isVerified: follower.isVerified,
          youFollow: youFollowUser,
          profilePicUrl: follower.profilePicUrl,
          anonymousProfilePicture: follower.hasAnonymousProfilePicture,
        ),
      );

      FollowerUser dbUser = dbFollowerUsers.firstWhere(
          (element) => element.id == follower.pk,
          orElse: () => null);
      int onStateChange = dbUser != null ? dbUser.onStateChange : nowDate;

      toUpsertFollowerUsers.add(FollowerUser(
        id: follower.pk,
        isValid: true,
        youFollow: youFollowUser,
        onStateChange: onStateChange,
      ));
    }

    await processUsersInBatchIfNotAlreadyProcessed(
        toProcessUsers, processedUsers);

    await _followerUserRepository.upsertFollowerUsers(
      fUsers: toUpsertFollowerUsers,
    );

    DataLoadBloc.printProfiling("Followers media DB loaded", now);
  }
}
