import 'dart:io' show Platform;

import 'package:GossipGram/themes/themes.dart';
import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/service/analytics/appsflyer_service.dart';
import 'package:GossipGram/service/setup_user_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:GossipGram/service/storage/secure_storage.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:lottie/lottie.dart';

part 'login_form_bottom_popup.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final SecureStorage _secureStorage = GetIt.I.get<SecureStorage>();
  final SetUpUserService _setUpUserService = GetIt.I.get<SetUpUserService>();
  final AppConfig _appConfig = GetIt.I.get<AppConfig>();
  final AppsflyerService _appsflyerService = GetIt.I.get<AppsflyerService>();

  void initState() {
    super.initState();
    _ShowBottomDialog(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).loginInstagram,
          style: appTextTheme.subtitle2,
        ),
      ),
      body: InstagramLogin(
        successCallback: () async {
          var instagramPrivateApi = InstagramPrivateApi.instance();
          String cookieString =
              instagramPrivateApi.cookies.map((e) => e.toString()).reduce(
                    (value, element) =>
                        value + SecureStorage.COOKIE_SPLIT + element,
                  );

          _secureStorage.setInstagramCookie(cookieString);

          await _setUpUserService.execute();
          _appsflyerService.logEvent(eventName: 'af_login');

          String packageBaseName = 'com.cremapps.gossipgram';

          await BackgroundFetch.scheduleTask(
            TaskConfig(
              taskId: "$packageBaseName.postqueue",
              delay: _appConfig.getInt(
                      AppConfig.BACKGROUND_FEED_SCHEDULE_DELAY_SECONDS) *
                  1000,
              requiredNetworkType: NetworkType.ANY,
              enableHeadless: true,
              forceAlarmManager: true,
              periodic: true,
              requiresBatteryNotLow: true,
            ),
          );

          await Navigator.pop(context);
          await Navigator.popAndPushNamed(context, '/home');
        },
        errorCallback: (e) {},
        loadingCallback: () => Center(
          child: Container(
            child: Lottie.asset(
              'assets/animations/loader.json',
              width: 100,
              height: 100,
            ),
          ),
        ),
        automaticLogin: _appConfig.getBool(
          AppConfig.INSTA_AUTOLOGIN,
        ),
      ),
    );
  }
}

void _ShowBottomDialog(BuildContext context) {
  WidgetsBinding.instance.addPostFrameCallback((_) {
    showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext bc) {
        return _LoginBottomPopup();
      },
    );
  });
}
