import 'package:flutter/material.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;

@singleton
class GetUsersYouLikedMediaService extends UserLoadService {
  InstagramPrivateApi _instagramPrivateApi;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  GetUsersYouLikedMediaService(
    this._appConfig,
    this._localStorage,
    UserRepository userRepository,
  ) : super(userRepository) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<List<int>> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);

    var likesResponse = await _instagramPrivateApi.feed.liked();
    DataLoadBloc.printProfiling("API liked media step loaded", now);

    var likeMedia = likesResponse.response.items;
    while (likesResponse.hasNext() &&
        likeMedia.length <= _appConfig.getInt(AppConfig.LIKED_THRESHOLD)) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));
      likesResponse = await likesResponse.next();
      likeMedia.addAll(likesResponse.response.items);
      DataLoadBloc.printProfiling("API liked media step loaded", now);
    }
    DataLoadBloc.printProfiling("API liked media all loaded", now);

    List<int> userIds = [];
    List<Account.User> toProcessUsers = [];

    for (var media in likeMedia) {
      if (!userIds.contains(media.user.pk)) {
        userIds.add(media.user.pk);

        toProcessUsers.add(
          Account.User(
            id: media.user.pk,
            username: media.user.username,
            fullName: media.user.fullName,
            isPrivate: media.user.isPrivate,
            isVerified: media.user.isVerified,
            profilePicUrl: media.user.profilePicUrl,
            youFollow: true,
            anonymousProfilePicture: false,
          ),
        );
      }
    }

    await processUsersInBatchIfNotAlreadyProcessed(
        toProcessUsers, processedUsers);
    DataLoadBloc.printProfiling("Liked media DB loaded", now);
    return userIds;
  }
}
