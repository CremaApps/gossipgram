// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'people_stories.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PeopleStoriesTearOff {
  const _$PeopleStoriesTearOff();

// ignore: unused_element
  _PeopleStories call({List<PeopleStory> stories}) {
    return _PeopleStories(
      stories: stories,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PeopleStories = _$PeopleStoriesTearOff();

/// @nodoc
mixin _$PeopleStories {
  List<PeopleStory> get stories;

  $PeopleStoriesCopyWith<PeopleStories> get copyWith;
}

/// @nodoc
abstract class $PeopleStoriesCopyWith<$Res> {
  factory $PeopleStoriesCopyWith(
          PeopleStories value, $Res Function(PeopleStories) then) =
      _$PeopleStoriesCopyWithImpl<$Res>;
  $Res call({List<PeopleStory> stories});
}

/// @nodoc
class _$PeopleStoriesCopyWithImpl<$Res>
    implements $PeopleStoriesCopyWith<$Res> {
  _$PeopleStoriesCopyWithImpl(this._value, this._then);

  final PeopleStories _value;
  // ignore: unused_field
  final $Res Function(PeopleStories) _then;

  @override
  $Res call({
    Object stories = freezed,
  }) {
    return _then(_value.copyWith(
      stories:
          stories == freezed ? _value.stories : stories as List<PeopleStory>,
    ));
  }
}

/// @nodoc
abstract class _$PeopleStoriesCopyWith<$Res>
    implements $PeopleStoriesCopyWith<$Res> {
  factory _$PeopleStoriesCopyWith(
          _PeopleStories value, $Res Function(_PeopleStories) then) =
      __$PeopleStoriesCopyWithImpl<$Res>;
  @override
  $Res call({List<PeopleStory> stories});
}

/// @nodoc
class __$PeopleStoriesCopyWithImpl<$Res>
    extends _$PeopleStoriesCopyWithImpl<$Res>
    implements _$PeopleStoriesCopyWith<$Res> {
  __$PeopleStoriesCopyWithImpl(
      _PeopleStories _value, $Res Function(_PeopleStories) _then)
      : super(_value, (v) => _then(v as _PeopleStories));

  @override
  _PeopleStories get _value => super._value as _PeopleStories;

  @override
  $Res call({
    Object stories = freezed,
  }) {
    return _then(_PeopleStories(
      stories:
          stories == freezed ? _value.stories : stories as List<PeopleStory>,
    ));
  }
}

/// @nodoc
class _$_PeopleStories implements _PeopleStories {
  const _$_PeopleStories({this.stories});

  @override
  final List<PeopleStory> stories;

  @override
  String toString() {
    return 'PeopleStories(stories: $stories)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PeopleStories &&
            (identical(other.stories, stories) ||
                const DeepCollectionEquality().equals(other.stories, stories)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(stories);

  @override
  _$PeopleStoriesCopyWith<_PeopleStories> get copyWith =>
      __$PeopleStoriesCopyWithImpl<_PeopleStories>(this, _$identity);
}

abstract class _PeopleStories implements PeopleStories {
  const factory _PeopleStories({List<PeopleStory> stories}) = _$_PeopleStories;

  @override
  List<PeopleStory> get stories;
  @override
  _$PeopleStoriesCopyWith<_PeopleStories> get copyWith;
}
