import 'bloc/analysis_bloc/ianalysis_bloc.dart';
import 'generated/l10n.dart';
import 'pages/analysis_page.dart';
import 'pages/dashboard_page/dashboard_page.dart';
import 'pages/home.dart';
import 'pages/insights_most_least_liked_page.dart';
import 'pages/insights_most_least_likes_by_user_page.dart';
import 'pages/login_form/login_form.dart';
import 'pages/login_page.dart';
import 'pages/my_stories_page/my_stories_page.dart';
import 'pages/profile_stories_page/people_stories_page.dart';
import 'pages/seen_your_story_page.dart';
import 'pages/stories_most_least_seen_by_user_page.dart';
import 'pages/tutorial_page.dart';

final routes = {
  '/login': (_) => LoginPage(),
  '/login-form': (_) => LoginForm(),
  '/home': (_) => Home(),
  '/dashboard': (_) => DashboardPage(),
  '/tutorial': (_) => TutorialPage(),
  '/mystories': (_) => MyStoriesPage(),
  '/peoplestories': (_) => PeopleStoriesPage(),
  '/most-least-liked-feed': (_) => InsightsMostLeastLikedPage(),
  '/most-least-liked-by-user': (_) => InsightsMostLeastLikesByUserPage(),
  '/seen-your-story': (_) => SeenYourStoryPage(),
  // Analysis Page
  '/not-following-you-back': (context) => AnalysisPage(
        title: S.of(context).notFollowingYouBack,
        keyName: IAnalysisBloc.NOT_FOLLOWING_BACK,
      ),
  '/youre-not-following-back': (context) => AnalysisPage(
        title: S.of(context).notFollowingBack,
        keyName: IAnalysisBloc.YOURE_NOT_FOLLOWING,
      ),
  '/blocked-you': (context) => AnalysisPage(
        title: S.of(context).blockedYou,
        keyName: IAnalysisBloc.BLOCKED_YOU,
      ),
  '/stopped-following-you': (context) => AnalysisPage(
        title: S.of(context).stoppedFollowingYou,
        keyName: IAnalysisBloc.STOPPED_FOLLOWING_YOU,
      ),
  '/seen-your-profile': (context) => AnalysisPage(
        title: S.of(context).seenYourProfile,
        keyName: IAnalysisBloc.SEEN_YOUR_PROFILE,
      ),
  '/new-followers': (context) => AnalysisPage(
        title: S.of(context).newfollowers,
        keyName: IAnalysisBloc.NEW_FOLLOWERS,
      ),
  '/secret-admirers': (context) => AnalysisPage(
        title: S.of(context).secretAdmirers,
        keyName: IAnalysisBloc.SECRET_ADMIRERS,
      ),
  '/deleted-you-likes-comments': (context) => AnalysisPage(
        title: S.of(context).deletedYouLikesOrComments,
        keyName: IAnalysisBloc.DELETED_YOU_LIKES_COMMENTS,
      ),
  '/liked-but-dont-follow': (context) => AnalysisPage(
        title: S.of(context).youLikedButDontFollow,
        keyName: IAnalysisBloc.LIKED_BUT_DONT_FOLLOW,
      ),
  '/stories/dont-follow-you': (context) => AnalysisPage(
        title: S.of(context).dontFollowYou,
        keyName: IAnalysisBloc.STORIES_DONT_FOLLOW_YOU,
      ),
  '/stories/you-dont-follow': (context) => AnalysisPage(
        title: S.of(context).youDontFollow,
        keyName: IAnalysisBloc.STORIES_YOU_DONT_FOLLOW,
      ),
  '/followers': (context) => AnalysisPage(
        title: S.of(context).followers,
        keyName: IAnalysisBloc.FOLLOWERS,
      ),
  '/followed': (context) => AnalysisPage(
        title: S.of(context).following,
        keyName: IAnalysisBloc.FOLLOWED,
      ),
  '/stories/most-seen-by-user': (_) => StoriesMostLeastSeenByUserPage(
        selectedTab: 0,
      ),
  '/stories/least-seen-by-user': (_) => StoriesMostLeastSeenByUserPage(
        selectedTab: 1,
      ),
};
