part of 'dashboard_page.dart';

class _DashboardLoader extends StatelessWidget {
  final DataLoadBloc dataLoadBloc;

  _DashboardLoader({@required this.dataLoadBloc});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataLoadBloc, DataLoadState>(
      cubit: dataLoadBloc,
      builder: (context, state) {
        if (state is DataLoadStateLoading) {
          return Positioned.directional(
            textDirection: Directionality.of(context),
            top: MediaQuery.of(context).size.height / 15,
            start: MediaQuery.of(context).size.width / 20,
            child: Container(
              child: CircularProgressIndicator(
                strokeWidth: 2.5,
              ),
              width: 16,
              height: 16,
            ),
          );
        }
        return Container();
      },
    );
  }
}
