// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'premium_subtitle_text.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PremiumSubtitleTextTearOff {
  const _$PremiumSubtitleTextTearOff();

// ignore: unused_element
  _PremiumSubtitleText call({String title, String subtitle}) {
    return _PremiumSubtitleText(
      title: title,
      subtitle: subtitle,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PremiumSubtitleText = _$PremiumSubtitleTextTearOff();

/// @nodoc
mixin _$PremiumSubtitleText {
  String get title;
  String get subtitle;

  $PremiumSubtitleTextCopyWith<PremiumSubtitleText> get copyWith;
}

/// @nodoc
abstract class $PremiumSubtitleTextCopyWith<$Res> {
  factory $PremiumSubtitleTextCopyWith(
          PremiumSubtitleText value, $Res Function(PremiumSubtitleText) then) =
      _$PremiumSubtitleTextCopyWithImpl<$Res>;
  $Res call({String title, String subtitle});
}

/// @nodoc
class _$PremiumSubtitleTextCopyWithImpl<$Res>
    implements $PremiumSubtitleTextCopyWith<$Res> {
  _$PremiumSubtitleTextCopyWithImpl(this._value, this._then);

  final PremiumSubtitleText _value;
  // ignore: unused_field
  final $Res Function(PremiumSubtitleText) _then;

  @override
  $Res call({
    Object title = freezed,
    Object subtitle = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed ? _value.title : title as String,
      subtitle: subtitle == freezed ? _value.subtitle : subtitle as String,
    ));
  }
}

/// @nodoc
abstract class _$PremiumSubtitleTextCopyWith<$Res>
    implements $PremiumSubtitleTextCopyWith<$Res> {
  factory _$PremiumSubtitleTextCopyWith(_PremiumSubtitleText value,
          $Res Function(_PremiumSubtitleText) then) =
      __$PremiumSubtitleTextCopyWithImpl<$Res>;
  @override
  $Res call({String title, String subtitle});
}

/// @nodoc
class __$PremiumSubtitleTextCopyWithImpl<$Res>
    extends _$PremiumSubtitleTextCopyWithImpl<$Res>
    implements _$PremiumSubtitleTextCopyWith<$Res> {
  __$PremiumSubtitleTextCopyWithImpl(
      _PremiumSubtitleText _value, $Res Function(_PremiumSubtitleText) _then)
      : super(_value, (v) => _then(v as _PremiumSubtitleText));

  @override
  _PremiumSubtitleText get _value => super._value as _PremiumSubtitleText;

  @override
  $Res call({
    Object title = freezed,
    Object subtitle = freezed,
  }) {
    return _then(_PremiumSubtitleText(
      title: title == freezed ? _value.title : title as String,
      subtitle: subtitle == freezed ? _value.subtitle : subtitle as String,
    ));
  }
}

/// @nodoc
class _$_PremiumSubtitleText implements _PremiumSubtitleText {
  const _$_PremiumSubtitleText({this.title, this.subtitle});

  @override
  final String title;
  @override
  final String subtitle;

  @override
  String toString() {
    return 'PremiumSubtitleText(title: $title, subtitle: $subtitle)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PremiumSubtitleText &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.subtitle, subtitle) ||
                const DeepCollectionEquality()
                    .equals(other.subtitle, subtitle)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(subtitle);

  @override
  _$PremiumSubtitleTextCopyWith<_PremiumSubtitleText> get copyWith =>
      __$PremiumSubtitleTextCopyWithImpl<_PremiumSubtitleText>(
          this, _$identity);
}

abstract class _PremiumSubtitleText implements PremiumSubtitleText {
  const factory _PremiumSubtitleText({String title, String subtitle}) =
      _$_PremiumSubtitleText;

  @override
  String get title;
  @override
  String get subtitle;
  @override
  _$PremiumSubtitleTextCopyWith<_PremiumSubtitleText> get copyWith;
}
