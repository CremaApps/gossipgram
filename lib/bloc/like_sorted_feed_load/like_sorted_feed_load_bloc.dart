import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/service/api/get_my_stored_feed_images_service.dart';
import 'package:injectable/injectable.dart';

part 'like_sorted_feed_load_event.dart';
part 'like_sorted_feed_load_state.dart';

@injectable
class LikeSortedFeedLoadBloc
    extends Bloc<LikeSortedFeedLoadEvent, LikeSortedFeedLoadState> {
  GetMyStoredFeedImagesService _getMyStoredFeedImagesService;

  LikeSortedFeedLoadBloc(
    this._getMyStoredFeedImagesService,
  ) : super(LikeSortedFeedLoadInitial());

  @override
  Stream<LikeSortedFeedLoadState> mapEventToState(
    LikeSortedFeedLoadEvent event,
  ) async* {
    if (event is LikeSortedFeedLoadEventStart) {
      yield LikeSortedFeedLoadInitial();

      List<FeedThumbnail> feedImages =
          await _getMyStoredFeedImagesService.execute();

      List<FeedThumbnail> mostLikedFeed = List.from(
          feedImages..sort((a, b) => b.likeCount.compareTo(a.likeCount)));
      List<FeedThumbnail> leastLikedFeed = mostLikedFeed.reversed.toList();

      yield LikeSortedFeedLoaded(
        mostLikedFeed: _cutList(mostLikedFeed, event.maxItems),
        leastLikedFeed: _cutList(leastLikedFeed, event.maxItems),
      );
    }
  }

  List<FeedThumbnail> _cutList(List<FeedThumbnail> list, int length) {
    return (list.length <= length) ? list : list.sublist(0, length);
  }
}
