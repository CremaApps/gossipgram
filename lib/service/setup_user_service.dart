import 'dart:io' show Platform;
import 'dart:math';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/subscription/subscription_service.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

@lazySingleton
class SetUpUserService {
  static const _KEY_ALIAS = "SUBSCRIPTION_ALIAS";

  SubscriptionService _subscriptionService;
  LocalStorage _localStorage;
  FirebaseAnalytics _firebaseAnalytics;
  InstagramPrivateApi _instagramPrivateApi;

  SetUpUserService(
    this._subscriptionService,
    this._localStorage,
  ) {
    _firebaseAnalytics =
        GetIt.I.get<FirebaseAnalytics>(instanceName: 'analytics');
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<void> execute() async {
    String subscriptionAlias = _localStorage.read(_KEY_ALIAS);

    if (subscriptionAlias == null) {
      Random r = Random();

      subscriptionAlias =
          'INSTA_${Platform.operatingSystem}_${_instagramPrivateApi.myUserId}_${String.fromCharCodes(List.generate(8, (index) => r.nextInt(33) + 89))}';

      await _localStorage.write(
        _KEY_ALIAS,
        subscriptionAlias,
      );

      await _subscriptionService.setAlias(subscriptionAlias);
    }

    FirebaseCrashlytics.instance.setUserIdentifier(
      subscriptionAlias,
    );

    _firebaseAnalytics.setUserId(
      subscriptionAlias,
    );

    return _localStorage.write(
      DashboardBloc.USER_ID,
      _instagramPrivateApi.myUserId,
      generalStorage: true,
    );
  }
}
