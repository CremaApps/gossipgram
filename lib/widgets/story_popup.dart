import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/story_popup/story_popup_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/story_popup.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:story_view/controller/story_controller.dart';
import 'package:story_view/story_view.dart';

class StoryPopupComponent extends StatefulWidget {
  final String userId;
  final String profilePic;
  final String fullname;

  const StoryPopupComponent({
    @required this.userId,
    @required this.profilePic,
    @required this.fullname,
  });

  @override
  _StoryPopupComponentState createState() => _StoryPopupComponentState();
}

class _StoryPopupComponentState extends State<StoryPopupComponent>
    with SingleTickerProviderStateMixin {
  StoryPopupBloc _storyPopupBloc;
  PageController _pageController;
  StoryController storyController = StoryController();

  @override
  void initState() {
    super.initState();
    _storyPopupBloc = GetIt.instance.get<StoryPopupBloc>();
    _storyPopupBloc.add(PopupStoriesStartLoadEvent(userId: widget.userId));
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
        cubit: _storyPopupBloc,
        builder: (context, state) {
          if (state is StoryPopupLoading) {
            return CenteredLoader();
          } else if (state is StoryPopupLoaded) {
            return Stack(
              children: [
                Material(
                  child: StoryView(
                    storyItems: _prepareStoriesList(state.storiesPopup),
                    onComplete: () {
                      Navigator.of(context).pop();
                    },
                    progressPosition: ProgressPosition.top,
                    repeat: false,
                    controller: storyController,
                  ),
                ),
                Positioned(
                  left: 20.0,
                  top: MediaQuery.of(context).size.height / 12,
                  child: _buildProfileView(
                    profilePic: widget.profilePic,
                    fullname: widget.fullname,
                  ),
                ),
                Positioned(
                  right: 20.0,
                  top: MediaQuery.of(context).size.height / 12,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Align(
                      alignment: Alignment.topRight,
                      child: CircleAvatar(
                        radius: 14.0,
                        backgroundColor: Colors.transparent,
                        child: Icon(
                          Icons.close,
                          size: 30,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: Text(S.of(context).genericError),
            );
          }
        },
      ),
    );
  }

  List<StoryItem> _prepareStoriesList(List<StoryPopup> storyPopups) {
    List<StoryItem> storiesList = [];

    storyPopups.forEach((element) {
      switch (element.storyType) {
        case StoryPopup.VIDEO_TYPE:
          storiesList.add(
            StoryItem.pageVideo(
              element.storyAsset,
              controller: storyController,
            ),
          );
          break;
        default:
          storiesList.add(
            StoryItem.pageImage(
              url: element.storyAsset,
              controller: storyController,
            ),
          );
      }
    });

    return storiesList;
  }
}

class _buildProfileView extends StatelessWidget {
  final String profilePic;
  final String fullname;
  _buildProfileView({this.profilePic, this.fullname});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CircleAvatar(
          radius: 24,
          backgroundImage: NetworkImage(profilePic),
        ),
        SizedBox(
          width: 16,
        ),
        Material(
          color: Colors.transparent,
          child: Text(
            fullname,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
