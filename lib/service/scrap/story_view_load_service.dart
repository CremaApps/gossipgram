import 'package:flutter/material.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/story_repository.dart';
import 'package:GossipGram/database/repository/story_view_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:GossipGram/model/objects/story_view.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:injectable/injectable.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;

@singleton
class StoryViewLoadService extends UserLoadService {
  StoryViewRepository _storyViewRepository;
  StoryRepository _storyRepository;
  InstagramPrivateApi _instagramPrivateApi;
  AppConfig _appConfig;

  StoryViewLoadService(
    this._storyViewRepository,
    this._storyRepository,
    this._appConfig,
    UserRepository userRepository,
  ) : super(userRepository) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<void> execute(
      {@required Story story,
      @required ProcessedUsersWrapper processedUsers,
      bool firstLoad = false}) async {
    var storyResponse =
        await _instagramPrivateApi.media.reelMediaViewer(story.id);

    var views = storyResponse.response.users;
    while (storyResponse.hasNext()) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));
      storyResponse = await storyResponse.next();
      views.addAll(storyResponse.response.users);
    }

    List<dynamic> activeStoryViewIds = await _storyViewRepository
        .getStoryViewIds(isValid: true, storyId: story.id);

    for (dynamic id in activeStoryViewIds) {
      if (!views.any((view) =>
          (story.id == id["storyId"] && view.pk == id["seenByUserId"]))) {
        await _storyViewRepository.upsertStoryView(
          storyView: StoryView(
            storyId: id["storyId"],
            seenByUserId: id["seenByUserId"],
            isValid: false,
          ),
        );
      }
    }

    int firstLoadOrder = 0;
    for (var storyView in views.reversed.toList()) {
      StoryView dbStoryView = await _storyViewRepository.getStoryView(
        storyId: story.id,
        seenByUserId: storyView.pk,
      );
      int createdAt = dbStoryView != null && dbStoryView.isValid == true
          ? dbStoryView.createdAt
          : firstLoad
              ? firstLoadOrder++
              : DateTime.now().millisecondsSinceEpoch;

      Account.User user = Account.User(
        id: storyView.pk,
        username: storyView.username,
        fullName: storyView.fullName,
        isPrivate: storyView.isPrivate,
        isVerified: storyView.isVerified,
        profilePicUrl: storyView.profilePicUrl,
        anonymousProfilePicture: false,
      );

      await processUserIfNotAlreadyProcessed(user, processedUsers);

      await _storyViewRepository.upsertStoryView(
          storyView: StoryView(
        storyId: story.id,
        seenByUserId: storyView.pk,
        isValid: true,
        createdAt: createdAt,
      ));

      await _storyRepository.upsertStory(
        story: Story(
          id: story.id,
          thumbnailUrl: story.thumbnailUrl,
          mediaUrl: story.mediaUrl,
          mediaType: story.mediaType,
          viewCount: views.length,
          originalCreatedAtDate: story.originalCreatedAtDate,
          createdAt: story.createdAt,
          isValid: story.isValid,
        ),
      );
    }
  }
}
