part of 'people_stories_page.dart';

// ignore: must_be_immutable
class _StoriesList extends StatelessWidget {
  final PeopleStories peopleStories;
  final bool finished;
  final PeopleStoriesBloc _peopleStoriesBloc =
      GetIt.instance.get<PeopleStoriesBloc>();
  _StoriesList({
    @required this.peopleStories,
    @required this.finished,
  }) : super();

  bool _lazyLoadWorking = false;
  ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    _controller.addListener(_scrollListener);
    _lazyLoadWorking = false;
    List<Widget> elements = [
      Container(
        padding: EdgeInsetsDirectional.only(start: 15, end: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                margin: EdgeInsetsDirectional.only(top: 15),
                child: Column(
                  children: <Widget>[
                    GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: peopleStories.stories.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 0.719148936170213,
                      ),
                      itemBuilder: (contxt, indx) {
                        PeopleStory peopleStory = peopleStories.stories[indx];
                        return _SingleStory(
                          peopleStory: peopleStory,
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ];

    if (!finished) elements.add(Center(child: CircularProgressIndicator()));
    return ListView(
      controller: _controller,
      children: elements,
    );
  }

  void _scrollListener() {
    if (!_lazyLoadWorking &&
        _controller.offset >= _controller.position.maxScrollExtent / 2) {
      _lazyLoadWorking = true;
      _peopleStoriesBloc.add(
        PeopleStoriesLoadEventGetStoryPage(),
      );
    }
  }
}

class _SingleStory extends StatelessWidget {
  final PeopleStory peopleStory;
  const _SingleStory({this.peopleStory}) : super();
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showGeneralDialog(
          context: context,
          barrierColor: Colors.white,
          barrierDismissible: false,
          transitionDuration: Duration(milliseconds: 200),
          pageBuilder: (_, __, ___) {
            return StoryPopupComponent(
              userId: peopleStory.userId,
              fullname: peopleStory.fullName,
              profilePic: peopleStory.profilePic,
            );
          },
        );
      },
      child: Container(
        margin: EdgeInsetsDirectional.only(bottom: 5, end: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              width: 56,
              height: 56,
              margin: EdgeInsetsDirectional.only(bottom: 10),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(peopleStory.profilePic),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                border: Border.all(
                  color: appColorScheme.secondary,
                  width: 2.0,
                ),
              ),
            ),
            Container(
              margin:
                  EdgeInsetsDirectional.only(bottom: 10, start: 10, end: 10),
              child: Text(peopleStory.fullName,
                  overflow: TextOverflow.ellipsis,
                  style: appTextTheme.bodyText1.copyWith(color: Colors.white)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: Colors.black.withOpacity(0.1),
              ),
            )
          ],
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.25), BlendMode.srcOver),
            image: NetworkImage(peopleStory.storyPic),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
