// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'profile.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ProfileTearOff {
  const _$ProfileTearOff();

// ignore: unused_element
  _Profile call(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      int relation,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      List<FeedThumbnail> likedFeedThumbnail,
      List<StoryThumbnail> storyThumbnail,
      InstagramResponse<dynamic> feedResponse}) {
    return _Profile(
      fullName: fullName,
      profilePicUrl: profilePicUrl,
      bio: bio,
      followers: followers,
      following: following,
      relation: relation,
      engagementRate: engagementRate,
      averageLikes: averageLikes,
      followerRatio: followerRatio,
      feedThumbnail: feedThumbnail,
      likedFeedThumbnail: likedFeedThumbnail,
      storyThumbnail: storyThumbnail,
      feedResponse: feedResponse,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Profile = _$ProfileTearOff();

/// @nodoc
mixin _$Profile {
  String get fullName;
  String get profilePicUrl;
  String get bio;
  int get followers;
  int get following;
  int get relation;
  double get engagementRate;
  double get averageLikes;
  double get followerRatio;
  List<FeedThumbnail> get feedThumbnail;
  List<FeedThumbnail> get likedFeedThumbnail;
  List<StoryThumbnail> get storyThumbnail;
  InstagramResponse<dynamic> get feedResponse;

  $ProfileCopyWith<Profile> get copyWith;
}

/// @nodoc
abstract class $ProfileCopyWith<$Res> {
  factory $ProfileCopyWith(Profile value, $Res Function(Profile) then) =
      _$ProfileCopyWithImpl<$Res>;
  $Res call(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      int relation,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      List<FeedThumbnail> likedFeedThumbnail,
      List<StoryThumbnail> storyThumbnail,
      InstagramResponse<dynamic> feedResponse});
}

/// @nodoc
class _$ProfileCopyWithImpl<$Res> implements $ProfileCopyWith<$Res> {
  _$ProfileCopyWithImpl(this._value, this._then);

  final Profile _value;
  // ignore: unused_field
  final $Res Function(Profile) _then;

  @override
  $Res call({
    Object fullName = freezed,
    Object profilePicUrl = freezed,
    Object bio = freezed,
    Object followers = freezed,
    Object following = freezed,
    Object relation = freezed,
    Object engagementRate = freezed,
    Object averageLikes = freezed,
    Object followerRatio = freezed,
    Object feedThumbnail = freezed,
    Object likedFeedThumbnail = freezed,
    Object storyThumbnail = freezed,
    Object feedResponse = freezed,
  }) {
    return _then(_value.copyWith(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl as String,
      bio: bio == freezed ? _value.bio : bio as String,
      followers: followers == freezed ? _value.followers : followers as int,
      following: following == freezed ? _value.following : following as int,
      relation: relation == freezed ? _value.relation : relation as int,
      engagementRate: engagementRate == freezed
          ? _value.engagementRate
          : engagementRate as double,
      averageLikes: averageLikes == freezed
          ? _value.averageLikes
          : averageLikes as double,
      followerRatio: followerRatio == freezed
          ? _value.followerRatio
          : followerRatio as double,
      feedThumbnail: feedThumbnail == freezed
          ? _value.feedThumbnail
          : feedThumbnail as List<FeedThumbnail>,
      likedFeedThumbnail: likedFeedThumbnail == freezed
          ? _value.likedFeedThumbnail
          : likedFeedThumbnail as List<FeedThumbnail>,
      storyThumbnail: storyThumbnail == freezed
          ? _value.storyThumbnail
          : storyThumbnail as List<StoryThumbnail>,
      feedResponse: feedResponse == freezed
          ? _value.feedResponse
          : feedResponse as InstagramResponse<dynamic>,
    ));
  }
}

/// @nodoc
abstract class _$ProfileCopyWith<$Res> implements $ProfileCopyWith<$Res> {
  factory _$ProfileCopyWith(_Profile value, $Res Function(_Profile) then) =
      __$ProfileCopyWithImpl<$Res>;
  @override
  $Res call(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      int relation,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      List<FeedThumbnail> likedFeedThumbnail,
      List<StoryThumbnail> storyThumbnail,
      InstagramResponse<dynamic> feedResponse});
}

/// @nodoc
class __$ProfileCopyWithImpl<$Res> extends _$ProfileCopyWithImpl<$Res>
    implements _$ProfileCopyWith<$Res> {
  __$ProfileCopyWithImpl(_Profile _value, $Res Function(_Profile) _then)
      : super(_value, (v) => _then(v as _Profile));

  @override
  _Profile get _value => super._value as _Profile;

  @override
  $Res call({
    Object fullName = freezed,
    Object profilePicUrl = freezed,
    Object bio = freezed,
    Object followers = freezed,
    Object following = freezed,
    Object relation = freezed,
    Object engagementRate = freezed,
    Object averageLikes = freezed,
    Object followerRatio = freezed,
    Object feedThumbnail = freezed,
    Object likedFeedThumbnail = freezed,
    Object storyThumbnail = freezed,
    Object feedResponse = freezed,
  }) {
    return _then(_Profile(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl as String,
      bio: bio == freezed ? _value.bio : bio as String,
      followers: followers == freezed ? _value.followers : followers as int,
      following: following == freezed ? _value.following : following as int,
      relation: relation == freezed ? _value.relation : relation as int,
      engagementRate: engagementRate == freezed
          ? _value.engagementRate
          : engagementRate as double,
      averageLikes: averageLikes == freezed
          ? _value.averageLikes
          : averageLikes as double,
      followerRatio: followerRatio == freezed
          ? _value.followerRatio
          : followerRatio as double,
      feedThumbnail: feedThumbnail == freezed
          ? _value.feedThumbnail
          : feedThumbnail as List<FeedThumbnail>,
      likedFeedThumbnail: likedFeedThumbnail == freezed
          ? _value.likedFeedThumbnail
          : likedFeedThumbnail as List<FeedThumbnail>,
      storyThumbnail: storyThumbnail == freezed
          ? _value.storyThumbnail
          : storyThumbnail as List<StoryThumbnail>,
      feedResponse: feedResponse == freezed
          ? _value.feedResponse
          : feedResponse as InstagramResponse<dynamic>,
    ));
  }
}

/// @nodoc
class _$_Profile implements _Profile {
  const _$_Profile(
      {this.fullName,
      this.profilePicUrl,
      this.bio,
      this.followers,
      this.following,
      this.relation,
      this.engagementRate,
      this.averageLikes,
      this.followerRatio,
      this.feedThumbnail,
      this.likedFeedThumbnail,
      this.storyThumbnail,
      this.feedResponse});

  @override
  final String fullName;
  @override
  final String profilePicUrl;
  @override
  final String bio;
  @override
  final int followers;
  @override
  final int following;
  @override
  final int relation;
  @override
  final double engagementRate;
  @override
  final double averageLikes;
  @override
  final double followerRatio;
  @override
  final List<FeedThumbnail> feedThumbnail;
  @override
  final List<FeedThumbnail> likedFeedThumbnail;
  @override
  final List<StoryThumbnail> storyThumbnail;
  @override
  final InstagramResponse<dynamic> feedResponse;

  @override
  String toString() {
    return 'Profile(fullName: $fullName, profilePicUrl: $profilePicUrl, bio: $bio, followers: $followers, following: $following, relation: $relation, engagementRate: $engagementRate, averageLikes: $averageLikes, followerRatio: $followerRatio, feedThumbnail: $feedThumbnail, likedFeedThumbnail: $likedFeedThumbnail, storyThumbnail: $storyThumbnail, feedResponse: $feedResponse)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Profile &&
            (identical(other.fullName, fullName) ||
                const DeepCollectionEquality()
                    .equals(other.fullName, fullName)) &&
            (identical(other.profilePicUrl, profilePicUrl) ||
                const DeepCollectionEquality()
                    .equals(other.profilePicUrl, profilePicUrl)) &&
            (identical(other.bio, bio) ||
                const DeepCollectionEquality().equals(other.bio, bio)) &&
            (identical(other.followers, followers) ||
                const DeepCollectionEquality()
                    .equals(other.followers, followers)) &&
            (identical(other.following, following) ||
                const DeepCollectionEquality()
                    .equals(other.following, following)) &&
            (identical(other.relation, relation) ||
                const DeepCollectionEquality()
                    .equals(other.relation, relation)) &&
            (identical(other.engagementRate, engagementRate) ||
                const DeepCollectionEquality()
                    .equals(other.engagementRate, engagementRate)) &&
            (identical(other.averageLikes, averageLikes) ||
                const DeepCollectionEquality()
                    .equals(other.averageLikes, averageLikes)) &&
            (identical(other.followerRatio, followerRatio) ||
                const DeepCollectionEquality()
                    .equals(other.followerRatio, followerRatio)) &&
            (identical(other.feedThumbnail, feedThumbnail) ||
                const DeepCollectionEquality()
                    .equals(other.feedThumbnail, feedThumbnail)) &&
            (identical(other.likedFeedThumbnail, likedFeedThumbnail) ||
                const DeepCollectionEquality()
                    .equals(other.likedFeedThumbnail, likedFeedThumbnail)) &&
            (identical(other.storyThumbnail, storyThumbnail) ||
                const DeepCollectionEquality()
                    .equals(other.storyThumbnail, storyThumbnail)) &&
            (identical(other.feedResponse, feedResponse) ||
                const DeepCollectionEquality()
                    .equals(other.feedResponse, feedResponse)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(fullName) ^
      const DeepCollectionEquality().hash(profilePicUrl) ^
      const DeepCollectionEquality().hash(bio) ^
      const DeepCollectionEquality().hash(followers) ^
      const DeepCollectionEquality().hash(following) ^
      const DeepCollectionEquality().hash(relation) ^
      const DeepCollectionEquality().hash(engagementRate) ^
      const DeepCollectionEquality().hash(averageLikes) ^
      const DeepCollectionEquality().hash(followerRatio) ^
      const DeepCollectionEquality().hash(feedThumbnail) ^
      const DeepCollectionEquality().hash(likedFeedThumbnail) ^
      const DeepCollectionEquality().hash(storyThumbnail) ^
      const DeepCollectionEquality().hash(feedResponse);

  @override
  _$ProfileCopyWith<_Profile> get copyWith =>
      __$ProfileCopyWithImpl<_Profile>(this, _$identity);
}

abstract class _Profile implements Profile {
  const factory _Profile(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      int relation,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      List<FeedThumbnail> likedFeedThumbnail,
      List<StoryThumbnail> storyThumbnail,
      InstagramResponse<dynamic> feedResponse}) = _$_Profile;

  @override
  String get fullName;
  @override
  String get profilePicUrl;
  @override
  String get bio;
  @override
  int get followers;
  @override
  int get following;
  @override
  int get relation;
  @override
  double get engagementRate;
  @override
  double get averageLikes;
  @override
  double get followerRatio;
  @override
  List<FeedThumbnail> get feedThumbnail;
  @override
  List<FeedThumbnail> get likedFeedThumbnail;
  @override
  List<StoryThumbnail> get storyThumbnail;
  @override
  InstagramResponse<dynamic> get feedResponse;
  @override
  _$ProfileCopyWith<_Profile> get copyWith;
}
