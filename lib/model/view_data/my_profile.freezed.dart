// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'my_profile.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$MyProfileTearOff {
  const _$MyProfileTearOff();

// ignore: unused_element
  _MyProfile call(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      InstagramResponse<dynamic> feedResponse}) {
    return _MyProfile(
      fullName: fullName,
      profilePicUrl: profilePicUrl,
      bio: bio,
      followers: followers,
      following: following,
      engagementRate: engagementRate,
      averageLikes: averageLikes,
      followerRatio: followerRatio,
      feedThumbnail: feedThumbnail,
      feedResponse: feedResponse,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $MyProfile = _$MyProfileTearOff();

/// @nodoc
mixin _$MyProfile {
  String get fullName;
  String get profilePicUrl;
  String get bio;
  int get followers;
  int get following;
  double get engagementRate;
  double get averageLikes;
  double get followerRatio;
  List<FeedThumbnail> get feedThumbnail;
  InstagramResponse<dynamic> get feedResponse;

  $MyProfileCopyWith<MyProfile> get copyWith;
}

/// @nodoc
abstract class $MyProfileCopyWith<$Res> {
  factory $MyProfileCopyWith(MyProfile value, $Res Function(MyProfile) then) =
      _$MyProfileCopyWithImpl<$Res>;
  $Res call(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      InstagramResponse<dynamic> feedResponse});
}

/// @nodoc
class _$MyProfileCopyWithImpl<$Res> implements $MyProfileCopyWith<$Res> {
  _$MyProfileCopyWithImpl(this._value, this._then);

  final MyProfile _value;
  // ignore: unused_field
  final $Res Function(MyProfile) _then;

  @override
  $Res call({
    Object fullName = freezed,
    Object profilePicUrl = freezed,
    Object bio = freezed,
    Object followers = freezed,
    Object following = freezed,
    Object engagementRate = freezed,
    Object averageLikes = freezed,
    Object followerRatio = freezed,
    Object feedThumbnail = freezed,
    Object feedResponse = freezed,
  }) {
    return _then(_value.copyWith(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl as String,
      bio: bio == freezed ? _value.bio : bio as String,
      followers: followers == freezed ? _value.followers : followers as int,
      following: following == freezed ? _value.following : following as int,
      engagementRate: engagementRate == freezed
          ? _value.engagementRate
          : engagementRate as double,
      averageLikes: averageLikes == freezed
          ? _value.averageLikes
          : averageLikes as double,
      followerRatio: followerRatio == freezed
          ? _value.followerRatio
          : followerRatio as double,
      feedThumbnail: feedThumbnail == freezed
          ? _value.feedThumbnail
          : feedThumbnail as List<FeedThumbnail>,
      feedResponse: feedResponse == freezed
          ? _value.feedResponse
          : feedResponse as InstagramResponse<dynamic>,
    ));
  }
}

/// @nodoc
abstract class _$MyProfileCopyWith<$Res> implements $MyProfileCopyWith<$Res> {
  factory _$MyProfileCopyWith(
          _MyProfile value, $Res Function(_MyProfile) then) =
      __$MyProfileCopyWithImpl<$Res>;
  @override
  $Res call(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      InstagramResponse<dynamic> feedResponse});
}

/// @nodoc
class __$MyProfileCopyWithImpl<$Res> extends _$MyProfileCopyWithImpl<$Res>
    implements _$MyProfileCopyWith<$Res> {
  __$MyProfileCopyWithImpl(_MyProfile _value, $Res Function(_MyProfile) _then)
      : super(_value, (v) => _then(v as _MyProfile));

  @override
  _MyProfile get _value => super._value as _MyProfile;

  @override
  $Res call({
    Object fullName = freezed,
    Object profilePicUrl = freezed,
    Object bio = freezed,
    Object followers = freezed,
    Object following = freezed,
    Object engagementRate = freezed,
    Object averageLikes = freezed,
    Object followerRatio = freezed,
    Object feedThumbnail = freezed,
    Object feedResponse = freezed,
  }) {
    return _then(_MyProfile(
      fullName: fullName == freezed ? _value.fullName : fullName as String,
      profilePicUrl: profilePicUrl == freezed
          ? _value.profilePicUrl
          : profilePicUrl as String,
      bio: bio == freezed ? _value.bio : bio as String,
      followers: followers == freezed ? _value.followers : followers as int,
      following: following == freezed ? _value.following : following as int,
      engagementRate: engagementRate == freezed
          ? _value.engagementRate
          : engagementRate as double,
      averageLikes: averageLikes == freezed
          ? _value.averageLikes
          : averageLikes as double,
      followerRatio: followerRatio == freezed
          ? _value.followerRatio
          : followerRatio as double,
      feedThumbnail: feedThumbnail == freezed
          ? _value.feedThumbnail
          : feedThumbnail as List<FeedThumbnail>,
      feedResponse: feedResponse == freezed
          ? _value.feedResponse
          : feedResponse as InstagramResponse<dynamic>,
    ));
  }
}

/// @nodoc
class _$_MyProfile implements _MyProfile {
  const _$_MyProfile(
      {this.fullName,
      this.profilePicUrl,
      this.bio,
      this.followers,
      this.following,
      this.engagementRate,
      this.averageLikes,
      this.followerRatio,
      this.feedThumbnail,
      this.feedResponse});

  @override
  final String fullName;
  @override
  final String profilePicUrl;
  @override
  final String bio;
  @override
  final int followers;
  @override
  final int following;
  @override
  final double engagementRate;
  @override
  final double averageLikes;
  @override
  final double followerRatio;
  @override
  final List<FeedThumbnail> feedThumbnail;
  @override
  final InstagramResponse<dynamic> feedResponse;

  @override
  String toString() {
    return 'MyProfile(fullName: $fullName, profilePicUrl: $profilePicUrl, bio: $bio, followers: $followers, following: $following, engagementRate: $engagementRate, averageLikes: $averageLikes, followerRatio: $followerRatio, feedThumbnail: $feedThumbnail, feedResponse: $feedResponse)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MyProfile &&
            (identical(other.fullName, fullName) ||
                const DeepCollectionEquality()
                    .equals(other.fullName, fullName)) &&
            (identical(other.profilePicUrl, profilePicUrl) ||
                const DeepCollectionEquality()
                    .equals(other.profilePicUrl, profilePicUrl)) &&
            (identical(other.bio, bio) ||
                const DeepCollectionEquality().equals(other.bio, bio)) &&
            (identical(other.followers, followers) ||
                const DeepCollectionEquality()
                    .equals(other.followers, followers)) &&
            (identical(other.following, following) ||
                const DeepCollectionEquality()
                    .equals(other.following, following)) &&
            (identical(other.engagementRate, engagementRate) ||
                const DeepCollectionEquality()
                    .equals(other.engagementRate, engagementRate)) &&
            (identical(other.averageLikes, averageLikes) ||
                const DeepCollectionEquality()
                    .equals(other.averageLikes, averageLikes)) &&
            (identical(other.followerRatio, followerRatio) ||
                const DeepCollectionEquality()
                    .equals(other.followerRatio, followerRatio)) &&
            (identical(other.feedThumbnail, feedThumbnail) ||
                const DeepCollectionEquality()
                    .equals(other.feedThumbnail, feedThumbnail)) &&
            (identical(other.feedResponse, feedResponse) ||
                const DeepCollectionEquality()
                    .equals(other.feedResponse, feedResponse)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(fullName) ^
      const DeepCollectionEquality().hash(profilePicUrl) ^
      const DeepCollectionEquality().hash(bio) ^
      const DeepCollectionEquality().hash(followers) ^
      const DeepCollectionEquality().hash(following) ^
      const DeepCollectionEquality().hash(engagementRate) ^
      const DeepCollectionEquality().hash(averageLikes) ^
      const DeepCollectionEquality().hash(followerRatio) ^
      const DeepCollectionEquality().hash(feedThumbnail) ^
      const DeepCollectionEquality().hash(feedResponse);

  @override
  _$MyProfileCopyWith<_MyProfile> get copyWith =>
      __$MyProfileCopyWithImpl<_MyProfile>(this, _$identity);
}

abstract class _MyProfile implements MyProfile {
  const factory _MyProfile(
      {String fullName,
      String profilePicUrl,
      String bio,
      int followers,
      int following,
      double engagementRate,
      double averageLikes,
      double followerRatio,
      List<FeedThumbnail> feedThumbnail,
      InstagramResponse<dynamic> feedResponse}) = _$_MyProfile;

  @override
  String get fullName;
  @override
  String get profilePicUrl;
  @override
  String get bio;
  @override
  int get followers;
  @override
  int get following;
  @override
  double get engagementRate;
  @override
  double get averageLikes;
  @override
  double get followerRatio;
  @override
  List<FeedThumbnail> get feedThumbnail;
  @override
  InstagramResponse<dynamic> get feedResponse;
  @override
  _$MyProfileCopyWith<_MyProfile> get copyWith;
}
