import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/service/navigator_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';

part 'my_story_box.freezed.dart';

@freezed
abstract class MyStoryBox with _$MyStoryBox {
  // ANALYSIS
  static const String STORIES_DONT_FOLLOW_YOU_BOX =
      'STORIES_DONT_FOLLOW_YOU_BOX';
  static const String STORIES_YOU_DONT_FOLLOW_BOX =
      'STORIES_YOU_DONT_FOLLOW_BOX';
  static const String STORIES_MOST_SEEN_BOX = 'STORIES_MOST_SEEN_BOX';
  static const String STORIES_LEAST_SEEN_BOX = 'STORIES_LEAST_SEEN_BOX';

  const factory MyStoryBox({
    String title,
    int value,
    Function redirectFunction,
    bool isPaidFunctionality,
    bool updated,
  }) = _MyStoryBox;

  factory MyStoryBox.fromConstants({
    @required BuildContext context,
    @required String boxName,
  }) {
    final S trans = S.of(context);
    final NavigatorService navigatorService =
        GetIt.instance.get<NavigatorService>();
    final Map<String, dynamic> paidFunctions =
        GetIt.instance.get<AppConfig>().getMap(AppConfig.PAID_FEATURES);

    switch (boxName) {
      case STORIES_DONT_FOLLOW_YOU_BOX:
        return MyStoryBox(
          title: trans.dontFollowYou,
          redirectFunction: () => navigatorService.navigateTo(
            '/stories/dont-follow-you',
          ),
          value: -1,
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.STORIES_DONT_FOLLOW_YOU_FEATURE],
          updated: false,
        );
      case STORIES_YOU_DONT_FOLLOW_BOX:
        return MyStoryBox(
          title: trans.youDontFollow,
          redirectFunction: () => navigatorService.navigateTo(
            '/stories/you-dont-follow',
          ),
          value: -1,
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.STORIES_YOU_DONT_FOLLOW_FEATURE],
          updated: false,
        );
      case STORIES_MOST_SEEN_BOX:
        return MyStoryBox(
          title: trans.seenYourStoryTheMost,
          redirectFunction: () => navigatorService.navigateTo(
            '/stories/most-seen-by-user',
          ),
          value: -1,
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.STORIES_MOST_SEEN_FEATURE],
          updated: false,
        );
      case STORIES_LEAST_SEEN_BOX:
        return MyStoryBox(
          title: trans.seenYourStoryTheLeast,
          redirectFunction: () => navigatorService.navigateTo(
            '/stories/least-seen-by-user',
          ),
          value: -1,
          isPaidFunctionality:
              paidFunctions[PaidFeaturesKeys.STORIES_LEAST_SEEN_FEATURE],
          updated: false,
        );
      default:
        throw UnimplementedError();
    }
  }
}
