// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:localizely_sdk/localizely_sdk.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    if (!Localizely.hasMetadata()) {
      Localizely.setMetadata(_metadata);
    } 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  static final Map<String, List<String>> _metadata = {
    'actived': [],
    'activeStories': [],
    'analysis': [],
    'averageLikes': [],
    'blockedYou': [],
    'buy_text_lifetime': [],
    'buy_text_monthly': [],
    'buy_text_one_week': [],
    'buy_text_six_months': [],
    'buy_text_three_months': [],
    'buy_text_two_months': [],
    'buy_text_yearly': [],
    'continuePremium': [],
    'dashboard': [],
    'deletedYouLikesOrComments': [],
    'deletedYourTaggedPosts': [],
    'disable': [],
    'discoverWhoBlockedYouAndMore': [],
    'discoverWhoSpendsTheMostTimeLooking': [],
    'doesNotFollow': [],
    'dontFollowYou': [],
    'engagementRate': [],
    'extraHiddenInformation': [],
    'failedPurchaseRestore': [],
    'findWhoLooksAtYourStories': [],
    'followerRatio': [],
    'followers': [],
    'following': [],
    'followingEachOther': [],
    'friendStories': [],
    'genericError': [],
    'getYourPlan': [],
    'homeEntry': [],
    'howCanWeHelpYou': [],
    'howWouldYouRateOurApp': [],
    'ifYouNeedToContactWithUs': [],
    'ILiked': [],
    'insight': [],
    'Least': [],
    'leastSeen': [],
    'LikedBy': [],
    'likedYouTheMostAndLeast': [],
    'likes': [],
    'loginInstagram': [],
    'loginWithInstagram': [],
    'logOut': [],
    'maybeLater': [],
    'Most': [],
    'mostAndLeastLikedPosts': [],
    'mostPopular': [],
    'mostRecent': [],
    'myAudience': [],
    'myStories': [],
    'newfollowers': [],
    'nightMode': [],
    'nobodyFound': [],
    'noOneYet': [],
    'noPosts': [],
    'noPostsYet': [],
    'noPostsYetUser': [],
    'noStories': [],
    'noStoriesShared': [],
    'notFollowingBack': [],
    'notFollowingEachOther': [],
    'notFollowingThisUser': [],
    'notFollowingYouBack': [],
    'notificationDailyReminderBody': [],
    'notificationDailyReminderTitle': [],
    'notifications': [],
    'notificationSeenYourProfileScheduledBody': [],
    'notificationSeenYourProfileScheduledTitle': [],
    'oldest': [],
    'onlyYouKnowYourPassword': [],
    'ourCommitmentToPrivacy': [],
    'people': [],
    'peopleStories': [],
    'posts': [],
    'postsLikedByThisUser': [],
    'privacyPolicy': [],
    'private': [],
    'privateProfile': [],
    'profile': [],
    'profileDoesntExists': [],
    'proModeSubtitle1': [],
    'proModeSubtitle2': [],
    'proModeSubtitle3': [],
    'proModeSubtitle4': [],
    'proModeSubtitle5': [],
    'proModeTitle': [],
    'purchasesRestored': [],
    'purchasesRestoredFailed': [],
    'purchasesRestoredSuccess': [],
    'restorePurchases': [],
    'returnToLimitedFunctionalities': [],
    'secretAdmirers': [],
    'secure': [],
    'seenby': [],
    'seenYourProfile': [],
    'seenYourStory': [],
    'seenYourStoryTheLeast': [],
    'seenYourStoryTheMost': [],
    'seenYourStoryTheMostAndTheLeast': [],
    'seeSecretAdmirers': [],
    'seeWhoSeenStoriesTheMostAndLeast': [],
    'settings': [],
    'signIn': ['privacyPolicy', 'termsOfUse'],
    'somethingWentWrong': [],
    'sortBy': [],
    'startSharing': [],
    'stoppedFollowingYou': [],
    'stories': [],
    'storiesSeenByThisUser': [],
    'submit': [],
    'Submit': [],
    'support': [],
    'supportMail': [],
    'termsOfUse': [],
    'thisActionOpenMailingApp': [],
    'trusted': [],
    'tutorialPageBodyFour': [],
    'tutorialPageBodyOne': [],
    'tutorialPageBodyThree': [],
    'tutorialPageBodyTwo': [],
    'tutorialPageTitleFour': [],
    'tutorialPageTitleOne': [],
    'tutorialPageTitleThree': [],
    'tutorialPageTitleTwo': [],
    'tutorialStartButton': [],
    'uncoverAnyoneActivities': [],
    'unimplementedState': [],
    'upgradePrimiumAndFindOutWhatPeopleUpTo': [],
    'userHasNotSeenYourPostsYet': [],
    'userHasNotSeenYourStoriesYet': [],
    'userPosts': [],
    'userStories': [],
    'weDontShareYourData': [],
    'weDontTrackOrSend': [],
    'whoAreYourBiggestFans': [],
    'whoReallyLikesYourStory': [],
    'wopsy': [],
    'youDontFollow': [],
    'youHaventSharedAStoryYet': [],
    'youLikedButDontFollow': [],
    'yourOpinionMattersToUs': [],
    'yourStories': []
  };

  /// `Actived`
  String get actived {
    return Intl.message(
      'Actived',
      name: 'actived',
      desc: '',
      args: [],
    );
  }

  /// `Active stories`
  String get activeStories {
    return Intl.message(
      'Active stories',
      name: 'activeStories',
      desc: '',
      args: [],
    );
  }

  /// `Analysis`
  String get analysis {
    return Intl.message(
      'Analysis',
      name: 'analysis',
      desc: '',
      args: [],
    );
  }

  /// `Average likes`
  String get averageLikes {
    return Intl.message(
      'Average likes',
      name: 'averageLikes',
      desc: '',
      args: [],
    );
  }

  /// `Blocked you`
  String get blockedYou {
    return Intl.message(
      'Blocked you',
      name: 'blockedYou',
      desc: '',
      args: [],
    );
  }

  /// `Lifetime`
  String get buy_text_lifetime {
    return Intl.message(
      'Lifetime',
      name: 'buy_text_lifetime',
      desc: '',
      args: [],
    );
  }

  /// `1 Month`
  String get buy_text_monthly {
    return Intl.message(
      '1 Month',
      name: 'buy_text_monthly',
      desc: '',
      args: [],
    );
  }

  /// `1 Week`
  String get buy_text_one_week {
    return Intl.message(
      '1 Week',
      name: 'buy_text_one_week',
      desc: '',
      args: [],
    );
  }

  /// `6 Months`
  String get buy_text_six_months {
    return Intl.message(
      '6 Months',
      name: 'buy_text_six_months',
      desc: '',
      args: [],
    );
  }

  /// `3 Months`
  String get buy_text_three_months {
    return Intl.message(
      '3 Months',
      name: 'buy_text_three_months',
      desc: '',
      args: [],
    );
  }

  /// `2 Months`
  String get buy_text_two_months {
    return Intl.message(
      '2 Months',
      name: 'buy_text_two_months',
      desc: '',
      args: [],
    );
  }

  /// `1 Year`
  String get buy_text_yearly {
    return Intl.message(
      '1 Year',
      name: 'buy_text_yearly',
      desc: '',
      args: [],
    );
  }

  /// `CONTINUE`
  String get continuePremium {
    return Intl.message(
      'CONTINUE',
      name: 'continuePremium',
      desc: '',
      args: [],
    );
  }

  /// `Dashboard`
  String get dashboard {
    return Intl.message(
      'Dashboard',
      name: 'dashboard',
      desc: '',
      args: [],
    );
  }

  /// `Deleted their likes or comments`
  String get deletedYouLikesOrComments {
    return Intl.message(
      'Deleted their likes or comments',
      name: 'deletedYouLikesOrComments',
      desc: '',
      args: [],
    );
  }

  /// `Deleted their tagged posts`
  String get deletedYourTaggedPosts {
    return Intl.message(
      'Deleted their tagged posts',
      name: 'deletedYourTaggedPosts',
      desc: '',
      args: [],
    );
  }

  /// `Disable`
  String get disable {
    return Intl.message(
      'Disable',
      name: 'disable',
      desc: '',
      args: [],
    );
  }

  /// `Discover who’s blocked you, who’s been looking at your profile, and much more!`
  String get discoverWhoBlockedYouAndMore {
    return Intl.message(
      'Discover who’s blocked you, who’s been looking at your profile, and much more!',
      name: 'discoverWhoBlockedYouAndMore',
      desc: '',
      args: [],
    );
  }

  /// `Discover who spends the most time looking at your stories.`
  String get discoverWhoSpendsTheMostTimeLooking {
    return Intl.message(
      'Discover who spends the most time looking at your stories.',
      name: 'discoverWhoSpendsTheMostTimeLooking',
      desc: '',
      args: [],
    );
  }

  /// `Does not follow you`
  String get doesNotFollow {
    return Intl.message(
      'Does not follow you',
      name: 'doesNotFollow',
      desc: '',
      args: [],
    );
  }

  /// `Don’t follow you`
  String get dontFollowYou {
    return Intl.message(
      'Don’t follow you',
      name: 'dontFollowYou',
      desc: '',
      args: [],
    );
  }

  /// `Engagement rate`
  String get engagementRate {
    return Intl.message(
      'Engagement rate',
      name: 'engagementRate',
      desc: '',
      args: [],
    );
  }

  /// `Extra, hidden information`
  String get extraHiddenInformation {
    return Intl.message(
      'Extra, hidden information',
      name: 'extraHiddenInformation',
      desc: '',
      args: [],
    );
  }

  /// `Failed restoring purchases, please try again later.`
  String get failedPurchaseRestore {
    return Intl.message(
      'Failed restoring purchases, please try again later.',
      name: 'failedPurchaseRestore',
      desc: '',
      args: [],
    );
  }

  /// `Find out who views your stories but doesn’t follow you.`
  String get findWhoLooksAtYourStories {
    return Intl.message(
      'Find out who views your stories but doesn’t follow you.',
      name: 'findWhoLooksAtYourStories',
      desc: '',
      args: [],
    );
  }

  /// `Follower ratio`
  String get followerRatio {
    return Intl.message(
      'Follower ratio',
      name: 'followerRatio',
      desc: '',
      args: [],
    );
  }

  /// `Followers`
  String get followers {
    return Intl.message(
      'Followers',
      name: 'followers',
      desc: '',
      args: [],
    );
  }

  /// `Following`
  String get following {
    return Intl.message(
      'Following',
      name: 'following',
      desc: '',
      args: [],
    );
  }

  /// `You're following each other`
  String get followingEachOther {
    return Intl.message(
      'You\'re following each other',
      name: 'followingEachOther',
      desc: '',
      args: [],
    );
  }

  /// `People Stories`
  String get friendStories {
    return Intl.message(
      'People Stories',
      name: 'friendStories',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong, please try again later`
  String get genericError {
    return Intl.message(
      'Something went wrong, please try again later',
      name: 'genericError',
      desc: '',
      args: [],
    );
  }

  /// `Get your plan`
  String get getYourPlan {
    return Intl.message(
      'Get your plan',
      name: 'getYourPlan',
      desc: '',
      args: [],
    );
  }

  /// `Discover the hidden analysis and insights of your social media`
  String get homeEntry {
    return Intl.message(
      'Discover the hidden analysis and insights of your social media',
      name: 'homeEntry',
      desc: '',
      args: [],
    );
  }

  /// `How can we help you?`
  String get howCanWeHelpYou {
    return Intl.message(
      'How can we help you?',
      name: 'howCanWeHelpYou',
      desc: '',
      args: [],
    );
  }

  /// `Do you like Gossipgram?`
  String get howWouldYouRateOurApp {
    return Intl.message(
      'Do you like Gossipgram?',
      name: 'howWouldYouRateOurApp',
      desc: '',
      args: [],
    );
  }

  /// `Want to get in touch with any question or suggestion? We would love to hear from you. Please don't hesitate to drop us a line, we will contact you as soon as possible`
  String get ifYouNeedToContactWithUs {
    return Intl.message(
      'Want to get in touch with any question or suggestion? We would love to hear from you. Please don\'t hesitate to drop us a line, we will contact you as soon as possible',
      name: 'ifYouNeedToContactWithUs',
      desc: '',
      args: [],
    );
  }

  /// `You liked`
  String get ILiked {
    return Intl.message(
      'You liked',
      name: 'ILiked',
      desc: '',
      args: [],
    );
  }

  /// `Insights`
  String get insight {
    return Intl.message(
      'Insights',
      name: 'insight',
      desc: '',
      args: [],
    );
  }

  /// `Least`
  String get Least {
    return Intl.message(
      'Least',
      name: 'Least',
      desc: '',
      args: [],
    );
  }

  /// `Least Seen`
  String get leastSeen {
    return Intl.message(
      'Least Seen',
      name: 'leastSeen',
      desc: '',
      args: [],
    );
  }

  /// `Liked by`
  String get LikedBy {
    return Intl.message(
      'Liked by',
      name: 'LikedBy',
      desc: '',
      args: [],
    );
  }

  /// `Liked you the most/least`
  String get likedYouTheMostAndLeast {
    return Intl.message(
      'Liked you the most/least',
      name: 'likedYouTheMostAndLeast',
      desc: '',
      args: [],
    );
  }

  /// `Likes`
  String get likes {
    return Intl.message(
      'Likes',
      name: 'likes',
      desc: '',
      args: [],
    );
  }

  /// `Login Instagram`
  String get loginInstagram {
    return Intl.message(
      'Login Instagram',
      name: 'loginInstagram',
      desc: '',
      args: [],
    );
  }

  /// `LOGIN WITH INSTAGRAM`
  String get loginWithInstagram {
    return Intl.message(
      'LOGIN WITH INSTAGRAM',
      name: 'loginWithInstagram',
      desc: '',
      args: [],
    );
  }

  /// `Log out`
  String get logOut {
    return Intl.message(
      'Log out',
      name: 'logOut',
      desc: '',
      args: [],
    );
  }

  /// `Maybe later`
  String get maybeLater {
    return Intl.message(
      'Maybe later',
      name: 'maybeLater',
      desc: '',
      args: [],
    );
  }

  /// `Most`
  String get Most {
    return Intl.message(
      'Most',
      name: 'Most',
      desc: '',
      args: [],
    );
  }

  /// `Your most/least liked posts`
  String get mostAndLeastLikedPosts {
    return Intl.message(
      'Your most/least liked posts',
      name: 'mostAndLeastLikedPosts',
      desc: '',
      args: [],
    );
  }

  /// `Most Popular`
  String get mostPopular {
    return Intl.message(
      'Most Popular',
      name: 'mostPopular',
      desc: '',
      args: [],
    );
  }

  /// `Newest`
  String get mostRecent {
    return Intl.message(
      'Newest',
      name: 'mostRecent',
      desc: '',
      args: [],
    );
  }

  /// `Your audience`
  String get myAudience {
    return Intl.message(
      'Your audience',
      name: 'myAudience',
      desc: '',
      args: [],
    );
  }

  /// `Your stories`
  String get myStories {
    return Intl.message(
      'Your stories',
      name: 'myStories',
      desc: '',
      args: [],
    );
  }

  /// `New followers`
  String get newfollowers {
    return Intl.message(
      'New followers',
      name: 'newfollowers',
      desc: '',
      args: [],
    );
  }

  /// `Night mode`
  String get nightMode {
    return Intl.message(
      'Night mode',
      name: 'nightMode',
      desc: '',
      args: [],
    );
  }

  /// `Nobody found!`
  String get nobodyFound {
    return Intl.message(
      'Nobody found!',
      name: 'nobodyFound',
      desc: '',
      args: [],
    );
  }

  /// `There’s no one here yet,`
  String get noOneYet {
    return Intl.message(
      'There’s no one here yet,',
      name: 'noOneYet',
      desc: '',
      args: [],
    );
  }

  /// `There are no posts to show.`
  String get noPosts {
    return Intl.message(
      'There are no posts to show.',
      name: 'noPosts',
      desc: '',
      args: [],
    );
  }

  /// `You haven't published any post yet.`
  String get noPostsYet {
    return Intl.message(
      'You haven\'t published any post yet.',
      name: 'noPostsYet',
      desc: '',
      args: [],
    );
  }

  /// `This user hasn't published any post yet.`
  String get noPostsYetUser {
    return Intl.message(
      'This user hasn\'t published any post yet.',
      name: 'noPostsYetUser',
      desc: '',
      args: [],
    );
  }

  /// `The are no stories to show.`
  String get noStories {
    return Intl.message(
      'The are no stories to show.',
      name: 'noStories',
      desc: '',
      args: [],
    );
  }

  /// `You haven't shared any stories since you installed Gossipgram`
  String get noStoriesShared {
    return Intl.message(
      'You haven\'t shared any stories since you installed Gossipgram',
      name: 'noStoriesShared',
      desc: '',
      args: [],
    );
  }

  /// `You’re not following back`
  String get notFollowingBack {
    return Intl.message(
      'You’re not following back',
      name: 'notFollowingBack',
      desc: '',
      args: [],
    );
  }

  /// `You're not following each other`
  String get notFollowingEachOther {
    return Intl.message(
      'You\'re not following each other',
      name: 'notFollowingEachOther',
      desc: '',
      args: [],
    );
  }

  /// `You're not following this user`
  String get notFollowingThisUser {
    return Intl.message(
      'You\'re not following this user',
      name: 'notFollowingThisUser',
      desc: '',
      args: [],
    );
  }

  /// `Is not following you back`
  String get notFollowingYouBack {
    return Intl.message(
      'Is not following you back',
      name: 'notFollowingYouBack',
      desc: '',
      args: [],
    );
  }

  /// `New juicy social stats waiting for you. Check them out! 📈`
  String get notificationDailyReminderBody {
    return Intl.message(
      'New juicy social stats waiting for you. Check them out! 📈',
      name: 'notificationDailyReminderBody',
      desc: '',
      args: [],
    );
  }

  /// `Check Gossipgram's report 🔮`
  String get notificationDailyReminderTitle {
    return Intl.message(
      'Check Gossipgram\'s report 🔮',
      name: 'notificationDailyReminderTitle',
      desc: '',
      args: [],
    );
  }

  /// `Notifications`
  String get notifications {
    return Intl.message(
      'Notifications',
      name: 'notifications',
      desc: '',
      args: [],
    );
  }

  /// `Someone has been looking your profile. Find out who! 🔥`
  String get notificationSeenYourProfileScheduledBody {
    return Intl.message(
      'Someone has been looking your profile. Find out who! 🔥',
      name: 'notificationSeenYourProfileScheduledBody',
      desc: '',
      args: [],
    );
  }

  /// `You're getting popular 🔎`
  String get notificationSeenYourProfileScheduledTitle {
    return Intl.message(
      'You\'re getting popular 🔎',
      name: 'notificationSeenYourProfileScheduledTitle',
      desc: '',
      args: [],
    );
  }

  /// `Oldest`
  String get oldest {
    return Intl.message(
      'Oldest',
      name: 'oldest',
      desc: '',
      args: [],
    );
  }

  /// `Your password is secret, we do not save it.`
  String get onlyYouKnowYourPassword {
    return Intl.message(
      'Your password is secret, we do not save it.',
      name: 'onlyYouKnowYourPassword',
      desc: '',
      args: [],
    );
  }

  /// `Our Commitment to Privacy`
  String get ourCommitmentToPrivacy {
    return Intl.message(
      'Our Commitment to Privacy',
      name: 'ourCommitmentToPrivacy',
      desc: '',
      args: [],
    );
  }

  /// `People`
  String get people {
    return Intl.message(
      'People',
      name: 'people',
      desc: '',
      args: [],
    );
  }

  /// `People Stories`
  String get peopleStories {
    return Intl.message(
      'People Stories',
      name: 'peopleStories',
      desc: '',
      args: [],
    );
  }

  /// `Posts`
  String get posts {
    return Intl.message(
      'Posts',
      name: 'posts',
      desc: '',
      args: [],
    );
  }

  /// `Posts liked by this user`
  String get postsLikedByThisUser {
    return Intl.message(
      'Posts liked by this user',
      name: 'postsLikedByThisUser',
      desc: '',
      args: [],
    );
  }

  /// `Privacy policy`
  String get privacyPolicy {
    return Intl.message(
      'Privacy policy',
      name: 'privacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `Private`
  String get private {
    return Intl.message(
      'Private',
      name: 'private',
      desc: '',
      args: [],
    );
  }

  /// `This profile is private`
  String get privateProfile {
    return Intl.message(
      'This profile is private',
      name: 'privateProfile',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get profile {
    return Intl.message(
      'Profile',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `This profile doesn't exist`
  String get profileDoesntExists {
    return Intl.message(
      'This profile doesn\'t exist',
      name: 'profileDoesntExists',
      desc: '',
      args: [],
    );
  }

  /// `Discover who is secretly viewing your profile`
  String get proModeSubtitle1 {
    return Intl.message(
      'Discover who is secretly viewing your profile',
      name: 'proModeSubtitle1',
      desc: '',
      args: [],
    );
  }

  /// `Keep track of who has seen any of your stories`
  String get proModeSubtitle2 {
    return Intl.message(
      'Keep track of who has seen any of your stories',
      name: 'proModeSubtitle2',
      desc: '',
      args: [],
    );
  }

  /// `Unmask the users that blocked you`
  String get proModeSubtitle3 {
    return Intl.message(
      'Unmask the users that blocked you',
      name: 'proModeSubtitle3',
      desc: '',
      args: [],
    );
  }

  /// `Reveal who has deleted your likes and comments`
  String get proModeSubtitle4 {
    return Intl.message(
      'Reveal who has deleted your likes and comments',
      name: 'proModeSubtitle4',
      desc: '',
      args: [],
    );
  }

  /// `Know your social media secret admirers`
  String get proModeSubtitle5 {
    return Intl.message(
      'Know your social media secret admirers',
      name: 'proModeSubtitle5',
      desc: '',
      args: [],
    );
  }

  /// `Unlock the full potential of social media`
  String get proModeTitle {
    return Intl.message(
      'Unlock the full potential of social media',
      name: 'proModeTitle',
      desc: '',
      args: [],
    );
  }

  /// `Finished purchase restoration`
  String get purchasesRestored {
    return Intl.message(
      'Finished purchase restoration',
      name: 'purchasesRestored',
      desc: '',
      args: [],
    );
  }

  /// `We couldn't find your subscription.`
  String get purchasesRestoredFailed {
    return Intl.message(
      'We couldn\'t find your subscription.',
      name: 'purchasesRestoredFailed',
      desc: '',
      args: [],
    );
  }

  /// `Your purchase has been successfully restored.`
  String get purchasesRestoredSuccess {
    return Intl.message(
      'Your purchase has been successfully restored.',
      name: 'purchasesRestoredSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Restore purchases`
  String get restorePurchases {
    return Intl.message(
      'Restore purchases',
      name: 'restorePurchases',
      desc: '',
      args: [],
    );
  }

  /// `Go on with limited functionalities`
  String get returnToLimitedFunctionalities {
    return Intl.message(
      'Go on with limited functionalities',
      name: 'returnToLimitedFunctionalities',
      desc: '',
      args: [],
    );
  }

  /// `Secret admirers`
  String get secretAdmirers {
    return Intl.message(
      'Secret admirers',
      name: 'secretAdmirers',
      desc: '',
      args: [],
    );
  }

  /// `Secure`
  String get secure {
    return Intl.message(
      'Secure',
      name: 'secure',
      desc: '',
      args: [],
    );
  }

  /// `Seen by`
  String get seenby {
    return Intl.message(
      'Seen by',
      name: 'seenby',
      desc: '',
      args: [],
    );
  }

  /// `Seen your profile`
  String get seenYourProfile {
    return Intl.message(
      'Seen your profile',
      name: 'seenYourProfile',
      desc: '',
      args: [],
    );
  }

  /// `Seen your stories`
  String get seenYourStory {
    return Intl.message(
      'Seen your stories',
      name: 'seenYourStory',
      desc: '',
      args: [],
    );
  }

  /// `Seen your stories the least`
  String get seenYourStoryTheLeast {
    return Intl.message(
      'Seen your stories the least',
      name: 'seenYourStoryTheLeast',
      desc: '',
      args: [],
    );
  }

  /// `Seen your stories the most`
  String get seenYourStoryTheMost {
    return Intl.message(
      'Seen your stories the most',
      name: 'seenYourStoryTheMost',
      desc: '',
      args: [],
    );
  }

  /// `Seen most/least`
  String get seenYourStoryTheMostAndTheLeast {
    return Intl.message(
      'Seen most/least',
      name: 'seenYourStoryTheMostAndTheLeast',
      desc: '',
      args: [],
    );
  }

  /// `Reveal your secret admirers!`
  String get seeSecretAdmirers {
    return Intl.message(
      'Reveal your secret admirers!',
      name: 'seeSecretAdmirers',
      desc: '',
      args: [],
    );
  }

  /// `Discover who has seen your stories the most and the least.`
  String get seeWhoSeenStoriesTheMostAndLeast {
    return Intl.message(
      'Discover who has seen your stories the most and the least.',
      name: 'seeWhoSeenStoriesTheMostAndLeast',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `By signing in, you agree to the {privacyPolicy} and its {termsOfUse}.`
  String signIn(Object privacyPolicy, Object termsOfUse) {
    return Intl.message(
      'By signing in, you agree to the $privacyPolicy and its $termsOfUse.',
      name: 'signIn',
      desc: 'Privacy policy and terms of use in home screen',
      args: [privacyPolicy, termsOfUse],
    );
  }

  /// `Something went wrong`
  String get somethingWentWrong {
    return Intl.message(
      'Something went wrong',
      name: 'somethingWentWrong',
      desc: '',
      args: [],
    );
  }

  /// `Sort by`
  String get sortBy {
    return Intl.message(
      'Sort by',
      name: 'sortBy',
      desc: '',
      args: [],
    );
  }

  /// `Share something cool with your followers and let us do the work`
  String get startSharing {
    return Intl.message(
      'Share something cool with your followers and let us do the work',
      name: 'startSharing',
      desc: '',
      args: [],
    );
  }

  /// `Stopped following you`
  String get stoppedFollowingYou {
    return Intl.message(
      'Stopped following you',
      name: 'stoppedFollowingYou',
      desc: '',
      args: [],
    );
  }

  /// `Stories`
  String get stories {
    return Intl.message(
      'Stories',
      name: 'stories',
      desc: '',
      args: [],
    );
  }

  /// `Stories seen by this user'`
  String get storiesSeenByThisUser {
    return Intl.message(
      'Stories seen by this user\'',
      name: 'storiesSeenByThisUser',
      desc: '',
      args: [],
    );
  }

  /// `Submit`
  String get submit {
    return Intl.message(
      'Submit',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `Submit`
  String get Submit {
    return Intl.message(
      'Submit',
      name: 'Submit',
      desc: '',
      args: [],
    );
  }

  /// `Support`
  String get support {
    return Intl.message(
      'Support',
      name: 'support',
      desc: '',
      args: [],
    );
  }

  /// `hello@cremaapps.com`
  String get supportMail {
    return Intl.message(
      'hello@cremaapps.com',
      name: 'supportMail',
      desc: '',
      args: [],
    );
  }

  /// `Terms of use`
  String get termsOfUse {
    return Intl.message(
      'Terms of use',
      name: 'termsOfUse',
      desc: '',
      args: [],
    );
  }

  /// `*This action will open your mailing app`
  String get thisActionOpenMailingApp {
    return Intl.message(
      '*This action will open your mailing app',
      name: 'thisActionOpenMailingApp',
      desc: '',
      args: [],
    );
  }

  /// `Trusted`
  String get trusted {
    return Intl.message(
      'Trusted',
      name: 'trusted',
      desc: '',
      args: [],
    );
  }

  /// `Find out who follows them. who likes them, and much more!`
  String get tutorialPageBodyFour {
    return Intl.message(
      'Find out who follows them. who likes them, and much more!',
      name: 'tutorialPageBodyFour',
      desc: '',
      args: [],
    );
  }

  /// `Discover who's blocked you, who's been looking at your profile.`
  String get tutorialPageBodyOne {
    return Intl.message(
      'Discover who\'s blocked you, who\'s been looking at your profile.',
      name: 'tutorialPageBodyOne',
      desc: '',
      args: [],
    );
  }

  /// `Find out who looks at your stories but doesn't follow you.`
  String get tutorialPageBodyThree {
    return Intl.message(
      'Find out who looks at your stories but doesn\'t follow you.',
      name: 'tutorialPageBodyThree',
      desc: '',
      args: [],
    );
  }

  /// `Navigate between the tabs to compare all your stats.`
  String get tutorialPageBodyTwo {
    return Intl.message(
      'Navigate between the tabs to compare all your stats.',
      name: 'tutorialPageBodyTwo',
      desc: '',
      args: [],
    );
  }

  /// `Analyze anyone's account`
  String get tutorialPageTitleFour {
    return Intl.message(
      'Analyze anyone\'s account',
      name: 'tutorialPageTitleFour',
      desc: '',
      args: [],
    );
  }

  /// `Use your dashboard to see all the information`
  String get tutorialPageTitleOne {
    return Intl.message(
      'Use your dashboard to see all the information',
      name: 'tutorialPageTitleOne',
      desc: '',
      args: [],
    );
  }

  /// `See your secret admirers`
  String get tutorialPageTitleThree {
    return Intl.message(
      'See your secret admirers',
      name: 'tutorialPageTitleThree',
      desc: '',
      args: [],
    );
  }

  /// `Analysis & Insights`
  String get tutorialPageTitleTwo {
    return Intl.message(
      'Analysis & Insights',
      name: 'tutorialPageTitleTwo',
      desc: '',
      args: [],
    );
  }

  /// `START NOW`
  String get tutorialStartButton {
    return Intl.message(
      'START NOW',
      name: 'tutorialStartButton',
      desc: '',
      args: [],
    );
  }

  /// `Uncover anyone’s activities`
  String get uncoverAnyoneActivities {
    return Intl.message(
      'Uncover anyone’s activities',
      name: 'uncoverAnyoneActivities',
      desc: '',
      args: [],
    );
  }

  /// `Unimplemented State`
  String get unimplementedState {
    return Intl.message(
      'Unimplemented State',
      name: 'unimplementedState',
      desc: '',
      args: [],
    );
  }

  /// `Upgrade to premium and find out what people you know are up to.`
  String get upgradePrimiumAndFindOutWhatPeopleUpTo {
    return Intl.message(
      'Upgrade to premium and find out what people you know are up to.',
      name: 'upgradePrimiumAndFindOutWhatPeopleUpTo',
      desc: '',
      args: [],
    );
  }

  /// `It seems that this user has not seen any of your posts yet`
  String get userHasNotSeenYourPostsYet {
    return Intl.message(
      'It seems that this user has not seen any of your posts yet',
      name: 'userHasNotSeenYourPostsYet',
      desc: '',
      args: [],
    );
  }

  /// `It seems that this user has not seen any of your stories yet`
  String get userHasNotSeenYourStoriesYet {
    return Intl.message(
      'It seems that this user has not seen any of your stories yet',
      name: 'userHasNotSeenYourStoriesYet',
      desc: '',
      args: [],
    );
  }

  /// `User Posts`
  String get userPosts {
    return Intl.message(
      'User Posts',
      name: 'userPosts',
      desc: '',
      args: [],
    );
  }

  /// `User Stories`
  String get userStories {
    return Intl.message(
      'User Stories',
      name: 'userStories',
      desc: '',
      args: [],
    );
  }

  /// `We do not share data with third parties.`
  String get weDontShareYourData {
    return Intl.message(
      'We do not share data with third parties.',
      name: 'weDontShareYourData',
      desc: '',
      args: [],
    );
  }

  /// `We do not track you or send promotions without your consent.`
  String get weDontTrackOrSend {
    return Intl.message(
      'We do not track you or send promotions without your consent.',
      name: 'weDontTrackOrSend',
      desc: '',
      args: [],
    );
  }

  /// `Who are your biggest fans?`
  String get whoAreYourBiggestFans {
    return Intl.message(
      'Who are your biggest fans?',
      name: 'whoAreYourBiggestFans',
      desc: '',
      args: [],
    );
  }

  /// `Who really likes your stories?`
  String get whoReallyLikesYourStory {
    return Intl.message(
      'Who really likes your stories?',
      name: 'whoReallyLikesYourStory',
      desc: '',
      args: [],
    );
  }

  /// `Oops!`
  String get wopsy {
    return Intl.message(
      'Oops!',
      name: 'wopsy',
      desc: '',
      args: [],
    );
  }

  /// `You don’t follow`
  String get youDontFollow {
    return Intl.message(
      'You don’t follow',
      name: 'youDontFollow',
      desc: '',
      args: [],
    );
  }

  /// `You haven’t shared any story since you installed Gossipgram`
  String get youHaventSharedAStoryYet {
    return Intl.message(
      'You haven’t shared any story since you installed Gossipgram',
      name: 'youHaventSharedAStoryYet',
      desc: '',
      args: [],
    );
  }

  /// `You liked but don’t follow`
  String get youLikedButDontFollow {
    return Intl.message(
      'You liked but don’t follow',
      name: 'youLikedButDontFollow',
      desc: '',
      args: [],
    );
  }

  /// `Your opinion matters to us!`
  String get yourOpinionMattersToUs {
    return Intl.message(
      'Your opinion matters to us!',
      name: 'yourOpinionMattersToUs',
      desc: '',
      args: [],
    );
  }

  /// `Your Stories`
  String get yourStories {
    return Intl.message(
      'Your Stories',
      name: 'yourStories',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'ar'),
      Locale.fromSubtags(languageCode: 'de', countryCode: 'DE'),
      Locale.fromSubtags(languageCode: 'es', countryCode: 'ES'),
      Locale.fromSubtags(languageCode: 'fr', countryCode: 'FR'),
      Locale.fromSubtags(languageCode: 'it', countryCode: 'IT'),
      Locale.fromSubtags(languageCode: 'ja', countryCode: 'JP'),
      Locale.fromSubtags(languageCode: 'ko'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),
      Locale.fromSubtags(languageCode: 'ru', countryCode: 'RU'),
      Locale.fromSubtags(languageCode: 'tr'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}