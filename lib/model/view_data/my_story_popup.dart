import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'my_story_popup.freezed.dart';

@freezed
abstract class MyStoryPopup with _$MyStoryPopup {
  const factory MyStoryPopup({
    @required String fullName,
    @required String profilePic,
    @required String storyAsset,
    @required String storyType,
    @required List<String> userSeenStories,
    @required int seenBy,
  }) = _MyStoryPopup;
}
