part of 'story_popup_bloc.dart';

abstract class StoryPopupState extends Equatable {
  const StoryPopupState();
}

class StoryPopupInitial extends StoryPopupState {
  @override
  List<Object> get props => [];
}

class StoryPopupLoading extends StoryPopupState {
  @override
  List<Object> get props => [];
}

class StoryPopupLoaded extends StoryPopupState {
  final List<StoryPopup> storiesPopup;

  StoryPopupLoaded({@required this.storiesPopup});

  @override
  List<Object> get props => [storiesPopup];
}
