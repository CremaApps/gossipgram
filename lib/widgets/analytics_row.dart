import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class AnalyticsRow extends StatelessWidget {
  final double averageLikes;
  final double engagementRate;
  final double followerRatio;

  AnalyticsRow(this.averageLikes, this.engagementRate, this.followerRatio);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              (averageLikes != null
                  ? Text("${averageLikes.toStringAsFixed(2)}",
                      style: appTextTheme.subtitle2)
                  : Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 25,
                          right: MediaQuery.of(context).size.width / 25,
                        ),
                      ))),
              SizedBox(
                height: 4,
              ),
              Text(S.of(context).averageLikes, style: appTextTheme.bodyText1)
            ]),
        Container(
          padding: EdgeInsets.symmetric(vertical: 2),
          height: 36,
          width: 0.5,
          color: Colors.grey[300],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            (averageLikes != null
                ? Text("${engagementRate.toStringAsFixed(2)} %",
                    style: appTextTheme.subtitle2)
                : Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    child: Container(
                      margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 25,
                        right: MediaQuery.of(context).size.width / 25,
                      ),
                    ))),
            SizedBox(
              height: 4,
            ),
            Text(S.of(context).engagementRate, style: appTextTheme.bodyText1)
          ],
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 2),
          height: 36,
          width: 0.5,
          color: Colors.grey[300],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            (averageLikes != null
                ? Text("${followerRatio.toStringAsFixed(2)} %",
                    style: appTextTheme.subtitle2)
                : Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    child: Container(
                      margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 25,
                        right: MediaQuery.of(context).size.width / 25,
                      ),
                    ))),
            SizedBox(
              height: 4,
            ),
            Text(
              S.of(context).followerRatio,
              style: appTextTheme.bodyText1,
            )
          ],
        )
      ],
    );
  }
}
