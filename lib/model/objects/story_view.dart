import 'package:flutter/material.dart';

class StoryView {

  final int storyId;
  final int seenByUserId;
  int createdAt;
  final bool isValid;

  StoryView({
    @required this.storyId,
    @required this.seenByUserId,
    this.createdAt,
    this.isValid = true,
  }) {
    if(this.createdAt == null) {
      this.createdAt = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Map<String, dynamic> toMap() => {
    'storyId': storyId,
    'seenByUserId': seenByUserId,
    'createdAt': createdAt,
    'isValid': isValid ? 1 : 0,
  };

  factory StoryView.fromMap(Map<String, dynamic> map) => StoryView(
    storyId: map['storyId'],
    seenByUserId: map['seenByUserId'],
    createdAt: map['createdAt'],
    isValid: map['isValid'] == 1,
  );

}