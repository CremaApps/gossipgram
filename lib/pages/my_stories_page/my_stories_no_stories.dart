part of 'my_stories_page.dart';

class _StoriesAudienceNoStories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height / 2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/empty-stories.png',
              fit: BoxFit.cover,
              color:
                  getThemeModeAsInt() == 0 ? null : appColorScheme.onBackground,
              height: 150,
              width: 150,
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width / 1.5,
                child: Text(S.of(context).noStories,
                    style: appTextTheme.headline6,
                    textAlign: TextAlign.center)),
            SizedBox(
              height: 8,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width / 1.5,
                child: Text(
                  S.of(context).noStoriesShared,
                  style: appTextTheme.bodyText1,
                  textAlign: TextAlign.center,
                )),
            SizedBox(
              height: 8,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width / 1.5,
                child: Text(S.of(context).startSharing,
                    style: appTextTheme.bodyText1, textAlign: TextAlign.center))
          ],
        ),
      ),
    );
  }
}
