import 'package:flutter/cupertino.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/service/api/get_liked_elements_service.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@singleton
class LikedButDontFollowService {
  static const String SERVICE_NAME = "liked_but_dont_follow_service";

  GetUsersYouLikedMediaService _getUsersYouLikedMediaService;
  FollowedUserRepository _followedUserRepository;
  LocalStorage _localStorage;

  LikedButDontFollowService(
    this._getUsersYouLikedMediaService,
    this._followedUserRepository,
    this._localStorage,
  );

  Future<void> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    List<int> likedUsers = await _getUsersYouLikedMediaService.execute(
      processedUsers: processedUsers,
      firstLoad: firstLoad,
    );
    List<int> followedIds = await _followedUserRepository.getFollowedUserIds(
      isValid: true,
    );

    List<int> _excludeUserIds =
        _localStorage.hasData(DashboardBloc.USER_ID, generalStorage: true)
            ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
            : [];

    followedIds.addAll(_excludeUserIds);

    List<int> nonFollowingAdmiredUsers = [];

    for (int user in likedUsers) {
      if (!followedIds.any((element) => element == user))
        nonFollowingAdmiredUsers.add(user);
    }

    await _localStorage.write(
      DashboardBloc.NON_FOLLOWING_LIKED_USERS,
      nonFollowingAdmiredUsers,
    );
  }
}
