import 'package:flutter/material.dart';

class Feed {

  final String id;
  final String mediaUrl;
  final int mediaType;
  final int likeCount;
  final int commentCount;
  final int originalCreatedAtDate;
  int createdAt;

  Feed({
    @required this.id,
    @required this.mediaUrl,
    @required this.mediaType,
    @required this.likeCount,
    @required this.commentCount,
    @required this.originalCreatedAtDate,
    this.createdAt,
  }) {
    if(this.createdAt == null) {
      this.createdAt = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Map<String, dynamic> toMap() => {
    'id': id,
    'mediaUrl': mediaUrl,
    'mediaType': mediaType,
    'likeCount': likeCount,
    'commentCount': commentCount,
    'originalCreatedAtDate': originalCreatedAtDate,
    'createdAt': createdAt,
  };

  factory Feed.fromMap(Map<String, dynamic> map) => Feed(
    id: map['id'],
    mediaUrl: map['mediaUrl'],
    mediaType: map['mediaType'],
    likeCount: map['likeCount'],
    commentCount: map['commentCount'],
    originalCreatedAtDate: map['originalCreatedAtDate'],
    createdAt: map['createdAt'],
  );

}