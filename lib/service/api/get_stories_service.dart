import 'package:flutter/material.dart';
import 'package:GossipGram/model/objects/thumbnail.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:instagram_private_api/src/Client/Response/CommonClasses.dart';

@singleton
class GetStoriesService {
  InstagramPrivateApi _instagramPrivateApi;

  GetStoriesService() {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<List<Thumbnail>> execute({
    @required int userId,
  }) async {
    var storyResponse = await _instagramPrivateApi.feed.reelsMedia([
      userId.toString(),
    ]);

    var reel = storyResponse.response.reels;
    var stories = reel.isNotEmpty ? reel.first.items : [];

    List<Thumbnail> storyThumbnailData = [];
    for (var story in stories) {
      ImageVersions2 image;
      if (story.videoVersions != null) {
        final video = story.videoVersions.first;
        image = ImageVersions2(
            candidates: [
          ImageCandidate(
              url: video.url, width: video.width, height: video.height)
        ].toList());
      } else {
        image = story.imageVersions2;
      }

      storyThumbnailData.add(
        Thumbnail(
          id: story.pk,
          image: image,
          mediaType:
              story.videoVersions != null ? Thumbnail.VIDEO : Thumbnail.IMAGE,
        ),
      );
    }

    return storyThumbnailData;
  }
}
