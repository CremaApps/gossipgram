import 'dart:convert';
import 'dart:core';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/model/view_data/dashboard_box.dart';
import 'package:GossipGram/model/view_data/my_story_box.dart';
import 'package:GossipGram/model/view_data/premium_page/premium_pricing.dart';
import 'package:GossipGram/model/view_data/premium_page/premium_subtitle_text.dart';
import 'package:GossipGram/service/local_notification/local_notification_register_service.dart';
import 'package:GossipGram/service/scrap/feed_load_service.dart';
import 'package:GossipGram/service/simulations/profile_views_simulation_service.dart';
import 'package:injectable/injectable.dart';

@singleton
class AppConfig {
  RemoteConfig _remoteConfig;
  static const String SLEEP_FIRST_LOAD = 'sleep_first_load';
  static const String SLEEP_LOAD = 'sleep_load';
  static const String LIKED_THRESHOLD = 'liked_threshold';
  static const String FULLY_LOAD_ON_DASHBOARD = 'fully_load_on_dashboard';
  static const String PAID_FEATURES = "paid_features";
  static const String ANALYSIS_BOXES = "analysis_boxes";
  static const String INSIGHT_BOXES = "insight_boxes";
  static const String STORY_BOXES = "story_boxes";
  static const String PREMIUM_SUBTITLE = "premium_subtitle";
  static const String SUBSCRIPTION_ORDER = "subscription_order";
  static const String SUBSCRIPTION_ORDER_LABEL = "subscription_order_label";
  static const String SUBSCRIPTION_PRESELECTED = "subscription_preselected";
  static const String TERMS_OF_USE_URL = "terms_of_use_url";
  static const String PRIVACY_POLICY_URL = "privacy_policy_url";
  static const String DEBUG_USERS = "debug_users";
  static const String BACKGROUND_MAX_FEED_PROCESSING =
      'background_max_feed_processing';
  static const String BACKGROUND_SAFE_SECONDS = 'background_safe_seconds';
  static const String BACKGROUND_FEED_SCHEDULE_DELAY_SECONDS =
      'background_feed_schedule_delay_seconds';
  static const String LOCALIZELY_CONFIG = 'localizely_config';
  static const String LOCALIZELY_SDK =
      '46a4fe6e08dc46a4bfac7700e6a1d88c45bb6e3d';
  static const String LOCALIZELY_DISTRIBUTION =
      'f543d71fdaaf4124bcd3dd6b4594ef67';
  static const String APPSFLYER_CONFIG = 'appsflyer_config';
  static const String APPSFLYER_AFDEVKEY = 'appsflyer_afdevkey';
  static const String APPSFLYER_AFAPPID = 'appsflyer_afappid';
  static const String INSTA_AUTOLOGIN = 'insta_autologin';
  static const String PRICE_MODE = 'price_mode';
  static const String PRICE_COLOR_POPUP = 'price_color_popup';
  static const String MOST_LEAST_LIKED_POSTS_MAX_ITEMS =
      'max_most_least_liked_posts';

  static const LIST_STRINGS = [
    ANALYSIS_BOXES,
    INSIGHT_BOXES,
    STORY_BOXES,
    PREMIUM_SUBTITLE,
    SUBSCRIPTION_ORDER,
    DEBUG_USERS,
  ];
  static const MAP_STRINGS = [
    PAID_FEATURES,
    SUBSCRIPTION_ORDER_LABEL,
    LOCALIZELY_CONFIG,
    APPSFLYER_CONFIG
  ];

  final Map<String, dynamic> defaults = {
    SLEEP_FIRST_LOAD: 100,
    SLEEP_LOAD: 200,
    LIKED_THRESHOLD: 200,
    FULLY_LOAD_ON_DASHBOARD: 60,
    BACKGROUND_MAX_FEED_PROCESSING: 5,
    BACKGROUND_SAFE_SECONDS: 15,
    BACKGROUND_FEED_SCHEDULE_DELAY_SECONDS: 60 * 5,
    ProfileViewsSimulationService.PROFILE_VIEWS_MAX_FEED_ELEMENTS_MIN: 3,
    ProfileViewsSimulationService.PROFILE_VIEWS_MAX_FEED_ELEMENTS_MAX: 3,
    ProfileViewsSimulationService.PROFILE_VIEWS_MIN_ELEMENTS_MIN: 4,
    ProfileViewsSimulationService.PROFILE_VIEWS_MIN_ELEMENTS_MAX: 4,
    ProfileViewsSimulationService.PROFILE_VIEWS_COEFFICIENT_MIN: 0.6,
    ProfileViewsSimulationService.PROFILE_VIEWS_COEFFICIENT_MAX: 0.6,
    FeedLoadService.FEED_SINGLE_LOAD_THRESHOLD: 20,
    FeedLoadService.FEED_MAX_LOAD_HISTORICAL_DATA: 120,
    DashboardBloc.USERS_RELEVANT_DATA_THRESHOLD: 7,
    DashboardBloc.USER_STORIES_RELEVANT_DATA_THRESHOLD: 31,
    DashboardBloc.SECRET_ADMIRERS_RELEVANT_DATA_THRESHOLD: 31,
    DashboardBloc.PROFILE_VIEWS_RELEVANT_DATA_DATA_THRESHOLD: 7,
    LocalNotificationRegisterService.NOTIFICATION_REMINDER_PERIODICITY: "daily",
    LocalNotificationRegisterService
        .NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_MINUTES: 60 * 48,
    LocalNotificationRegisterService
        .NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_MINUTES: 60 * 72,
    LocalNotificationRegisterService
        .NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_MINUTES: 60 * 96,
    PAID_FEATURES: jsonEncode({
      PaidFeaturesKeys.NOT_FOLLOWING_YOU_BACK_PAID_FEATURE: false,
      PaidFeaturesKeys.BLOCKED_YOU_PAID_FEATURE: true,
      PaidFeaturesKeys.STOPPED_FOLLOWING_YOU_PAID_FEATURE: false,
      PaidFeaturesKeys.SEEN_YOUR_PROFILE_PAID_FEATURE: true,
      PaidFeaturesKeys.SEEN_YOUR_STORY_PAID_FEATURE: true,
      PaidFeaturesKeys.YOURE_NOT_FOLLOWING_BACK_PAID_FEATURE: false,
      PaidFeaturesKeys.NEW_FOLLOWERS_PAID_FEATURE: false,
      PaidFeaturesKeys.LIKED_YOU_THE_MOST_AND_LEAST_PAID_FEATURE: false,
      PaidFeaturesKeys.MOST_AND_LEAST_LIKED_POSTS_PAID_FEATURE: false,
      PaidFeaturesKeys.SECRET_ADMIRERS_PAID_FEATURE: true,
      PaidFeaturesKeys.DELETED_YOU_LIKES_OR_COMMENTS_PAID_FEATURE: true,
      PaidFeaturesKeys.YOU_LIKED_BUT_DONT_FOLLOW_PAID_FEATURE: false,
      PaidFeaturesKeys.STORIES_DONT_FOLLOW_YOU_FEATURE: false,
      PaidFeaturesKeys.STORIES_YOU_DONT_FOLLOW_FEATURE: false,
      PaidFeaturesKeys.STORIES_MOST_SEEN_FEATURE: false,
      PaidFeaturesKeys.STORIES_LEAST_SEEN_FEATURE: false,
    }),
    ANALYSIS_BOXES: jsonEncode([
      DashboardBox.STOPPED_FOLLOWING_YOU_BOX,
      DashboardBox.YOURE_NOT_FOLLOWING_BACK_BOX,
      DashboardBox.NOT_FOLLOWING_YOU_BACK_BOX,
      DashboardBox.SEEN_YOUR_PROFILE_BOX,
      DashboardBox.SEEN_YOUR_STORY_BOX,
      DashboardBox.BLOCKED_YOU_BOX,
    ]),
    INSIGHT_BOXES: jsonEncode([
      DashboardBox.NEW_FOLLOWERS_BOX,
      DashboardBox.YOU_LIKED_BUT_DONT_FOLLOW_BOX,
      DashboardBox.LIKED_YOU_THE_MOST_AND_LEAST_BOX,
      DashboardBox.MOST_AND_LEAST_LIKED_POSTS_BOX,
      DashboardBox.DELETED_YOU_LIKES_OR_COMMENTS_BOX,
      DashboardBox.SECRET_ADMIRERS_BOX,
    ]),
    STORY_BOXES: jsonEncode([
      MyStoryBox.STORIES_DONT_FOLLOW_YOU_BOX,
      MyStoryBox.STORIES_YOU_DONT_FOLLOW_BOX,
      MyStoryBox.STORIES_MOST_SEEN_BOX,
    ]),
    PREMIUM_SUBTITLE: jsonEncode([
      PremiumSubtitleText.UNCOVER_ANYONE_ACTIVITIES,
      PremiumSubtitleText.WHO_REALLY_LIKES_YOUR_STORY,
      PremiumSubtitleText.SEE_SECRET_ADMIRERS,
      PremiumSubtitleText.WHO_ARE_YOUR_BIGGEST_FANS,
      PremiumSubtitleText.EXTRA_HIDDEN_INFORMATION,
    ]),
    SUBSCRIPTION_ORDER: jsonEncode([
      PremiumPricing.SUBSCRIPTION_SIX_MONTH,
      PremiumPricing.SUBSCRIPTION_ANNUAL,
      PremiumPricing.SUBSCRIPTION_MONTHLY,
    ]),
    SUBSCRIPTION_ORDER_LABEL: jsonEncode({
      PremiumPricing.SUBSCRIPTION_LIFETIME: "",
      PremiumPricing.SUBSCRIPTION_ANNUAL: "Best",
      PremiumPricing.SUBSCRIPTION_SIX_MONTH: "",
      PremiumPricing.SUBSCRIPTION_THREE_MONTH: "",
      PremiumPricing.SUBSCRIPTION_TWO_MONTH: "",
      PremiumPricing.SUBSCRIPTION_MONTHLY: "",
      PremiumPricing.SUBSCRIPTION_WEEKLY: "",
    }),
    SUBSCRIPTION_PRESELECTED: 1,
    TERMS_OF_USE_URL: "https://cremaapps.com/terms.html",
    PRIVACY_POLICY_URL: "https://cremaapps.com/privacy.html",
    DEBUG_USERS: [],
    LOCALIZELY_CONFIG: jsonEncode({
      LOCALIZELY_SDK: "87b86d2961ec4c6c842ae256800d18a46f01ac39",
      LOCALIZELY_DISTRIBUTION: "e0825e321fa34b0c8fd9187e8d43eac0"
    }),
    APPSFLYER_CONFIG: jsonEncode({
      APPSFLYER_AFDEVKEY: "azWHdvzrmpKfL46DW74hAo",
      APPSFLYER_AFAPPID: "1548999175"
    }),
    INSTA_AUTOLOGIN: true,
    PRICE_MODE: 1,
    PRICE_COLOR_POPUP: 'NORMAL',
    MOST_LEAST_LIKED_POSTS_MAX_ITEMS: 30
  };

  bool get isLoaded => _remoteConfig != null;

  Future<void> init() async {
    try {
      _remoteConfig = await RemoteConfig.instance;

      await _remoteConfig.setDefaults(defaults);

      await _remoteConfig.fetch(expiration: const Duration(hours: 5));
      await _remoteConfig.activateFetched();
    } on Error catch (e, stack) {
      FirebaseCrashlytics.instance.recordError(e, stack);
    }
  }

  String getString(String key) {
    return _remoteConfig != null ? _remoteConfig.getString(key) : defaults[key];
  }

  int getInt(String key) {
    return _remoteConfig != null ? _remoteConfig.getInt(key) : defaults[key];
  }

  double getDouble(String key) {
    return _remoteConfig != null ? _remoteConfig.getDouble(key) : defaults[key];
  }

  bool getBool(String key) {
    return _remoteConfig != null ? _remoteConfig.getBool(key) : defaults[key];
  }

  List<String> getListString(String listName) {
    assert(LIST_STRINGS.contains(listName));

    List<dynamic> list = _remoteConfig != null &&
            _remoteConfig.getValue(listName).asString() != ""
        ? json.decode(_remoteConfig.getValue(listName).asString())
        : defaults[listName];

    return list.map((e) => e.toString()).toList();
  }

  Map<String, dynamic> getMap(String mapName) {
    assert(MAP_STRINGS.contains(mapName));

    return _remoteConfig != null &&
            _remoteConfig.getValue(mapName).asString() != ""
        ? json.decode(_remoteConfig.getValue(mapName).asString())
        : defaults[mapName];
  }
}

class DashboardFeaturesKeys {
  static const String NOT_FOLLOWING_YOU_BACK_DASHB =
      'NOT_FOLLOWING_YOU_BACK_PAID_FEATURE';
  static const String BLOCKED_YOU_PAID_FEATURE = 'BLOCKED_YOU_PAID_FEATURE';
  static const String STOPPED_FOLLOWING_YOU_PAID_FEATURE =
      'STOPPED_FOLLOWING_YOU_PAID_FEATURE';
  static const String SEEN_YOUR_PROFILE_PAID_FEATURE =
      'SEEN_YOUR_PROFILE_PAID_FEATURE';
  static const String SEEN_YOUR_STORY_PAID_FEATURE =
      'SEEN_YOU_STORY_PAID_FEATURE';
  static const String YOURE_NOT_FOLLOWING_BACK_PAID_FEATURE =
      'YOURE_NOT_FOLLOWING_BACK_PAID_FEATURE';
  static const String NEW_FOLLOWERS_PAID_FEATURE = 'NEW_FOLLOWERS_PAID_FEATURE';
  static const String LIKED_YOU_THE_MOST_AND_LEAST_PAID_FEATURE =
      'LIKED_YOU_THE_MOST_AND_LEAST_PAID_FEATURE';
  static const String MOST_AND_LEAST_LIKED_POSTS_PAID_FEATURE =
      'MOST_AND_LEAST_LIKED_POSTS_PAID_FEATURE';
  static const String SECRET_ADMIRERS_PAID_FEATURE =
      'SECRET_ADMIRERS_PAID_FEATURE';
  static const String DELETED_YOU_LIKES_OR_COMMENTS_PAID_FEATURE =
      'DELETED_YOU_LIKES_OR_COMMENTS_PAID_FEATURE';
  static const String YOU_LIKED_BUT_DONT_FOLLOW_PAID_FEATURE =
      'YOU_LIKED_BUT_DONT_FOLLOW_PAID_FEATURE';
}

class StoryFeaturesKeys {
  static const String STORIES_DONT_FOLLOW_YOU_BOX =
      'STORIES_DONT_FOLLOW_YOU_BOX';
  static const String STORIES_YOU_DONT_FOLLOW_BOX =
      'STORIES_YOU_DONT_FOLLOW_BOX';
  static const String STORIES_MOST_SEEN_BOX = 'STORIES_MOST_SEEN_BOX';
  static const String STORIES_LEAST_SEEN_BOX = 'STORIES_LEAST_SEEN_BOX';
}

class PaidFeaturesKeys {
  // ANALYSIS PAID FEATURE
  static const String NOT_FOLLOWING_YOU_BACK_PAID_FEATURE =
      'NOT_FOLLOWING_YOU_BACK_PAID_FEATURE';
  static const String BLOCKED_YOU_PAID_FEATURE = 'BLOCKED_YOU_PAID_FEATURE';
  static const String STOPPED_FOLLOWING_YOU_PAID_FEATURE =
      'STOPPED_FOLLOWING_YOU_PAID_FEATURE';
  static const String SEEN_YOUR_PROFILE_PAID_FEATURE =
      'SEEN_YOUR_PROFILE_PAID_FEATURE';
  static const String SEEN_YOUR_STORY_PAID_FEATURE =
      'SEEN_YOUR_STORY_PAID_FEATURE';
  static const String YOURE_NOT_FOLLOWING_BACK_PAID_FEATURE =
      'YOURE_NOT_FOLLOWING_BACK_PAID_FEATURE';

  // INSIGHT PAID FEATURE
  static const String NEW_FOLLOWERS_PAID_FEATURE = 'NEW_FOLLOWERS_PAID_FEATURE';
  static const String LIKED_YOU_THE_MOST_AND_LEAST_PAID_FEATURE =
      'LIKED_YOU_THE_MOST_AND_LEAST_PAID_FEATURE';
  static const String MOST_AND_LEAST_LIKED_POSTS_PAID_FEATURE =
      'MOST_AND_LEAST_LIKED_POSTS_PAID_FEATURE';
  static const String SECRET_ADMIRERS_PAID_FEATURE =
      'SECRET_ADMIRERS_PAID_FEATURE';
  static const String DELETED_YOU_LIKES_OR_COMMENTS_PAID_FEATURE =
      'DELETED_YOU_LIKES_OR_COMMENTS_PAID_FEATURE';
  static const String YOU_LIKED_BUT_DONT_FOLLOW_PAID_FEATURE =
      'YOU_LIKED_BUT_DONT_FOLLOW_PAID_FEATURE';

  // STORIES PAID FEATURE
  static const String STORIES_DONT_FOLLOW_YOU_FEATURE =
      'STORIES_DONT_FOLLOW_YOU_FEATURE';
  static const String STORIES_YOU_DONT_FOLLOW_FEATURE =
      'STORIES_YOU_DONT_FOLLOW_FEATURE';
  static const String STORIES_MOST_SEEN_FEATURE = 'STORIES_MOST_SEEN_FEATURE';
  static const String STORIES_LEAST_SEEN_FEATURE =
      'STORIES__LEAST_SEEN_FEATURE';
}
