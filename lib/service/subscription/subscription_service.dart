import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/services.dart';
import 'package:GossipGram/service/local_notification/local_notification_register_service.dart';
import 'package:GossipGram/service/local_notifications_service.dart';
import 'package:injectable/injectable.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

@lazySingleton
class SubscriptionService {
  static const _apiKey = "mwlSyZjMxDgCVAZzIHuYfIgQzOkHRBPm";
  static const _entitlementApp = "Pro";
  final LocalNotificationService _localNotificationService;

  bool _initialized = false;
  PurchaserInfo _purchaserInfo;
  Offerings _offerings;

  SubscriptionService(this._localNotificationService);

  bool get isInitialized => _initialized;
  bool get isPayed =>
      _purchaserInfo.entitlements?.all[_entitlementApp]?.isActive ?? false;

  Offering get offerings => _offerings.getOffering(_entitlementApp);

  Future<void> startUp() async {
    await Purchases.setDebugLogsEnabled(kDebugMode);
    await Purchases.setup(_apiKey);

    var futures = await Future.wait(
        [Purchases.getPurchaserInfo(), Purchases.getOfferings()]);

    _purchaserInfo = futures[0];
    _offerings = futures[1];
    _initialized = true;
  }

  Future<bool> purchasePackage(Package package) async {
    try {
      _purchaserInfo = await Purchases.purchasePackage(package);
    } on PlatformException catch (error, trace) {
      if (PurchasesErrorHelper.getErrorCode(error) !=
          PurchasesErrorCode.purchaseCancelledError) {
        FirebaseCrashlytics.instance.recordError(error, trace);
      }
    } catch (e) {
      print(e);
    }

    if (isPayed) {
      _localNotificationService
          .cancelNotificationWithId(
              id: LocalNotificationRegisterService
                  .NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_ID)
          .catchError((error) {
        FirebaseCrashlytics.instance.recordError(error, StackTrace.current);
      });
      _localNotificationService
          .cancelNotificationWithId(
              id: LocalNotificationRegisterService
                  .NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_ID)
          .catchError((error) {
        FirebaseCrashlytics.instance.recordError(error, StackTrace.current);
      });
      _localNotificationService
          .cancelNotificationWithId(
              id: LocalNotificationRegisterService
                  .NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_ID)
          .catchError((error) {
        FirebaseCrashlytics.instance.recordError(error, StackTrace.current);
      });
    }

    return isPayed;
  }

  Future<void> setAlias(String alias) async {
    return Purchases.createAlias(alias);
  }

  Future<bool> restorePurchases() async {
    try {
      _purchaserInfo = await Purchases.restoreTransactions();
    } catch (e) {
      print(e);
    }

    return isPayed;
  }

  Future<bool> payPackage(Package package) async {
    _purchaserInfo = await Purchases.purchasePackage(package);

    return _purchaserInfo.entitlements?.all[_entitlementApp]?.isActive ?? false;
  }

  Future<void> resetSubscription() async {
    _purchaserInfo = await Purchases.reset();
  }
}
