import 'dart:async';

import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:injectable/injectable.dart';

@injectable
class FollowersBloc extends IAnalysisBloc {
  final UserRepository _userRepository;
  final FollowerUserRepository _followerUserRepository;

  FollowersBloc(
    this._userRepository,
    this._followerUserRepository,
  ) : super();

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      var followerIds = await _followerUserRepository.getFollowerUserIds(
        isValid: true,
      );

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          followerIds.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            youFollow: element.youFollow,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
