import 'dart:async';

import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:injectable/injectable.dart';

@injectable
class FollowedBloc extends IAnalysisBloc {
  final UserRepository _userRepository;
  final FollowedUserRepository _followedUserRepository;

  FollowedBloc(
    this._userRepository,
    this._followedUserRepository,
  ) : super();

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      var followerIds = await _followedUserRepository.getFollowedUserIds(
        isValid: true,
      );

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          followerIds.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            imageUrl: element.profilePicUrl,
            youFollow: element.youFollow));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
