import 'package:GossipGram/bloc/dashboard/dashboard_state.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:GossipGram/widgets/analytics_row.dart';
import 'package:GossipGram/widgets/profile_menu/top_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/profile_load/profile_load_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/profile.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/number_people_converter.dart';

import 'package:GossipGram/widgets/user_profile_tabs.dart';
import 'package:lottie/lottie.dart';

part 'user_profile_details.dart';
part 'user_profile_status.dart';

class UserProfile extends StatefulWidget {
  final String username;
  final int userId;

  const UserProfile({
    @required this.username,
    @required this.userId,
  });

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  ProfileLoadBloc _profileLoadBloc;
  ScrollController _controller;
  bool _lazyLoadWorking = false;
  int _tabIndex = 0;

  @override
  void initState() {
    _controller = ScrollController();
    _profileLoadBloc = GetIt.instance.get<ProfileLoadBloc>();
    _profileLoadBloc.add(ProfileLoadEventStart(userId: widget.userId));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _profileLoadBloc?.close();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          widget.username,
          style: appTextTheme.subtitle2,
        ),
        centerTitle: false,
      ),
      body: BlocBuilder<ProfileLoadBloc, ProfileLoadState>(
        cubit: _profileLoadBloc,
        builder: (context, state) {
          if (state is ProfileLoadFeedLoading) {
            _controller.addListener(_scrollListener);
            _lazyLoadWorking = false;
          } else {
            _controller.removeListener(_scrollListener);
          }

          if (state is ProfileLoadInitial) {
            return Center(
              child: Lottie.asset(
                'assets/animations/loader.json',
                width: 100,
                height: 100,
              ),
            );
          } else if (state is ProfileNotExistsState) {
            return Center(
              child: Text(
                S.of(context).profileDoesntExists,
              ),
            );
          } else if (state is ProfileLoadData) {
            var partials = _makePartials(state);

            if (!(state is ProfileLoadFullyLoaded) &&
                !(state is ProfileIsPrivateState) &&
                !(state is ProfileLoadFeedLoading)) {}

            return ListView(
              physics: ClampingScrollPhysics(),
              children: partials,
              controller: _controller,
            );
          } else {
            return Center(
              child: Text(
                S.of(context).profileDoesntExists,
              ),
            );
          }
        },
      ),
    );
  }

  List<Widget> _makePartials(ProfileLoadState state) {
    List<Widget> _partials = [];

    if (state is ProfileLoadProfileInfoLoaded) {
      _partials.addAll(
        [
          TopBar(
            loading: false,
            userName: state.profile.fullName,
            profilePicUrl: state.profile.profilePicUrl,
            fullName: state.profile.fullName,
            bio: state.profile.bio,
            followers: state.profile.followers,
            following: state.profile.following,
            clicableProfile: false,
            dataLoadBloc: null,
          ),
          _UserProfileStatus(
            relation: state.profile.relation,
          ),
        ],
      );
    }

    if (state is ProfileIsPrivateState) {
      _partials.add(
        Center(
          child: Text(S.of(context).privateProfile,
              style: appTextTheme.subtitle2
                  .copyWith(color: appColorScheme.onBackground)),
        ),
      );
    }

    if (state is ProfileLoadFeedLoading || state is ProfileLoadFullyLoaded) {
      _partials.add(UserProfileTabs(
        profileLoadBloc: _profileLoadBloc,
        updateSelectedIndex: (index) => _tabIndex = index,
      ));
    }
    return _partials;
  }

  void _scrollListener() {
    if (_tabIndex == 0 &&
        !_lazyLoadWorking &&
        _controller.offset >= _controller.position.maxScrollExtent / 2 &&
        !_controller.position.outOfRange) {
      _lazyLoadWorking = true;
      _profileLoadBloc.add(
        ProfileLoadEventGetFeedPage(),
      );
    }
  }
}
