import 'package:GossipGram/themes/themes.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/media_load/media_load_bloc.dart';
import 'package:GossipGram/bloc/media_load/media_load_events.dart';
import 'package:GossipGram/bloc/media_load/media_load_state.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/model/objects/thumbnail.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:GossipGram/widgets/instagram_video.dart';

class MediaPopup extends StatefulWidget {
  final int mediaId;
  final int mediaType;

  const MediaPopup({
    @required this.mediaId,
    @required this.mediaType,
  }) : super();

  @override
  _MediaPopupState createState() => _MediaPopupState();
}

class _MediaPopupState extends State<MediaPopup> {
  int _currentAlbumIndex = 0;
  MediaLoadBloc _mediaLoadBloc;

  @override
  void initState() {
    super.initState();
    _mediaLoadBloc = GetIt.instance.get<MediaLoadBloc>();
    _mediaLoadBloc.add(MediaLoadEventStart(
      mediaId: widget.mediaId,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MediaLoadBloc, MediaLoadState>(
      cubit: _mediaLoadBloc,
      builder: (context, state) {
        if (state is MediaLoaded) {
          return Dialog(
            backgroundColor: appColorScheme.primary,
            shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(
                Radius.circular(12.0),
              ),
            ),
            insetPadding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Flexible(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Container(
                          padding: EdgeInsets.all(16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                state.mediaData.username ?? "",
                                style: appTextTheme.subtitle2,
                              ),
                              Text(
                                state.mediaData.date,
                                style: appTextTheme.bodyText1,
                              ),
                            ],
                          )),
                      Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Builder(
                                builder: (context) {
                                  switch (widget.mediaType) {
                                    case Thumbnail.IMAGE:
                                      return Image.network(
                                        state.mediaData.urlThumbnail.first,
                                      );

                                    case FeedThumbnail.CAROUSEL:
                                      return Column(
                                        children: [
                                          Container(
                                            child: CarouselSlider(
                                              options: CarouselOptions(
                                                  viewportFraction: 1,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height /
                                                      1.8,
                                                  onPageChanged:
                                                      (index, reason) {
                                                    setState(() {
                                                      _currentAlbumIndex =
                                                          index;
                                                    });
                                                  }),
                                              items:
                                                  state.mediaData.urlThumbnail
                                                      .map(
                                                        (item) => Image.network(
                                                          item,
                                                          fit: BoxFit.cover,
                                                        ),
                                                      )
                                                      .toList(),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: state
                                                .mediaData.urlThumbnail
                                                .map((url) {
                                              int index = state
                                                  .mediaData.urlThumbnail
                                                  .indexOf(url);
                                              return Container(
                                                width: 5.0,
                                                height: 5.0,
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 10.0,
                                                    horizontal: 2.0),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: _currentAlbumIndex ==
                                                          index
                                                      ? Color.fromRGBO(
                                                          255, 255, 255, 0.8)
                                                      : Color.fromRGBO(
                                                          255, 255, 255, 0.4),
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                        ],
                                      );
                                    case Thumbnail.VIDEO:
                                      return InstagramVideo(
                                        state.mediaData.urlThumbnail.first,
                                      );
                                    default:
                                      return CenteredLoader(
                                        size: 'medium',
                                      );
                                  }
                                },
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(16),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsetsDirectional.only(
                                          end: 6,
                                        ),
                                        child: Icon(
                                          Icons.favorite,
                                          size: 16,
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          state.mediaData.likes.toString(),
                                          style: appTextTheme.subtitle2,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsetsDirectional.only(
                                          start: 16,
                                        ),
                                        child: Icon(
                                          Icons.comment,
                                          size: 16,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsetsDirectional.only(
                                          start: 6,
                                        ),
                                        child: Text(
                                          state.mediaData.comments.toString(),
                                          style: appTextTheme.subtitle2,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    child: Text(state.mediaData.contents,
                                        style: appTextTheme.bodyText1),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  thickness: 0.5,
                  height: 0.5,
                ),
                Container(
                  width: double.infinity,
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Close",
                    ),
                  ),
                ),
              ],
            ),
          );
        } else {
          return CenteredLoader(
            size: 'medium',
          );
        }
      },
    );
  }
}
