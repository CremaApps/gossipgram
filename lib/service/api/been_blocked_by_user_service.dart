import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

@singleton
class BeenBlockedByUserService {
  static const String SERVICE_NAME = "been_blocked_by_user_load_service";

  static const String BLOCKED_TIMED_KEY = 'BLOCKED_TIMED';
  static const String BLOCKED_CACHE = 'BLOCKED_CACHE';
  final LocalStorage _localStorage;

  FollowedUserRepository _followedUserRepository;
  FollowerUserRepository _followerUserRepository;

  InstagramPrivateApi _instagramPrivateApi;

  BeenBlockedByUserService(
    this._followedUserRepository,
    this._followerUserRepository,
    this._localStorage,
  ) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<List<int>> execute({
    bool firstLoad = false,
  }) async {
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME) ?? 0;

    List<int> blockedByUsers = [];

    List<int> lostFollowed = await _followedUserRepository.getFollowedUserIds(
      isValid: false,
    );
    List<int> lostFollower = await _followerUserRepository.getFollowerUserIds(
      isValid: false,
    );
    List<int> potentialBlockedByUsers = [];

    for (int lostFollowedItem in lostFollowed) {
      if (lostFollower.any((element) => (element == lostFollowedItem)))
        potentialBlockedByUsers.add(lostFollowedItem);
      else if (!await _followerUserRepository.existsFollowerUser(
          id: lostFollowedItem)) potentialBlockedByUsers.add(lostFollowedItem);
    }

    for (int lostFollowerItem in lostFollower) {
      if (!potentialBlockedByUsers
          .any((element) => (element == lostFollowerItem))) {
        if (lostFollowed.any((element) => (element == lostFollowerItem)))
          potentialBlockedByUsers.add(lostFollowerItem);
        else if (!await _followedUserRepository.existsFollowedUser(
            id: lostFollowerItem))
          potentialBlockedByUsers.add(lostFollowerItem);
      }
    }
    DataLoadBloc.printProfiling("BD users media loaded", now);

    for (int userId in potentialBlockedByUsers) {
      try {
        await _instagramPrivateApi.user.getAccountInfo(userId);
      } catch (e) {
        blockedByUsers.add(userId);
      }
    }
    DataLoadBloc.printProfiling("API blocked profiles loaded", now);

    _localStorage.writeInMemory(BLOCKED_CACHE, blockedByUsers);
    _localStorage.writeInMemory(BLOCKED_TIMED_KEY, DateTime.now().toString());

    DataLoadBloc.printProfiling("Blocked profiles DB loaded", now);

    return blockedByUsers;
  }
}
