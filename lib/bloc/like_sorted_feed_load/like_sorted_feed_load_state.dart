part of 'like_sorted_feed_load_bloc.dart';

abstract class LikeSortedFeedLoadState extends Equatable {
  const LikeSortedFeedLoadState();

  @override
  List<Object> get props => [];
}

class LikeSortedFeedLoadInitial extends LikeSortedFeedLoadState {}

abstract class LikeSortedFeedLoadData extends LikeSortedFeedLoadState {
  final List<FeedThumbnail> mostLikedFeed;
  final List<FeedThumbnail> leastLikedFeed;
  const LikeSortedFeedLoadData({
    @required this.mostLikedFeed,
    @required this.leastLikedFeed,
  });

  @override
  List<Object> get props => [
    mostLikedFeed,
    leastLikedFeed,
  ];
}

class LikeSortedFeedLoaded extends LikeSortedFeedLoadData {
  const LikeSortedFeedLoaded({
    @required mostLikedFeed,
    @required leastLikedFeed,
  }) : super(
    mostLikedFeed: mostLikedFeed,
    leastLikedFeed: leastLikedFeed,
  );
}
