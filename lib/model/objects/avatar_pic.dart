import 'package:flutter/material.dart';

class AvatarPic {

  final int id;
  final String userId;
  int createdAt;

  AvatarPic({
    @required this.id,
    @required this.userId,
    this.createdAt,
  }) {
    if(this.createdAt == null) {
      this.createdAt = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Map<String, dynamic> toMap() => {
    'id': id,
    'userId': userId,
    'createdAt': createdAt,
  };

  factory AvatarPic.fromMap(Map<String, dynamic> map) => AvatarPic(
    id: map['id'],
    userId: map['userId'],
    createdAt: map['createdAt'],
  );

}