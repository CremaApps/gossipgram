import 'package:flutter/material.dart';

class Like {

  final String feedId;
  final int userId;
  int onStateChange;
  final bool isValid;

  Like({
    @required this.feedId,
    @required this.userId,
    this.onStateChange,
    this.isValid = true,
  }) {
    if(this.onStateChange == null) {
      this.onStateChange = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Map<String, dynamic> toMap() => {
    'feedId': feedId,
    'userId': userId,
    'onStateChange': onStateChange,
    'isValid': isValid ? 1 : 0,
  };

  factory Like.fromMap(Map<String, dynamic> map) => Like(
    feedId: map['feedId'],
    userId: map['userId'],
    onStateChange: map['onStateChange'],
    isValid: map['isValid'] == 1,
  );

}