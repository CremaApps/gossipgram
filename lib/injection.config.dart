// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:GossipGram/bloc/analysis_bloc/blocs/friendship_cubit.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'service/storage/app_config.dart';
import 'service/analytics/appsflyer_service.dart';
import 'database/repository/avatar_pic_repository.dart';
import 'service/background_task/background_task_register_service.dart';
import 'service/api/been_blocked_by_user_service.dart';
import 'service/scrap/best_friend_user_load_service.dart';
import 'database/repository/best_friend_user_repository.dart';
import 'service/scrap/blocked_user_load_service.dart';
import 'database/repository/blocked_user_repository.dart';
import 'bloc/analysis_bloc/blocs/blocked_you_bloc.dart';
import 'service/scrap/comment_load_service.dart';
import 'database/repository/comment_repository.dart';
import 'bloc/comment_sorted_feed_load/comment_sorted_feed_load_bloc.dart';
import 'bloc/dashboard/dashboard_bloc.dart';
import 'bloc/data_load/data_load_bloc.dart';
import 'database/db_provider.dart';
import 'bloc/analysis_bloc/blocs/deleted_you_likes_comments_bloc.dart';
import 'service/scrap/feed_load_service.dart';
import 'database/repository/feed_repository.dart';
import 'bloc/analysis_bloc/blocs/followed_bloc.dart';
import 'service/scrap/followed_user_load_service.dart';
import 'database/repository/followed_user_repository.dart';
import 'service/scrap/follower_user_load_service.dart';
import 'database/repository/follower_user_repository.dart';
import 'bloc/analysis_bloc/blocs/followers_bloc.dart';
import 'service/api/get_active_stories_service.dart';
import 'service/api/get_active_story_by_user_id.dart';
import 'service/api/get_feed_images_service.dart';
import 'service/api/get_feed_next_page_service.dart';
import 'service/api/get_media_info_service.dart';
import 'service/api/get_my_stored_feed_images_service.dart';
import 'service/api/get_my_stored_stories_service.dart';
import 'service/api/get_stories_service.dart';
import 'service/api/get_liked_elements_service.dart';
import 'service/scrap/like_load_service.dart';
import 'database/repository/like_repository.dart';
import 'bloc/like_sorted_feed_load/like_sorted_feed_load_bloc.dart';
import 'bloc/like_sorted_users_load/like_sorted_users_load_bloc.dart';
import 'bloc/analysis_bloc/blocs/liked_but_dont_follow_bloc.dart';
import 'service/scrap/liked_but_dont_follow_service.dart';
import 'service/local_notification/local_notification_register_service.dart';
import 'service/local_notifications_service.dart';
import 'service/local_storage/local_storage.dart';
import 'service/interceptor/logged_out_interceptor.dart';
import 'service/logout_service.dart';
import 'bloc/media_load/media_load_bloc.dart';
import 'bloc/my_profile_load/my_profile_load_bloc.dart';
import 'bloc/my_story/my_story_bloc.dart';
import 'bloc/my_story_popup/my_story_popup_bloc.dart';
import 'service/navigator_service.dart';
import 'bloc/analysis_bloc/blocs/new_followers_bloc.dart';
import 'bloc/analysis_bloc/blocs/not_following_you_back_bloc.dart';
import 'bloc/people_stories/people_stories_bloc.dart';
import 'bloc/profile_load/profile_load_bloc.dart';
import 'service/simulations/profile_views_simulation_service.dart';
import 'bloc/analysis_bloc/blocs/secret_admirers_bloc.dart';
import 'service/storage/secure_storage.dart';
import 'bloc/analysis_bloc/blocs/seen_your_profile_bloc.dart';
import 'bloc/seen_your_story_bloc/seen_your_story_bloc.dart';
import 'service/interceptor/service_denial_interceptor.dart';
import 'service/setup_user_service.dart';
import 'service/setup_external_requirements.dart';
import 'bloc/analysis_bloc/blocs/stopped_following_you_bloc.dart';
import 'bloc/analysis_bloc/blocs/stories_not_following_you_back_bloc.dart';
import 'bloc/analysis_bloc/blocs/stories_you_dont_follow_back_bloc.dart';
import 'service/scrap/story_load_service.dart';
import 'bloc/story_popup/story_popup_bloc.dart';
import 'database/repository/story_repository.dart';
import 'service/scrap/story_view_load_service.dart';
import 'database/repository/story_view_repository.dart';
import 'bloc/story_view_sorted_users_load/story_view_sorted_users_load_bloc.dart';
import 'service/subscription/subscription_service.dart';
import 'database/repository/user_repository.dart';
import 'bloc/analysis_bloc/blocs/youre_not_following_back_bloc.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  gh.singletonAsync(() => SharedPreferences.getInstance());
  gh.lazySingleton<BackgroundTaskRegisterService>(
      () => BackgroundTaskRegisterService());
  gh.lazySingleton<GetActiveStoryByUserId>(() => GetActiveStoryByUserId());
  gh.lazySingleton<LocalNotificationRegisterService>(
      () => LocalNotificationRegisterService());
  gh.factory<MediaLoadBloc>(() => MediaLoadBloc(get<GetMediaInfoService>()));
  gh.lazySingleton<NavigatorService>(() => NavigatorService());
  gh.lazySingleton<SecureStorage>(() => SecureStorage());
  gh.lazySingleton<ServiceDenialInterceptor>(() => ServiceDenialInterceptor());
  gh.lazySingleton<SetupExternalRequirements>(
      () => SetupExternalRequirements());
  gh.factory<StoryPopupBloc>(
      () => StoryPopupBloc(get<GetActiveStoryByUserId>()));
  gh.lazySingleton<AppsflyerService>(
      () => AppsflyerService(get<LocalStorage>()));
  gh.factory<CommentSortedFeedLoadBloc>(
      () => CommentSortedFeedLoadBloc(get<GetFeedImagesService>()));
  gh.lazySingleton<GetActiveStoriesService>(
      () => GetActiveStoriesService(get<LocalStorage>()));
  gh.factory<LikeSortedFeedLoadBloc>(
      () => LikeSortedFeedLoadBloc(get<GetMyStoredFeedImagesService>()));
  gh.lazySingleton<LocalNotificationService>(() =>
      LocalNotificationService(get<LocalStorage>(), get<AppsflyerService>()));
  gh.lazySingleton<PeopleStoriesBloc>(
      () => PeopleStoriesBloc(get<GetActiveStoriesService>()));
  gh.lazySingleton<SubscriptionService>(
      () => SubscriptionService(get<LocalNotificationService>()));
  gh.factory<YoureNotFollowingBackBloc>(() => YoureNotFollowingBackBloc(
      get<FollowerUserRepository>(), get<UserRepository>()));
  gh.factory<BlockedYouBloc>(
      () => BlockedYouBloc(get<UserRepository>(), get<LocalStorage>()));
  gh.factory<DeletedYouLikesCommentsBloc>(() => DeletedYouLikesCommentsBloc(
        get<CommentRepository>(),
        get<LikeRepository>(),
        get<UserRepository>(),
        get<AppConfig>(),
        get<LocalStorage>(),
      ));
  gh.factory<FollowedBloc>(
      () => FollowedBloc(get<UserRepository>(), get<FollowedUserRepository>()));
  gh.factory<FollowersBloc>(() =>
      FollowersBloc(get<UserRepository>(), get<FollowerUserRepository>()));
  gh.factory<LikeSortedUsersLoadBloc>(() =>
      LikeSortedUsersLoadBloc(get<UserRepository>(), get<LocalStorage>()));
  gh.factory<LikedButDontFollowBloc>(
      () => LikedButDontFollowBloc(get<UserRepository>(), get<LocalStorage>()));
  gh.lazySingleton<LoggedOutInterceptor>(() =>
      LoggedOutInterceptor(get<LogoutService>(), get<NavigatorService>()));
  gh.lazySingleton<MyStoriesBloc>(() => MyStoriesBloc(get<StoryRepository>()));
  gh.factory<MyStoryPopupBloc>(() => MyStoryPopupBloc(get<UserRepository>()));
  gh.factory<NewFollowersBloc>(() => NewFollowersBloc(
        get<FollowerUserRepository>(),
        get<UserRepository>(),
        get<AppConfig>(),
      ));
  gh.factory<NotFollowingYouBackBloc>(() => NotFollowingYouBackBloc(
      get<FollowedUserRepository>(), get<UserRepository>()));
  gh.factory<ProfileLoadBloc>(() => ProfileLoadBloc(
        get<GetFeedImagesService>(),
        get<GetMyStoredFeedImagesService>(),
        get<GetMyStoredStoriesService>(),
        get<FollowedUserRepository>(),
        get<FollowerUserRepository>(),
        get<LikeRepository>(),
        get<StoryViewRepository>(),
      ));
  gh.factory<SecretAdmirersBloc>(() => SecretAdmirersBloc(
        get<FollowerUserRepository>(),
        get<CommentRepository>(),
        get<StoryViewRepository>(),
        get<LikeRepository>(),
        get<UserRepository>(),
        get<LocalStorage>(),
        get<AppConfig>(),
      ));
  gh.factory<SeenYourProfileBloc>(() => SeenYourProfileBloc(
        get<UserRepository>(),
        get<CommentRepository>(),
        get<LikeRepository>(),
        get<FollowerUserRepository>(),
        get<LocalStorage>(),
        get<AppConfig>(),
      ));
  gh.factory<SeenYourStoryBloc>(() => SeenYourStoryBloc(
        get<StoryRepository>(),
        get<StoryViewRepository>(),
        get<UserRepository>(),
      ));
  gh.lazySingleton<SetUpUserService>(
      () => SetUpUserService(get<SubscriptionService>(), get<LocalStorage>()));
  gh.factory<StoppedFollowingYouBloc>(() => StoppedFollowingYouBloc(
        get<FollowerUserRepository>(),
        get<UserRepository>(),
        get<AppConfig>(),
      ));
  gh.factory<StoriesNotFollowingYouBackBloc>(() =>
      StoriesNotFollowingYouBackBloc(
          get<StoryViewRepository>(), get<UserRepository>()));
  gh.factory<StoriesYouDontFollowBackBloc>(() => StoriesYouDontFollowBackBloc(
      get<StoryViewRepository>(), get<UserRepository>()));
  gh.factory<StoryViewSortedUsersLoadBloc>(
      () => StoryViewSortedUsersLoadBloc(get<UserRepository>()));
  gh.lazySingleton<DataLoadBloc>(() => DataLoadBloc(
        get<DashboardBloc>(),
        get<FollowedUserLoadService>(),
        get<FollowerUserLoadService>(),
        get<BestFriendUserLoadService>(),
        get<BlockedUserLoadService>(),
        get<StoryLoadService>(),
        get<FeedLoadService>(),
        get<BeenBlockedByUserService>(),
        get<ProfileViewsSimulationService>(),
        get<LikedButDontFollowService>(),
        get<LocalStorage>(),
        get<SetupExternalRequirements>(),
      ));

  // Eager singletons must be registered in the right order
  gh.singleton<AppConfig>(AppConfig());
  gh.singleton<GetFeedImagesService>(GetFeedImagesService());
  gh.singleton<GetFeedNextPageService>(GetFeedNextPageService());
  gh.singleton<GetMediaInfoService>(GetMediaInfoService());
  gh.singleton<GetStoriesService>(GetStoriesService());
  gh.singleton<LocalStorage>(LocalStorage());
  gh.singleton<DbProvider>(DbProvider(get<LocalStorage>()));
  gh.singleton<FeedRepository>(FeedRepository(get<DbProvider>()));
  gh.singleton<FollowedUserRepository>(
      FollowedUserRepository(get<DbProvider>()));
  gh.singleton<FollowerUserRepository>(
      FollowerUserRepository(get<DbProvider>()));
  gh.singleton<GetMyStoredFeedImagesService>(
      GetMyStoredFeedImagesService(feedRepository: get<FeedRepository>()));
  gh.singleton<LikeRepository>(LikeRepository(get<DbProvider>()));
  gh.singleton<LogoutService>(LogoutService(
    get<SecureStorage>(),
    get<LocalNotificationService>(),
    get<DbProvider>(),
  ));
  gh.singleton<StoryRepository>(StoryRepository(get<DbProvider>()));
  gh.singleton<StoryViewRepository>(StoryViewRepository(get<DbProvider>()));
  gh.singleton<UserRepository>(UserRepository(get<DbProvider>()));
  gh.singleton<AvatarPicRepository>(AvatarPicRepository(get<DbProvider>()));
  gh.singleton<BeenBlockedByUserService>(BeenBlockedByUserService(
    get<FollowedUserRepository>(),
    get<FollowerUserRepository>(),
    get<LocalStorage>(),
  ));
  gh.singleton<BestFriendUserRepository>(
      BestFriendUserRepository(get<DbProvider>()));
  gh.singleton<BlockedUserRepository>(BlockedUserRepository(get<DbProvider>()));
  gh.singleton<CommentRepository>(CommentRepository(get<DbProvider>()));
  gh.singleton<DashboardBloc>(DashboardBloc(
    get<FollowerUserRepository>(),
    get<FollowedUserRepository>(),
    get<StoryRepository>(),
    get<LikeRepository>(),
    get<CommentRepository>(),
    get<StoryViewRepository>(),
    get<AppConfig>(),
    get<LocalStorage>(),
    get<UserRepository>(),
  ));
  gh.singleton<FollowedUserLoadService>(FollowedUserLoadService(
    get<FollowedUserRepository>(),
    get<AppConfig>(),
    get<LocalStorage>(),
    get<UserRepository>(),
  ));
  gh.singleton<FollowerUserLoadService>(FollowerUserLoadService(
    get<FollowerUserRepository>(),
    get<AppConfig>(),
    get<LocalStorage>(),
    get<UserRepository>(),
  ));
  gh.singleton<GetMyStoredStoriesService>(
      GetMyStoredStoriesService(storyRepository: get<StoryRepository>()));
  gh.singleton<GetUsersYouLikedMediaService>(GetUsersYouLikedMediaService(
    get<AppConfig>(),
    get<LocalStorage>(),
    get<UserRepository>(),
  ));
  gh.singleton<LikeLoadService>(LikeLoadService(
    get<LikeRepository>(),
    get<LocalStorage>(),
    get<UserRepository>(),
  ));
  gh.singleton<LikedButDontFollowService>(LikedButDontFollowService(
    get<GetUsersYouLikedMediaService>(),
    get<FollowedUserRepository>(),
    get<LocalStorage>(),
  ));
  gh.singleton<MyProfileLoadBloc>(MyProfileLoadBloc(
    get<GetFeedImagesService>(),
    get<FollowedUserRepository>(),
    get<FollowerUserRepository>(),
    get<LikeRepository>(),
    get<CommentRepository>(),
    get<FeedRepository>(),
  ));
  gh.singleton<FriendshipCubit>(FriendshipCubit(
    get<FollowedUserRepository>(),
    get<FollowerUserRepository>(),
    get<UserRepository>(),
  ));
  gh.singleton<ProfileViewsSimulationService>(ProfileViewsSimulationService(
    get<FollowerUserRepository>(),
    get<LikeRepository>(),
    get<CommentRepository>(),
    get<FeedRepository>(),
    get<AppConfig>(),
  ));
  gh.singleton<StoryViewLoadService>(StoryViewLoadService(
    get<StoryViewRepository>(),
    get<StoryRepository>(),
    get<AppConfig>(),
    get<UserRepository>(),
  ));
  gh.singleton<BestFriendUserLoadService>(BestFriendUserLoadService(
    get<BestFriendUserRepository>(),
    get<AppConfig>(),
    get<LocalStorage>(),
    get<AvatarPicRepository>(),
    get<UserRepository>(),
  ));
  gh.singleton<BlockedUserLoadService>(BlockedUserLoadService(
    get<BlockedUserRepository>(),
    get<AppConfig>(),
    get<LocalStorage>(),
    get<UserRepository>(),
  ));
  gh.singleton<CommentLoadService>(CommentLoadService(
    get<CommentRepository>(),
    get<AppConfig>(),
    get<LocalStorage>(),
    get<UserRepository>(),
  ));
  gh.singleton<FeedLoadService>(FeedLoadService(
    get<LikeLoadService>(),
    get<CommentLoadService>(),
    get<FeedRepository>(),
    get<AppConfig>(),
    get<LocalStorage>(),
  ));
  gh.singleton<StoryLoadService>(StoryLoadService(
    get<StoryRepository>(),
    get<StoryViewLoadService>(),
    get<AppConfig>(),
    get<LocalStorage>(),
  ));
  return get;
}
