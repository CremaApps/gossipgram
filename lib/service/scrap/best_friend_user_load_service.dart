import 'package:flutter/material.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/avatar_pic_repository.dart';
import 'package:GossipGram/database/repository/best_friend_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/avatar_pic.dart';
import 'package:GossipGram/model/objects/user/best_friend_user.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@singleton
class BestFriendUserLoadService extends UserLoadService {
  static const String SERVICE_NAME = "best_friend_user_load_service";

  BestFriendUserRepository _bestFriendUserRepository;
  AvatarPicRepository _avatarPicRepository;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  BestFriendUserLoadService(
    this._bestFriendUserRepository,
    this._appConfig,
    this._localStorage,
    this._avatarPicRepository,
    UserRepository userRepository,
  ) : super(userRepository);

  Future<void> execute({
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    int nowDate = firstLoad ? 0 : DateTime.now().millisecondsSinceEpoch;
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);

    var bestFriendsResponse = await instagramPrivateApi.friendship.besties(
      instagramPrivateApi.myUserId,
    );
    DataLoadBloc.printProfiling("API best friends media step loaded", now);

    var bestFriends = bestFriendsResponse.response.users;
    while (bestFriendsResponse.hasNext()) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));
      bestFriendsResponse = await bestFriendsResponse.next();
      bestFriends.addAll(bestFriendsResponse.response.users);
      DataLoadBloc.printProfiling("API best friends media step loaded", now);
    }
    DataLoadBloc.printProfiling("API best friends media all loaded", now);

    List<int> activeFollowerUserIds =
        await _bestFriendUserRepository.getBestFriendUserIds(
      isValid: true,
    );

    for (int id in activeFollowerUserIds) {
      if (!bestFriends.any((user) => user.pk == id)) {
        await _bestFriendUserRepository.upsertBestFriendUser(
          bFUser: BestFriendUser(id: id, isValid: false, youFollow: true),
        );

        await _avatarPicRepository
            .upsertAvatarPic(AvatarPic(id: id, userId: id.toString()));
      }
    }
    List<int> activeFollowerUserIdsThatUDOntFollow =
        await _bestFriendUserRepository.getBestFriendUserIds(
            isValid: true, notFollowed: true);

    for (var bestFriend in bestFriends) {
      bool youFollowUser = false;
      for (int id in activeFollowerUserIdsThatUDOntFollow) {
        if (bestFriend.pk == id) {
          youFollowUser == false;
        }
      }

      Account.User user = Account.User(
        id: bestFriend.pk,
        username: bestFriend.username,
        fullName: bestFriend.fullName,
        isPrivate: bestFriend.isPrivate,
        isVerified: bestFriend.isVerified,
        youFollow: youFollowUser,
        profilePicUrl: bestFriend.profilePicUrl,
        anonymousProfilePicture: bestFriend.hasAnonymousProfilePicture,
      );

      await processUserIfNotAlreadyProcessed(user, processedUsers);

      BestFriendUser dbUser =
          await _bestFriendUserRepository.getBestFriendUser(id: user.id);
      int onStateChange = dbUser != null && dbUser.isValid == true
          ? dbUser.onStateChange
          : nowDate;

      await _bestFriendUserRepository.upsertBestFriendUser(
          bFUser: BestFriendUser(
        id: user.id,
        isValid: true,
        youFollow: youFollowUser,
        onStateChange: onStateChange,
      ));
    }
    DataLoadBloc.printProfiling("Best friends DB loaded", now);
  }
}
