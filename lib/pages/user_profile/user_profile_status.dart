part of 'user_profile_page.dart';

class _UserProfileStatus extends StatelessWidget {
  final int relation;

  _UserProfileStatus({@required this.relation});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16),
      alignment: Alignment.center,
      width: double.infinity,
      child: _statusText(context),
    );
  }

  Text _statusText(BuildContext context) {
    switch (relation) {
      case ProfileLoadBloc.STATUS_STRANGERS:
        return Text(
          S.of(context).notFollowingEachOther,
          style: appTextTheme.caption
              .copyWith(fontSize: 10, color: appColorScheme.onBackground),
        );
        break;
      case ProfileLoadBloc.STATUS_FRIENDS:
        return Text(
          S.of(context).followingEachOther,
          style: appTextTheme.caption
              .copyWith(fontSize: 10, color: appColorScheme.onBackground),
        );
        break;
      case ProfileLoadBloc.STATUS_FOLLOWED:
        return Text(
          S.of(context).doesNotFollow,
          style: appTextTheme.caption
              .copyWith(fontSize: 10, color: appColorScheme.onBackground),
        );
        break;
      case ProfileLoadBloc.STATUS_FOLLOWING:
        return Text(
          S.of(context).notFollowingThisUser,
          style: appTextTheme.caption
              .copyWith(fontSize: 10, color: appColorScheme.onBackground),
        );
        break;
      default:
        throw UnimplementedError();
    }
  }

  Icon _statusIcon(BuildContext context) {
    switch (relation) {
      case ProfileLoadBloc.STATUS_STRANGERS:
        return Icon(
          Icons.info_outline,
          color: AppTheme.errors['600'],
        );
        break;
      case ProfileLoadBloc.STATUS_FRIENDS:
        return Icon(
          Icons.done,
          color: AppTheme.complementaries['900'],
        );
        break;
      case ProfileLoadBloc.STATUS_FOLLOWED:
        return Icon(
          Icons.keyboard_arrow_right,
          color: AppTheme.errors['600'],
        );
        break;
      case ProfileLoadBloc.STATUS_FOLLOWING:
        return Icon(
          Icons.keyboard_arrow_left,
          color: AppTheme.errors['600'],
        );
        break;
      default:
        throw UnimplementedError();
    }
  }
}
