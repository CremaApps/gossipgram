import 'package:flutter/material.dart';

class User {
  final int id;
  final String username;
  final String fullName;
  final bool isPrivate;
  final bool isVerified;
  final bool youFollow;
  final bool anonymousProfilePicture;
  final String profilePicUrl;
  int createdAt;

  User({
    @required this.id,
    @required this.username,
    @required this.fullName,
    @required this.isPrivate,
    @required this.isVerified,
    @required this.youFollow,
    @required this.anonymousProfilePicture,
    this.profilePicUrl = '',
    this.createdAt,
  });

  Map<String, dynamic> toMap() => {
        'id': id,
        'username': username,
        'fullName': fullName,
        'isPrivate': isPrivate ? 1 : 0,
        'isVerified': isVerified ? 1 : 0,
        'youFollow': (youFollow != null && youFollow) ? 1 : 0,
        'anonymousProfilePicture': anonymousProfilePicture ? 1 : 0,
        'profilePicUrl': profilePicUrl,
        'createdAt': createdAt,
      };

  factory User.fromMap(Map<String, dynamic> map) => User(
        id: map['id'],
        username: map['username'],
        fullName: map['fullName'],
        isPrivate: map['isPrivate'] == 1,
        isVerified: map['isVerified'] == 1,
        youFollow: map['youFollow'] == 1,
        anonymousProfilePicture: map['anonymousProfilePicture'] == 1,
        profilePicUrl: map['profilePicUrl'],
        createdAt: map['createdAt'],
      );
}
