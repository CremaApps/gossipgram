import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/repository/followed_user_repository.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/database/repository/story_view_repository.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/model/objects/story_thumbnail.dart';
import 'package:GossipGram/model/view_data/profile.dart';
import 'package:GossipGram/service/api/get_feed_images_service.dart';
import 'package:GossipGram/service/api/get_my_stored_feed_images_service.dart';
import 'package:GossipGram/service/api/get_my_stored_stories_service.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

part 'profile_load_event.dart';
part 'profile_load_state.dart';

@injectable
class ProfileLoadBloc extends Bloc<ProfileLoadEvent, ProfileLoadState> {
  static const int STATUS_STRANGERS = 0;
  static const int STATUS_FRIENDS = 1;
  static const int STATUS_FOLLOWED = 2;
  static const int STATUS_FOLLOWING = 3;

  InstagramPrivateApi _instagramPrivateApi;
  GetFeedImagesService _getFeedImagesService;
  GetMyStoredFeedImagesService _getMyStoredFeedImagesService;
  GetMyStoredStoriesService _getMyStoredStoriesService;
  FollowedUserRepository _followedUserRepository;
  FollowerUserRepository _followerUserRepository;
  LikeRepository _likeRepository;
  StoryViewRepository _storyViewRepository;
  Profile _profile = Profile();

  ProfileLoadBloc(
    this._getFeedImagesService,
    this._getMyStoredFeedImagesService,
    this._getMyStoredStoriesService,
    this._followedUserRepository,
    this._followerUserRepository,
    this._likeRepository,
    this._storyViewRepository,
  ) : super(ProfileLoadInitial()) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  @override
  Stream<ProfileLoadState> mapEventToState(
    ProfileLoadEvent event,
  ) async* {
    if (event is ProfileLoadEventStart) {
      yield ProfileLoadInitial();

      _profile = Profile();

      InstagramResponse<UserInfoResponse> userInfo;

      try {
        userInfo = await _instagramPrivateApi.user.getAccountInfo(event.userId);
      } on DioError catch (e) {
        if (e.type == DioErrorType.RESPONSE && e.response.statusCode == 404) {
          yield ProfileNotExistsState();
        }
      }

      if (userInfo != null) {
        bool existsFollowerUser = await _followerUserRepository
            .existsFollowerUser(id: userInfo.response.user.pk);
        bool follows = existsFollowerUser
            ? (await _followerUserRepository.getFollowerUser(
                    id: userInfo.response.user.pk))
                .isValid
            : false;

        bool existsFollowedUser = await _followedUserRepository
            .existsFollowedUser(id: userInfo.response.user.pk);

        bool followed = existsFollowedUser
            ? (await _followedUserRepository.getFollowedUser(
                    id: userInfo.response.user.pk))
                .isValid
            : false;

        int relation = follows
            ? (followed
                ? ProfileLoadBloc.STATUS_FRIENDS
                : ProfileLoadBloc.STATUS_FOLLOWING)
            : followed
                ? ProfileLoadBloc.STATUS_FOLLOWED
                : ProfileLoadBloc.STATUS_STRANGERS;

        int followerCount = userInfo.response.user.followerCount;
        int followedCount = userInfo.response.user.followingCount;

        _profile = _profile.copyWith(
          fullName: userInfo.response.user.fullName,
          profilePicUrl: userInfo.response.user.profilePicUrl,
          bio: userInfo.response.user.biography,
          followers: followerCount,
          following: followedCount,
          relation: relation,
        );

        yield ProfileLoadProfileInfoLoaded(profile: _profile);

        dynamic feedImageData;

        try {
          feedImageData = await _getFeedImagesService.execute(
            userId: event.userId,
            singlePage: true,
          );
        } on DioError catch (e) {
          if (e.type == DioErrorType.RESPONSE && e.response.statusCode == 400) {
            yield ProfileIsPrivateState(profile: _profile);
          }
        }

        if (feedImageData != null) {
          int totalLikes = 0;
          int totalComments = 0;
          int totalFeedElements = 0;

          for (int i = 3;
              i <= 10 && i < feedImageData["feedThumbnailData"].length;
              ++i) {
            totalLikes +=
                feedImageData["feedThumbnailData"].elementAt(i).likeCount;
            totalComments +=
                feedImageData["feedThumbnailData"].elementAt(i).commentCount;
            ++totalFeedElements;
          }

          double averageLikes =
              totalFeedElements > 0 ? totalLikes / totalFeedElements : 0;
          double averageComments =
              totalFeedElements > 0 ? totalComments / totalFeedElements : 0;
          double followerRatio =
              followedCount > 0 ? followerCount / followedCount : 0;
          double engagementRate = followerCount > 0
              ? (averageLikes + averageComments) * 100 / followerCount
              : 0;

          _profile = _profile.copyWith(
            averageLikes: averageLikes,
            followerRatio: followerRatio,
            engagementRate: engagementRate,
          );

          yield ProfileLoadInfoAnalyticsLoaded(profile: _profile);

          List<dynamic> likedFeed = await _likeRepository.getLikeIds(
            isValid: true,
            userId: event.userId,
          );

          List<FeedThumbnail> likedFeedImages = [];

          // TODO: This info could be stored somewhere for future access on lazy load of the profile images
          // TODO: Evaluate if a sequential scan (our first approach) is more or less efficient than indexed single searches
          List<FeedThumbnail> userImages =
              await _getMyStoredFeedImagesService.execute();

          for (var image in userImages) {
            if (likedFeed
                .any((like) => like['feedId'] == image.id.toString())) {
              likedFeedImages.add(image);
            }
          }

          List<dynamic> storyViews = await _storyViewRepository.getStoryViewIds(
            isValid: true,
            seenByUserId: event.userId,
          );

          List<StoryThumbnail> seenStories = [];

          List<StoryThumbnail> userStoryViews =
              await _getMyStoredStoriesService.execute();

          for (var storyView in userStoryViews) {
            if (storyViews.any((story) => story['storyId'] == storyView.id)) {
              seenStories.add(storyView);
            }
          }

          _profile = _profile.copyWith(
            feedThumbnail: feedImageData["feedThumbnailData"],
            likedFeedThumbnail: likedFeedImages,
            storyThumbnail: seenStories,
            feedResponse: feedImageData["response"],
          );

          yield feedImageData["response"].hasNext()
              ? ProfileLoadFeedLoading(profile: _profile)
              : ProfileLoadFullyLoaded(profile: _profile);
        }
      }
    } else if (event is ProfileLoadEventGetFeedPage) {
      var feedImageData = await _getFeedImagesService.execute(
        singlePage: true,
        feedItemsResponse: _profile.feedResponse,
      );

      _profile = _profile.copyWith(
        feedThumbnail: _profile.feedThumbnail
          ..addAll(feedImageData["feedThumbnailData"]),
        feedResponse: feedImageData["response"],
      );

      yield feedImageData["response"].hasNext()
          ? ProfileLoadFeedLoading(profile: _profile)
          : ProfileLoadFullyLoaded(profile: _profile);
    }
  }
}
