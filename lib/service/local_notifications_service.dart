import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:GossipGram/service/analytics/appsflyer_service.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

@lazySingleton
class LocalNotificationService {
  static const String DEFAULT_ANDROID_CHANNEL_NAME = 'main';
  static const String DEFAULT_ANDROID_CHANNEL_ID = 'main_id';
  static const String DEFAULT_ANDROID_CHANNEL_DESCRIPTION = 'default_channel';
  static const String HAS_ACCEPTED_PERMISSIONS = 'has_accepted_permissions';

  // TODO These constants should be on an interface to avoid coupling
  static const RepeatInterval PERIODIC_NOTIFICATIONS_INTERVAL_EVERY_MINUTE =
      RepeatInterval.everyMinute;
  static const RepeatInterval PERIODIC_NOTIFICATIONS_INTERVAL_HOURLY =
      RepeatInterval.hourly;
  static const RepeatInterval PERIODIC_NOTIFICATIONS_INTERVAL_DAILY =
      RepeatInterval.daily;
  static const RepeatInterval PERIODIC_NOTIFICATIONS_INTERVAL_WEEKLY =
      RepeatInterval.weekly;

  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  NotificationDetails _defaultPlatformChannelSpecifics;

  bool _initialized = false;
  LocalStorage _localStorage;
  AppsflyerService _appsflyerService;

  LocalNotificationService(
    this._localStorage,
    this._appsflyerService,
  );

  Future<void> initialize({
    Function onDidReceiveLocalNotification,
    Function selectNotification,
  }) async {
    if (!_initialized) {
      tz.initializeTimeZones();
      const AndroidInitializationSettings initializationSettingsAndroid =
          AndroidInitializationSettings('@drawable/app_icon');

      final IOSInitializationSettings initializationSettingsIOS =
          IOSInitializationSettings(
              onDidReceiveLocalNotification: onDidReceiveLocalNotification ??
                  _onDidReceiveLocalNotification);

      final InitializationSettings initializationSettings =
          InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
      );

      await _flutterLocalNotificationsPlugin.initialize(
        initializationSettings,
        onSelectNotification: selectNotification ?? _selectNotification,
      );

      const AndroidNotificationDetails defaultAndroidPlatformChannelSpecifics =
          AndroidNotificationDetails(
        DEFAULT_ANDROID_CHANNEL_ID,
        DEFAULT_ANDROID_CHANNEL_NAME,
        DEFAULT_ANDROID_CHANNEL_DESCRIPTION,
      );
      _defaultPlatformChannelSpecifics =
          NotificationDetails(android: defaultAndroidPlatformChannelSpecifics);

      _initialized = true;
    }
  }

  Future<bool> requestNotificationPermissions({
    bool alert = true,
    bool badge = true,
    bool sound = true,
  }) async {
    if (!_localStorage.hasData(HAS_ACCEPTED_PERMISSIONS,
        generalStorage: true)) {
      bool accepted = Platform.isAndroid
          ? true
          : await _flutterLocalNotificationsPlugin
              .resolvePlatformSpecificImplementation<
                  IOSFlutterLocalNotificationsPlugin>()
              ?.requestPermissions(
                alert: alert,
                badge: badge,
                sound: sound,
              );
      await _localStorage.write(
        HAS_ACCEPTED_PERMISSIONS,
        accepted,
        generalStorage: true,
      );
      return accepted;
    }
    return _localStorage.read(HAS_ACCEPTED_PERMISSIONS, generalStorage: true);
  }

  Future<void> sendSimpleInstantLocalNotification({
    @required int notificationId,
    @required String title,
    @required String body,
    String payload,
  }) async {
    if (!_initialized) await initialize();
    if (_localStorage.hasData(HAS_ACCEPTED_PERMISSIONS, generalStorage: true) &&
        _localStorage.read(HAS_ACCEPTED_PERMISSIONS, generalStorage: true)) {
      await _flutterLocalNotificationsPlugin.show(
          notificationId, title, body, _defaultPlatformChannelSpecifics,
          payload: payload);
    }
  }

  Future<void> sendScheduledNotification({
    @required int notificationId,
    @required String title,
    @required String body,
    @required int seconds,
    String payload,
  }) async {
    if (!_initialized) await initialize();
    if (_localStorage.hasData(HAS_ACCEPTED_PERMISSIONS, generalStorage: true) &&
        _localStorage.read(HAS_ACCEPTED_PERMISSIONS, generalStorage: true)) {
      await _flutterLocalNotificationsPlugin.zonedSchedule(
          notificationId,
          title,
          body,
          tz.TZDateTime.now(tz.local).add(Duration(seconds: seconds)),
          _defaultPlatformChannelSpecifics,
          androidAllowWhileIdle: true,
          payload: payload,
          uiLocalNotificationDateInterpretation:
              UILocalNotificationDateInterpretation.absoluteTime);
    }
  }

  Future<void> sendPeriodicNotification({
    @required int notificationId,
    @required String title,
    @required String body,
    @required RepeatInterval interval,
    String payload,
  }) async {
    if (!_initialized) await initialize();
    if (_localStorage.hasData(HAS_ACCEPTED_PERMISSIONS, generalStorage: true) &&
        _localStorage.read(HAS_ACCEPTED_PERMISSIONS, generalStorage: true)) {
      await _flutterLocalNotificationsPlugin.periodicallyShow(notificationId,
          title, body, interval, _defaultPlatformChannelSpecifics,
          payload: payload, androidAllowWhileIdle: true);
    }
  }

  Future<void> cancelNotificationWithId({
    @required int id,
  }) async {
    await _flutterLocalNotificationsPlugin.cancel(id);
  }

  Future<void> cancelAllNotifications() async {
    await _flutterLocalNotificationsPlugin.cancelAll();
  }

  // Notification callback when the user clicks on a notification
  Future _selectNotification(String payload) async {
    if (payload != null) {
      _appsflyerService.logEvent(
        eventName: 'local_notification_selected',
        eventValues: {'local_notification_type': payload},
      );
    }
  }

  // Notification callback for older versions of iOS. It fires when receiving a
  // local notification while on foreground
  Future _onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    print(title);
  }
}
