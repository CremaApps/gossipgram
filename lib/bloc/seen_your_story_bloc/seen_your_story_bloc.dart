import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/seen_your_story_bloc/seen_your_story_event.dart';
import 'package:GossipGram/bloc/seen_your_story_bloc/seen_your_story_state.dart';
import 'package:GossipGram/database/repository/story_repository.dart';
import 'package:GossipGram/database/repository/story_view_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:GossipGram/model/objects/story_view.dart';
import 'package:GossipGram/model/objects/user/user.dart';
import 'package:GossipGram/model/view_data/seen_your_story_user_item.dart';
import 'package:injectable/injectable.dart';

@injectable
class SeenYourStoryBloc extends Bloc<SeenYourStoryEvent, SeenYourStoryState> {
  StoryRepository _storyRepository;
  StoryViewRepository _storyViewRepository;
  UserRepository _userRepository;

  SeenYourStoryBloc(
    this._storyRepository,
    this._storyViewRepository,
    this._userRepository,
  ) : super(SeenYourStoryInitial());

  Stream<SeenYourStoryState> mapEventToState(
    SeenYourStoryEvent event,
  ) async* {
    if (event is SeenYourStoryLoadEvent) {
      yield SeenYourStoryLoadingState();

      List<Story> activeStories = (await _storyRepository.queryStories(
        isValid: true,
      ));

      List<StoryView> storyViews = [];
      for (Story story in activeStories) {
        storyViews += (await _storyViewRepository.queryStoryViews(
          storyId: story.id,
          isValid: true,
        ));
      }

      List<int> userIds = [];

      storyViews.forEach((storyView) {
        if (userIds
            .where((userId) => userId == storyView.seenByUserId)
            .isEmpty) {
          userIds.add(storyView.seenByUserId);
        }
      });

      var users =
          await Future.wait(userIds.map((e) => _userRepository.getUser(id: e)));

      List<SeenYourStoryUserItem> seenYourStoryUserItems = [];

      storyViews.forEach((storyView) async {
        User user = users.firstWhere((e) => e.id == storyView.seenByUserId);
        seenYourStoryUserItems.add(
          SeenYourStoryUserItem(
            userId: user.id,
            name: user.fullName != '' ? user.fullName : user.username,
            userName: user.username,
            imageUrl: user.profilePicUrl,
            storyId: storyView.storyId,
          ),
        );
      });

      yield SeenYourStoryLoadedState(seenYourStoryUserItems);
    } else
      throw UnimplementedError();
  }
}
