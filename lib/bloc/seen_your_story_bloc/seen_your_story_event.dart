import 'package:equatable/equatable.dart';

abstract class SeenYourStoryEvent extends Equatable {
  const SeenYourStoryEvent();
}

class SeenYourStoryLoadEvent extends SeenYourStoryEvent {
  @override
  List<Object> get props => [];
}
