import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/user/blocked_user.dart';
import 'package:injectable/injectable.dart';

@singleton
class BlockedUserRepository {
  final DbProvider _dbProvider;

  BlockedUserRepository(this._dbProvider);

  Future<BlockedUser> getBlockedUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('BlockedUser', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? BlockedUser.fromMap(res.first) : null;
  }

  Future<List<BlockedUser>> queryBlockedUsers({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
            'FROM BlockedUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var dbUsers = await db.rawQuery(query);
    List<BlockedUser> users = dbUsers.isNotEmpty
        ? dbUsers.map((a) => BlockedUser.fromMap(a)).toList()
        : [];
    return users;
  }

  Future<int> queryCountBlockedUsers({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM BlockedUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var dbBUsersCount = await db.rawQuery(query);

    return dbBUsersCount.first.values.first;
  }

  Future<int> upsertBlockedUser({@required BlockedUser user}) async {
    final db = await _dbProvider.database;
    var dbUser = await db.query(
      'BlockedUser',
      where: 'id = ?',
      whereArgs: [user.id],
    );
    if (dbUser.isNotEmpty) {
      return db.update(
        'BlockedUser',
        user.toMap(),
        where: 'id = ?',
        whereArgs: [user.id],
      );
    } else {
      return await db.insert('BlockedUser', user.toMap());
    }
  }

  Future<int> deleteBlockedUser({@required BlockedUser user}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'BlockedUser',
      where: 'id = ?',
      whereArgs: [user.id],
    );
  }

  Future<List<int>> getBlockedUserIds(
      {bool isValid = true,
      int stateChangedAtGreaterThan,
      int stateChangedAtLesserThan,
      bool notFollowed}) async {
    final db = await _dbProvider.database;
    var query = 'SELECT id '
            'FROM BlockedUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }
    if (notFollowed != null && notFollowed) {
      query += 'AND id NOT IN (SELECT id FROM FollowedUser WHERE isValid = 1)';
    }

    var queryResponse = await db.rawQuery(query);
    List<int> blockedUserIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int id = a['id'];
            return id;
          }).toList()
        : [];
    return blockedUserIds;
  }
}
