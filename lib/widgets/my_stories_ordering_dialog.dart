import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';

class MyStoriesOrderingDialog extends StatefulWidget {
  final MyStoriesOrderingOptions currentOrdering;

  MyStoriesOrderingDialog({this.currentOrdering});
  @override
  _MyStoriesOrderingDialogState createState() =>
      _MyStoriesOrderingDialogState();
}

class _MyStoriesOrderingDialogState extends State<MyStoriesOrderingDialog> {
  MyStoriesOrderingOptions radioSelectedValue;

  @override
  void initState() {
    super.initState();
    radioSelectedValue = widget.currentOrdering;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: appColorScheme.background,
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          width: double.infinity,
          child: Text(
            "Sort your stories by",
            style: appTextTheme.subtitle2,
          ),
        ),
        Theme(
          data: Theme.of(context).copyWith(
              unselectedWidgetColor: getThemeModeAsInt() == 0
                  ? Color(0xFFCCD1D6)
                  : Color(0xFF829AC1),
              disabledColor: Colors.blue),
          child: RadioListTile<MyStoriesOrderingOptions>(
            title: Text(
              S.of(context).mostRecent,
              style: appTextTheme.subtitle1.copyWith(
                  color: radioSelectedValue.index == 0
                      ? appColorScheme.secondary
                      : getThemeModeAsInt() == 0
                          ? Color(0xFF636D77)
                          : Color(0xFF829AC1)),
            ),
            value: MyStoriesOrderingOptions.newest,
            groupValue: radioSelectedValue,
            onChanged: (MyStoriesOrderingOptions value) {
              setState(() {
                radioSelectedValue = value;
              });
            },
          ),
        ),
        Theme(
          data: Theme.of(context).copyWith(
              unselectedWidgetColor: getThemeModeAsInt() == 0
                  ? Color(0xFFCCD1D6)
                  : Color(0xFF829AC1),
              disabledColor: Colors.blue),
          child: RadioListTile<MyStoriesOrderingOptions>(
            title: Text(
              S.of(context).oldest,
              style: appTextTheme.subtitle1.copyWith(
                  color: radioSelectedValue.index == 1
                      ? appColorScheme.secondary
                      : getThemeModeAsInt() == 0
                          ? Color(0xFF636D77)
                          : Color(0xFF829AC1)),
            ),
            value: MyStoriesOrderingOptions.oldest,
            groupValue: radioSelectedValue,
            onChanged: (MyStoriesOrderingOptions value) {
              setState(() {
                radioSelectedValue = value;
              });
            },
          ),
        ),
        Theme(
          data: Theme.of(context).copyWith(
              unselectedWidgetColor: getThemeModeAsInt() == 0
                  ? Color(0xFFCCD1D6)
                  : Color(0xFF829AC1),
              disabledColor: Colors.blue),
          child: RadioListTile<MyStoriesOrderingOptions>(
            title: Text(
              S.of(context).mostPopular,
              style: appTextTheme.subtitle1.copyWith(
                  color: radioSelectedValue.index == 2
                      ? appColorScheme.secondary
                      : getThemeModeAsInt() == 0
                          ? Color(0xFF636D77)
                          : Color(0xFF829AC1)),
            ),
            value: MyStoriesOrderingOptions.mostSeen,
            groupValue: radioSelectedValue,
            onChanged: (MyStoriesOrderingOptions value) {
              setState(() {
                radioSelectedValue = value;
              });
            },
          ),
        ),
        Theme(
          data: Theme.of(context).copyWith(
              unselectedWidgetColor: getThemeModeAsInt() == 0
                  ? Color(0xFFCCD1D6)
                  : Color(0xFF829AC1),
              disabledColor: Colors.blue),
          child: RadioListTile<MyStoriesOrderingOptions>(
            title: Text(
              S.of(context).leastSeen,
              style: appTextTheme.subtitle1.copyWith(
                  color: radioSelectedValue.index == 3
                      ? appColorScheme.secondary
                      : getThemeModeAsInt() == 0
                          ? Color(0xFF636D77)
                          : Color(0xFF829AC1)),
            ),
            value: MyStoriesOrderingOptions.leastSeen,
            groupValue: radioSelectedValue,
            onChanged: (MyStoriesOrderingOptions value) {
              setState(() {
                radioSelectedValue = value;
              });
            },
          ),
        ),
        Divider(
          thickness: 0.5,
          height: 0.5,
        ),
        Container(
          width: double.infinity,
          child: TextButton(
            onPressed: () {
              Navigator.of(context).pop(radioSelectedValue);
            },
            child: Text(
              "Close",
            ),
          ),
        ),
      ]),
    );
  }
}

enum MyStoriesOrderingOptions { newest, oldest, mostSeen, leastSeen }
