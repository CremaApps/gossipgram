import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:GossipGram/model/view_data/story_popup.dart';
import 'package:GossipGram/service/api/get_active_story_by_user_id.dart';
import 'package:injectable/injectable.dart';

part 'story_popup_event.dart';
part 'story_popup_state.dart';

@injectable
class StoryPopupBloc extends Bloc<StoryPopupEvent, StoryPopupState> {
  GetActiveStoryByUserId _getActiveStoryByUserId;

  StoryPopupBloc(this._getActiveStoryByUserId) : super(StoryPopupInitial());

  @override
  Stream<StoryPopupState> mapEventToState(
    StoryPopupEvent event,
  ) async* {
    if (event is PopupStoriesStartLoadEvent) {
      yield StoryPopupLoading();

      yield StoryPopupLoaded(
          storiesPopup:
              await this._getActiveStoryByUserId.execute(userId: event.userId));
    } else
      throw UnimplementedError();
  }
}
