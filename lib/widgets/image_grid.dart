import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/profile_load/profile_load_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/widgets/media_popup.dart';
import 'package:lottie/lottie.dart';

class ImageGrid extends StatefulWidget {
  final ProfileLoadBloc profileLoadBloc;
  final String emptyMessage;

  ImageGrid({
    @required this.profileLoadBloc,
    @required this.emptyMessage,
  });

  @override
  _ImageGridState createState() => _ImageGridState();
}

class _ImageGridState extends State<ImageGrid> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileLoadBloc, ProfileLoadState>(
        cubit: widget.profileLoadBloc,
        builder: (context, state) {
          if (state is ProfileLoadData &&
              (state is ProfileLoadFullyLoaded ||
                  state is ProfileLoadFeedLoading)) {
            if (state.profile.feedThumbnail.isNotEmpty) {
              return Container(
                  child: GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: state.profile.feedThumbnail.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 3,
                  crossAxisSpacing: 3,
                  childAspectRatio: 1,
                ),
                itemBuilder: (contxt, indx) {
                  return InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return MediaPopup(
                            mediaId:
                                state.profile.feedThumbnail.elementAt(indx).id,
                            mediaType: state.profile.feedThumbnail
                                .elementAt(indx)
                                .mediaType,
                          );
                        },
                      );
                    },
                    child: LayoutBuilder(
                      builder: (_, BoxConstraints constraints) {
                        return Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(state.profile.feedThumbnail
                                  .elementAt(indx)
                                  .image
                                  .getSizedCandidate(
                                      width: constraints.maxWidth,
                                      height: constraints.maxHeight)
                                  .url),
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      },
                    ),
                  );
                },
              ));
            } else {
              return Center(
                child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(S.of(context).noPosts,
                            style: appTextTheme.headline6),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(widget.emptyMessage,
                              style: appTextTheme.bodyText1,
                              textAlign: TextAlign.center),
                        ),
                      ],
                    ),
                    height: MediaQuery.of(context).size.height / 3.5,
                    width: MediaQuery.of(context).size.width / 1.5),
              );
            }
          } else {
            return Container(
              child: Lottie.asset(
                'assets/animations/loader.json',
                width: 100,
                height: 100,
              ),
            );
          }
        });
  }
}
