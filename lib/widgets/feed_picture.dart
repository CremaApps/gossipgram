import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FeedPicture extends StatelessWidget {
  final String profilePicUrl;

  const FeedPicture({@required this.profilePicUrl});

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: profilePicUrl,
      fit: BoxFit.cover,
      placeholder: (context, url) => Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
        ),
      ),
      errorWidget: (context, url, error) =>
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
          ),
    );
  }
}
