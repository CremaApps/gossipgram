import 'package:flutter/material.dart';

class BlockedUser {
  final int id;
  int onStateChange;
  final bool isValid;
  final bool youFollow;

  BlockedUser(
      {@required this.id,
      this.onStateChange,
      this.isValid = true,
      this.youFollow}) {
    if (this.onStateChange == null) {
      this.onStateChange = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'onStateChange': onStateChange,
        'isValid': isValid ? 1 : 0,
        'youFollow': (youFollow != null && youFollow) ? 1 : 0,
      };

  factory BlockedUser.fromMap(Map<String, dynamic> map) => BlockedUser(
        id: map['id'],
        onStateChange: map['onStateChange'],
        isValid: map['isValid'] == 1,
        youFollow: map['youFollow'] == 1,
      );
}
