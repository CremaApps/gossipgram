import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/service/api/been_blocked_by_user_service.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:injectable/injectable.dart';

@injectable
class BlockedYouBloc extends IAnalysisBloc {
  final UserRepository _userRepository;
  final LocalStorage _localStorage;

  BlockedYouBloc(
    this._userRepository,
    this._localStorage,
  );

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      List<int> _blockedByUsers = _localStorage
          .read<List<dynamic>>(BeenBlockedByUserService.BLOCKED_CACHE)
          .map((e) => (e is int) ? e : int.parse(e))
          .toList();

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          _blockedByUsers.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) async {
        analysisUserItems.add(
          AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            youFollow: false,
            imageUrl: element.profilePicUrl,
          ),
        );
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
