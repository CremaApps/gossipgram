import 'package:flutter/foundation.dart';
import 'package:GossipGram/model/objects/user/user.dart';

class UserLikeCount {
  User user;
  int likeCount;

  UserLikeCount({
    @required this.user,
    @required this.likeCount,
  });
}
