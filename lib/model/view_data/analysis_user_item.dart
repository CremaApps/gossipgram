import 'package:freezed_annotation/freezed_annotation.dart';

part 'analysis_user_item.freezed.dart';

@freezed
abstract class AnalysisUserItem with _$AnalysisUserItem {
  const factory AnalysisUserItem(
      {int userId,
      String name,
      String userName,
      String imageUrl,
      bool youFollow}) = _AnalysisUserItem;
}
