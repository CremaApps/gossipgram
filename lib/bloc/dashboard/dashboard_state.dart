import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:GossipGram/model/view_data/dashboard.dart';

abstract class DashboardState extends Equatable {
  final Dashboard dashboard;

  const DashboardState({@required this.dashboard});

  @override
  List<Object> get props => [
        dashboard,
      ];
}

class DashboardStateLoading extends DashboardState {
  const DashboardStateLoading({
    @required dashboard,
  }) : super(dashboard: dashboard);
}

class DashboardStateProfileLoaded extends DashboardState {
  const DashboardStateProfileLoaded({
    @required dashboard,
  }) : super(dashboard: dashboard);
}

class DashboardStateFullLoaded extends DashboardState {
  const DashboardStateFullLoaded({
    @required dashboard,
  }) : super(dashboard: dashboard);
}
