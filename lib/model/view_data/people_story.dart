import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'people_story.freezed.dart';

@freezed
abstract class PeopleStory with _$PeopleStory {
  const factory PeopleStory({
    @required String userId,
    @required String fullName,
    @required String profilePic,
    @required String storyPic,
  }) = _PeopleStory;
}
