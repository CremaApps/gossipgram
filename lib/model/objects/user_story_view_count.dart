import 'package:flutter/foundation.dart';
import 'package:GossipGram/model/objects/user/user.dart';

class UserStoryViewCount {
  User user;
  int viewCount;

  UserStoryViewCount({
    @required this.user,
    @required this.viewCount,
  });
}
