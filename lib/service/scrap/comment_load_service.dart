import 'package:flutter/material.dart';
import 'package:GossipGram/bloc/data_load/data_load_bloc.dart';
import 'package:GossipGram/database/repository/comment_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/comment.dart';
import 'package:GossipGram/model/objects/feed.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/scrap/user_load_service.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:instagram_private_api/instagram_private_api.dart';
import 'package:injectable/injectable.dart';
import 'package:GossipGram/model/objects/user/user.dart' as Account;

@singleton
class CommentLoadService extends UserLoadService {
  CommentRepository _commentRepository;
  InstagramPrivateApi _instagramPrivateApi;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  CommentLoadService(this._commentRepository, this._appConfig,
      this._localStorage, UserRepository userRepository)
      : super(userRepository) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<List<Comment>> execute({
    @required Feed feed,
    @required ProcessedUsersWrapper processedUsers,
    bool firstLoad = false,
  }) async {
    int nowDate = firstLoad ? 0 : DateTime.now().millisecondsSinceEpoch;
    var now = _localStorage.read(DataLoadBloc.PROFILING_BASE_TIME);

    var commentResponse = await _instagramPrivateApi.media.comments(
      feed.id,
    );
    var comments = commentResponse.response.comments;
    DataLoadBloc.printProfiling("API comments media step loaded", now);

    while (commentResponse.hasNext()) {
      if (firstLoad)
        await Future.delayed(Duration(
            milliseconds: _appConfig.getInt(AppConfig.SLEEP_FIRST_LOAD)));
      else
        await Future.delayed(
            Duration(milliseconds: _appConfig.getInt(AppConfig.SLEEP_LOAD)));

      commentResponse = await commentResponse.next();
      comments.addAll(commentResponse.response.comments);
      DataLoadBloc.printProfiling("API comments media step loaded", now);
    }
    DataLoadBloc.printProfiling("API comments media all loaded", now);

    List<int> activeCommentIds =
        await _commentRepository.getCommentIds(isValid: true, feedId: feed.id);

    for (int id in activeCommentIds) {
      if (!comments.any((comment) => (comment.pk == id))) {
        Comment dbComment = await _commentRepository.getComment(
          id: id,
        );

        await _commentRepository.upsertComment(
          comment: Comment(
            id: id,
            feedId: dbComment.feedId,
            userId: dbComment.userId,
            isValid: false,
            createdAt: dbComment.createdAt,
            deletedAt: DateTime.now().millisecondsSinceEpoch,
          ),
        );
      }
    }

    List<Comment> toUpsertComments = [];
    List<Account.User> toProcessUsers = [];
    List<Comment> dbComments = await _commentRepository.queryComments(
      isValid: true,
    );

    for (var comment in comments) {
      Comment dbComment = dbComments.firstWhere(
          (element) => element.id == comment.pk,
          orElse: () => null);

      int createdAt = dbComment != null ? dbComment.createdAt : nowDate;

      Account.User newUser = Account.User(
        id: comment.user.pk,
        username: comment.user.username,
        fullName: comment.user.fullName,
        isPrivate: comment.user.isPrivate,
        isVerified: comment.user.isVerified,
        profilePicUrl: comment.user.profilePicUrl,
        anonymousProfilePicture: false,
      );

      if (!toProcessUsers.any((Account.User e) => e.id == newUser.id))
        toProcessUsers.add(newUser);

      toUpsertComments.add(
        Comment(
          id: comment.pk,
          feedId: feed.id,
          userId: comment.user.pk,
          isValid: true,
          createdAt: createdAt,
        ),
      );
    }

    await processUsersInBatchIfNotAlreadyProcessed(
        toProcessUsers, processedUsers);

    await _commentRepository.upsertComments(
      comments: toUpsertComments,
    );

    DataLoadBloc.printProfiling("Comments DB loaded", now);

    return <Comment>[];
  }
}
