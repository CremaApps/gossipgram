import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/service/local_notifications_service.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:GossipGram/service/subscription/subscription_service.dart';
import 'package:injectable/injectable.dart';
import '../../injection.dart';

part 'reminder_periodic_local_notification.dart';
part 'seen_your_profile_scheduled_local_notification.dart';

@lazySingleton
class LocalNotificationRegisterService {
  static const int NOTIFICATION_REMINDER_ID = 1;
  static const int NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_ID = 2;
  static const int NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_ID = 3;
  static const int NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_ID = 4;

  static const String NOTIFICATION_REMINDER_PAYLOAD = "daily_reminder";
  static const String NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_PAYLOAD =
      "first_seen_your_profile_reminder";
  static const String NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_PAYLOAD =
      "second_seen_your_profile_reminder";
  static const String NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_PAYLOAD =
      "third_seen_your_profile_reminder";

  static const String NOTIFICATION_REMINDER_PERIODICITY =
      "notification_daily_reminder_periodicity";
  static const String NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_MINUTES =
      "notification_seen_your_profile_first_minutes";
  static const String NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_MINUTES =
      "notification_seen_your_profile_second_minutes";
  static const String NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_MINUTES =
      "notification_seen_your_profile_third_minutes";

  static const String DAILY_REMINDER_PERIODICITY = "daily_reminder_periodicity";

  static const String FIRST_NOTIFICATION_STARTUP_TIME =
      'first_notification_startup_time';

  Map<int, Function> _allValidNotifications = {
    NOTIFICATION_REMINDER_ID: _reminderPeriodicLocalNotification,
    NOTIFICATION_SEEN_YOUR_PROFILE_FIRST_ID:
        _seenYourProfileScheduledLocalNotificationFirst,
    NOTIFICATION_SEEN_YOUR_PROFILE_SECOND_ID:
        _seenYourProfileScheduledLocalNotificationSecond,
    NOTIFICATION_SEEN_YOUR_PROFILE_THIRD_ID:
        _seenYourProfileScheduledLocalNotificationThird,
  };

  LocalNotificationRegisterService();

  // For tasks with the app closed
  Future<void> registerNotification({
    @required BuildContext context,
    @required int notificationId,
  }) async {
    if (_allValidNotifications.containsKey(notificationId)) {
      await _allValidNotifications[notificationId](context);
    }
  }

  // For tasks with the app closed
  Future<void> registerNotifications({
    @required BuildContext context,
    @required List<int> notificationIds,
  }) async {
    for (int notificationId in notificationIds) {
      await registerNotification(
          context: context, notificationId: notificationId);
    }
  }
}
