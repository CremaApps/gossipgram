import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class NavigatorService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }

  Future<dynamic> logoutUser({String error}) async {
    bool isNewRouteSameAsCurrent = false;

    navigatorKey.currentState.popUntil((route) {
      if(route.settings.name == "/login") {
        isNewRouteSameAsCurrent = true;
      }
      return true;
    });

    if (!isNewRouteSameAsCurrent) {
      if (navigatorKey.currentState.canPop()) {
        navigatorKey.currentState.pop();
      }

      await navigatorKey.currentState.pushReplacementNamed('/login');
    }
  }

  void pop() {
    navigatorKey.currentState.pop();
  }
}