import 'dart:async';

import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/like_sorted_feed_load/like_sorted_feed_load_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/media_popup.dart';
import 'package:GossipGram/widgets/number_people_converter.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:lottie/lottie.dart';

class InsightsMostLeastLikedPage extends StatefulWidget {
  @override
  _InsightsMostLeastLikedPageState createState() =>
      _InsightsMostLeastLikedPageState();
}

class _InsightsMostLeastLikedPageState extends State<InsightsMostLeastLikedPage>
    with TickerProviderStateMixin {
  TabController _insightsILikeTabController;
  LikeSortedFeedLoadBloc _likeSortedFeedLoadBloc;
  final AppConfig _appConfig = GetIt.instance.get<AppConfig>();
  var _totalResults = StreamController<int>.broadcast();
  var totalResults;
  Function(int) changeTotalResults;

  @override
  void initState() {
    super.initState();
    _insightsILikeTabController = TabController(length: 2, vsync: this);
    _likeSortedFeedLoadBloc = GetIt.instance.get<LikeSortedFeedLoadBloc>();
    _likeSortedFeedLoadBloc.add(LikeSortedFeedLoadEventStart(
      maxItems: _appConfig.getInt(AppConfig.MOST_LEAST_LIKED_POSTS_MAX_ITEMS),
    ));
    totalResults = _totalResults.stream;
    changeTotalResults = _totalResults.sink.add;
  }

  @override
  void dispose() {
    super.dispose();
    _insightsILikeTabController.dispose();
    _totalResults.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: false,
        title: StreamBuilder<int>(
          initialData: 0,
          stream: totalResults,
          builder: (context, snapshot) {
            return Text(
                "${S.of(context).mostAndLeastLikedPosts} (${snapshot.data})",
                style: appTextTheme.subtitle2);
          },
        ),
        bottom: TabBar(
          labelStyle: appTextTheme.subtitle2.copyWith(fontSize: 15),
          unselectedLabelStyle: appTextTheme.subtitle1.copyWith(fontSize: 15),
          labelColor: appColorScheme.secondary,
          unselectedLabelColor: appColorScheme.onPrimary,
          controller: _insightsILikeTabController,
          tabs: <Widget>[
            Tab(
              child: Text(
                S.of(context).Most,
              ),
            ),
            Tab(
              child: Text(
                S.of(context).Least,
              ),
            ),
          ],
        ),
      ),
      body: BlocBuilder(
        cubit: _likeSortedFeedLoadBloc,
        builder: (context, state) {
          if (state is LikeSortedFeedLoadInitial) {
            return Center(
              child: Container(
                child: Lottie.asset(
                  'assets/animations/loader.json',
                  width: 100,
                  height: 100,
                ),
              ),
            );
          } else if (state is LikeSortedFeedLoadData) {
            if (state.leastLikedFeed.isEmpty && state.mostLikedFeed.isEmpty) {
              return Center(
                child: Container(
                  margin: EdgeInsetsDirectional.only(
                      top: MediaQuery.of(context).size.height / 3.2),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(S.of(context).nobodyFound,
                          style: appTextTheme.headline6),
                      Container(
                        margin: EdgeInsetsDirectional.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        child: Text(
                          S.of(context).noPostsYet,
                          style: appTextTheme.bodyText1,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
            changeTotalResults(state.mostLikedFeed.length);
            return Container(
              child: TabBarView(
                controller: _insightsILikeTabController,
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsetsDirectional.only(
                            start: 8, end: 8, top: 8),
                        child: GridView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: state.mostLikedFeed.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            mainAxisSpacing: 3,
                            crossAxisSpacing: 3,
                            childAspectRatio: 1,
                          ),
                          itemBuilder: (contxt, indx) {
                            FeedThumbnail feedThumbnail =
                                state.mostLikedFeed[indx];

                            return cellLayout(feedThumbnail, indx);
                          },
                        ),
                      ),
                    ],
                  ),
                  ListView(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsetsDirectional.only(
                            start: 8, end: 8, top: 8),
                        child: GridView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: state.leastLikedFeed.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            mainAxisSpacing: 3,
                            crossAxisSpacing: 3,
                            childAspectRatio: 1,
                          ),
                          itemBuilder: (contxt, indx) {
                            FeedThumbnail feedThumbnail =
                                state.leastLikedFeed[indx];

                            return cellLayout(feedThumbnail, indx);
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          } else {
            return Text(S.of(context).somethingWentWrong);
          }
        },
      ),
    );
  }

  Widget cellLayout(FeedThumbnail feedThumbnail, int indx) {
    return InkWell(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return MediaPopup(
              mediaId: feedThumbnail.id,
              mediaType: feedThumbnail.mediaType,
            );
          },
        );
      },
      child: LayoutBuilder(
        builder: (_, BoxConstraints constraints) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(feedThumbnail.image
                    .getSizedCandidate(
                        width: constraints.maxWidth,
                        height: constraints.maxHeight)
                    .url),
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: [
                Positioned(
                  bottom: 0,
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    color: Color(0x10000000),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  child: Container(
                    width: 16,
                    height: 16,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(4))),
                    child: Text("${indx + 1}"),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    alignment: WrapAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(2),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 2),
                              child: Text(
                                  IntNumberSymbolConverter(
                                      feedThumbnail.likeCount),
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Poppins",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 13.0)),
                            ),
                            Icon(
                              Icons.favorite,
                              color: Colors.white,
                              size: 13,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
