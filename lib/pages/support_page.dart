import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../generated/l10n.dart';
import '../themes/themes.dart';

class SupportPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        elevation: 0.00,
        title: Text(
          S.of(context).support,
          style: appTextTheme.subtitle2,
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height / 2.3,
        child: Center(
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            height: MediaQuery.of(context).size.height / 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: double.infinity,
                  margin: EdgeInsetsDirectional.only(top: 20),
                  child: Text(S.of(context).howCanWeHelpYou,
                      textAlign: TextAlign.center,
                      style: appTextTheme.subtitle1
                          .copyWith(fontWeight: FontWeight.bold)),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsetsDirectional.only(top: 20, bottom: 20),
                  child: Text(S.of(context).ifYouNeedToContactWithUs,
                      textAlign: TextAlign.center,
                      style: appTextTheme.bodyText1),
                ),
                OutlineButton(
                  onPressed: () {
                    launch(
                        'mailto:${S.of(context).supportMail}?subject=Contact%20Support%20from%20Gossipgram%20App');
                  },
                  child: Text(
                    S.of(context).supportMail,
                    style: appTextTheme.button,
                  ),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsetsDirectional.only(top: 8),
                  child: Text(S.of(context).thisActionOpenMailingApp,
                      textAlign: TextAlign.center, style: appTextTheme.caption),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
