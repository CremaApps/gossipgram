import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/user/followed_user.dart';
import 'package:injectable/injectable.dart';

@singleton
class FollowedUserRepository {
  final DbProvider _dbProvider;

  FollowedUserRepository(this._dbProvider);

  Future<bool> existsFollowedUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('FollowedUser', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty;
  }

  Future<FollowedUser> getFollowedUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('FollowedUser', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? FollowedUser.fromMap(res.first) : null;
  }

  Future<List<FollowedUser>> queryFollowedUsers(
      {bool isValid = true,
      int stateChangedAtGreaterThan,
      int stateChangedAtLesserThan,
      bool notFollower = false}) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
            'FROM FollowedUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    if (notFollower) {
      query += 'AND id NOT IN (SELECT id FROM FollowerUser WHERE isValid = 1)';
    }

    var dbFUsers = await db.rawQuery(query);
    List<FollowedUser> fUsers = dbFUsers.isNotEmpty
        ? dbFUsers.map((a) => FollowedUser.fromMap(a)).toList()
        : [];
    return fUsers;
  }

  Future<int> queryCountFollowedUsers({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM FollowedUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var dbFUsersCount = await db.rawQuery(query);

    return dbFUsersCount.first.values.first;
  }

  Future<int> upsertFollowedUser({@required FollowedUser fUser}) async {
    final db = await _dbProvider.database;
    var dbFUser = await db.query(
      'FollowedUser',
      where: 'id = ?',
      whereArgs: [fUser.id],
    );
    if (dbFUser.isNotEmpty) {
      return db.update(
        'FollowedUser',
        fUser.toMap(),
        where: 'id = ?',
        whereArgs: [fUser.id],
      );
    } else {
      return await db.insert('FollowedUser', fUser.toMap());
    }
  }

  Future<int> deleteFollowedUser({@required FollowedUser fUser}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'FollowedUser',
      where: 'id = ?',
      whereArgs: [fUser.id],
    );
  }

  Future<List<int>> getFollowedUserIds({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT id '
            'FROM FollowedUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> followerUserIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int id = a['id'];
            return id;
          }).toList()
        : [];
    return followerUserIds;
  }
}
