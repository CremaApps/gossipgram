import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter_svg/svg.dart';

String _home;
Function _customFunction;
AsyncLoading _promiseCustomFunction;
String _imagePath;
int _duration;
SplashType _runfor;

typedef Future<dynamic> AsyncLoading();

enum SplashType { StaticDuration, BackgroundProcess }

Map<dynamic, String> _outputAndHome = {};

class SplashPage extends StatefulWidget {
  SplashPage(
      {@required String imagePath,
      @required String home,
      Function customFunction,
      AsyncLoading futureCustomFunction,
      int duration,
      SplashType type,
      Map<dynamic, String> outputAndHome}) {
    assert(duration != null);
    assert(home != null);
    assert(imagePath != null);

    _home = home;
    _duration = duration;
    _customFunction = customFunction;
    _imagePath = imagePath;
    _runfor = type;
    _promiseCustomFunction = futureCustomFunction;
    _outputAndHome = outputAndHome;
  }

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;

  _SplashPageState();

  @override
  void initState() {
    super.initState();
    if (_duration < 1000) _duration = 2000;
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800));
    _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.easeInCirc));
    _animationController.forward();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _runfor == SplashType.BackgroundProcess
        ? Future.delayed(Duration.zero).then((value) {
            if (_promiseCustomFunction != null) {
              var res = _promiseCustomFunction();
              Future.delayed(Duration(milliseconds: _duration)).then((value) {
                res.then((key) {
                  Navigator.of(context)
                      .pushReplacementNamed(_outputAndHome[key]);
                }).catchError((error) {
                  Navigator.of(context).pushReplacementNamed(_home);
                });
              });
            } else {
              var res = _customFunction();
              Future.delayed(Duration(milliseconds: _duration)).then((value) {
                Navigator.of(context).pushReplacementNamed(_outputAndHome[res]);
              });
            }
          })
        : Future.delayed(Duration(milliseconds: _duration)).then((value) {
            Navigator.of(context).pushReplacementNamed(_home);
          });
  }

  @override
  void dispose() {
    super.dispose();
    _animationController?.reset();
    _animationController?.dispose();
  }

  navigator(home) {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (BuildContext context) => home));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FadeTransition(
        opacity: _animation,
        child: Center(
          child: SizedBox(
            height: 250.0,
            child: Image.asset(
              _imagePath,
            ),
          ),
        ),
      ),
    );
  }
}
