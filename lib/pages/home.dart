import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/pages/dashboard_page/dashboard_page.dart';
import 'package:GossipGram/pages/my_stories_page/my_stories_page.dart';
import 'package:GossipGram/pages/profile_stories_page/people_stories_page.dart';
import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: appColorScheme.primary,
        selectedItemColor: appColorScheme.secondary,
        unselectedItemColor: appColorScheme.onPrimary,
        selectedLabelStyle:
            TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.dashboard),
            label: S.of(context).dashboard,
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/images/icon-mystories.png'),
            ),
            label: S.of(context).yourStories,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: S.of(context).peopleStories,
          ),
        ],
        onTap: onTap,
      ),
    );
  }

  void onTap(int value) {
    if (mounted) {
      setState(() {
        _currentIndex = value;
      });
    }
  }

  Widget buildBody() {
    return IndexedStack(
      index: _currentIndex,
      children: <Widget>[DashboardPage(), MyStoriesPage(), PeopleStoriesPage()],
    );
  }
}
