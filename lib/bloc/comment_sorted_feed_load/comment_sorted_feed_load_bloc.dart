import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:GossipGram/model/objects/feed_thumbnail.dart';
import 'package:GossipGram/service/api/get_feed_images_service.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

part 'comment_sorted_feed_load_event.dart';
part 'comment_sorted_feed_load_state.dart';

@injectable
class CommentSortedFeedLoadBloc
    extends Bloc<CommentSortedFeedLoadEvent, CommentSortedFeedLoadState> {
  InstagramPrivateApi _instagramPrivateApi;
  GetFeedImagesService _getFeedImagesService;

  CommentSortedFeedLoadBloc(
    this._getFeedImagesService,
  ) : super(CommentSortedFeedLoadInitial()) {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  @override
  Stream<CommentSortedFeedLoadState> mapEventToState(
    CommentSortedFeedLoadEvent event,
  ) async* {
    if (event is CommentSortedFeedLoadEventStart) {
      yield CommentSortedFeedLoadInitial();

      var feedImages = await _getFeedImagesService.execute(
        userId: _instagramPrivateApi.myUserId,
      );

      List<FeedThumbnail> mostLikedFeed = List.from(
          feedImages..sort((a, b) => a.likeCount.compareTo(b.likeCount)));
      List<FeedThumbnail> leastLikedFeed = mostLikedFeed.reversed.toList();

      yield CommentSortedFeedLoaded(
        mostLikedFeed: mostLikedFeed,
        leastLikedFeed: leastLikedFeed,
      );
    }
  }
}
