import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:shimmer/shimmer.dart';

import '../bloc/dashboard/dashboard_bloc.dart';
import '../bloc/dashboard/dashboard_state.dart';
import '../bloc/data_load/data_load_bloc.dart';
import '../model/view_data/dashboard.dart';
import '../model/view_data/dashboard_box.dart';
import '../service/storage/app_config.dart';
import '../service/subscription/subscription_service.dart';
import '../themes/app_theme_default.dart';
import '../themes/themes.dart';
import 'premium_popup.dart';

class DashboardTabs extends StatefulWidget {
  final DashboardBloc dashboardBloc;
  final DataLoadBloc dataLoadBloc;

  DashboardTabs({@required this.dashboardBloc, @required this.dataLoadBloc});

  @override
  _DashboardTabsState createState() => _DashboardTabsState();
}

class _DashboardTabsState extends State<DashboardTabs> {
  final AppConfig _appConfig = GetIt.instance.get<AppConfig>();
  final SubscriptionService _subscriptionService =
      GetIt.I.get<SubscriptionService>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DashboardBloc, DashboardState>(
      cubit: widget.dashboardBloc,
      builder: (context, state) => Container(
        child: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            _buildDashBoardTab(
                AppConfig.ANALYSIS_BOXES, state.dashboard, state),
            _buildDashBoardTab(AppConfig.INSIGHT_BOXES, state.dashboard, state),
          ],
        ),
      ),
    );
  }

  Container _buildDashBoardTab(
      String tabName, Dashboard dashboard, DashboardState state) {
    List<String> analysisBoxes = _appConfig.getListString(tabName);

    return Container(
      margin: EdgeInsets.all(16),
      child: GridView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: analysisBoxes.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 8,
          mainAxisSpacing: 8,
          childAspectRatio: 1.5,
        ),
        itemBuilder: (contxt, indx) => _infoBox(
          DashboardBox.fromConstants(
            context: context,
            boxName: analysisBoxes[indx],
            dashboard: dashboard,
          ),
        ),
      ),
    );
  }

  InkWell _infoBox(DashboardBox dashboardBox) {
    return InkWell(
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(4),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(16.0),
              ),
              color: appColorScheme.primary,
              boxShadow: [
                BoxShadow(
                  color: appColorScheme.onSurface,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 0.1,
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                dashboardBox.icon ?? SizedBox(),
                SizedBox(
                  height: 5,
                ),
                _loadingOrText(dashboardBox.value, dashboardBox),
                SizedBox(
                  height: 5,
                ),
                Flexible(
                  child: AutoSizeText(dashboardBox.title,
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.clip,
                      maxLines: 2,
                      style: appTextTheme.bodyText1.copyWith(fontSize: 13)),
                ),
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: Opacity(
              opacity: dashboardBox.isPaidFunctionality &&
                      !_subscriptionService.isPayed
                  ? 1.0
                  : 0.0,
              child: Container(
                  padding: EdgeInsets.all(2.5),
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(22.0),
                    ),
                    gradient: LinearGradient(
                      colors: [Color(0xFFFDC52A), Color(0xFFFF9714)],
                    ),
                  ),
                  child: Icon(
                    Icons.star,
                    size: 15,
                    color: Colors.white,
                  )),
            ),
          ),
        ],
      ),
      onTap: () {
        if (dashboardBox.isPaidFunctionality && !_subscriptionService.isPayed) {
          Navigator.push(
              context,
              MaterialPageRoute(
                fullscreenDialog: true,
                builder: (context) {
                  return PremiumPopup(callback: () {
                    setState(() {});
                  });
                },
              ));
        } else {
          if (dashboardBox.value != null) {
            dashboardBox.redirectFunction();
          }
        }
      },
    );
  }

  Widget _loadingOrText(int value, DashboardBox dashboardBox) {
    if (value == null) {
      return Shimmer.fromColors(
        baseColor: AppTheme.complementaries['50'],
        highlightColor: Colors.white,
        child: FractionallySizedBox(
          child: Container(
            width: 38.0,
            height: 20.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
        ),
      );
    }

    String valueToPrint = '-';

    if (dashboardBox.isPaidFunctionality) {
      if (!_subscriptionService.isPayed) {
        valueToPrint = '';
      } else {
        if (value > 0) {
          valueToPrint = value.toString();
        }
      }
    } else {
      if (value > 0) {
        valueToPrint = value.toString();
      }
    }

    return Text(
      valueToPrint,
      style: appTextTheme.subtitle2.copyWith(fontSize: 16),
    );
  }
}
