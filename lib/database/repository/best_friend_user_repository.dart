import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/user/best_friend_user.dart';
import 'package:injectable/injectable.dart';

@singleton
class BestFriendUserRepository {
  final DbProvider _dbProvider;

  BestFriendUserRepository(this._dbProvider);

  Future<BestFriendUser> getBestFriendUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res =
        await db.query('BestFriendUser', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? BestFriendUser.fromMap(res.first) : null;
  }

  // Future<List<BestFriendUser>> queryBestFriendUsers(
  //     {bool isValid = true,
  //     int stateChangedAtGreaterThan,
  //     int stateChangedAtLesserThan,
  //     bool notFollowed}) async {
  //   final db = await _dbProvider.database;
  //   var query = 'SELECT * '
  //           'FROM BestFriendUser '
  //           'WHERE isValid = ' +
  //       (isValid ? '1' : '0') +
  //       ' ';

  //   if (stateChangedAtGreaterThan != null) {
  //     query +=
  //         'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
  //   }

  //   if (stateChangedAtLesserThan != null) {
  //     query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
  //   }
  //   if (notFollowed) {
  //     query += 'AND id NOT IN (SELECT id FROM FollowedUser WHERE isValid = 1)';
  //   }

  //   var dbBFUsers = await db.rawQuery(query);
  //   List<BestFriendUser> bFUsers = dbBFUsers.isNotEmpty
  //       ? dbBFUsers.map((a) => BestFriendUser.fromMap(a)).toList()
  //       : [];
  //   return bFUsers;
  // }

  Future<int> queryCountBestFriendUsers({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM BestFriendUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var dbBFUsersCount = await db.rawQuery(query);

    return dbBFUsersCount.first.values.first;
  }

  Future<int> upsertBestFriendUser({@required BestFriendUser bFUser}) async {
    final db = await _dbProvider.database;
    var dbFUser = await db.query(
      'BestFriendUser',
      where: 'id = ?',
      whereArgs: [bFUser.id],
    );
    if (dbFUser.isNotEmpty) {
      return db.update(
        'BestFriendUser',
        bFUser.toMap(),
        where: 'id = ?',
        whereArgs: [bFUser.id],
      );
    } else {
      return await db.insert('BestFriendUser', bFUser.toMap());
    }
  }

  Future<int> deleteBestFriendUser({@required BestFriendUser bFUser}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'BestFriendUser',
      where: 'id = ?',
      whereArgs: [bFUser.id],
    );
  }

  Future<List<int>> getBestFriendUserIds(
      {bool isValid = true,
      int stateChangedAtGreaterThan,
      int stateChangedAtLesserThan,
      bool notFollowed}) async {
    final db = await _dbProvider.database;
    var query = 'SELECT id '
            'FROM BestFriendUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }
    if (notFollowed != null && notFollowed) {
      query += 'AND id NOT IN (SELECT id FROM FollowedUser WHERE isValid = 1)';
    }

    var queryResponse = await db.rawQuery(query);
    List<int> bestFriendUserIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int id = a['id'];
            return id;
          }).toList()
        : [];
    return bestFriendUserIds;
  }
}
