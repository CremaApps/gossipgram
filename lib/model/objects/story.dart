import 'package:flutter/material.dart';

class Story {

  final int id;
  final String thumbnailUrl;
  final String mediaUrl;
  final int mediaType;
  final int viewCount;
  final int originalCreatedAtDate;
  int createdAt;
  final bool isValid;

  Story({
    @required this.id,
    @required this.thumbnailUrl,
    @required this.mediaUrl,
    @required this.mediaType,
    @required this.viewCount,
    @required this.originalCreatedAtDate,
    this.createdAt,
    this.isValid = true,
  }) {
    if(this.createdAt == null) {
      this.createdAt = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Map<String, dynamic> toMap() => {
    'id': id,
    'thumbnailUrl': thumbnailUrl,
    'mediaUrl': mediaUrl,
    'mediaType': mediaType,
    'viewCount': viewCount,
    'originalCreatedAtDate': originalCreatedAtDate,
    'createdAt': createdAt,
    'isValid': isValid ? 1 : 0,
  };

  factory Story.fromMap(Map<String, dynamic> map) => Story(
    id: map['id'],
    thumbnailUrl: map['thumbnailUrl'],
    mediaUrl: map['mediaUrl'],
    mediaType: map['mediaType'],
    viewCount: map['viewCount'],
    originalCreatedAtDate: map['originalCreatedAtDate'],
    createdAt: map['createdAt'],
    isValid: map['isValid'] == 1,
  );

}