import 'package:GossipGram/utils/date_utils.dart';
import 'package:flutter/material.dart';
import 'package:GossipGram/model/objects/media_data.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/instagram_private_api.dart';

@singleton
class GetMediaInfoService {
  InstagramPrivateApi _instagramPrivateApi;

  GetMediaInfoService() {
    _instagramPrivateApi = InstagramPrivateApi.instance();
  }

  Future<MediaData> execute({
    @required int mediaId,
  }) async {
    var mediaInfoResponse =
        await _instagramPrivateApi.media.info(mediaId.toString());
    var mediaItem = mediaInfoResponse.response.items.first;
    List<String> urlThumbnail = [];

    if (mediaItem.carouselMedia != null) {
      for (var carouselItem in mediaItem.carouselMedia) {
        urlThumbnail.add(carouselItem.imageVersions2.candidates.first.url);
      }
    } else if (mediaItem.videoVersions != null)
      urlThumbnail.add(mediaItem.videoVersions.first.url);
    else if (mediaItem.imageVersions != null)
      urlThumbnail.add(mediaItem.imageVersions.candidates.first.url);

    mediaItem.user.fullName;

    MediaData media = MediaData(
        urlThumbnail: urlThumbnail,
        comments: mediaItem.commentCount,
        likes: mediaItem.likeCount,
        contents: mediaItem.caption?.text ?? "",
        username: mediaItem.user.username,
        date: DateUtils.getFormattedDateDMYwithLetters(
            DateTime.fromMillisecondsSinceEpoch(mediaItem.takenAt * 1000)
                .toIso8601String()));
    return media;
  }
}
