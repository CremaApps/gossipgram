import 'package:flutter/foundation.dart';
import 'package:GossipGram/model/objects/thumbnail.dart';

class FeedThumbnail extends Thumbnail {
  static const int CAROUSEL = 2;

  int likeCount;
  int commentCount;

  FeedThumbnail({
    @required id,
    @required image,
    @required mediaType,
    @required this.likeCount,
    @required this.commentCount,
  }) : super(
          id: id,
          image: image,
          mediaType: mediaType,
        );
}
