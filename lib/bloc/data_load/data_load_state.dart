import 'package:equatable/equatable.dart';

abstract class DataLoadState extends Equatable {
  const DataLoadState();

  @override
  List<Object> get props => [];
}

class DataLoadStateStarted extends DataLoadState {}

class DataLoadStateLoading extends DataLoadState {
  final bool showLoading;

  DataLoadStateLoading(this.showLoading);
}

class DataLoadStateFinished extends DataLoadState {}
