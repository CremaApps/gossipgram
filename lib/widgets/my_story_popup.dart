import 'dart:io';

import 'package:GossipGram/themes/themes.dart';
import 'package:GossipGram/widgets/number_people_converter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/bloc/my_story_popup/my_story_popup_bloc.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/view_data/story_popup.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:story_view/controller/story_controller.dart';
import 'package:story_view/story_view.dart';

class MyStoryPopupComponent extends StatefulWidget {
  final int storyId;
  final int storyMediaType;
  final int storyViewCount;
  final String path;

  const MyStoryPopupComponent({
    @required this.storyId,
    @required this.storyMediaType,
    @required this.storyViewCount,
    @required this.path,
  });

  @override
  _MyStoryPopupComponentState createState() => _MyStoryPopupComponentState();
}

class _MyStoryPopupComponentState extends State<MyStoryPopupComponent>
    with SingleTickerProviderStateMixin {
  MyStoryPopupBloc _myStoryPopupBloc;
  StoryController storyController = StoryController();

  @override
  void initState() {
    super.initState();
    _myStoryPopupBloc = GetIt.instance.get<MyStoryPopupBloc>();
    _myStoryPopupBloc.add(MyPopupStoryStartLoadEvent(storyId: widget.storyId));
  }

  @override
  void dispose() {
    storyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: _myStoryPopupBloc,
      // ignore: missing_return
      builder: (context, state) {
        if (state is MyStoryPopupInitial || state is MyStoryPopupLoading) {
          return CenteredLoader();
        } else if (state is MyStoryPopupLoaded) {
          List<StoryItem> storiesList = [];

          switch (widget.storyMediaType) {
            case StoryPopup.IMAGE_TYPE:
              storiesList.add(
                StoryItem.pageProviderImage(
                  FileImage(
                    File("${widget.path}/images/${widget.storyId}.jpg"),
                  ),
                ),
              );
              break;
            case StoryPopup.VIDEO_TYPE:
            default:
              storiesList.add(
                StoryItem.pageVideo(
                  "${widget.path}/videos/${widget.storyId}.mp4",
                  controller: storyController,
                ),
              );
          }
          ;
          return Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 1.08,
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: StoryView(
                      storyItems: storiesList,
                      onComplete: () {
                        Navigator.of(context).pop();
                      },
                      progressPosition: ProgressPosition.top,
                      repeat: false,
                      controller: storyController,
                    ),
                  ),
                ],
              ),
              /* Positioned(
                left: 30.0,
                top: MediaQuery.of(context).size.height / 25,
                child: _buildProfileView(
                  profilePic: profilePic,
                  fullname: fullname,
                ),
              ),*/
              Positioned(
                right: 30.0,
                top: MediaQuery.of(context).size.height / 25,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Align(
                    alignment: Alignment.topRight,
                    child: CircleAvatar(
                      radius: 14.0,
                      backgroundColor: Colors.transparent,
                      child: Icon(
                        Icons.close,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: _buildBottomFollowers(
                  profilePics: state.seenBy,
                  seenBy: widget.storyViewCount,
                ),
              ),
            ],
          );
        } else {
          return Center(
            child: Text(
              S.of(context).somethingWentWrong,
              style: TextStyle(
                color: AppTheme.neutrals['900'],
                fontWeight: FontWeight.w900,
                fontFamily: "Poppins",
                fontStyle: FontStyle.normal,
                fontSize: 12.0,
              ),
            ),
          );
        }
      },
    );
  }
}

class _buildBottomFollowers extends StatelessWidget {
  final List<String> profilePics;
  final int seenBy;
  _buildBottomFollowers({this.profilePics, this.seenBy});

  @override
  Widget build(BuildContext context) {
    List<Positioned> avatars = [];

    double i = 0;
    int maxLength = profilePics.length >= 3 ? 3 : profilePics.length;
    for (String thumbnailUrl in profilePics.sublist(0, maxLength)) {
      avatars.add(
        Positioned(
          bottom: 16,
          left: 30 + i++ * 20,
          child: CircleAvatar(
            radius: 16,
            backgroundImage: NetworkImage(thumbnailUrl),
          ),
        ),
      );
    }

    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 8),
          height: 60,
          width: maxLength == 1 ? 65 : (maxLength == 2 ? 90 : 110),
          child: Stack(
            children: avatars,
          ),
        ),
        Material(
          color: Colors.transparent,
          child: Text(
              "${S.of(context).seenby} ${IntNumberSymbolConverter(seenBy)} ${S.of(context).people}",
              style: appTextTheme.caption.copyWith(color: Colors.white)),
        ),
      ],
    );
  }
}
