import 'dart:async';

import 'package:GossipGram/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/bloc/like_sorted_users_load/like_sorted_users_load_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GossipGram/generated/l10n.dart';
import 'package:GossipGram/model/objects/user_like_count.dart';
import 'package:GossipGram/pages/user_profile/user_profile_page.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/number_people_converter.dart';
import 'package:GossipGram/widgets/profile_picture.dart';
import 'package:lottie/lottie.dart';

class InsightsMostLeastLikesByUserPage extends StatefulWidget {
  @override
  _InsightsMostLeastLikesByUserPageState createState() =>
      _InsightsMostLeastLikesByUserPageState();
}

class _InsightsMostLeastLikesByUserPageState
    extends State<InsightsMostLeastLikesByUserPage>
    with TickerProviderStateMixin {
  TabController _insightsTabController;
  LikeSortedUsersLoadBloc _likeSortedUsersLoadBloc;

  var _totalResults = StreamController<int>.broadcast();
  var totalResults;
  Function(int) changeTotalResults;
  @override
  void initState() {
    super.initState();
    _insightsTabController = TabController(length: 2, vsync: this);
    _likeSortedUsersLoadBloc = GetIt.instance.get<LikeSortedUsersLoadBloc>();
    _likeSortedUsersLoadBloc.add(LikeSortedUsersLoadEventStart());
    totalResults = _totalResults.stream;
    changeTotalResults = _totalResults.sink.add;
  }

  @override
  void dispose() {
    super.dispose();
    _insightsTabController.dispose();
    _totalResults.close();
  }

  Widget _buildRow(int index, List<UserLikeCount> userList, bool mostLeast) {
    int likesNumber = userList[index].likeCount;

    return ListTile(
      title: Text(
          (userList[index].user.fullName != null &&
                  userList[index].user.fullName != "")
              ? userList[index].user.fullName
              : userList[index].user.username,
          overflow: TextOverflow.ellipsis,
          style: appTextTheme.subtitle1),
      subtitle: Text(
        userList[index].user.username,
        style: appTextTheme.caption,
      ),
      leading: Container(
        width: MediaQuery.of(context).size.width / 5,
        child: Row(
          children: [
            Text(IntNumberSymbolConverter(index + 1),
                style: appTextTheme.subtitle1),
            Container(
              margin: EdgeInsets.only(left: 10),
              width: MediaQuery.of(context).size.width / 8,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: ProfilePicture(
                  profilePicUrl: userList[index].user.profilePicUrl,
                ),
              ),
            ),
          ],
        ),
      ),
      trailing: SizedBox(
        width: 100,
        child: Wrap(
          alignment: WrapAlignment.end,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 0.5,
                      color: getThemeModeAsInt() == 0
                          ? appColorScheme.onSurface
                          : appColorScheme.onPrimary),
                  borderRadius: BorderRadius.circular(20)),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 4),
                    child: Icon(
                      Icons.favorite,
                      color: Colors.pink,
                      size: 16,
                    ),
                  ),
                  Text(IntNumberSymbolConverter(likesNumber),
                      style: appTextTheme.subtitle1),
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => UserProfile(
              userId: userList[index].user.id,
              username: userList[index].user.username,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: StreamBuilder<int>(
          initialData: 0,
          stream: totalResults,
          builder: (context, snapshot) {
            return Text(
                "${S.of(context).likedYouTheMostAndLeast} (${snapshot.data})",
                style: appTextTheme.subtitle2);
          },
        ),
        bottom: TabBar(
          labelStyle: appTextTheme.subtitle2.copyWith(fontSize: 15),
          unselectedLabelStyle: appTextTheme.subtitle1.copyWith(fontSize: 15),
          labelColor: appColorScheme.secondary,
          unselectedLabelColor: appColorScheme.onPrimary,
          controller: _insightsTabController,
          tabs: <Widget>[
            Tab(
              child: Text(
                S.of(context).Most,
              ),
            ),
            Tab(
              child: Text(
                S.of(context).Least,
              ),
            ),
          ],
        ),
      ),
      body: BlocBuilder(
          cubit: _likeSortedUsersLoadBloc,
          builder: (context, state) {
            if (state is LikeSortedUsersLoadInitial) {
              return Center(
                child: Container(
                  child: Lottie.asset(
                    'assets/animations/loader.json',
                    width: 100,
                    height: 100,
                  ),
                ),
              );
            } else if (state is LikeSortedUsersLoadData) {
              if (state.usersWhoLeastLikedYou.isEmpty &&
                  state.usersWhoMostLikedYou.isEmpty) {
                return Center(
                  child: Container(
                    margin: EdgeInsetsDirectional.only(
                        top: MediaQuery.of(context).size.height / 3.2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(S.of(context).nobodyFound,
                            style: appTextTheme.headline6),
                        Container(
                          margin: EdgeInsetsDirectional.only(top: 10),
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text(
                            S.of(context).noPostsYet,
                            style: appTextTheme.bodyText1,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }
              return Column(
                children: <Widget>[
                  Expanded(
                    child: TabBarView(
                      controller: _insightsTabController,
                      children: <Widget>[
                        Container(
                          child: Builder(
                            builder: (context) {
                              if (state.usersWhoMostLikedYou.isNotEmpty) {
                                changeTotalResults(
                                    state.usersWhoMostLikedYou.length);

                                return ListView(
                                  physics: ClampingScrollPhysics(),
                                  children: state.usersWhoMostLikedYou
                                      .asMap()
                                      .entries
                                      .map(
                                        (MapEntry map) => _buildRow(map.key,
                                            state.usersWhoMostLikedYou, true),
                                      )
                                      .toList(),
                                );
                              } else {
                                return Center(
                                  child: Container(
                                    margin: EdgeInsetsDirectional.only(
                                        top:
                                            MediaQuery.of(context).size.height /
                                                3.2),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text(S.of(context).nobodyFound,
                                            style: appTextTheme.headline6),
                                        Container(
                                          margin: EdgeInsetsDirectional.only(
                                              top: 10),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          child: Text(
                                            S.of(context).noOneYet,
                                            style: appTextTheme.bodyText1,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                        Container(
                          child: Builder(
                            builder: (context) {
                              if (state.usersWhoLeastLikedYou.isNotEmpty) {
                                changeTotalResults(
                                    state.usersWhoMostLikedYou.length);
                                return ListView(
                                  physics: ClampingScrollPhysics(),
                                  children: state.usersWhoLeastLikedYou
                                      .asMap()
                                      .entries
                                      .map(
                                        (MapEntry map) => _buildRow(map.key,
                                            state.usersWhoLeastLikedYou, false),
                                      )
                                      .toList(),
                                );
                              } else {
                                return Center(
                                  child: Container(
                                    margin: EdgeInsetsDirectional.only(
                                        top:
                                            MediaQuery.of(context).size.height /
                                                3.2),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text(S.of(context).nobodyFound,
                                            style: appTextTheme.headline6),
                                        Container(
                                          margin: EdgeInsetsDirectional.only(
                                              top: 10),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          child: Text(
                                            S.of(context).noOneYet,
                                            style: appTextTheme.bodyText1,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              );
            } else {
              return Text(S.of(context).somethingWentWrong);
            }
          }),
    );
  }
}
