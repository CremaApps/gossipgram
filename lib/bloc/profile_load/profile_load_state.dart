part of 'profile_load_bloc.dart';

abstract class ProfileLoadState extends Equatable {
  const ProfileLoadState();

  @override
  List<Object> get props => [];
}

class ProfileLoadInitial extends ProfileLoadState {}

abstract class ErrorProfileLoad extends ProfileLoadState {}

class ProfileNotExistsState extends ErrorProfileLoad {}

class ProfileLoadGenericErrorState extends ErrorProfileLoad {}

abstract class ProfileLoadData extends ProfileLoadState {
  final Profile profile;
  const ProfileLoadData({@required this.profile});

  @override
  List<Object> get props => [profile];
}

class ProfileLoadProfileInfoLoaded extends ProfileLoadData {
  const ProfileLoadProfileInfoLoaded({
    @required profile,
  }) : super(profile: profile);
}

class ProfileIsPrivateState extends ProfileLoadProfileInfoLoaded {
  const ProfileIsPrivateState({
    @required profile,
  }) : super(profile: profile);
}

class ProfileLoadInfoAnalyticsLoaded extends ProfileLoadProfileInfoLoaded {
  const ProfileLoadInfoAnalyticsLoaded({
    @required profile,
  }) : super(profile: profile);
}

class ProfileLoadFeedLoading extends ProfileLoadInfoAnalyticsLoaded {
  const ProfileLoadFeedLoading({
    @required profile,
  }) : super(profile: profile);
}

class ProfileLoadFullyLoaded extends ProfileLoadInfoAnalyticsLoaded {
  const ProfileLoadFullyLoaded({
    @required profile,
  }) : super(profile: profile);
}
