part of 'comment_sorted_feed_load_bloc.dart';

abstract class CommentSortedFeedLoadEvent extends Equatable {
  const CommentSortedFeedLoadEvent();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class CommentSortedFeedLoadEventStart extends CommentSortedFeedLoadEvent {}
