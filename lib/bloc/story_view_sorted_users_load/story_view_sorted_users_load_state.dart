part of 'story_view_sorted_users_load_bloc.dart';

abstract class StoryViewSortedUsersLoadState extends Equatable {
  const StoryViewSortedUsersLoadState();

  @override
  List<Object> get props => [];
}

class StoryViewSortedUsersLoadInitial extends StoryViewSortedUsersLoadState {}

abstract class StoryViewSortedUsersLoadData extends StoryViewSortedUsersLoadState {
  final List<UserStoryViewCount> usersWhoMostStoryViewedYou;
  final List<UserStoryViewCount> usersWhoLeastStoryViewedYou;
  const StoryViewSortedUsersLoadData({
    @required this.usersWhoMostStoryViewedYou,
    @required this.usersWhoLeastStoryViewedYou,
  });

  @override
  List<Object> get props => [
    usersWhoMostStoryViewedYou,
    usersWhoLeastStoryViewedYou,
  ];
}

class StoryViewSortedUsersLoaded extends StoryViewSortedUsersLoadData {
  const StoryViewSortedUsersLoaded({
    @required List<UserStoryViewCount> usersWhoMostStoryViewedYou,
    @required List<UserStoryViewCount> usersWhoLeastStoryViewedYou,
  }) : super(
    usersWhoMostStoryViewedYou: usersWhoMostStoryViewedYou,
    usersWhoLeastStoryViewedYou: usersWhoLeastStoryViewedYou,
  );
}
