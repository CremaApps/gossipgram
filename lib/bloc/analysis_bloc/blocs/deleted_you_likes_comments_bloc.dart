import 'dart:async';

import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/database/repository/comment_repository.dart';
import 'package:GossipGram/database/repository/like_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@injectable
class DeletedYouLikesCommentsBloc extends IAnalysisBloc {
  final CommentRepository _commentRepository;
  final LikeRepository _likeRepository;
  final UserRepository _userRepository;
  int _usersRelevantDataDate;
  AppConfig _appConfig;
  LocalStorage _localStorage;

  DeletedYouLikesCommentsBloc(
    this._commentRepository,
    this._likeRepository,
    this._userRepository,
    this._appConfig,
    this._localStorage,
  ) : super() {
    _usersRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig.getInt(DashboardBloc.USERS_RELEVANT_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;
  }

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      List<int> usersDeletedLikes = await _likeRepository.getLikeUserIds(
        isValid: false,
        stateChangedAtGreaterThan: _usersRelevantDataDate,
        excluding: _localStorage.hasData(DashboardBloc.USER_ID,
                generalStorage: true)
            ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
            : [],
      );

      List<int> usersDeletedComments =
          await _commentRepository.getCommentUserIds(
        isValid: false,
        deletedAtGreaterThan: _usersRelevantDataDate,
        excluding: _localStorage.hasData(DashboardBloc.USER_ID,
                generalStorage: true)
            ? [_localStorage.read(DashboardBloc.USER_ID, generalStorage: true)]
            : [],
      );

      List<int> users = [
        ...{
          ...[
            ...usersDeletedLikes,
            ...usersDeletedComments,
          ]
        }
      ];

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises =
          await Future.wait(users.map((e) => _userRepository.getUser(id: e)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            youFollow: element.youFollow != null ? element.youFollow : true,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
