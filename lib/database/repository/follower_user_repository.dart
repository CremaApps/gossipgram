import 'dart:async';
import 'package:flutter/material.dart';
import 'package:GossipGram/database/db_provider.dart';
import 'package:GossipGram/model/objects/user/follower_user.dart';
import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class FollowerUserRepository {
  final DbProvider _dbProvider;

  FollowerUserRepository(this._dbProvider);

  Future<bool> existsFollowerUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('FollowerUser', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty;
  }

  Future<FollowerUser> getFollowerUser({@required int id}) async {
    final db = await _dbProvider.database;
    var res = await db.query('FollowerUser', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? FollowerUser.fromMap(res.first) : null;
  }

  Future<List<FollowerUser>> queryFollowerUsers(
      {bool isValid = true,
      int stateChangedAtGreaterThan,
      int stateChangedAtLesserThan,
      bool notFollowed = false}) async {
    final db = await _dbProvider.database;
    var query = 'SELECT * '
            'FROM FollowerUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    if (notFollowed != null && notFollowed) {
      query += 'AND id NOT IN (SELECT id FROM FollowedUser WHERE isValid = 1)';
    }

    var dbFUsers = await db.rawQuery(query);
    List<FollowerUser> fUsers = dbFUsers.isNotEmpty
        ? dbFUsers.map((a) => FollowerUser.fromMap(a)).toList()
        : [];
    return fUsers;
  }

  Future<int> queryCountFollowerUsers({
    bool isValid = true,
    int stateChangedAtGreaterThan,
    int stateChangedAtLesserThan,
  }) async {
    final db = await _dbProvider.database;
    var query = 'SELECT COUNT(*) '
            'FROM FollowerUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var dbFUsersCount = await db.rawQuery(query);

    return dbFUsersCount.first.values.first;
  }

  Future<int> updateFollowerUser({@required FollowerUser fUser}) async {
    final db = await _dbProvider.database;
    var dbFUser = await db.query(
      'FollowerUser',
      where: 'id = ?',
      whereArgs: [fUser.id],
    );
    if (dbFUser.isNotEmpty) {
      return db.update(
        'FollowerUser',
        fUser.toMap(),
        where: 'id = ?',
        whereArgs: [fUser.id],
      );
    } else {
      return 0;
    }
  }

  Future<int> upsertFollowerUser({@required FollowerUser fUser}) async {
    final db = await _dbProvider.database;
    var dbFUser = await db.query(
      'FollowerUser',
      where: 'id = ?',
      whereArgs: [fUser.id],
    );
    if (dbFUser.isNotEmpty) {
      return db.update(
        'FollowerUser',
        fUser.toMap(),
        where: 'id = ?',
        whereArgs: [fUser.id],
      );
    } else {
      return await db.insert('FollowerUser', fUser.toMap());
    }
  }

  Future<void> upsertFollowerUsers(
      {@required List<FollowerUser> fUsers}) async {
    final db = await _dbProvider.database;
    Batch batch = db.batch();
    var dbFUsers = await db.query(
      'FollowerUser',
    );

    Set<FollowerUser> allFollowerUsers = Set<FollowerUser>.from(
        dbFUsers.isNotEmpty
            ? dbFUsers.map((a) => FollowerUser.fromMap(a)).toList()
            : []);

    Set<FollowerUser> toUpdateUsers = Set<FollowerUser>.from(fUsers)
        .where((element) =>
            allFollowerUsers.any((FollowerUser e) => e.id == element.id))
        .toSet();
    Set<FollowerUser> toInsertUsers =
        Set<FollowerUser>.from(fUsers).difference(toUpdateUsers);

    for (FollowerUser fUser in toUpdateUsers) {
      batch.update(
        'FollowerUser',
        fUser.toMap(),
        where: 'id = ?',
        whereArgs: [fUser.id],
      );
    }

    for (FollowerUser fUser in toInsertUsers) {
      batch.insert('FollowerUser', fUser.toMap());
    }
    await batch.commit(noResult: true);
  }

  Future<int> deleteFollowerUser({@required FollowerUser fUser}) async {
    final db = await _dbProvider.database;
    return await db.delete(
      'FollowerUser',
      where: 'id = ?',
      whereArgs: [fUser.id],
    );
  }

  Future<List<int>> getFollowerUserIds(
      {bool isValid = true,
      int stateChangedAtGreaterThan,
      int stateChangedAtLesserThan,
      List<int> notIn}) async {
    final db = await _dbProvider.database;
    var query = 'SELECT id '
            'FROM FollowerUser '
            'WHERE isValid = ' +
        (isValid ? '1' : '0') +
        ' ';

    if (stateChangedAtGreaterThan != null) {
      query +=
          'AND onStateChange > ' + stateChangedAtGreaterThan.toString() + ' ';
    }

    if (stateChangedAtLesserThan != null) {
      query += 'AND onStateChange < ' + stateChangedAtLesserThan.toString();
    }

    var queryResponse = await db.rawQuery(query);
    List<int> followerUserIds = queryResponse.isNotEmpty
        ? queryResponse.map((a) {
            int id = a['id'];
            return id;
          }).toList()
        : [];
    return followerUserIds;
  }
}
