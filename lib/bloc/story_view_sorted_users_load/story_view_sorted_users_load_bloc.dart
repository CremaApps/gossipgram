import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/objects/user_story_view_count.dart';
import 'package:injectable/injectable.dart';

part 'story_view_sorted_users_load_event.dart';
part 'story_view_sorted_users_load_state.dart';

@injectable
class StoryViewSortedUsersLoadBloc
    extends Bloc<StoryViewSortedUsersLoadEvent, StoryViewSortedUsersLoadState> {
  UserRepository _userRepository;

  StoryViewSortedUsersLoadBloc(
    this._userRepository,
  ) : super(StoryViewSortedUsersLoadInitial());

  @override
  Stream<StoryViewSortedUsersLoadState> mapEventToState(
    StoryViewSortedUsersLoadEvent event,
  ) async* {
    if (event is StoryViewSortedUsersLoadEventStart) {
      yield StoryViewSortedUsersLoadInitial();

      List<UserStoryViewCount> usersWhoMostStoryViewedYou =
          await _userRepository.getUsersSortedByStoryViewCount(
        isValid: true,
        maxElems: 20,
        asc: false,
      );

      List<UserStoryViewCount> usersWhoLeastStoryViewedYou =
          await _userRepository.getUsersSortedByStoryViewCount(
        isValid: true,
        maxElems: 20,
        asc: true,
      );

      yield StoryViewSortedUsersLoaded(
        usersWhoMostStoryViewedYou: usersWhoMostStoryViewedYou,
        usersWhoLeastStoryViewedYou: usersWhoLeastStoryViewedYou,
      );
    }
  }
}
