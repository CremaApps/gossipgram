import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GossipGram/service/local_storage/local_storage.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_events.dart';
import 'package:GossipGram/bloc/data_load/data_load_events.dart';
import 'package:GossipGram/bloc/data_load/data_load_state.dart';
import 'package:GossipGram/service/api/been_blocked_by_user_service.dart';
import 'package:GossipGram/service/scrap/best_friend_user_load_service.dart';
import 'package:GossipGram/service/scrap/blocked_user_load_service.dart';
import 'package:GossipGram/service/scrap/feed_load_service.dart';
import 'package:GossipGram/service/scrap/followed_user_load_service.dart';
import 'package:GossipGram/service/scrap/follower_user_load_service.dart';
import 'package:GossipGram/service/scrap/liked_but_dont_follow_service.dart';
import 'package:GossipGram/service/scrap/story_load_service.dart';
import 'package:GossipGram/service/setup_external_requirements.dart';
import 'package:GossipGram/service/simulations/profile_views_simulation_service.dart';
import 'package:injectable/injectable.dart';
import 'dart:developer' as developer;

@lazySingleton
class DataLoadBloc extends Bloc<DataLoadEvent, DataLoadState> {
  static const String LAST_LOAD_KEY = 'ig_last_load';
  static const String FIRST_LOAD_DATA = 'ig_first_load';
  static const String FIRST_LOAD_STATUS = 'first_load_status';
  static const String PROFILING_BASE_TIME = 'profiling_base_time';

  FollowedUserLoadService _followedUserLoadService;
  FollowerUserLoadService _followerUserLoadService;
  BestFriendUserLoadService _bestFriendUserLoadService;
  BlockedUserLoadService _blockedUserLoadService;
  FeedLoadService _feedLoadService;
  StoryLoadService _storyLoadService;
  DashboardBloc dashboardBloc;
  BeenBlockedByUserService _beenBlockedByUserService;
  ProfileViewsSimulationService _profileViewsSimulationService;
  LikedButDontFollowService _likedButDontFollowService;
  LocalStorage _localStorage;
  SetupExternalRequirements _setupExternalRequirements;

  DataLoadBloc(
      this.dashboardBloc,
      this._followedUserLoadService,
      this._followerUserLoadService,
      this._bestFriendUserLoadService,
      this._blockedUserLoadService,
      this._storyLoadService,
      this._feedLoadService,
      this._beenBlockedByUserService,
      this._profileViewsSimulationService,
      this._likedButDontFollowService,
      this._localStorage,
      this._setupExternalRequirements)
      : super(DataLoadStateStarted());

  @override
  Stream<DataLoadState> mapEventToState(DataLoadEvent event) async* {
    if (event is DataLoadStartEvent) {
      bool hasErrors = false;
      yield DataLoadStateLoading(event.showLoading);
      bool firstLoad = !_localStorage.hasData(LAST_LOAD_KEY);
      Map<String, dynamic> firstLoadStatus =
          _localStorage.hasData(FIRST_LOAD_STATUS)
              ? _localStorage.read(FIRST_LOAD_STATUS)
              : {
                  FollowedUserLoadService.SERVICE_NAME: false,
                  FollowerUserLoadService.SERVICE_NAME: false,
                  BeenBlockedByUserService.SERVICE_NAME: false,
                  BestFriendUserLoadService.SERVICE_NAME: false,
                  BlockedUserLoadService.SERVICE_NAME: false,
                  StoryLoadService.SERVICE_NAME: false,
                  FeedLoadService.SERVICE_NAME: false,
                  LikedButDontFollowService.SERVICE_NAME: false,
                };

      double now = DateTime.now().millisecondsSinceEpoch / 1000;
      await _localStorage.write(PROFILING_BASE_TIME, now);
      printProfiling("Comienza la carga", now);

      //var a = await _followedUserLoadService.getFollowing();

      try {
        await _setupExternalRequirements.execute();
      } catch (e, trace) {
        FirebaseCrashlytics.instance.recordError(e, trace);
      }

      ProcessedUsersWrapper processedUsers = ProcessedUsersWrapper([]);
      if (!firstLoad ||
          !firstLoadStatus[FollowedUserLoadService.SERVICE_NAME]) {
        await _followedUserLoadService
            .execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
        )
            .then((_) {
          if (firstLoad)
            firstLoadStatus[FollowedUserLoadService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus,
          event: DashboardLoadFollowedsEvent());
      printProfiling("Followeds cargados", now);

      if (!firstLoad ||
          !firstLoadStatus[FollowerUserLoadService.SERVICE_NAME]) {
        await _followerUserLoadService
            .execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
        )
            .then((_) {
          if (firstLoad)
            firstLoadStatus[FollowerUserLoadService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus,
          event: DashboardLoadFollowersEvent());
      printProfiling("Followers cargados", now);

      if (!firstLoad ||
          !firstLoadStatus[BeenBlockedByUserService.SERVICE_NAME]) {
        await _beenBlockedByUserService
            .execute(
          firstLoad: firstLoad,
        )
            .then((_) {
          if (firstLoad)
            firstLoadStatus[BeenBlockedByUserService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus,
          event: DashboardLoadFollowRelationshipsEvent());
      printProfiling("Usuarios que te bloquearon cargados", now);

      if (!firstLoad ||
          !firstLoadStatus[BestFriendUserLoadService.SERVICE_NAME]) {
        await _bestFriendUserLoadService
            .execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
        )
            .then((_) {
          if (firstLoad)
            firstLoadStatus[BestFriendUserLoadService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus);
      printProfiling("Mejores amigos cargados", now);

      if (!firstLoad || !firstLoadStatus[BlockedUserLoadService.SERVICE_NAME]) {
        await _blockedUserLoadService
            .execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
        )
            .then((_) {
          if (firstLoad)
            firstLoadStatus[BlockedUserLoadService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus);
      printProfiling("Usuarios bloqueados cargados", now);

      if (!firstLoad || !firstLoadStatus[StoryLoadService.SERVICE_NAME]) {
        await _storyLoadService
            .execute(
          firstLoad: firstLoad,
          processedUsers: processedUsers,
        )
            .then((activeStories) {
          if (firstLoad) firstLoadStatus[StoryLoadService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus,
          event: DashboardLoadStoriesEvent());
      printProfiling("Historias cargadas", now);

      if (!firstLoad || !firstLoadStatus[FeedLoadService.SERVICE_NAME]) {
        await _feedLoadService
            .execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
        )
            .then((_) {
          if (firstLoad) firstLoadStatus[FeedLoadService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus);
      printProfiling("Feed cargado", now);

      List<int> simulatedProfileViews =
          await _profileViewsSimulationService.execute();
      await _localStorage.write(
        DashboardBloc.SIMULATED_PROFILE_VIEWS,
        simulatedProfileViews,
      );
      await _localStorage.write(
          FIRST_LOAD_DATA, DateTime.now().millisecondsSinceEpoch);
      await _loadStepFinished(firstLoad, firstLoadStatus,
          event: DashboardLoadFeedEvent());
      printProfiling("Gente que vio tu perfil cargada", now);

      if (!firstLoad ||
          (!firstLoadStatus[LikedButDontFollowService.SERVICE_NAME] &&
              firstLoadStatus[FollowedUserLoadService.SERVICE_NAME])) {
        await _likedButDontFollowService
            .execute(
          processedUsers: processedUsers,
          firstLoad: firstLoad,
        )
            .then((_) {
          if (firstLoad)
            firstLoadStatus[LikedButDontFollowService.SERVICE_NAME] = true;
        }).catchError((_) {
          hasErrors = true;
          return Future<Null>.value();
        });
      }
      await _loadStepFinished(firstLoad, firstLoadStatus);

      if (!hasErrors && !firstLoadStatus.values.any((e) => !e))
        await _localStorage.write(LAST_LOAD_KEY, DateTime.now().toString());

      await _loadStepFinished(firstLoad, firstLoadStatus,
          event: DashboardLoadDataEvent());
      printProfiling("Me gustó pero no lo sigo cargado", now);
      printProfiling("Fin carga", now);

      yield DataLoadStateFinished();
    } else {
      throw UnimplementedError();
    }
  }

  void _loadStepFinished(bool firstLoad, dynamic firstLoadStatus,
      {DashboardEvent event}) async {
    if (firstLoad)
      await _localStorage.write(FIRST_LOAD_STATUS, firstLoadStatus);
    if (event != null) dashboardBloc.add(event);
  }

  static void printProfiling(String message, double now) {
    developer.log(
        "$message: ${DateTime.now().millisecondsSinceEpoch / 1000 - now} seconds",
        name: 'DATALOAD_LOG');
  }
}

class ProcessedUsersWrapper {
  var value;
  ProcessedUsersWrapper(this.value);
}
