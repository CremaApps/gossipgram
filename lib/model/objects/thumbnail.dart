import 'package:flutter/foundation.dart';
import 'package:instagram_private_api/src/Client/Response/CommonClasses.dart';

class Thumbnail {
  static const int IMAGE = 0;
  static const int VIDEO = 1;

  int id;
  ImageVersions2 image;
  int mediaType;

  Thumbnail({
    @required this.id,
    this.image,
    @required this.mediaType,
  });
}
