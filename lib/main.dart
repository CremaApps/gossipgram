import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';

import 'application.dart';
import 'injection.dart';
import 'service/background_task/background_task_register_service.dart';
import 'service/storage/app_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(
    IGViews(),
  );

  BackgroundTaskRegisterService backgroundTaskRegisterService =
      GetIt.I.get<BackgroundTaskRegisterService>();

  backgroundTaskRegisterService.setHeadlessListeners().then((res) async {
    AppConfig _appConfig = GetIt.I.get<AppConfig>();

    while (!_appConfig.isLoaded) {
      await Future.delayed(Duration(milliseconds: 250));
    }

    //every 8 hours
    await backgroundTaskRegisterService.registerTask(
      taskName: BackgroundTaskRegisterService.LOAD_FEED_INITIAL_BUFFER,
      delay:
          _appConfig.getInt(AppConfig.BACKGROUND_FEED_SCHEDULE_DELAY_SECONDS) *
              1000,
      periodic: true,
    );
  });
}
