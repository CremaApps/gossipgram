part of 'friendship_cubit.dart';

@freezed
abstract class FriendshipState with _$FriendshipState {
  const factory FriendshipState.initial() = _FriendshipInitial;
  const factory FriendshipState.loading() = _FriendshipLoading;
  const factory FriendshipState.loaded(bool succes) = _FriendshipLoaded;
  const factory FriendshipState.error(String errorMessage) = _FriendshipError;
}
