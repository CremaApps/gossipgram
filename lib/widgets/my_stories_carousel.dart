import 'dart:io';

import 'package:GossipGram/themes/themes.dart';
import 'package:GossipGram/widgets/centered_loader.dart';
import 'package:flutter/material.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:GossipGram/themes/app_theme_default.dart';
import 'package:GossipGram/widgets/my_story_popup.dart';
import 'package:GossipGram/widgets/number_people_converter.dart';
import 'package:path_provider/path_provider.dart';

class MyStoryCarousel extends StatelessWidget {
  final List<Story> myStories;

  const MyStoryCarousel({this.myStories}) : super();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getApplicationDocumentsDirectory(),
      builder: (BuildContext context, AsyncSnapshot<Directory> snapshot) {
        if (!snapshot.hasData) {
          // while data is loading:
          return CenteredLoader();
        } else {
          var firstPath = snapshot.data.path + "/images/";
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              GridView(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 3,
                  crossAxisSpacing: 3,
                  childAspectRatio: 0.7,
                ),
                children: myStories.map((story) {
                  return InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        child: MyStoryPopupComponent(
                          storyId: story.id,
                          storyMediaType: story.mediaType,
                          storyViewCount: story.viewCount,
                          path: snapshot.data.path,
                        ),
                      );
                    },
                    child: _buildRow(
                      story,
                      firstPath,
                    ),
                  );
                }).toList(),
              ),
            ],
          );
        }
      },
    );
  }

  Widget _buildRow(Story story, String path) {
    int viewNumber = story.viewCount;
    DecorationImage image;
    var filePathAndName = "$path${story.id}.jpg";
    image = DecorationImage(
        image: FileImage(
          File(filePathAndName),
        ),
        fit: BoxFit.cover,
        colorFilter:
            ColorFilter.mode(Colors.black.withOpacity(0.1), BlendMode.darken));

    return Container(
      child: Container(
        decoration: BoxDecoration(
          image: image,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              margin:
                  EdgeInsetsDirectional.only(bottom: 10, start: 10, end: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.remove_red_eye,
                    color: Colors.white,
                    size: 17,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(IntNumberSymbolConverter(viewNumber),
                      style:
                          appTextTheme.subtitle2.copyWith(color: Colors.white)),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class UnicornOutlineButton extends StatelessWidget {
  final _GradientPainter _painter;
  final Widget _child;
  final VoidCallback _callback;
  final double _radius;

  UnicornOutlineButton({
    @required double strokeWidth,
    @required double radius,
    @required Gradient gradient,
    @required Widget child,
    @required VoidCallback onPressed,
  })  : this._painter = _GradientPainter(
            strokeWidth: strokeWidth, radius: radius, gradient: gradient),
        this._child = child,
        this._callback = onPressed,
        this._radius = radius;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _painter,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: _callback,
        child: InkWell(
          borderRadius: BorderRadius.circular(_radius),
          onTap: _callback,
          child: Container(
            constraints: BoxConstraints(minWidth: 88, minHeight: 48),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _child,
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _GradientPainter extends CustomPainter {
  final Paint _paint = Paint();
  final double radius;
  final double strokeWidth;
  final Gradient gradient;

  _GradientPainter(
      {@required double strokeWidth,
      @required double radius,
      @required Gradient gradient})
      : this.strokeWidth = strokeWidth,
        this.radius = radius,
        this.gradient = gradient;

  @override
  void paint(Canvas canvas, Size size) {
    // create outer rectangle equals size
    Rect outerRect = Offset.zero & size;
    var outerRRect =
        RRect.fromRectAndRadius(outerRect, Radius.circular(radius));

    // create inner rectangle smaller by strokeWidth
    Rect innerRect = Rect.fromLTWH(strokeWidth, strokeWidth,
        size.width - strokeWidth * 2, size.height - strokeWidth * 2);
    var innerRRect = RRect.fromRectAndRadius(
        innerRect, Radius.circular(radius - strokeWidth));

    // apply gradient shader
    _paint.shader = gradient.createShader(outerRect);

    // create difference between outer and inner paths and draw it
    Path path1 = Path()..addRRect(outerRRect);
    Path path2 = Path()..addRRect(innerRRect);
    var path = Path.combine(PathOperation.difference, path1, path2);
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => oldDelegate != this;
}
