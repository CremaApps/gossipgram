import 'dart:async';

import 'package:GossipGram/bloc/analysis_bloc/ianalysis_bloc.dart';
import 'package:GossipGram/bloc/dashboard/dashboard_bloc.dart';
import 'package:GossipGram/database/repository/follower_user_repository.dart';
import 'package:GossipGram/database/repository/user_repository.dart';
import 'package:GossipGram/model/view_data/analysis_user_item.dart';
import 'package:GossipGram/service/storage/app_config.dart';
import 'package:injectable/injectable.dart';

@injectable
class NewFollowersBloc extends IAnalysisBloc {
  final FollowerUserRepository _followerUserRepository;
  final UserRepository _userRepository;
  final AppConfig _appConfig;
  int _usersRelevantDataDate;

  NewFollowersBloc(
    this._followerUserRepository,
    this._userRepository,
    this._appConfig,
  ) : super() {
    _usersRelevantDataDate = DateTime.now()
        .add(Duration(
          days: -_appConfig.getInt(DashboardBloc.USERS_RELEVANT_DATA_THRESHOLD),
        ))
        .millisecondsSinceEpoch;
  }

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is StartAnalysisEvent) {
      yield AnalysisLoadingState();

      var newFollowers = await _followerUserRepository.queryFollowerUsers(
        stateChangedAtGreaterThan: _usersRelevantDataDate,
        isValid: true,
      );

      List<AnalysisUserItem> analysisUserItems = [];

      var userPromises = await Future.wait(
          newFollowers.map((e) => _userRepository.getUser(id: e.id)));

      userPromises.forEach((element) async {
        analysisUserItems.add(AnalysisUserItem(
            userId: element.id,
            name: element.fullName != '' ? element.fullName : element.username,
            userName: element.username,
            youFollow: element.youFollow,
            imageUrl: element.profilePicUrl));
      });

      yield AnalysisLoadedState(analysisUserItems);
    } else
      throw UnimplementedError();
  }
}
