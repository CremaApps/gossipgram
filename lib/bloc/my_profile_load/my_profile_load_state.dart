part of 'my_profile_load_bloc.dart';

abstract class MyProfileLoadState extends Equatable {
  const MyProfileLoadState();

  @override
  List<Object> get props => [];
}

abstract class MyProfileLoadData extends MyProfileLoadState {
  final MyProfile myProfile;
  const MyProfileLoadData({@required this.myProfile});

  @override
  List<Object> get props => [myProfile];
}

class MyProfileLoadInitial extends MyProfileLoadState {}

class MyProfileLoadProfileInfoLoaded extends MyProfileLoadData {
  const MyProfileLoadProfileInfoLoaded({
    @required myProfile,
  }) : super(myProfile: myProfile);
}

class MyProfileLoadInfoAnalyticsLoaded extends MyProfileLoadData {
  const MyProfileLoadInfoAnalyticsLoaded({
    @required myProfile,
  }) : super(myProfile: myProfile);
}

class MyProfileLoadFeedLoading extends MyProfileLoadData {
  const MyProfileLoadFeedLoading({
    @required myProfile,
  }) : super(myProfile: myProfile);
}

class MyProfileLoadFullyLoaded extends MyProfileLoadData {
  const MyProfileLoadFullyLoaded({
    @required myProfile,
  }) : super(myProfile: myProfile);
}
