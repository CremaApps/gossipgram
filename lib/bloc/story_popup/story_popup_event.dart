part of 'story_popup_bloc.dart';

abstract class StoryPopupEvent extends Equatable {
  const StoryPopupEvent();
}

class PopupStoriesStartLoadEvent extends StoryPopupEvent {
  final String userId;

  const PopupStoriesStartLoadEvent({
    @required this.userId
  });

  @override
  // TODO: implement props
  List<Object> get props => [this.userId];
}
