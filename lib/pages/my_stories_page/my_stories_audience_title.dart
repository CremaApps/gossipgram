part of 'my_stories_page.dart';

class _StoriesAudienceTitle extends StatelessWidget {
  const _StoriesAudienceTitle();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Text("Viewer Analytics", style: appTextTheme.bodyText1),
    );
  }
}
