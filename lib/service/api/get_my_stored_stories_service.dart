import 'package:flutter/material.dart';
import 'package:GossipGram/database/repository/story_repository.dart';
import 'package:GossipGram/model/objects/story.dart';
import 'package:GossipGram/model/objects/story_thumbnail.dart';
import 'package:injectable/injectable.dart';
import 'package:instagram_private_api/src/Client/Response/CommonClasses.dart';

@singleton
class GetMyStoredStoriesService {
  StoryRepository storyRepository;

  GetMyStoredStoriesService({
    @required this.storyRepository,
  });

  Future<List<StoryThumbnail>> execute() async {
    Set<Story> activeStoryItems = Set.from(await storyRepository.queryStories(
      isValid: true,
    ));
    Set<Story> oldStoryItems = Set.from(await storyRepository.queryStories(
      isValid: false,
    ));
    List<StoryThumbnail> storyThumbnailData = [];

    for (var storyItem in activeStoryItems.union(oldStoryItems).toList()) {
      ImageVersions2 image = ImageVersions2(
          candidates: [
        ImageCandidate(url: storyItem.mediaUrl, width: 0, height: 0)
      ].toList());

      // Hemos puesto 0 a la width y a la height porque la imagen se recoge de base de datos
      // Y no tenemos estos valores. Si no ponemos nada (width y height no son required) peta la vista.
      // Está claramente pendiente de refactor

      storyThumbnailData.add(
        StoryThumbnail(
          id: storyItem.id,
          image: image,
          mediaType: storyItem.mediaType,
          viewCount: storyItem.viewCount,
        ),
      );
    }

    return storyThumbnailData;
  }
}
