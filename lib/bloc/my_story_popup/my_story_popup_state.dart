part of 'my_story_popup_bloc.dart';

abstract class MyStoryPopupState extends Equatable {
  const MyStoryPopupState();
}

class MyStoryPopupInitial extends MyStoryPopupState {
  @override
  List<Object> get props => [];
}

class MyStoryPopupLoading extends MyStoryPopupState {
  @override
  List<Object> get props => [];
}

class MyStoryPopupLoaded extends MyStoryPopupState {
  final List<String> seenBy;

  MyStoryPopupLoaded({@required this.seenBy});

  @override
  List<Object> get props => [seenBy];
}
